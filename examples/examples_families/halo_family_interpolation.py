"""
Interpolation of the entire abacuses of Earth-Moon L1 and L2 Halo families.

@author: Alberto FOSSA', Paolo GUARDABASSO
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import Akima1DInterpolator

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo

# %% environment
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
param = 'Cjac'  # define interpolation parameter among Azdim, Cjac and T

li = cr3bp.l1  # define libration point
fam = Halo.Family.southern  # define family
# %% load abacus
halo = Halo(cr3bp, li, fam, Azdim=30e3)     # dummy orbit to get abacus data
_, az, rp, jc, per = halo.load_abacus()
idx_aba = {'Azdim': 0, 'Cjac': 2, 'T': 3}   # map parameters names to corresponding indexes
x_aba = np.arange(az.size, dtype=float)  # abacus indexes as 1D array (from 1 to ...)
y_aba = np.vstack((az, rp, jc, per)).T      # orbits along rows, parameters along columns

# %% interpolate abacus entries w.r.t. abacus indexes
f = Akima1DInterpolator(x_aba, y_aba)  # spline: orbits parameters as function of abacus indexes
df = f.derivative()  # spline: first derivative of f

# %% identify maxima and minima of f
rts = df.roots()  # roots of df, they are the values of x_aba for which y_aba is 0. It is an
# array of arrays. There are 4 arrays of array (one for each parameter) and then each parameter
# can have zero or more roots.
maxima, minima = np.empty(4, dtype=object), np.empty(4, dtype=object)

for i, r in enumerate(rts):  # loop over components
    if r.size > 0:  # at least one root of df has been identified
        # check if the value of df at a root is less (maxima) or more (minima) than the previous.
        # It is vectorized, so if r has 'n' elements,  df.__call__(r) gives an n x 4 matrix. Then
        # [:,i] selects the column for each parameter.
        maxima[i] = r[df.__call__(r - 1)[:, i] > 0.0]  # extract maxima
        minima[i] = r[df.__call__(r - 1)[:, i] < 0.0]  # extract minima

# %% identify switches in root parameter
switches = np.empty(4, dtype=object)  # The parameter switches is composed by 4 arrays (one
# for each parametera Az, Cjac etc)
for i, yi in enumerate(y_aba.T):  # loop over parameters (yi is a vector of Az, Cjac, etc.)
    xi = list(np.concatenate(([x_aba[0]], [x_aba[-1]], rts[i])))  # x of first, last and roots of df
    fxi = np.concatenate(([yi[0]], [yi[-1]], f.__call__(rts[i])[:, i]))  # fx of first, last and
    # fx evaluated in the extrema (roots of df) for each parameter
    for j, yj in enumerate(fxi):  # loop over values of f
        fj = Akima1DInterpolator(x_aba, yi - yj)  # spline that has the x_aba as x and has
        # the parameters (like Az) minus their value in the extrema points (so that the zeros of
        # the spline are the extrema).
        xi.extend(fj.roots())  # finding the roots of fj means finding the values of x at which
        # fx has the same values of the limits, but also finding again the values of the extrema,
        # 0 and idx_max (for this reason an unique test is run after).
    switches[i] = np.unique(np.asarray(xi).round(3))  # Here there are 2 things happening: the
    # first is that xi values are rounded, because while x_aba are integers, fj roots are not and
    # so 1 and 1.00001 wouldn't be considered the same. Rounding helps finding duplicates. The
    # second thing is eliminating the duplicates and sorting the elements from the .


# %% define values of the root parameter for Az, Rp, Jc and T across EML-1 and EML-2
# The parameter switches is composed by 4 arrays (one for each parameters). Each array contains
# the values of x at which there is a switch  for a certain parameter.
# TODO: how to automatize this process???
rpm = np.zeros(y_aba.shape, dtype=int)
if li is cr3bp.l1:
    rpm[:, 2][np.logical_and(x_aba > switches[2][2], x_aba <= switches[2][3])] = 1
    rpm[:, 2][np.logical_and(x_aba > switches[2][3], x_aba <= switches[2][4])] = 2
    rpm[:, 3][np.logical_and(x_aba > switches[3][1], x_aba <= switches[3][2])] = 1
    rpm[:, 3][np.logical_and(x_aba > switches[3][4], x_aba <= switches[3][5])] = 1
elif li is cr3bp.l2:
    rpm[:, 0][np.logical_and(x_aba > switches[0][2], x_aba <= switches[0][3])] = 1
    rpm[:, 2][np.logical_and(x_aba > switches[2][2], x_aba <= switches[2][3])] = 1
else:
    raise NotImplementedError
# This process allows to select the right root for each part of the curve.


# %% interpolate Halo family
halos = []
for i in range(x_aba.size):
    if param == 'Azdim':
        halos.append(Halo(cr3bp, li, fam, Azdim=y_aba[i, idx_aba[param]] * cr3bp.L))
    elif param == 'Cjac':
        halos.append(Halo(cr3bp, li, fam, Cjac=y_aba[i, idx_aba[param]]))
    elif param == 'T':
        halos.append(Halo(cr3bp, li, fam, T=y_aba[i, idx_aba[param]]))
    else:
        raise Exception
    halos[-1].interpolation(root=rpm[i, idx_aba[param]])  # Interpolate with the correct root

# %% extract interpolated parameters
y_interp = np.empty(y_aba.shape)
y_interp[:, 0] = np.asarray([h.Az for h in halos])
y_interp[:, 1] = np.asarray([h.m2_apsis['periapsis_radius'] for h in halos])
y_interp[:, 2] = np.asarray([h.C for h in halos])
y_interp[:, 3] = np.asarray([h.T for h in halos])
# Here you can potentially extract other parameters (the ones that have been extracted are
# already in the abacus).

# %% plot splines
fig, ax = plt.subplots(2, 2, sharex='col', constrained_layout=True)
fig.suptitle(f"Parameters for EML-{halo.li.number} {halo.family.name} {halo.__class__.__name__}")
y_labels = (r'$A_z\ [-]$', r'$r_p\ [-]$', r'$J_c\ [-]$', r'$T\ [-]$')
y_labels2 = (r'$\partial A_z\ [-]$', r'$\partial r_p\ [-]$',
             r'$\partial J_c\ [-]$', r'$\partial T\ [-]$')

# Evaluate splines, getting their values at each x_aba
y, dy = f.__call__(x_aba), df.__call__(x_aba)  # evaluate splines

for j in range(4):  # loop over parameters
    # Plot the parameter line
    ax[j // 2, j % 2].scatter(x_aba[rpm[:, j] == 0], y[:, j][rpm[:, j] == 0], s=.5,
                              color='deepskyblue', marker='o')
    ax[j // 2, j % 2].scatter(x_aba[rpm[:, j] == 1], y[:, j][rpm[:, j] == 1], s=.5,
                              color='blue', marker='o')
    ax[j // 2, j % 2].scatter(x_aba[rpm[:, j] == 2], y[:, j][rpm[:, j] == 2], s=.5,
                              color='darkblue', marker='o')
    # Draw switch points (that are not extrema)
    ax[j // 2, j % 2].scatter(switches[j][1:-1], f.__call__(switches[j][1:-1])[:, j],
                              color='k', marker='*', label='switches', zorder=5)
    # Dummy plots for legend
    ax[j // 2, j % 2].plot(np.nan, color='deepskyblue', label='root 0')
    ax[j // 2, j % 2].plot(np.nan, color='blue', label='root 1')
    ax[j // 2, j % 2].plot(np.nan, color='darkblue', label='root 2')
    ax[j // 2, j % 2].plot(np.nan, color='tab:orange', label='derivative')

    # Create twin axis that shares the same xaxis
    ax2 = ax[j // 2, j % 2].twinx()

    # Plot the parameter's derivative
    ax2.plot(dy[:, j], color='tab:orange', label='derivative')

    # Draw maximum and minimum points
    if maxima[j] is not None:
        ax[j // 2, j % 2].scatter(maxima[j], f.__call__(maxima[j])[:, j],
                                  color='tab:green', label='maxima', zorder=10)
        ax2.scatter(maxima[j], df.__call__(maxima[j])[:, j],
                    color='tab:green', label='maxima', zorder=10)
    if minima[j] is not None:
        ax[j // 2, j % 2].scatter(minima[j], f.__call__(minima[j])[:, j],
                                  color='tab:red', label='minima', zorder=10)
        ax2.scatter(minima[j], df.__call__(minima[j])[:, j],
                    color='tab:red', label='minima', zorder=10)
    ax[j // 2, j % 2].grid(True)
    ax[j // 2, j % 2].set_ylabel(y_labels[j])
    ax2.set_ylabel(y_labels2[j])
ax[1, 0].legend(bbox_to_anchor=(-0.3, 0), loc='lower left')

# %% plot interpolated w.r.t. abacus parameters
fig, ax = plt.subplots(2, 2, sharex='col', constrained_layout=True)
fig.suptitle(f"Interpolated w.r.t. abacus parameters for "
             f"EML-{halo.li.number} {halo.family.name} {halo.__class__.__name__}")
for j in range(4):
    ax[j // 2, j % 2].plot(y_aba[:, j], color='tab:blue', label='abacus')
    ax[j // 2, j % 2].plot(y_interp[:, j], '--', color='tab:orange', label='interpolated')
    ax[j // 2, j % 2].grid(True)
    ax[j // 2, j % 2].set_ylabel(y_labels[j])
ax[1, 0].legend(bbox_to_anchor=(-0.3, 0), loc='lower left')
