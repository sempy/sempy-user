"""
Interpolation of the entire abacuses of Earth-Moon L1 and L2 Halo families.

@author: Paolo GUARDABASSO
"""

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.plotting.util import sphere, set_axes_equal


def set_axes_lim_labels_title(ax, title):
    """Convenience function to set the axes limits, labels and the figure title. """
    ax.set_xlabel('x [-]')
    ax.set_ylabel('y [-]')
    ax.set_zlabel('z [-]')
    ax.set_title(title)
    set_axes_equal(ax)


# %% environment
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

li_list = [cr3bp.l1, cr3bp.l2]
fam = Halo.Family.southern
halos_all = [[] for i in range(len(li_list))]

for j, li in enumerate(li_list):
    # load abacus
    halo = Halo(cr3bp, li, fam, Azdim=30e3)     # dummy orbit to get abacus data
    _, az, _, _, per = halo.load_abacus()

    # Choose injective parameters to interpolate in the family
    if li == cr3bp.l1:
        param_vec = az
    else:
        param_vec = per

    # Reduce the number of elements to be interpolated
    param_vec = param_vec[::25]

    # %% interpolate Halo family
    for i, param in enumerate(param_vec):
        if li == cr3bp.l1:
            halos_all[j].append(Halo(cr3bp, li, fam, Azdim=param*cr3bp.L))
        else:
            halos_all[j].append(Halo(cr3bp, li, fam, T=param))
        halos_all[j][-1].interpolation()

# %% Plot family
fig3 = plt.figure()
ax3 = fig3.add_subplot(111, projection='3d')
ax3.grid(False)
ax3.set_axis_off()

cmap_list = [['cividis_r', 'winter_r'], ['autumn', 'summer']]

for x in range(len(li_list)):
    cmap_south = plt.cm.get_cmap(cmap_list[x][0], len(halos_all[x]))
    cmap_north = plt.cm.get_cmap(cmap_list[x][1], len(halos_all[x]))
    for i, halo in enumerate(halos_all[x]):
        ax3.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2],
                 color=cmap_north(i), zorder=2, linewidth=.5)
        ax3.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], -halo.state_vec[:, 2],
                 color=cmap_south(i), zorder=2, linewidth=.5)

# Plot Moon
x_2, y_2, z_2 = sphere(100, cr3bp.R2 / cr3bp.L, cr3bp.m2_pos)
ax3.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)

# Plot libration points
pos_l1 = np.array(cr3bp.l1.position)
pos_l2 = np.array(cr3bp.l2.position)
ax3.scatter(pos_l1[0], pos_l1[1], pos_l1[2], marker='*', color='r')
ax3.scatter(pos_l2[0], pos_l2[1], pos_l2[2], marker='*', color='g')
set_axes_equal(ax3)
