"""
Continuation of the EML-2 southern Halo and Butterfly families.

@author: Alberto FOSSA'
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.cm import ScalarMappable
from scipy.interpolate import CubicSpline
from copy import deepcopy
from warnings import simplefilter
from tqdm import tqdm, TqdmWarning

import sempy.core.crtbp.jacobi as jac
import sempy.core.init.defaults as dft
from sempy.core.init.constants import DAYS2SEC
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.orbits.nrho import NRHO
from sempy.core.diffcorr.diff_corr_3d import DiffCorr3D
from sempy.core.diffcorr.multiple_shooting import MultipleShooting
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.plotting.simple.utils import decorate_3d_axes
from sempy.core.utils.pickle_encoder import load_pickle, save_pickle
from sempy.core.utils.json_encoder import save_json
from sempy.core.__init__ import DIRNAME


def halo_continuation(_cr3bp, _li, _fam, _az0_dim, _rpf_dim, _delta_s):
    """Continuation of the EML-2 Halo orbit family. """

    _halo = Halo(_cr3bp, _li, _fam, Azdim=_az0_dim)  # initial Halo (will be overwritten)
    _halo.computation(fix_dim=Halo.DiffCorrFixDim.z0)  # correction (x0, vy0) free
    _halo.computation(fix_dim=Halo.DiffCorrFixDim.min_norm)  # correction (x0, z0, vy0, T12) free
    _rp0 = _halo.m2_apsis['periapsis_radius']  # initial (max) periselene radius
    _rpf = _rpf_dim / _cr3bp.L  # final (min) periselene radius

    _diff_corr = DiffCorr3D(_cr3bp.mu, max_internal_steps=8_000_000)  # DC instance
    _orbs = [deepcopy(_halo)]  # list of Halos
    _bar = tqdm(total=1.0, desc=_halo.get_abacus_name(separator=' '))  # progress bar in (0, 1)

    while _delta_s > 1e-8 and _halo.m2_apsis['periapsis_radius'] > _rpf:
        _halo.state0[::2] += _delta_s * _halo.continuation['null_vec'][:3, 0]  # update state
        _halo.T12 += _delta_s * _halo.continuation['null_vec'][3, 0]  # update period
        _halo.state0, _halo.T12, _halo.T, _halo.continuation, _ = \
            _diff_corr.diff_corr_3d_cont(_halo.state0, _halo.T12, _delta_s, _halo.continuation)
        _halo.postprocess()  # postprocessing (Jacobi constant, stability characteristics, ...)
        if _halo.m2_apsis['apoapsis_radius'] < 1.0:  # good orbit
            _orbs.append(deepcopy(_halo))  # add Halo to list
            _up = (_halo.m2_apsis['periapsis_radius'] - _orbs[-2].m2_apsis['periapsis_radius']) \
                / (_rpf - _rp0)  # update progress bar
            _bar.update(_up)
        else:  # bad orbit, back one step
            _halo = _orbs[-1]
            _delta_s *= 0.1
    _bar.close()
    return _orbs


def plot_params(_x, _y, _y1=None, _y_legend=None, _y1_legend=None,
                _x_label='', _y_label='', _title='', _stability=False):
    """Plot the orbit parameters across the family."""
    _, _ax = plt.subplots()
    _ax.plot(_x, _y, 'b', label=_y_legend)
    if _y1 is not None:
        _ax.plot(_x, _y1, 'r', label=_y1_legend)
    if _stability:
        _ax.plot(_x, np.ones(np.size(_x)), 'k')
        _ax.plot(_x, -np.ones(np.size(_x)), 'k')
    if _y_legend is not None:
        _ax.legend(loc=0)
    _ax.set_xlabel(_x_label)
    _ax.set_ylabel(_y_label)
    _ax.set_title(_title)
    _ax.grid(True)


class Butterfly:
    """Minimal implementation of the butterfly orbit class. """

    def __init__(self, _cr3bp, _t, _state0):
        """Initialization via orbit period and initial state. """
        self.cr3bp = _cr3bp  # Cr3bp instance
        self.T, self.T12 = _t, _t / 2.0  # orbit period and half-period
        self.state0 = _state0  # initial state
        self.state12 = None  # state after half-period
        self.C = None  # Jacobi constant
        self.t_vec = self.state_vec = None  # state vector
        self.continuation = {}  # free variables vector and null vector for continuation

    def postprocess(self, _ts):
        """Postprocessing. """
        _ts = _ts if _ts % 2 == 1 else _ts + 1  # propagation with odd number of time steps
        _prop = Cr3bpSynodicPropagator(self.cr3bp.mu, with_stm=True, time_steps=_ts)
        self.t_vec, self.state_vec, _, _ = _prop.propagate([0.0, self.T], self.state0)
        self.C = jac.jacobi(self.state0, self.cr3bp.mu)  # Jacobi constant

    def computation(self, _ts):
        """Differential correction with fixed period. """
        _ms = MultipleShooting(self.cr3bp, precision=dft.single_shooting_precision)
        _prop = Cr3bpSynodicPropagator(self.cr3bp.mu, time_steps=None)
        _t_p, _s_p, _, _ = _prop.propagate([0.0, self.T12], self.state0)  # patch points at 0, t12
        _s_p[:, 1::2] = 0.0  # reset spurious non-zero components (y, vx, vz) at both 0 and t12
        _msk = np.zeros(12, dtype=np.bool_)  # boolean mask to fix (y, vx, vz) to zero
        _msk[1::2] = True
        _t_c, _s_c, _ = _ms.correct(_t_p, _s_p, False, False, None, False, state_mask=_msk)
        self.state0, self.state12, self.T12, self.T = _s_c[0], _s_c[-1], _t_c[-1], _t_c[-1] * 2.0
        self.postprocess(_ts)  # postprocessing (Jacobi constant, state vector)

    def interpolation(self, _t_vec, _state0_vec, _ts):
        """Interpolation from generated abacus w.r.t. orbit period. """
        _x = np.arange(_t_vec.size, dtype=np.float64)  # abacus index, independent variable
        _f = CubicSpline(_x, _t_vec - self.T, extrapolate=False)  # T - T_target = f(index)
        _r = _f.roots()  # find index_target, root of f(index) = 0
        _g = CubicSpline(_x, _state0_vec, extrapolate=False)  # state0 = g(index)
        self.state0 = _g(_r[0])  # compute state0 = g(index_target)
        self.computation(_ts)  # correct the initial guess via single-shooting with fix period


# %% definition of the EM system
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
t_c = cr3bp.T / 2.0 / np.pi  # characteristic time [s]

# %% input parameters
li = cr3bp.l2  # libration point L2
fam = Halo.Family.southern  # southern Halo family
az0_dim = 10.0  # vertical extension of the first computed Halo [km]
rpf_dim = 1000.0  # minimum periselene radius (inside the Moon to improve interpolation) [km]
per_max = 45. * DAYS2SEC / t_c  # maximum period, stopping condition for butterfly continuation
cont_dir = -1  # continuation direction, +1 to go away from the Earth, -1 to go towards it
delta_s = 0.001  # continuation stepsize
nb_steps = 2001  # number of time steps in butterfly orbits state vectors

use_stored_halos = False  # if True use stored list of halos
use_stored_btfly = False  # if True use stored list of butterflies
save_abacus = False  # if True store the butterflies' initial conditions in a .json file
fn_halos = 'eml2_halos.pkl'  # binary file in which the list of halos is stored
fn_butterflies = 'eml2_butterflies.pkl'  # binary file in which the list of butterflies is stored
dn_pkl = os.path.join(os.path.split(DIRNAME)[0], 'examples', 'continuation')  # directory for .pkl
plot_step = 50  # step at which successive orbits are displayed
plot_per_max = 20. * 86400. / t_c  # maximum period when plotting butterflies together with Halos

# %% continuation of the Halo family
simplefilter('ignore', TqdmWarning)
if use_stored_halos:
    halos = load_pickle(os.path.join(dn_pkl, fn_halos))
else:
    halos = halo_continuation(cr3bp, li, fam, az0_dim, rpf_dim, delta_s)
    save_pickle(halos, os.path.join(dn_pkl, fn_halos))

# %% NRHOs extraction (projection of the periselene onto the x-axis falls inside the Moon)
nrhos, constr = [], [o.m2_apsis['periapsis_position'][0] for o in halos]
for i, o in enumerate(halos):
    if np.abs(constr[i] - cr3bp.m2_pos[0]) < cr3bp.m2.Rm / cr3bp.L:
        nrhos.append(o)

# %% NRHOs parameters extraction
state0_vec = np.asarray([o.state0 for o in nrhos])  # initial state
az_vec = np.asarray([o.Az for o in nrhos])  # vertical extension
rp_vec = np.asarray([o.m2_apsis['periapsis_radius'] for o in nrhos])  # periselene radius
cjac_vec = np.asarray([o.C for o in nrhos])  # Jacobi constant
per_vec = np.asarray([o.T for o in nrhos])  # orbit period
stb_idx_vec = np.asarray([o.stability_idx for o in nrhos])  # stability indexes

# %% alpha, beta parameters of Brook stability diagram
# period doubling bifurcation for f(alpha, beta) = 2 + beta - 2 * alpha = 0
alpha_vec = 2.0 - np.asarray([o.monodromy.trace() for o in nrhos])
beta_vec = 0.5 * (alpha_vec * alpha_vec + 2.0 -
                  np.asarray([np.linalg.matrix_power(o.monodromy, 2).trace() for o in nrhos]))

# %% NRHO parameters interpolation for first guess of the bifurcating orbit
x_interp = np.arange(az_vec.size, dtype=np.float64)
f_interp = CubicSpline(x_interp, 2. + beta_vec - 2. * alpha_vec, extrapolate=False)
f_roots = f_interp.roots()
g_interp = [CubicSpline(x_interp, o, extrapolate=False) for o in (state0_vec, per_vec)]
state0_interp, per_interp = [o(f_roots[f_roots.size - 1]) for o in g_interp]

# %% initialization and (double) correction of the bifurcating NRHO

# initialization and correction at periselene with (x0, z0, vy0, T12) free
nrho_bif = NRHO(cr3bp, li, fam, state0=state0_interp, T=per_interp)
nrho_bif.computation(NRHO.DiffCorrFixDim.min_norm)

# propagation to aposelene
prop = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=None)
_, state_apo, _, _ = prop.propagate([0.0, nrho_bif.T12], nrho_bif.state0)
state_apo = state_apo[-1]  # NRHO state at aposelene
state_apo[1::2] = 0.0  # reset spurious non-zero components (y0, vx0, vz0)

# correction at aposelene over an entire period
nrho_bif = NRHO(cr3bp, li, fam, state0=state_apo, T=2.0 * nrho_bif.T)
nrho_bif.computation(NRHO.DiffCorrFixDim.min_norm)

# compact singular value decomposition (SVD) of the Jacobian matrix at the last iteration.
# Only the non-zero singular values and the corresponding right-singular vectors of the input
# matrix are returned. In this case:
# dfx_mat: 3x4 input matrix with rank 3
# u_mat:   3x3 square matrix whose columns are the left-singular vectors of dfx_mat
# s_val:   1x3 array whose elements are the non-zero singular values of dfx_mat
# vh_mat:  3x4 rectangular matrix whose rows are the right-singular vectors of dfx_mat
#          corresponding to its non-zero singular values
# In this case the SVD is computed such that u_mat @ np.diag(s_val) @ vh_mat = dfx_mat.
# In its full form, the SVD would have returned vh_mat as a 4x4 square matrix with the first three
# rows equivalent to the above and the last one containing the right-singular vector corresponding
# to the zero singular value of dfx_mat. This vector constitutes the null space of dfx_mat.
# Note that in both cases singular values equal to zero are omitted from s_val which has always
# a dimension equal to the rank of the input matrix dfx_mat.
u_mat, s_val, vh_mat = np.linalg.svd(nrho_bif.continuation['dfx_mat'], full_matrices=False)

# modified null vector and first guess for butterfly initial state.
# The sign ambiguity of the modified null vector is solved forcing its first entry to be positive
# such that the x component of the butterfly's initial state will be slightly larger than the
# x component at the aposelene of the bifurcating NRHO (i.e. the butterfly's initial state is
# behind the NRHO as seen from either the Moon or the Earth). This behaviour can be modified to
# obtain an initial state on the opposite side of the NRHO aposelene setting the cont_dir parameter
# equal to -1.
null_vec_mod = cont_dir * np.sign(vh_mat[np.argmin(s_val), 0]) * \
               vh_mat[np.argmin(s_val)].reshape(nrho_bif.continuation['null_vec'].shape)
cont_bif = {'g_vec': nrho_bif.continuation['g_vec'], 'null_vec': null_vec_mod}
state0_bif = nrho_bif.state0.copy()
state0_bif[::2] += delta_s * cont_bif['null_vec'][:3, 0]
t12_bif = nrho_bif.T12 + delta_s * cont_bif['null_vec'][-1, 0]

# %% correction of the first butterfly orbit
diff_corr = DiffCorr3D(cr3bp.mu, max_internal_steps=8_000_000)

# correction with modified continuation to jump from the NRHO to the butterfly family
state0_bif, t12_bif, _, _, _ = \
    diff_corr.diff_corr_3d_cont(state0_bif, t12_bif, delta_s, cont_bif)

# correction to obtain the direction of continuation for the butterfly family
state0_bif, t12_bif, t_bif, cont_bif = diff_corr.diff_corr_3d_full(state0_bif, t12_bif)
btfly = Butterfly(cr3bp, t_bif, state0_bif)
btfly.continuation = cont_bif
btfly.continuation['null_vec'] *= (cont_dir * np.sign(cont_bif['null_vec'][0, 0]))
btfly.postprocess(nb_steps)

# %% continuation procedure
if use_stored_btfly:
    butterflies = \
        load_pickle(os.path.join(dn_pkl, fn_butterflies))
else:
    butterflies = [deepcopy(btfly)]
    progress_bar = tqdm(total=1.0, desc='EM L2 southern Butterfly')
    while delta_s > 1e-8 and butterflies[-1].T < per_max:
        btfly.T12 += delta_s * btfly.continuation['null_vec'][3, 0]  # update period
        btfly.state0[::2] += delta_s * btfly.continuation['null_vec'][:3, 0]  # update state
        btfly.state0, btfly.T12, btfly.T, btfly.continuation, max_iter_reached = \
            diff_corr.diff_corr_3d_cont(btfly.state0, btfly.T12, delta_s, btfly.continuation)
        btfly.postprocess(nb_steps)  # postprocess (Jacobi constant, stability properties, ...)
        if max_iter_reached:  # reject step if differential correction failed to converge
            btfly = butterflies[-1]
            delta_s *= 0.5
        else:  # accept step otherwise
            butterflies.append(deepcopy(btfly))
            progress_bar.update((btfly.T - butterflies[-2].T) / (per_max - butterflies[0].T))
    progress_bar.close()
    save_pickle(butterflies,
                os.path.join(dn_pkl, fn_butterflies))

# %% butterfly parameters extraction and abacus construction
per_vec_bf = np.asarray([b.T for b in butterflies])
cjac_vec_bf = np.asarray([b.C for b in butterflies])
state0_vec_bf = np.asarray([b.state0 for b in butterflies])
if save_abacus:
    save_json('EM_L2_southern_Butterfly', State0=state0_vec_bf, Cjac=cjac_vec_bf, T=per_vec_bf)

# %% abacus validation
#
# reference initial conditions and orbit periods are taken from:
# E. Zimovan Spreen, "Trajectory Design and Targeting for Applications
# to the Exploration Program in Cislunar Space," Ph.D., May 2021
# https://engineering.purdue.edu/people/kathleen.howell.1/Publications/dissertations/2021_ZimovanSpreen.pdf

raw_ex = np.loadtxt(os.path.join(DIRNAME, 'orbits', 'tests', 'data', 'EM_P2HO1_Zimovan.txt'))
cjac_vec_ex = raw_ex[:, 0]  # expected Jacobi constants
per_vec_ex = raw_ex[:, 1]  # expected orbit periods
state0_vec_ex = np.zeros((cjac_vec_ex.size, 6))  # expected initial states
state0_vec_ex[:, ::2] = raw_ex[:, 2:]  # update (x, z, vy) components

# except for the first orbit, reference initial conditions have an x component smaller than 1.0 in
# normalized units. As such, they correspond to the butterfly's initial state if the cont_dir
# parameter is set equal to -1 or to the butterfly's state at t12 if cont_dir is set equal to +1.
# The opposite is true for the first orbit in the reference abacus. Concerning the first butterfly
# in the reference abacus, its period lies outside the domain of validity of the computed abacus
# thus preventing from a successful recovery of this orbit via interpolation routines. As such,
# the loop below is started from 1 rather than 0 to skip the validation against the very first
# reference solution.
for i in range(1, cjac_vec_ex.size):
    bf = Butterfly(cr3bp, per_vec_ex[i], np.zeros(6))  # initialize butterfly with dummy state
    bf.interpolation(per_vec_bf, state0_vec_bf, nb_steps)  # interpolation and correction
    state_act = bf.state0 if cont_dir < 0 else bf.state12  # choose the state to be validated
    np.testing.assert_equal(bf.T, per_vec_ex[i])  # validate orbit period
    np.testing.assert_allclose(state_act, state0_vec_ex[i], rtol=0.0, atol=1e-7)  # validate state

# %% trajectory plots

# colormap
norm = Normalize(clip=True)
norm.autoscale(cjac_vec_bf)
smap = ScalarMappable(norm, 'inferno_r')
carr = smap.to_rgba(cjac_vec_bf)

# 3D plot
fig, axs = plt.figure(), plt.axes(projection='3d')
for i in np.arange(0, cjac_vec_bf.size, plot_step):
    axs.plot3D(butterflies[i].state_vec[:, 0], butterflies[i].state_vec[:, 1],
               butterflies[i].state_vec[:, 2], color=carr[i])
cbar = plt.colorbar(smap)
cbar.set_label('Jacobi constant')
decorate_3d_axes(axs, 'EML-2 Butterfly family', '-', False, True)

fig, axs = plt.figure(), plt.axes(projection='3d')
for i in np.arange(0, len(halos), plot_step):
    axs.plot3D(halos[i].state_vec[:, 0], halos[i].state_vec[:, 1], halos[i].state_vec[:, 2], 'b')
for i in np.arange(0, cjac_vec_bf.size, plot_step):
    axs.plot3D(butterflies[i].state_vec[:, 0], butterflies[i].state_vec[:, 1],
               butterflies[i].state_vec[:, 2], 'm')
    if butterflies[i].T > plot_per_max:
        break
decorate_3d_axes(axs, 'EML-2 Halo and Butterfly family', '-', False, True)

# 2D plots
labs = (('y', 'x', 'x'), ('z', 'y', 'z'))
fig, ax = plt.subplots(nrows=1, ncols=3)
for i in np.arange(0, cjac_vec_bf.size, plot_step):
    ax[0].plot(butterflies[i].state_vec[:, 1], butterflies[i].state_vec[:, 2], color=carr[i])
    ax[1].plot(butterflies[i].state_vec[:, 0], butterflies[i].state_vec[:, 1], color=carr[i])
    ax[2].plot(butterflies[i].state_vec[:, 0], butterflies[i].state_vec[:, 2], color=carr[i])
for i in range(3):
    ax[i].set_xlabel(labs[0][i] + ' [-]')
    ax[i].set_ylabel(labs[1][i] + ' [-]')
    ax[i].grid(True)
    ax[i].set_aspect('equal')
cbar = plt.colorbar(smap)
cbar.set_label('Jacobi constant')

# %% orbit's characteristics plots
plot_params(per_vec * t_c / DAYS2SEC, stb_idx_vec[:, 0], _y1=stb_idx_vec[:, 1],
            _y_legend=r'$\nu_1$', _y1_legend=r'$\nu_2$', _x_label=r'$T\ [days]$',
            _y_label=r'$\nu_i$', _title='Stability indexes for EML-2 NRHOs', _stability=True)
plot_params(per_vec * t_c / DAYS2SEC, 2.0 * alpha_vec, _y1=beta_vec + 2.0,
            _y_legend=r'$2 \cdot \alpha$', _y1_legend=r'$\beta + 2$', _x_label=r'$T\ [days]$',
            _y_label=r'$2 \cdot \alpha, \beta + 2$',
            _title=r"Brook's $\alpha, \beta$ parameters for EML-2 NRHOs", _stability=False)
plot_params(per_vec_bf * t_c / DAYS2SEC, cjac_vec_bf, _y1=nrho_bif.C * np.ones(per_vec_bf.size),
            _y_legend='butterfly family', _y1_legend='bifurcating NRHO',
            _x_label=r'$T\ [days]$', _y_label=r'$J_c\ [-]$',
            _title='Jacobi constant for EML-2 butterflies', _stability=False)
plot_params(per_vec_bf * t_c / DAYS2SEC, state0_vec_bf[:, 0],
            _y1=nrho_bif.state0[0] * np.ones(per_vec_bf.size),
            _y_legend='butterfly family', _y1_legend='bifurcating NRHO',
            _x_label=r'$T\ [days]$', _y_label=r'$x_0\ [-]$',
            _title=r"$x$ component of the initial state for EML-2 butterflies", _stability=False)
plt.show()
