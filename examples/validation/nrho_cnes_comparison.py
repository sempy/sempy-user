"""
Near Rectilinear Halo Orbit computation and comparison with CNES results.

@author: Alberto FOSSA'
"""

import os
from platform import system
from psutil import cpu_count
import numpy as np
import spiceypy as sp
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.orbits.halo import Halo
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.propagation.patch_points_propagator import PatchPointsPropagator
from src.diffcorr.multiple_shooting import MultipleShooting
from src.init.load_kernels import load_kernels
from src.coc.synodic_j2000 import j2000_to_synodic, synodic_to_j2000
from src.coc.translation_j2000 import translation_j2000
from src.plotting.util import set_axes_equal

load_kernels()
dirname = os.path.dirname(__file__)
host_os = system()
processes = None if host_os == 'Windows' else cpu_count(logical=False)
sem = True  # perform comparison in SEM model


def decorate(ax, title, units='-'):
    """Decorates figures with title, labels, legend, aspect ratio. """
    ax.grid()
    ax.set_title(title)
    ax.set_xlabel(f"x [{units}]")
    ax.set_ylabel(f"y [{units}]")
    ax.set_zlabel(f"z [{units}]")
    ax.legend(loc=0)
    set_axes_equal(ax)


if __name__ == '__main__':

    # CR3BP approximation
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

    # CNES data
    mu_a = 0.012150668  # CR3BP mass ratio [-]
    T_dim_a = 2 / 9 * 29.530588853  # orbit period [days]
    T_a = 1.5091501123205779  # orbit period [-]

    # initial conditions [-]
    y0_a = np.array([0.98738376315705323, 0., 0.00837734870408136, 0., 1.67367757741687151, 0.])

    # update constants to match CNES ones
    cr3bp.mu = mu_a  # CR3BP mass ratio [-]
    t_c = T_dim_a * 86400 / T_a  # characteristic time [s]
    cr3bp.T = 2.0 * np.pi * t_c  # EM rotation period [s]

    # propagation
    prop = Cr3bpSynodicPropagator(mu_a)
    _, sv_a, _, _ = prop.propagate([0.0, T_a], y0_a)

    # correction with single shooting
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, state0=y0_a)

    # minimum norm solution (variable period)
    halo.computation(Halo.DiffCorrFixDim.min_norm)
    np.testing.assert_allclose(halo.state0, y0_a, rtol=0.0, atol=1e-16)
    np.testing.assert_allclose(halo.T, T_a, rtol=0.0, atol=1e-10)

    # correction with multiple shooting as single shooter (fixed period)
    multi_shoot = MultipleShooting(cr3bp, precision=1e-11)

    t_patch = np.asarray([0.0, T_a])  # time at patch points
    state_patch = np.vstack((y0_a, y0_a))  # state at patch points (force periodicity)

    state_mask = np.zeros(12, dtype=np.bool)  # dimension 2 * 6
    state_mask[1:12:2] = True  # fixed y, vx, vz components equal to zero
    t_mask = np.ones(2, dtype=np.bool)  # fixed time at both endpoints

    t_corr, state_corr, sol = \
        multi_shoot.correct(t_patch, state_patch, state_mask=state_mask,
                            t_mask=t_mask, procs=processes)
    np.testing.assert_array_equal(t_corr, t_patch)
    np.testing.assert_allclose(state_corr, state_patch, rtol=0.0, atol=2e-9)

    tv_corr, sv_corr, _, _ = prop.propagate(t_corr, state_corr[0, :])

    # manifolds
    halo.all_manifolds_computation(10, 10 * halo.T, procs=processes, d_man=50)

    if sem:  # analysis in Sun-Earth-Moon system
        # Sun-Earth-Moon ephemeris propagation
        eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

        # CNES data
        utc0_a = '2020 JAN 01 00:00:00.000'  # initial epoch in UTC calendar format
        et0_a = sp.str2et(utc0_a)  # initial epoch in ET [s]
        data_a = np.loadtxt(os.path.join(dirname, 'data', 'ephem_lopg_test_stage.txt'))
        tv_j2000_dim_a = et0_a + np.ascontiguousarray(data_a[:, 0])  # time vector in ET [s]

        # state vector in J2000 frame centered at Moon's center [km, km/s]
        sv_j2000_moon_dim_a = np.ascontiguousarray(data_a[:, 1:] * 1e-3)

        # state vector in J2000 frame centered at Earth's center [km, km/s]
        _, sv_j2000_dim_a = translation_j2000(tv_j2000_dim_a, sv_j2000_moon_dim_a,
                                              Primary.MOON, Primary.EARTH)

        # time and state vectors in J2000 frame centered at Earth's center [-, -]
        tv_j2000_a = tv_j2000_dim_a / t_c
        sv_j2000_a = sv_j2000_dim_a / cr3bp.L
        sv_j2000_a[:, 3:6] *= t_c

        # propagation of raw initial conditions

        # Moon centered, J2000 inertial frame, dimensional units [km, km/s]
        eph_moon = Ephemeris((Primary.MOON, Primary.EARTH, Primary.SUN))
        eph_moon.gm_dim = 1e-9 * np.asarray([1.23000371e-2 * 3.986004415e14,
                                             3.986004415e14, 1.32712440041e20])
        eph_prop_moon = EphemerisPropagator(eph_moon)

        tv_j2000_moon_dim_raw, sv_j2000_moon_dim_raw, _, _ = \
            eph_prop_moon.propagate(tv_j2000_dim_a, sv_j2000_moon_dim_a[0])
        np.testing.assert_allclose(sv_j2000_moon_dim_a[:, :3], sv_j2000_moon_dim_raw[:, :3],
                                   rtol=0.0, atol=1e-2)
        np.testing.assert_allclose(sv_j2000_moon_dim_raw[:, 3:], sv_j2000_moon_dim_a[:, 3:],
                                   rtol=0.0, atol=1e-3)

        # Earth centered, J2000 inertial frame, non-dimensional units [-, -]
        eph_prop = EphemerisPropagator(eph, t_c=t_c, l_c=cr3bp.L)
        tv_j2000_raw, sv_j2000_raw, _, _ = eph_prop.propagate(tv_j2000_a, sv_j2000_a[0])
        np.testing.assert_allclose(sv_j2000_raw, sv_j2000_a, rtol=0.0, atol=2e-3)

        tv_syn_raw, sv_syn_raw = j2000_to_synodic(tv_j2000_raw, sv_j2000_raw, et0_a, cr3bp,
                                                  cr3bp.m1, adim=True, bary_from_spice=True)

        # time and states vectors in synodic frame [-, -]
        tv_syn_a, sv_syn_a = j2000_to_synodic(tv_j2000_dim_a, sv_j2000_dim_a, et0_a, cr3bp,
                                              cr3bp.m1, bary_from_spice=True)

        # sampling and correction
        multi_shoot = MultipleShooting(eph, epoch_constr=True, t_c=t_c, l_c=cr3bp.L,
                                       precision=1e-6)
        nb_revs = int(np.ceil((tv_j2000_dim_a[-1] - tv_j2000_dim_a[0]) / t_c / halo.T))
        nb_patch_rev = 50  # number of patch points per revolution
        t_patch_syn, state_patch_syn = halo.sampling(nb_revs,
                                                     nb_patch_rev)  # patch points in synodic
        t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch_syn, state_patch_syn, et0_a,
                                                            cr3bp, cr3bp.m1,
                                                            adim=True, bary_from_spice=True)

        tp_j2000, sp_j2000, _ = \
            multi_shoot.correct(t_patch_j2000, state_patch_j2000, procs=processes, t_fix=0)
        tp_pf_j2000, sp_pf_j2000, _ = multi_shoot.correct(t_patch_j2000, state_patch_j2000,
                                                          procs=processes, pos_fix=0, t_fix=0)

        # updated initial epochs
        utc0 = sp.et2utc(tp_j2000[0] * t_c, 'C', 3)
        utc0_pf = sp.et2utc(tp_pf_j2000[0] * t_c, 'C', 0)
        utc0_list = (utc0_a, utc0, utc0_pf)
        print('\nInitial epochs:\n')
        for i, n in enumerate(('CNES', 'All free', 'Init pos fix')):
            print(f"{n:<12s}: {utc0_list[i]}")

        # propagation of corrected patch points
        patch_prop = PatchPointsPropagator(eph, t_c=t_c, l_c=cr3bp.L, time_steps=100)
        tv_j2000, sv_j2000 = patch_prop.propagate(tp_j2000, sp_j2000, procs=processes)
        tv_pf_j2000, sv_pf_j2000 = patch_prop.propagate(tp_pf_j2000, sp_pf_j2000,
                                                        procs=processes)

        # transformation back to synodic
        tv_syn, sv_syn = j2000_to_synodic(tv_j2000, sv_j2000, tv_j2000[0] * t_c, cr3bp,
                                          cr3bp.m1, adim=True, bary_from_spice=True)
        tv_pf_syn, sv_pf_syn = j2000_to_synodic(tv_pf_j2000, sv_pf_j2000, tv_pf_j2000[0] * t_c,
                                                cr3bp, cr3bp.m1,
                                                adim=True, bary_from_spice=True)

        # plot
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111, projection='3d')
        ax1.plot(sv_syn_a[:, 0], sv_syn_a[:, 1], sv_syn_a[:, 2],
                 color='b', label='SEM CNES')
        ax1.plot(sv_syn_raw[:, 0], sv_syn_raw[:, 1], sv_syn_raw[:, 2],
                 color='m', label='SEM raw prop')
        ax1.plot(sv_syn[:, 0], sv_syn[:, 1], sv_syn[:, 2],
                 color='g', label='SEM free')
        ax1.plot(sv_pf_syn[:, 0], sv_pf_syn[:, 1], sv_pf_syn[:, 2],
                 color='r', label='SEM pos fix')
        decorate(ax1, 'NRHO corrected in SEM model')

    # plot
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    ax2.plot(sv_a[:, 0], sv_a[:, 1], sv_a[:, 2],
             color='k', label='IC CNES')
    ax2.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2], color='b',
             label='IC corrected')
    for kind in halo.manifolds:
        for branch in halo.manifolds[kind]:
            color = 'r' if branch.stability == 'unstable' else 'g'
            ax2.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2],
                     color=color)
    decorate(ax2, 'NRHO in CR3BP model')
    plt.show()
