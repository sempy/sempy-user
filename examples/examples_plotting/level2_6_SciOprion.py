#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 24 20:09:56 2019

@author: Andrea ZOLLO

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING OPTIONS : SCI.
"""
#___________imports_____________
from sempy.core.init.primary import Primary

from sempy.core.init.cr3bp import Cr3bp

from sempy.core.orbits.halo import Halo
"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from sempy.core.plotting.plotting_module import PlottingOptions, MasterPlotter

#___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim = 12000)
orbit.computation()


#___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close('all')
"""Initialization of the plotting options. """


"""Example"""
plotting_options = PlottingOptions(sci = True) 

#The scientific notation is activated on all the figures.

"""Creation of a MasterPlotter object"""

plot = MasterPlotter(plotting_options, orbit)

#the 2D views are displayes by running the following method
plot.scene_twoD()
#the 3D view is displayed by running the following method
plot.scene_TD()