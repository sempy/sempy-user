#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 10:20:17 2019

@author: an.zollo
"""

#___________imports_____________
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
import matplotlib.pyplot as plt
from sempy.core.plotting.plotting_module import PlottingOptions, MasterPlotter
#_____________computation____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON) 

orbit = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim = 12000)
orbit.computation() 

orbit1 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim = 18000)
orbit1.computation()

orbit2 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim = 20000)
orbit2.computation()  

#___________plotting_________________

plt.ioff()
plt.close('all')
plotting_options = PlottingOptions(l2 = False, disp_first_pr = False, sci = True, orbit_color = 'b', legend = False) 
plot = MasterPlotter(plotting_options, orbit, orbit1 = orbit1, orbit2 = orbit2)
plot.show_all()
