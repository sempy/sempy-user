"""
Propagation of an Initial State.
================================

Example of propagation of an initial state until impact with one of the two primaries or SOI
crossing.

@author: Paolo GUARDABASSO
"""

# %%
# Import all necessary modules and classes:

import numpy as np
import matplotlib.pyplot as plt
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.init.constants import DAYS2SEC
from sempy.core.diffcorr.ode_event import generate_impact_event, generate_soi_event
from sempy.core.plotting.util import sphere, set_axes_equal


def set_axes_lim_labels_title(_ax, title):
    """Convenience function to set the axes limits, labels and the figure title. """
    _ax.set_xlabel('x [-]')
    _ax.set_ylabel('y [-]')
    _ax.set_zlabel('z [-]')
    _ax.set_title(title)
    set_axes_equal(_ax)


# %%
# Initialize the CRTBP system:
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# Set the initial state to be propagated:
# state0 = np.array([0.1, 0, 0, 0, 0, 0])  # for m1 impact
# state0 = np.array([1.0, 0, 0, 0, 0, 0])  # for m2 impact
# state0 = np.array([2.5, 0, 0, -1, 0, 0])  # for SOI crossing
state0 = np.array([cr3bp.l1.position[0]+0.1, 0, 0, -1, 0, 0])  # for L1 gate crossing
# state0 = np.array([cr3bp.l2.position[0]-0.1, 0, 0, 1, 0, 0])  # for L2 gate crossing

# %%
# Set the time for the propagation:
T = 10
t_span = [0, T]

# %%
# Events initialisation
m1_impact_event = generate_impact_event(cr3bp, "m1")
m2_impact_event = generate_impact_event(cr3bp, "m2")
soi_escape_event = generate_soi_event(cr3bp, "exit")
soi_enter_event = generate_soi_event(cr3bp, "enter")

events = (m1_impact_event, m2_impact_event,
          soi_escape_event, soi_enter_event)

events_name = ["m1 impact", "m2 impact",
               "soi escape", "soi enter"]

# %%
# Initialize the propagator:
prop = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=200000)

# %%
# Now the class Cr3bpSynodicPropagator can be used to propagate the initial state:
t_vec, state_vec, t_events, state_events = prop.propagate(t_span, state0, events=events)

# %%
# Plot the results:

plt.rcParams['legend.fontsize'] = 10

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Plot state
ax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], 'y', label='Propagation state0')
ax.scatter(state_vec[0, 0], state_vec[0, 1], state_vec[0, 2], color='r', label='State0')

# Plot Moon
x_2, y_2, z_2 = sphere(100, cr3bp.R2 / cr3bp.L, cr3bp.m2_pos)
ax.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)

# Plot trajectory
for i, t_event in enumerate(t_events):
    if t_event.size > 0:
        ax.scatter(state_events[i][:, 0], state_events[i][:, 1], state_events[i][:, 2],
                   label=events_name[i])
ax.legend()
set_axes_lim_labels_title(ax, 'State 0 propagation with events')

# %%
# Print event information
for i, t_event in enumerate(t_events):
    if t_event.size > 0:
        print(f"Time to {events_name[i]}: {t_event[0]*cr3bp.T/2/np.pi/DAYS2SEC:.2f} days")
