import os

def find(name, path_back):
	for root, dirs, files in os.walk(path_back, topdown=False):
		if name in files:
	            return root

owd = os.getcwd()
dirs = owd.split('/')

N = len(dirs)

path_back = '../'

for i in range(N-2):
	path_back = os.path.join(path_back, '../')

# CONFIGURATION FILE

# general parameters
REMOVE = True
CONFIGFILE = "config_file"
PROJECTPATH = os.path.abspath("")
PROJECTPATH = PROJECTPATH.replace("\\", "/")
# BLENDERPATH = "/home/ygary/git_v1/dependencies/blender-2.93.0-stable+blender-v293-release.84da05a8b806-linux.x86_64-release"  # REPLACE BY YOUR BLENDER PATH (only for Linux)
# VTSPATH = "/home/ygary/git_v1/dependencies/Vts-Linux-64bits-3.5.1"  # REPLACE BY YOUR VTS PATH
BLENDERPATH = find('blender', path_back)
VTSPATH = find('startVTS', path_back)
FILE3DPATH = "models"
DATAPATH = "data"
MOVIEPATH = "movies"
TIMESTEP = 1

# satellite parameters
WEIGHT = 0
ATTITUDEFILE = None
SATELLITERATIO = 1

# visualisation parameters
SOFTWARE = "Blender"
RENDER = False

# Blender parameters
CENTER = None

# VTS parameters
INITIALDATE = "55270 0.000000"  # MJD format

# screen parameters
RESOLUTION = "1920*1080"
FPS = 30  # frames per second
FRAMESTEP = 1
SAMPLES = 64
RATIO = None
BOXSIZE = 2000  # in meters
MOVIEDURATION = 60  # in seconds
MOVIENAME = "MovieRDV"
LIGHTORIENTATION = 90, 0, 30  # in degrees
PAUSED = False
MINIMIZE = False

# camera parameters
PLAN = "XY"
CENTEREDOBJECT = None
DISTANCECLOSE = 125  # in meters
DISTANCEFAR = 10000  # in meters
SEEINSIDE = False
NOTTOSCALE = False
SCALERATIO = 1

# constraint parameters
DISPLAY = True
COLOR = "red"
POSROT = None
SIZE = 1  # ratio (ex: 50, the object will be 50 times its initial size)
FILE3D = None
FILEPOS = None
ACTIVE = True
FILE3DCONEBLENDER = "Cone.blend"
FILE3DSPHEREBLENDER = "Sphere.blend"
FILE3DSPHEREVTS = "_sphere.obj"
FILE3DPLUMEBLENDER = "Plume.blend"
FILE3DPLUMEVTS = "plume.obj"
FILE3DPOINTBLENDER = "Point.blend"
