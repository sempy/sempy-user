# -*- coding: utf-8 -*-
# pylint: disable=line-too-long
"""
Created on Wed Apr 22 10:00:09 2020

@author: Valentin Prudhomme
"""

# _________________________________IMPORTS_____________________________________

from xml.etree import ElementTree
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, Comment
import sempy.visualisation.default_config as config


def color_to_int(color):
    """
    This method will return the color in an integer RGB format
    (1 0 0 for the red)

    Parameters:
        color (str: "red", "orange", "yellow", "sky blue", "blue",
        "pink", "purple" or "green"): color to transform to an integer

    Return:
        to_return (RGB str): The color given in parameter in the RGB format
    """
    if color in ("red", "Red"):
        to_return = "1 0 0"
    elif color in ("orange", "Orange"):
        to_return = "1 0.330000 0"
    elif color in ("yellow", "Yellow"):
        to_return = "1 1 0"
    elif color in ("green", "Green"):
        to_return = "0 1 0"
    elif color in ("sky blue", "Sky blue"):
        to_return = "0 1 1"
    elif color in ("blue", "Blue"):
        to_return = "0 0 1"
    elif color in ("purple", "Purple"):
        to_return = "0.330000 0 1"
    elif color in ("pink", "Pink"):
        to_return = "1 0 0.800000"
    else:
        print(
            "The color '" + color +
            "' is not available, please choose between red, orange, yellow, green, blue, sky blue, purple or pink")
        to_return = "1 0 0"
    return to_return


# _____________________________VTS VISUALISATION CLASS__________________________


class VTSVisu:
    """
    The VTSvisu class contains all the information and methods for the
    VTS visualisation.

    Parameters:
        global_visualisation (obj1): Class with needed objects
        initial_time (str): Initial time in the MJD format (Example : "55276 18745")

        Tobeusedapp (dict1): Dictionary with the application used in VTS. If you want to use an application, just turn
        it to True. You can use all the applications at the same time:
            infobox (bool)
            celestia (bool): Main application for the 3D visualisation (only one active by default)
            nadirview (bool)
            prestoplot (bool)
            sensorview (bool)
            skyview (bool)
            surfaceview (bool)
            zenithview (bool)
    """

    def __init__(self, global_visualisation, initial_time=config.INITIALDATE):
        self.global_visualisation = global_visualisation
        self.screen = global_visualisation.Screen
        self.camera = global_visualisation.Camera
        self.project = Element("Project", {'Revision': "8200"})
        self.component = None
        self.list_file = []
        self.initial_time = initial_time
        self.list_appused = []

    Tobeusedapp = {"infobox": False, "celestia": True, "nadirview": False,
                   "prestoplot": False, "sensorview": False, "skyview": False,
                   "surfaceview": False, "zenithview": False}

    def new_step_vts(self, element, state):
        """
        The new_step_vts method will create a step from the list of
        step in the list_step in the GlobalVisualisation class
        (where the steps were added with the add_step method).

        Parameters:
            state (XML Element): Mandatory to write in this Element in
            the XML file
            element (dict): Dictionary with the state attributes.
        """
        if element["label"] == "initial state for time ratio":
            element["time_ratio"] = float(self.global_visualisation.list_sat_visu[0]["list_time_sat"][-1]) / \
                                    self.screen["movie_duration"]
        sat_cam = None
        initial_time = self.initial_time.split(" ")
        instant = SubElement(state, 'Instant', {'Time': initial_time[0] + " " + str(float(element["time"]) +
                                                                                    float(initial_time[1])),
                                                'TimeRatio': str(element["time_ratio"]), 'Label': element["label"]})
        for app in self.list_appused:
            appstate = SubElement(instant, 'AppState', {'Id': app})
            comment = Comment("Add a step to the visualisation (to undisplay constraints and change"
                              " time ratio and camera)")
            appstate.append(comment)
            if app == "1":
                res = self.screen["resolution"].split("*")
                resolution = [Element('Command', Str="CMD PROP WindowGeometry 0 0 " + res[0] + " " + res[1])]
                appstate.extend(resolution)
                for sat in self.global_visualisation.list_sat_visu:
                    if sat["satellite"].name == self.camera["centered_object"]:
                        sat_cam = sat
                if sat_cam is None:
                    raise NameError("The satellite followed by the camera doesn't exist")
                camera = [Element('Command',
                                  Str="CMD PROP CameraDesc bodyfixed " + '"' + str(sat_cam["parent_path"] +
                                                                                   "/" + sat_cam["satellite"].name +
                                                                                   "_ref/" + sat_cam["satellite"].name +
                                                                                   "_QswAxes") + '"' +
                                  " nil -2.55516e-09 4.361542e-09 " +
                                  "-1.29603709999999e-08 -0.05573241" +
                                  " -0.7291487 0.6555173 0.188502 " +
                                  "0.872664625997165")]
                appstate.extend(camera)
                for sat in self.global_visualisation.list_sat_visu:
                    time_traj = [Element('Command', Str="CMD STRUCT TrackWindow " + '"' + str(sat["parent_path"] +
                                                                                              "/" +
                                                                                              sat["satellite"].name +
                                                                                              '"')
                                                        + " 1000 1000")]
                    appstate.extend(time_traj)
                for cons in element["constraint_not_displayed"]:
                    for sat in self.global_visualisation.list_sat_visu:
                        for cons_sat in sat["list_cons"]:
                            if cons_sat["label"] == cons:
                                sat_name = sat["satellite"].name
                                if cons_sat["category"] in ("Sphere", "Plume", "Point"):
                                    command = [Element('Command',
                                                       Str="CMD STRUCT Visible " + '"' + str(sat["parent_path"] +
                                                                                             "/" + sat_name +
                                                                                             "/" + cons + '"' +
                                                                                             " false"))]
                                    appstate.extend(command)
                                elif cons_sat["category"] == "Cone":
                                    command = [Element('Command',
                                                       Str="CMD STRUCT AimVolumeVisible " + '"' + str(
                                                           sat["parent_path"] +
                                                           "/" + sat_name + "/" +
                                                           cons + '"' +
                                                           " false"))]
                                    appstate.extend(command)
                                elif cons_sat["category"] == "Trajectory":
                                    command = [Element('Command',
                                                       Str="CMD STRUCT TrackVisible " + '"' + str(sat["parent_path"] +
                                                                                                  "/" + sat_name +
                                                                                                  '"' + " false"))]
                                    appstate.extend(command)

    def prettify(self):
        """
        This method will take the main Element of the XML file
        and will print it correctly in a XML format

        Return:
            a pretty-printed XML string for the Element.
        """
        rough_string = ElementTree.tostring(self.project, 'UTF-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent=" ")

    def writing_file(self, category, list_to_copy, old_file_name, list_time_sat, sat_name, center_name):
        """
        This method will write in the good format the position and attitude
        file used by VTS (Format: dateMJD X Y Z or dateMJD q0 q1 q2 q3)

        Parameters:
            category (str1): Either attitude or position. The format of the
            file is different for these two categories
            list_to_copy (list of int): list of the positions or the
            attitude to copy in the good format in the file
            old_file_name (str2): Name of the old file with the position or
            the attitudes
            list_time_sat (list of int): List of time for the positions or
            the attitudes
            sat_name (str3): Name of the satellite
            center_name (str4): Name of the central body of the scene

        Return:
            new_file_name (str or None): The name of the new file in the good
            format or None if the file was None at the start.
        """
        if isinstance(old_file_name, list) and category == "attitude":
            if len(old_file_name)/4 != len(list_time_sat):
                raise IndexError("The attitude data and the time data of the satellite:", sat_name, "don't have the"
                                                                                                    " same number of"
                                                                                                    " lines")
        if isinstance(old_file_name, list) and category == "position":
            if len(old_file_name)/3 != len(list_time_sat):
                raise IndexError("The position data and the time data of the satellite:", sat_name, "don't have the"
                                                                                                    " same number of"
                                                                                                    " lines")
        if old_file_name is not None:
            if isinstance(old_file_name, str):
                old_file_name = old_file_name.split(".")
                initial_time = self.initial_time.split(" ")
                fichier_trajectoire = open(self.global_visualisation.data_path + "/" + old_file_name[0] +
                                           "_vts." + old_file_name[1], "w")
                new_file_name = old_file_name[0] + "_vts." + old_file_name[1]
            elif isinstance(old_file_name, list):
                initial_time = self.initial_time.split(" ")
                fichier_trajectoire = open(self.global_visualisation.data_path + "/" + "default_file_from_a_list_" +
                                           sat_name + ".txt", "w")
                new_file_name = "default_file_from_a_list_" + sat_name + ".txt"
            else:
                raise TypeError("The position, attitude and time can only be a list or a name of a file")
            if category == "attitude":
                fichier_trajectoire.write(
                    "CIC_AEM_VERS = 1.0\nCREATION_DATE  = 2020-06-05T09:00:00\n" +
                    "ORIGINATOR     = SPACEBEL\n\nMETA_START\n\nOBJECT_NAME = " + sat_name +
                    "\nOBJECT_ID = " + sat_name + "\n\nCENTER_NAME = " + center_name +
                    "\nREF_FRAME_A = EME2000\nREF_FRAME_B = SC_BODY_1\nATTITUDE_DIR = A2B\n\n" +
                    "TIME_SYSTEM = UTC\n\nATTITUDE_TYPE = QUATERNION\n\nMETA_STOP\n\n")
                for i, item in enumerate(list_time_sat):
                    fichier_trajectoire.write(str(initial_time[0]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(float(item) + float(initial_time[1])))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[4 * i]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[4 * i + 1]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[4 * i + 2]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[4 * i + 3]))
            elif category == "position":
                fichier_trajectoire.write(
                    "CIC_OEM_VERS = 2.0\nCREATION_DATE  = 2020-06-05T09:00:00\n" +
                    "ORIGINATOR     = SPACEBEL\n\nMETA_START\n\nOBJECT_NAME = " + sat_name +
                    "\nOBJECT_ID = " + sat_name + "\n\nCENTER_NAME = " + center_name +
                    "\nREF_FRAME   = EME2000\nTIME_SYSTEM = UTC\n\nMETA_STOP\n\n")
                for j, item in enumerate(list_time_sat):
                    fichier_trajectoire.write(str(initial_time[0]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(float(item) + float(initial_time[1])))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[3 * j]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[3 * j + 1]))
                    fichier_trajectoire.write(' ')
                    fichier_trajectoire.write(str(list_to_copy[3 * j + 2]))
            else:
                raise NameError("Category must be either Position or Attitude")
            fichier_trajectoire.close()
        else:
            new_file_name = None
        return new_file_name

    def initialisation_time_monitor(self):
        """
        This method will write the beginning of the XML file
        (it uses the starting time given by the user, the files...)

        Return:
            timeline_options (XML Element): The XML Element created during
            this method. It will be used by other methods to write lines
            in this Element.
        """
        initial_time = self.initial_time.split(" ")
        end_time = self.global_visualisation.list_sat_visu[0]["list_time_sat"][-1]
        general = [Element('General', Name="", StartDateTime=self.initial_time,
                           EndDateTime=initial_time[0] + " " + str(float(initial_time[1]) + float(end_time)))]
        self.project.extend(general)
        comment = Comment("Initialise the starting time and end time and some parameters for the visualisation")
        self.project.append(comment)
        metadata = SubElement(self.project, 'MetaData')
        SubElement(metadata, 'Description')
        monitorconfiguration = SubElement(self.project, 'MonitorConfiguration')
        monitor = [Element('Monitor', X="0", Y="0", Height="1080", Width="1920")]
        monitorconfiguration.extend(monitor)
        startoptions = [Element('StartOptions', TimeRatio="1", UseStateTimeRatio="1", SysTimeSynced="0",
                                Paused=str(int(self.screen["initially_paused"])), Looped="1",
                                Minimized=str(int(self.screen["minimize_broker"])), Hidden="0", AutoClosed="0")]
        self.project.extend(startoptions)
        brokeroptions = [
            Element('BrokerOptions', WindowMode="Undocked", Collapsed="1", AlwaysOnTop="1", XPos="605", YPos="266",
                    Width="710", Height="84", ActiveTab="0", HiddenTabs="")]
        self.project.extend(brokeroptions)
        timeline_options = SubElement(self.project, 'TimelineOptions',
                                      {'ProjectLocked': "1", 'CursorLocked': "0", 'CursorRatio': "0",
                                       'ViewStart': "33282 0.000000", 'ViewSpan': "0", 'DateFormat': "ISODATE",
                                       'NoBadgeFiltered': "0", 'BadgeFiltered': ""})
        timelinescenario = [Element('TimelineScenario', Name="Scenario", Pos="0", Size="22")]
        timeline_options.extend(timelinescenario)
        return timeline_options

    def initialisation_sky_sun(self):
        """
        This method will write the lines to initialise the sky and the sun
        in the Celestia application
        """
        sky = SubElement(self.project, 'Sky')
        comment = Comment("Initialise planets and sky for the visualisation")
        sky.append(comment)
        sun = SubElement(sky, 'Sun')
        prop2d = SubElement(sun, "Prop2d")
        icon = SubElement(prop2d, "Icon", {'Anchor': "CENTER", 'Size': "MEDIUM", 'Opacity': "100"})
        font = [Element('Font', Color="1 1 1", Size="MEDIUM")]
        icon.extend(font)
        font = [Element('ImageLayer', Type="Default")]
        icon.extend(font)
        track = [Element('Track', Color="0.862745 0.862745 0", PenStyle="SolidLine", PenWidth="2")]
        sun.extend(track)
        visibility_circle = [
            Element('VisibilityCircle', ContourColor="0.501961 0.501961 0", FillColor="0 0 0", FillOpacity="50")]
        sun.extend(visibility_circle)
        star_catalog = SubElement(sky, "StarCatalog", {'CatalogMode': "Builtin"})
        track = [Element('Track', Color="0.862745 0.862745 0", PenStyle="DotLine", PenWidth="1")]
        star_catalog.extend(track)

    def to_be_used_app(self):
        """
        This method will write the lines to add all the correct
        applications (added with in the Tobeusedapp dictionary)
        the user wants to use
        """
        tobeusedapps = SubElement(self.project, "ToBeUsedApps")
        comment = Comment("Initialise all the application for the visualisation")
        tobeusedapps.append(comment)
        if self.Tobeusedapp["infobox"]:
            application = [Element('Application', Name="Infobox", Id="0", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("0")
        if self.Tobeusedapp["celestia"]:
            application = [Element('Application', Name="Celestia", Id="1", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("1")
        if self.Tobeusedapp["nadirview"]:
            application = [Element('Application', Name="NadirView", Id="2", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("2")
        if self.Tobeusedapp["prestoplot"]:
            application = [Element('Application', Name="PrestoPlot", Id="3", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("3")
        if self.Tobeusedapp["sensorview"]:
            application = [Element('Application', Name="SensorView", Id="4", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("4")
        if self.Tobeusedapp["skyview"]:
            application = [Element('Application', Name="SkyView", Id="5", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("5")
        if self.Tobeusedapp["surfaceview"]:
            application = [Element('Application', Name="SurfaceView", Id="6", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("6")
        if self.Tobeusedapp["zenithview"]:
            application = [Element('Application', Name="ZenithView", Id="7", Label="", AutoStarted="1")]
            tobeusedapps.extend(application)
            self.list_appused.append("7")
        number = len(self.list_appused)
        if number == 0:
            raise IndexError("You need to have at least one application in VTS: Celestia by default")

    def new_planet_vts(self, entities):
        """
        This method will write lines to add the planets given by the user
        with the add_planet method

        Parameters:
            entities (XML Element): The XML Element where the lines will
            be written
        """
        for plan in self.global_visualisation.list_planet:
            planet_name = plan["name"]
            if planet_name == "Moon":
                parent_path = "Sol/Earth"
            else:
                parent_path = "Sol"
            body = SubElement(entities, 'Body', {'Name': planet_name, 'ParentPath': parent_path})
            comment = Comment("Set all the planets for needed for the visualisation")
            body.append(comment)
            prop2d = SubElement(body, "Prop2d")
            icon = SubElement(prop2d, 'Icon', {'Anchor': "CENTER", 'Size': "MEDIUM", 'Opacity': "100"})
            font = [Element('Font', Size="MEDIUM", Color="1 1 1")]
            icon.extend(font)
            imagelayer = [Element('ImageLayer', Type="Default")]
            icon.extend(imagelayer)
            track = [Element('Track', Color="1 0 0.0156863", PenStyle="SolidLine", PenWidth="2")]  # A CHANGER
            body.extend(track)
            visibilitycircle = [Element('VisibilityCircle', ContourColor="1 0 0", FillColor="1 0 0", FillOpacity="100")]
            body.extend(visibilitycircle)
            ephemerismode = [Element('EphemerisMode', Mode="Default")]
            body.extend(ephemerismode)
            layers = SubElement(body, 'Layers')
            builtinlayer = [Element('BuiltinLayer', Name="defaultLayer")]
            layers.extend(builtinlayer)

    def new_sat_vts(self, sat_name, list_constraint, file3d, file_pos,
                    file_att, list_pos_sat, list_att_sat, file_time_sat,
                    parent_path, entities, category):
        """
        This method writes the lines to add a Satellite or a Point
        (treated as a Satellite in VTS)

        Parameters:
            sat_name (str1): Name of the satellite
            list_constraint (list of Dict): List of the constraints
            attached to the satellite
            file3d (str2): Name of the 3d file of the satellite
            file_pos (str3): Name of the trajectory file of the satellite
            file_att (str4): Name of the attitude file of the satellite
            list_pos_sat (list1): List of the positions of the satellite
            list_att_sat (list2): List of the attitudes of the satellite
            (can be None type)
            file_time_sat (List3): List of the time for every positions
            and attitudes
            parent_path (str): Path of the planet on which the satellite is attached to ("Sol/Earth")
            entities (XML Element): Element where to write the lines
            category (str): Either Point or Satellite

        Return:
            satellite (XML Element): XML Element where to put the
            constraints
            component (XML Element): Idem
        """
        if isinstance(file_pos, str):
            file = open(self.global_visualisation.data_path + "/" + file_pos, "r")
            arr = file.readlines()
            file.close()
            arr = arr[0].split(' ')
        elif isinstance(file_pos, list):
            arr = file_pos
        else:
            raise TypeError("The position file must be a List (List of position) or a String (Name of the file)")
        satellite = SubElement(entities, 'Satellite', {'Name': sat_name, 'ParentPath': parent_path})
        comment = Comment("Add the satellite for the visualisation with its trajectory and attitude file and 3D file")
        satellite.append(comment)
        commonprop = SubElement(satellite, "CommonProp")
        color = color_to_int("red")
        size = "2"
        for cons in list_constraint:
            if cons["category"] == "Trajectory":
                color = color_to_int(cons["color"])
                size = str(cons["size"])
                break
        orbitpath = [Element('OrbitPath', Color=color, PenStyle="SolidLine", PenWidth=size)]
        commonprop.extend(orbitpath)
        prop2d = SubElement(satellite, "Prop2d")
        icon = SubElement(prop2d, 'Icon', {'Anchor': "CENTER", 'Size': "MEDIUM", 'Opacity': "100"})
        font = [Element('Font', Size="MEDIUM", Color="1 1 1")]
        icon.extend(font)
        imagelayer = [Element('ImageLayer', Type="Default")]
        icon.extend(imagelayer)
        track = [Element('Track', Color=color, PenStyle="SolidLine", PenWidth=size)]
        satellite.extend(track)
        visibilitycircle = [Element('VisibilityCircle', ContourColor="1 0 0", FillColor="1 0 0", FillOpacity="100")]
        satellite.extend(visibilitycircle)
        self.component = [Element('Component', Name=sat_name)]
        satellite.extend(self.component)
        graphics3d = SubElement(self.component[0], "Graphics3d")
        file3ds = [Element('File3ds', Name=config.FILE3DPATH + "/" + file3d)]
        graphics3d.extend(file3ds)
        radius = [Element('Radius', Value="1")]
        graphics3d.extend(radius)
        lightsensitive = [Element('LightSensitive', Value="1")]
        graphics3d.extend(lightsensitive)
        use3dscoords = [Element('Use3dsCoords', Value="1", MeshScale="1")]
        graphics3d.extend(use3dscoords)
        axesposition = [Element('AxesPosition', Value="1")]
        graphics3d.extend(axesposition)
        rotationcenter = [Element('RotationCenter', X="0", Y="0", Z="0")]
        graphics3d.extend(rotationcenter)
        geometry = SubElement(self.component[0], "Geometry")
        position = SubElement(geometry, "Position")
        value = SubElement(position, "Value")
        if category == "satellite":
            file = [Element('File', Name=self.global_visualisation.data_path + "/" + self.writing_file("position",
                                                                                                       list_pos_sat,
                                                                                                       file_pos,
                                                                                                       file_time_sat,
                                                                                                       sat_name,
                                                                                                       parent_path))]
        elif category == "Point":
            file = [Element('Fixed', Data=str(arr[0]) + " " + str(arr[1]) + " " + str(arr[2]))]
        else:
            raise NameError("category must be either 'satellite' or 'Point'")
        value.extend(file)
        orientation = SubElement(geometry, 'Orientation')
        quaternion = SubElement(orientation, "Quaternion")
        value = SubElement(quaternion, "Value")
        if file_att is None:
            data = [Element('Fixed', Data="1 0 0 0")]
        elif isinstance(file_att, list) and len(file_att) == 4:  # for a constant attitude
            data = [Element('Fixed', Data=str(file_att[0])+" "+str(file_att[1])+" "+str(file_att[2]) +
                            " "+str(file_att[3]))]
        else:
            data = [Element('File', Name=self.global_visualisation.data_path + "/" + self.writing_file("attitude",
                                                                                                       list_att_sat,
                                                                                                       file_att,
                                                                                                       file_time_sat,
                                                                                                       sat_name,
                                                                                                       parent_path))]
        value.extend(data)
        return satellite

    def new_plume_or_sphere_vts(self, label, file3d, category, color, pos_rot, size=1):
        """
        This method write the lines to add either a plume or a sphere
        constraint

        Parameters:
            label (str1): Label of the constraint
            file3d (str2): Name of the 3d file of the constraint
            category (str3): Either Plume or Sphere
            color (str4): Color of the constraint (name of the color)
            pos_rot (list1): List of the positions and rotations of the
            constraint in the satellite coordinates
            size (int1): size of the Sphere or of the Plume
        """
        component_bis = SubElement(self.component[0], "Component", {'Name': label})
        comment = Comment("Add a constraint to the satellite")
        component_bis.append(comment)
        graphics3d = SubElement(component_bis, "Graphics3d")
        if category == "Sphere":
            file3ds = [Element('File3ds', Name=config.FILE3DPATH + "/" + color.lower() + file3d)]
        elif category == "Plume":
            file3ds = [Element('File3ds', Name=config.FILE3DPATH + "/" + file3d)]
        else:
            raise NameError("category must be either 'Sphere' or 'Plume'")
        graphics3d.extend(file3ds)
        radius = [Element('Radius', Value=str(2 * size))]
        graphics3d.extend(radius)
        lightsensitive = [Element('LightSensitive', Value="0")]
        graphics3d.extend(lightsensitive)
        if category == "Sphere":
            use3dscoords = [Element('Use3dsCoords', Value="0", MeshScale="1")]
        elif category == "Plume":
            use3dscoords = [Element('Use3dsCoords', Value="1", MeshScale="1")]
        else:
            raise NameError("The category must be either 'Sphere' or 'Plume'")
        graphics3d.extend(use3dscoords)
        axesposition = [Element('AxesPosition', Value="1")]
        graphics3d.extend(axesposition)
        rotationcenter = [Element('RotationCenter', X="0", Y="0", Z="0")]
        graphics3d.extend(rotationcenter)
        geometry = SubElement(component_bis, "Geometry")
        position = SubElement(geometry, 'Position')
        value = SubElement(position, 'Value')
        fixed = [Element('Fixed', Data=str(pos_rot[0]) + " " + str(pos_rot[1]) + " " + str(pos_rot[2]))]
        value.extend(fixed)
        orientation = SubElement(geometry, 'Orientation')
        eulerangle = SubElement(orientation, 'EulerAngle', {'RotationSequence': "123"})
        value = SubElement(eulerangle, 'Value')
        fixed = [Element('Fixed', Data=str(pos_rot[3]) + " " + str(pos_rot[4]) + " " + str(pos_rot[5]))]
        value.extend(fixed)

    def new_cone_vts(self, label, color, pos_rot):
        """
        This method write the lines to add a cone to the satellite

        Parameters:
            label (str1): Label of the constraint
            color (str2): Color of the constraint
            pos_rot (list1): List of the positions and rotations of the
            constraint in the satellite coordinates
        """
        sensorsatellite = SubElement(self.component[0], 'SensorSatellite')
        comment = Comment("Add cone constraint to the satellite with a sensor")
        sensorsatellite.append(comment)
        sensor = SubElement(sensorsatellite, 'Sensor', {'Name': label})
        sensorprop = SubElement(sensor, 'SensorProp')
        sensorelliptical = [Element('SensorElliptical', HalfAngleX="0.258698569856985", HalfAngleY="0.254258458584")]
        sensorprop.extend(sensorelliptical)
        sensorgraphics = SubElement(sensorprop, 'SensorGraphics',
                                    {'Range': "300", 'VolumeColor': color_to_int(color), 'VolumeOpacity': "35",
                                     'ContourColor': color_to_int(color)})  # A CHANGER
        sensortrace = SubElement(sensorgraphics, 'SensorTrace', {'Duration': "0", 'Opacity': "60"})  # A VOIR
        fixedcolor = [Element('FixedColor', Value="1 0 0")]  # A VOIR
        sensortrace.extend(fixedcolor)
        geometry = SubElement(sensor, 'Geometry')
        position = SubElement(geometry, 'Position')
        value = SubElement(position, 'Value')
        fixed = [Element('Fixed', Data=str(pos_rot[0]) + " " + str(pos_rot[1]) + " " + str(pos_rot[2]))]
        value.extend(fixed)
        orientation = SubElement(geometry, 'Orientation')
        eulerangle = SubElement(orientation, 'EulerAngle', {'RotationSequence': "123"})
        value = SubElement(eulerangle, 'Value')
        fixed = [Element('Fixed', Data=str(pos_rot[3]) + " " + str(pos_rot[4]) + " " + str(pos_rot[5]))]
        value.extend(fixed)

    def startvts(self):
        """
        This is the main method of the VTSVisu class. It will compile all
        the methods int the class to create a XML file with all the
        satellites, planet, constraints... given by the user.

        This method is called by thr Start() method in the GlobalVisualisation class
        """
        file = self.global_visualisation.config_file_name.split('.')
        if len(file) == 1:
            raise NameError("configuration files names must have a file type (.vts for VTS)")
        if len(file) > 2:
            raise NameError("A file must not have a point in its name")
        if self.initial_time == config.INITIALDATE:
            print("BEWARE: The initial time was not given. Initial time has been initialized with the value "
                  "(MJD format):", config.INITIALDATE)
        if len(self.global_visualisation.list_step) == 0:
            print("BEWARE: No steps were given. It means that every given constraint will be displayed and the"
                  " visualisation will last", self.global_visualisation.Screen["movie_duration"],
                  "seconds. You can change the movie_duration by changing Screen['movie_duration']")
        if len(self.global_visualisation.list_sat_visu) == 0:
            raise IndexError("You must at least have one satellite for the visualisation")
        if len(self.global_visualisation.list_planet) == 0:
            raise IndexError("You must put at least the celestial bodies that will be used during the visualisation")
        timelineoptions = self.initialisation_time_monitor()
        self.initialisation_sky_sun()
        self.to_be_used_app()
        if len(self.global_visualisation.list_planet) == 0:
            raise IndexError("You need to have at least the central body of your scene")
        entities = SubElement(self.project, "Entities")
        self.new_planet_vts(entities)
        for sat in self.global_visualisation.list_sat_visu:
            if sat["satellite"].trajectoryfile is not None and isinstance(sat["satellite"].trajectoryfile, str):
                self.list_file.append(sat["satellite"].trajectoryfile)
            if sat["satellite"].attitudefile is not None and isinstance(sat["satellite"].attitudefile, str):
                self.list_file.append(sat["satellite"].attitudefile)
            satellite = self.new_sat_vts(sat["satellite"].name,
                                         sat["list_cons"],
                                         sat["satellite"].file3dvts,
                                         sat["satellite"].trajectoryfile,
                                         sat["satellite"].attitudefile,
                                         sat["list_pos_sat"],
                                         sat["list_att_sat"],
                                         sat["list_time_sat"],
                                         sat["parent_path"],
                                         entities, "satellite")
            for cons in sat["list_cons"]:
                if cons["display"]:
                    if cons["category"] == "Point":
                        file3d_sphere = cons["color"] + "_sphere.obj"
                        sphere = self.new_sat_vts(cons["label"], [],
                                                  file3d_sphere,
                                                  cons["file_pos"], None,
                                                  [], [], [],
                                                  sat["parent_path"],
                                                  entities,
                                                  category=cons["category"])
                        events = [Element('Events')]
                        sphere.extend(events)
                    elif cons["category"] == "Sphere":
                        self.new_plume_or_sphere_vts(cons["label"],
                                                     cons["file3d"],
                                                     cons["category"],
                                                     cons["color"],
                                                     cons["pos_rot"],
                                                     size=cons["size"])
                    elif cons["category"] == "Plume":
                        self.new_plume_or_sphere_vts(cons["label"],
                                                     cons["file3d"],
                                                     cons["category"],
                                                     cons["color"],
                                                     cons["pos_rot"])
                    elif cons["category"] == "Cone":
                        self.new_cone_vts(cons["label"], cons["color"],
                                          cons["pos_rot"])
            events = [Element('Events')]
            satellite.extend(events)
        events = [Element('Events')]
        self.project.extend(events)
        pos = 0
        for file in self.list_file:
            file_to_add = open(self.global_visualisation.data_path + "/" + file, 'r')
            arr = file_to_add.readlines()
            file_to_add.close()
            pos += 1
            timelinefile = [
                Element('TimelineFile', Name=file, Pos=str(pos), Size=str(len(arr)), Badges="", Mode="DEFAULT",
                        Overlay="false")]
            timelineoptions.extend(timelinefile)
        state = SubElement(self.project, 'States')
        self.global_visualisation.add_step("initial step for time ratio", 0)
        for element in self.global_visualisation.list_step:
            self.new_step_vts(element, state)
        config_vts = open(self.global_visualisation.config_file_name, "w")
        config_vts.write(self.prettify())
