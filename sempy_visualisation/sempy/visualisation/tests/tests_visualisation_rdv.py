# -*- coding: utf-8 -*-
"""
Created on Thu May  7 15:51:54 2020

@author: Valentin Prudhomme
"""

# _________________________________IMPORTS_____________________________________

import os
import unittest
from xml.etree.ElementTree import SubElement, Element
from sempy.visualisation.global_visualisation import GlobalVisualisation
from sempy.visualisation import vts_visu as v_v
from sempy.visualisation import blender_visu as bv
from sempy.visualisation.blender_visu import BlenderVisu
from sempy.visualisation.vts_visu import VTSVisu
GV = GlobalVisualisation(software="Blender", render=False,
                         config_file_name="config_file", remove=False, data_path = 'Data')


def test_texte(fcor, ftest):
    filecor = open(fcor, 'r')
    filetest = open(ftest, 'r')
    arrcorr = filecor.readlines()
    arrtest = filetest.readlines()
    for i, line in enumerate(arrcorr):
        if line != arrtest[i]:
            print("The error comes from this line :", line)
            filecor.close()
            filetest.close()
            return False
    filecor.close()
    filetest.close()
    return True


class TestVisualisationRdv(unittest.TestCase):

    # ______________________TESTS IN GLOBAL VISUALISATION__________________________

    def test_add_remove_sat(self):
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        self.assertEqual(len(GV.list_sat_visu), 1)
        GV.remove_satellite(name="Orion")
        self.assertEqual(len(GV.list_sat_visu), 0)

    def test_add_remove_constraint(self):
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        self.assertEqual(len(GV.list_sat_visu[0]["list_cons"]), 1)
        GV.remove_constraint("OrionPlume")
        self.assertEqual(len(GV.list_sat_visu[0]["list_cons"]), 0)
        GV.remove_satellite("Orion")

    def test_add_planet(self):
        GV.add_planet(name="Earth", file3d="Earth.blend", position_file=[0, 0, 0])
        self.assertEqual(len(GV.list_planet), 1)
        GV.list_planet = []

    def test_add_step(self):
        GV.add_step("step test", 0, 1, "XY", None)
        self.assertEqual(len(GV.list_step), 1)
        GV.list_step = []

    # _____________________TESTS IN BLENDER VISUALISATION__________________________

    def test_dist_points(self):
        self.assertEqual(bv.dist_points([0, 0, 0], [0, 0, 0]), 0)  # for the zero case
        self.assertEqual(bv.dist_points([1, 1, 0], [13, 1, 0]), 12)  # for the positive case
        self.assertEqual(bv.dist_points([-1, -1, 0], [-13, -1, 0]), 12)  # for the negative case

    def test_init_center(self):
        GV.software = "Blender"
        b_v = BlenderVisu(GV, center="Orion")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        test = b_v.init_center()
        self.assertEqual(test, GV.list_sat_visu[0]["list_pos_sat"])
        GV.remove_satellite("Orion")

    def test_new_step_blender(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_new_step_blender.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        GV.add_step("step test", 0, 1, "XY", ["OrionPlume"])
        b_v.new_step_blender(GV.list_step[0])
        GV.remove_satellite("Orion")
        GV.list_step = []
        self.assertTrue(test_texte("test_files/test_new_step_blender.txt", "test_files/correct_new_step_blender.txt"))

    def test_import_object(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_import_object.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        GV.add_planet(name="Earth", file3d="Earth.blend", position_file=[0, 0, 0])
        b_v.import_object(GV.list_planet[0]["file3d"])
        lines = []
        file = open('test_files/correct_import_object.txt', 'r')
        for l1 in file.readlines():
            lsplit = l1.split(" ")
            if lsplit[0] != "blendfile":
                lines.append(l1)
            else:
                projectpath = os.path.abspath("")
                projectpath = projectpath.replace("\\", "/")
                lines.append("blendfile = '" + projectpath + "/models/Earth.blend'  # satellite or planets)\n")
        file.close()
        file = open('test_files/correct_import_object.txt', 'w')
        for l2 in lines:
            if l2 != "\n":
                file.write(l2)
        file.close()
        GV.list_planet = []
        self.assertTrue(test_texte("test_files/test_import_object.txt", "test_files/correct_import_object.txt"))

    def test_trajectory_color(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_trajectory_color.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Trajectory", label="OrionTrajectory",
                          display=True, color="red")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.trajectory_color(GV.list_sat_visu[0]["list_cons"][0]["color"],
                             GV.list_sat_visu[0]["satellite"].name)
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_trajectory_color.txt", "test_files/correct_trajectory_color.txt"))

    def test_constraint_color(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_constraint_color.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Sphere", label="OrionSphere", display=True,
                          color="red", pos_rot=[0, 0, -3.7, 0, 180, 0])
        b_v = BlenderVisu(GV, center="Orion")
        b_v.constraints_color(GV.list_sat_visu[0]["list_cons"][0]["category"],
                              GV.list_sat_visu[0]["list_cons"][0]["label"],
                              GV.list_sat_visu[0]["list_cons"][0]["color"])
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_constraint_color.txt", "test_files/correct_constraint_color.txt"))

    def test_rename_object(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_rename_object.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        GV.add_planet("Earth", "Earth.blend", [0, 0, 0])
        b_v.rename_object(GV.list_planet[0]["name"], "Earth2.0")
        self.assertTrue(test_texte("test_files/test_rename_object.txt", "test_files/correct_rename_object.txt"))

    def test_resize_object(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_resize_object.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        GV.add_planet("Earth", "Earth.blend", [0, 0, 0])
        b_v.resize_object(GV.list_planet[0]["name"])
        self.assertTrue(test_texte("test_files/test_resize_object.txt", "test_files/correct_resize_object.txt"))

    def test_rescale_objects(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_rescale_objects.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_satellite(name="Gateway", file3dblender="Gateway.blend", file3dvts="Gateway.obj",
                         trajectoryfile="Gateway_Position_RDV_trop_long.txt",
                         time_file="time_Orion_Gateway.txt")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.rescale_objects(GV.list_sat_visu[0]["satellite"].name, "satellite", GV.list_sat_visu[0]["list_time_sat"])
        GV.remove_satellite("Orion")
        GV.remove_satellite("Gateway")
        self.assertTrue(test_texte("test_files/test_rescale_objects.txt",
                                   "test_files/correct_rescale_objects.txt"))

    def test_move_constraint_to(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_move_constraint_to.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        b_v = BlenderVisu(GV, center="Orion")
        b_v.move_constraint_to(GV.list_sat_visu[0]["list_cons"][0]["category"],
                               GV.list_sat_visu[0]["list_cons"][0]["pos_rot"],
                               GV.list_sat_visu[0]["satellite"].ratio)
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_move_constraint_to.txt",
                                   "test_files/correct_move_constraint_to.txt"))

    def test_parent_obj_sphere(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_parent_obj_to_sphere.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Sphere", label="OrionSphere", display=True)
        b_v = BlenderVisu(GV, center="Orion")
        b_v.parent_obj(GV.list_sat_visu[0]["list_cons"][0]["category"],
                       GV.list_sat_visu[0]["satellite"].name, GV.list_sat_visu[0]["list_cons"][0]["category"])
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_parent_obj_to_sphere.txt",
                                   "test_files/correct_parent_obj_to_sphere.txt"))

    def test_parent_obj_not_sphere(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_parent_obj_to.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        b_v = BlenderVisu(GV, center="Orion")
        b_v.parent_obj(GV.list_sat_visu[0]["list_cons"][0]["category"],
                       GV.list_sat_visu[0]["satellite"].name, GV.list_sat_visu[0]["list_cons"][0]["category"])
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_parent_obj_to.txt", "test_files/correct_parent_obj_to.txt"))

    def test_averaging_position(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_averaging_position.txt"
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.screen["global_ratio"] = 1/1000
        b_v.averaging_position()
        self.assertEqual(b_v.screen["global_ratio"], 1/1000)
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v.screen["global_ratio"] = None
        b_v.averaging_position()
        GV.remove_satellite("Orion")
        b_v.screen["global_ratio"] = 1 / 1000
        self.assertTrue(test_texte("test_files/test_averaging_position.txt",
                                   "test_files/correct_averaging_position.txt"))

    def test_get_point_to_location(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_get_point_to_location.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion", category="Point",
                          label="OrionPoint", display=True,
                          file_pos=[379455.6727, 1944.584128, 7540.378581], color="pink")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.get_point_to_location(GV.list_sat_visu[0]["list_cons"][0]["category"],
                                  GV.list_sat_visu[0]["list_cons"][0]["file_pos"], GV.list_sat_visu[0]["list_time_sat"])
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_get_point_to_location.txt",
                                   "test_files/correct_get_point_to_location.txt"))

    def test_draw_trajectory(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_draw_trajectory.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.draw_trajectory(GV.list_sat_visu[0]["satellite"].name,
                            GV.list_sat_visu[0]["list_pos_sat"], 1, 1, 0, True)
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_draw_trajectory.txt", "test_files/correct_draw_trajectory.txt"))

    def test_set_keyframes(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_set_keyframes.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        b_v = BlenderVisu(GV, center="Orion")
        b_v.set_keyframes(GV.list_sat_visu[0]["satellite"].name,
                          GV.list_sat_visu[0]["list_pos_sat"],
                          GV.list_sat_visu[0]["list_att_sat"], GV.list_sat_visu[0]["list_time_sat"])
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_set_keyframes.txt", "test_files/correct_set_keyframes.txt"))

    def test_initialisation(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_initialisation.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        b_v.initialisation()
        self.assertTrue(test_texte("test_files/test_initialisation.txt", "test_files/correct_initialisation.txt"))

    def test_init_camera(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_init_camera.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.Camera["centered_object"] = "Orion"
        b_v = BlenderVisu(GV, center="Orion")
        dist_ref = 1
        imax = 0
        b_v.init_camera(dist_ref, imax)
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_init_camera.txt", "test_files/correct_init_camera.txt"))

    def test_init_screen(self):
        GV.software = "Blender"
        GV.config_file_name = "test_files/test_init_screen.txt"
        file = open(GV.config_file_name, "w")
        file.write("")
        file.close()
        b_v = BlenderVisu(GV, center="Orion")
        b_v.init_screen()
        lines = []
        file = open('test_files/correct_init_screen.txt', 'r')
        for l1 in file.readlines():
            lsplit = l1.split(" ")
            if lsplit[0] != "bpy.context.scene.render.filepath":
                lines.append(l1)
            else:
                projectpath = os.path.abspath("")
                projectpath = projectpath.replace("\\", "/")
                lines.append("bpy.context.scene.render.filepath = '" + projectpath + "/movies/MovieRDV'\n")
        file.close()
        file = open('test_files/correct_init_screen.txt', 'w')
        for l2 in lines:
            if l2 != "\n":
                file.write(l2)
        file.close()
        self.assertTrue(test_texte("test_files/test_init_screen.txt", "test_files/correct_init_screen.txt"))

    def test_start_blender(self):
        pass
    # _______________________TESTS IN VTS VISUALISATION____________________________

    def test_new_step_vts(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        GV.add_step("step test", 0, 1, "XY", ["OrionPlume"])
        visu.to_be_used_app()
        state = SubElement(visu.project, 'States')
        visu.new_step_vts(GV.list_step[0], state)
        file = open("test_files/test_new_step_vts.txt", "w")
        file.write(visu.prettify())
        file.close()
        GV.remove_satellite("Orion")
        GV.list_step = []
        self.assertTrue(test_texte("test_files/test_new_step_vts.txt", "test_files/correct_new_step_vts.txt"))

    def test_prettify(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        sky = SubElement(visu.project, 'Sky')
        sun = SubElement(sky, 'Sun')
        prop2d = SubElement(sun, "Prop2d")
        icon = SubElement(prop2d, "Icon", {'Anchor': "CENTER", 'Size': "MEDIUM", 'Opacity': "100"})
        font = [Element('Font', Color="1 1 1", Size="MEDIUM")]
        icon.extend(font)
        file = open("test_files/test_prettify.txt", "w")
        file.write(visu.prettify())
        file.close()
        self.assertTrue(test_texte("test_files/test_prettify.txt", "test_files/correct_prettify.txt"))

    def test_writing_file(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        visu.writing_file("position", GV.list_sat_visu[0]["list_pos_sat"],
                          "Orion_Position_RDV_trop_long.txt",
                          GV.list_sat_visu[0]["list_time_sat"],
                          GV.list_sat_visu[0]["satellite"].name, "Orion")
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("Data/Orion_Position_RDV_trop_long_vts.txt",
                                   "test_files/correct_Orion_Position_vts.txt"))

    def test_initialisation_time_monitor(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        visu.initialisation_time_monitor()
        file = open("test_files/test_initialisation_time_monitor.txt", "w")
        file.write(visu.prettify())
        file.close()
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_initialisation_time_monitor.txt",
                                   "test_files/correct_initialisation_time_monitor.txt"))

    def test_initialisation_sky_sun(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        visu.initialisation_sky_sun()
        file = open("test_files/test_initialisation_sky_sun.txt", "w")
        file.write(visu.prettify())
        file.close()
        self.assertTrue(test_texte("test_files/test_initialisation_sky_sun.txt",
                                   "test_files/correct_initialisation_sky_sun.txt"))

    def test_to_be_used_app(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        visu.to_be_used_app()
        file = open("test_files/test_to_be_used_app.txt", "w")
        file.write(visu.prettify())
        file.close()
        self.assertTrue(test_texte("test_files/test_to_be_used_app.txt", "test_files/correct_to_be_used_app.txt"))
        self.assertTrue(len(visu.list_appused), 1)

    def test_new_planet_vts(self):
        GV.software = "VTS"
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_planet("Earth")
        entities = SubElement(visu.project, "Entities")
        visu.new_planet_vts(entities)
        file = open("test_files/test_new_planet_vts.txt", "w")
        file.write(visu.prettify())
        file.close()
        self.assertTrue(test_texte("test_files/test_new_planet_vts.txt", "test_files/correct_new_planet_vts.txt"))
        GV.list_planet = []

    def test_color_to_int(self):
        self.assertTrue(v_v.color_to_int("red"), "1 0 0")
        self.assertTrue(v_v.color_to_int("orange"), "1 0.330000 0")
        self.assertTrue(v_v.color_to_int("yellow"), "1 1 0")
        self.assertTrue(v_v.color_to_int("green"), "0 1 0")
        self.assertTrue(v_v.color_to_int("blue"), "0 1 1")
        self.assertTrue(v_v.color_to_int("sky blue"), "0 0 1")
        self.assertTrue(v_v.color_to_int("purple"), "0.330000 0 1")
        self.assertTrue(v_v.color_to_int("pink"), "1 0 0.800000")

    def test_new_sat_vts(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        entities = SubElement(visu.project, "Entities")
        visu.new_sat_vts(GV.list_sat_visu[0]["satellite"].name,
                         GV.list_sat_visu[0]["list_cons"],
                         GV.list_sat_visu[0]["satellite"].file3dvts,
                         GV.list_sat_visu[0]["satellite"].trajectoryfile,
                         GV.list_sat_visu[0]["satellite"].attitudefile,
                         GV.list_sat_visu[0]["list_pos_sat"],
                         GV.list_sat_visu[0]["list_att_sat"],
                         GV.list_sat_visu[0]["list_time_sat"],
                         GV.list_sat_visu[0]["parent_path"],
                         entities, "satellite")
        file = open("test_files/test_new_sat_vts.txt", "w")
        file.write(visu.prettify())
        file.close()
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_new_sat_vts.txt", "test_files/correct_new_sat_vts.txt"))

    def test_new_plume_or_sphere_vts(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion",
                          category="Plume", label="OrionPlume", display=True,
                          pos_rot=[0, 0, -3.7, 0, 180, 0])
        entities = SubElement(visu.project, "Entities")
        visu.new_sat_vts(GV.list_sat_visu[0]["satellite"].name,
                         GV.list_sat_visu[0]["list_cons"],
                         GV.list_sat_visu[0]["satellite"].file3dvts,
                         GV.list_sat_visu[0]["satellite"].trajectoryfile,
                         GV.list_sat_visu[0]["satellite"].attitudefile,
                         GV.list_sat_visu[0]["list_pos_sat"],
                         GV.list_sat_visu[0]["list_att_sat"],
                         GV.list_sat_visu[0]["list_time_sat"],
                         GV.list_sat_visu[0]["parent_path"],
                         entities, "satellite")
        visu.new_plume_or_sphere_vts(GV.list_sat_visu[0]["list_cons"][0]["label"],
                                     GV.list_sat_visu[0]["list_cons"][0]["file3d"],
                                     GV.list_sat_visu[0]["list_cons"][0]["category"],
                                     GV.list_sat_visu[0]["list_cons"][0]["color"],
                                     GV.list_sat_visu[0]["list_cons"][0]["pos_rot"],
                                     size=GV.list_sat_visu[0]["list_cons"][0]["size"])
        file = open("test_files/test_new_plume_or_sphere_vts.txt", "w")
        file.write(visu.prettify())
        file.close()
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_new_plume_or_sphere_vts.txt",
                                   "test_files/correct_new_plume_or_sphere_vts.txt"))

    def test_new_cone_vts(self):
        GV.software = "VTS"
        visu = VTSVisu(GV, "55276 0.000000")
        GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                         trajectoryfile="Orion_Position_RDV_trop_long.txt",
                         attitudefile="Orion_attitude.txt",
                         time_file="time_Orion_Gateway.txt")
        GV.add_constraint(satellite_name="Orion", category="Cone",
                          label="OrionCone", display=True,
                          color="green", pos_rot=[0, 0, 3, 0, 0, 0])
        entities = SubElement(visu.project, "Entities")
        visu.new_sat_vts(GV.list_sat_visu[0]["satellite"].name,
                         GV.list_sat_visu[0]["list_cons"],
                         GV.list_sat_visu[0]["satellite"].file3dvts,
                         GV.list_sat_visu[0]["satellite"].trajectoryfile,
                         GV.list_sat_visu[0]["satellite"].attitudefile,
                         GV.list_sat_visu[0]["list_pos_sat"],
                         GV.list_sat_visu[0]["list_att_sat"],
                         GV.list_sat_visu[0]["list_time_sat"],
                         GV.list_sat_visu[0]["parent_path"], entities,
                         "satellite")
        visu.new_cone_vts(GV.list_sat_visu[0]["list_cons"][0]["label"],
                          GV.list_sat_visu[0]["list_cons"][0]["color"],
                          GV.list_sat_visu[0]["list_cons"][0]["pos_rot"])
        file = open("test_files/test_new_cone_vts.txt", "w")
        file.write(visu.prettify())
        file.close()
        GV.remove_satellite("Orion")
        self.assertTrue(test_texte("test_files/test_new_cone_vts.txt", "test_files/correct_new_cone_vts.txt"))

    def test_startvts(self):
        pass

# ________________________________UNITTEST_____________________________________


if __name__ == '__main__':
    unittest.main()
