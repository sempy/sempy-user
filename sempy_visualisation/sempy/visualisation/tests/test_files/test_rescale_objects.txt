bpy.context.scene.frame_set(0)  # Calculating the ratio to rescale objects to see 
bpy.ops.object.select_all(action='DESELECT')  # them all in a camera
Orion = bpy.data.objects['Orion']
Orion.select_set(True)
bpy.ops.transform.resize(value=(30.0, 30.0, 30.0))
Orion.keyframe_insert(data_path="scale", index =-1)
bpy.context.scene.frame_set(7822.0)
bpy.ops.transform.resize(value=(0.03333333333333333, 0.03333333333333333, 0.03333333333333333))
Orion.keyframe_insert(data_path="scale", index =-1)

