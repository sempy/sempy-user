import bpy # imports
from bpy import context, data, ops

for a in bpy.context.screen.areas: # Initialisation for some parameters of the camera and the light
    if a.type == 'VIEW_3D':
        for s in a.spaces:
            if s.type == 'VIEW_3D':
                s.clip_start = 0.0001
                s.clip_end = 1000000
ob = bpy.data.objects['Camera']
ob.data.clip_start = 0.0001
ob.data.clip_end = 1000000
bpy.ops.object.delete(use_global=False)
bpy.ops.object.select_all(action='DESELECT')
lamp1 = bpy.data.objects['Light']
lamp1.select_set(True)
bpy.context.view_layer.objects.active = lamp1
bpy.context.object.data.type = 'SUN'
bpy.context.object.data.energy = 0.15
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-90*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-0*3.14/180-3.14, orient_axis='Y')
bpy.ops.transform.rotate(value=-30*3.14/180-3.14, orient_axis='Z')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.light_add(type='SUN', radius=1, location=(0, 0, 0))
lamp2 = bpy.data.objects['Sun']
lamp2.select_set(True)
bpy.context.view_layer.objects.active = lamp2
bpy.context.object.data.energy = 7
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-90*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-0*3.14/180, orient_axis='Y')
bpy.ops.transform.rotate(value=-30*3.14/180, orient_axis='Z')
bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0, 0, 0, 1)
#bpy.context.scene.render.engine = 'CYCLES'
#bpy.context.scene.cycles.device = 'GPU'
bpy.context.scene.cycles.max_bounces = 1
bpy.context.scene.cycles.diffuse_bounces = 1
bpy.context.scene.cycles.glossy_bounces = 1
bpy.context.scene.cycles.transparent_max_bounces = 6
bpy.context.scene.cycles.transmission_bounces = 1
bpy.context.scene.render.tile_x = 512
bpy.context.scene.render.tile_y = 512

