bpy.context.scene.render.resolution_x = 1920 #initialisation of the screen
bpy.context.scene.render.resolution_y = 1080
#bpy.context.scene.cycles.samples = 64
bpy.context.scene.eevee.taa_render_samples = 64
bpy.context.scene.render.fps = 222.0
bpy.context.scene.eevee.use_bloom = True
my_areas = bpy.context.workspace.screens[0].areas
my_shading = 'RENDERED'  # 'WIREFRAME' 'SOLID' 'MATERIAL' 'RENDERED'
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.shading.type = my_shading
for area in my_areas:
    if area.type == 'VIEW_3D':
        area.spaces[0].region_3d.view_perspective = 'CAMERA'
        break
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.overlay.show_overlays = False
bpy.context.scene.frame_current = 0
bpy.context.scene.render.filepath = 'C:/Users/Valentin Prudhomme/Desktop/Dossier_Git/sempy/src/visualisation/tests/movies/MovieRDV'
bpy.context.scene.render.image_settings.file_format = 'AVI_JPEG'
bpy.ops.object.select_all(action='DESELECT')
