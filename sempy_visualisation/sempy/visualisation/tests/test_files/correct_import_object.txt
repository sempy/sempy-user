print("importing Earth.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = 'C:/Users/Valentin Prudhomme/Desktop/Dossier_Git/sempy/src/visualisation/tests/models/Earth.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Earth'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Earth']
ob.select_set(True)
ob.active_material.use_backface_culling = True
