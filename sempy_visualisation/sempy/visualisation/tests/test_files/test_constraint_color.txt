for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Sphere':
        material.name = 'OrionSphere'
bpy.data.materials['OrionSphere'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)

