<?xml version="1.0" ?>
<Project Revision="8200">
 <Entities>
  <Satellite Name="Orion" ParentPath="Sol/Earth">
   <!--Add the satellite for the visualisation with its trajectory and attitude file and 3D file-->
   <CommonProp>
    <OrbitPath Color="1 0 0" PenStyle="SolidLine" PenWidth="2"/>
   </CommonProp>
   <Prop2d>
    <Icon Anchor="CENTER" Size="MEDIUM" Opacity="100">
     <Font Size="MEDIUM" Color="1 1 1"/>
     <ImageLayer Type="Default"/>
    </Icon>
   </Prop2d>
   <Track Color="1 0 0" PenStyle="SolidLine" PenWidth="2"/>
   <VisibilityCircle ContourColor="1 0 0" FillColor="1 0 0" FillOpacity="100"/>
   <Component Name="Orion">
    <Graphics3d>
     <File3ds Name="models/Orion.obj"/>
     <Radius Value="1"/>
     <LightSensitive Value="1"/>
     <Use3dsCoords Value="1" MeshScale="1"/>
     <AxesPosition Value="1"/>
     <RotationCenter X="0" Y="0" Z="0"/>
    </Graphics3d>
    <Geometry>
     <Position>
      <Value>
       <File Name="Data/Orion_Position_RDV_trop_long_vts.txt"/>
      </Value>
     </Position>
     <Orientation>
      <Quaternion>
       <Value>
        <File Name="Data/Orion_attitude_vts.txt"/>
       </Value>
      </Quaternion>
     </Orientation>
    </Geometry>
    <Component Name="OrionPlume">
     <!--Add a constraint to the satellite-->
     <Graphics3d>
      <File3ds Name="models/plume.obj"/>
      <Radius Value="2"/>
      <LightSensitive Value="0"/>
      <Use3dsCoords Value="1" MeshScale="1"/>
      <AxesPosition Value="1"/>
      <RotationCenter X="0" Y="0" Z="0"/>
     </Graphics3d>
     <Geometry>
      <Position>
       <Value>
        <Fixed Data="0 0 -3.7"/>
       </Value>
      </Position>
      <Orientation>
       <EulerAngle RotationSequence="123">
        <Value>
         <Fixed Data="0 180 0"/>
        </Value>
       </EulerAngle>
      </Orientation>
     </Geometry>
    </Component>
   </Component>
  </Satellite>
 </Entities>
</Project>
