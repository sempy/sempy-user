bpy.ops.object.select_all(action='DESELECT') # Initialisation of the camera
sat = bpy.data.objects['Orion']
sat.select_set(True)
cam = bpy.data.objects['Camera']
cam.select_set(True)
bpy.context.view_layer.objects.active = Orion
bpy.ops.object.location_clear(clear_delta=False)
bpy.ops.object.rotation_clear(clear_delta=False)
cam.location.x = sat.location.x 
cam.location.y = sat.location.y 
cam.location.z = sat.location.z
bpy.context.scene.frame_set(0)
if 'XY' == 'YZ':
    cam.location.x += 125*RATIO*1
elif 'XY' == 'XZ':
    cam.location.y += 125*RATIO*1
elif 'XY' == 'XY':
    cam.location.z += 125*RATIO*1
    print(RATIO)
elif 'XY' == 'far':
    cam.location.x += 10000*RATIO*10000
cam.keyframe_insert(data_path="location", index =-1)
bpy.context.scene.frame_set(13335.0)
if 'XY' == 'YZ':
    cam.location.x = sat.location.x 
    cam.location.x += 125*RATIO*1
elif 'XY' == 'XZ':
    cam.location.y = sat.location.y 
    cam.location.y += 125*RATIO*1
elif 'XY' == 'XY':
    cam.location.z = sat.location.z
    cam.location.z += 125*RATIO*1
elif 'XY' == 'far':
    cam.location.x += 10000*RATIO*10000
cam.location.x -= sat.location.x 
cam.location.y -= sat.location.y 
cam.location.z -= sat.location.z
cam.keyframe_insert(data_path="location", index =-1)
cons1 = cam.constraints.new(type='COPY_LOCATION')
cons1.target = sat
cam.constraints["Copy Location"].use_offset = True
cons2 = cam.constraints.new(type='TRACK_TO')
cons2.target = sat
cons2.track_axis = 'TRACK_NEGATIVE_Z'
cons2.up_axis = 'UP_Y'
if 'XY' == 'XY':
    cons2.track_axis = 'TRACK_Y'

