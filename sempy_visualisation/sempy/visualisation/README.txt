Installation guide:

I - Configuration and installation:

1) Install Blender 2.83 on: https://www.blender.org/download/ (Linux or Windows)

2) Install VTS 3.4.2 on: https://timeloop.fr/vts/ (Linux ou Windows)

3) Go to the folder sempy/src/visualisation :
	>>> cd sempy/src/visualisation
	Open the configuration file <default_config.py> and modify BLENDERPATH and VTSPATH in the general part with the path of the Blender and VTS executable.
	This file also constains all the default values of the paraeters for the visualisation.
	
4) Go to the tests folder:
	       >>> cd tests
   Run all the tests of the tool with (in the tests forlder):
	       >>> python -m unittest tests_visualisation_rdv.py

----------------------------------------------------------------------------------
II - a) Running the visualisation:
Come back to the visualisation folder (>>> cd..)

4) Change the data_path in the GlobalVisualisation parameters for the absolute path to the data (trajectory, attitude and time file)

5) Choose your software to visualise the scene and put all the details of the scene (following examples given in Sempy)

5) Run the file : >>> python visualisation_code.py (Le logiciel devrait alors s'ouvrir automatiquement ou alors lancer directement le rendu en fonction de la demande)
