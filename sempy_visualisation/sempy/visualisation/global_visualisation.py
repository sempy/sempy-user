# -*- coding: utf-8 -*-
# pylint: disable=line-too-long
"""
Created on Wed Apr 22 10:00:09 2020

@author: Valentin Prudhomme
"""

# _________________________________IMPORTS_____________________________________

import os
import subprocess
import sempy.visualisation.default_config as config
from sempy.visualisation.satellite import Satellite
from sempy.visualisation.blender_visu import BlenderVisu
from sempy.visualisation.vts_visu import VTSVisu

# __________________________MAIN VISUALISATION CLASS___________________________


class GlobalVisualisation:
    """ Warning: Before any visualisation make sure that:
        - You put the good absolute path to you data files in the data_path parameter in GlobalVisualisation class
        - The 3D files are in the <models> folder and must be .blend for Blender and .obj for VTS
        - All your time, position, and attitudes (if used) are in the folder that you just specified
        in the data_path parameter of the global visualisation
        - All the time files used during the visualisation has a constant step of time and the same
        number of points.
        - You have same times in the files of every satellite to know where each satellites are at
        the same time
        - All the files of one satellite (attitude, position and time) must have the same number of lines
        - If you add multiple times the same 3D file for satellites, put numbers to differenciate satellites and don't
        call the satellites by there normal names

    The GlobalVisualisation class contains all the methods used to plot the
    visualisation and to add the objects and planets. It will also allow to
    modify some camera and screen parameters.

    Parameters:
        software (str1): Either "Blender" or "VTS"
        render (Bool): If True, the config file of Blender will write one more line to render the visualisation. If False, it will just open the software to have a previsualisation.
        list_sat_visu (list of Satellite_Visu objects): each object in the list contains a satellite and a list of its constraints
        config_file_name (str2): Name of the configuration file
        list_planet (list of Planet): List of the planets in the scene
        remove (bool): If True,, it will remove the configuration file that has just been created
        data_path (str3): Absolute path to the data folder you are using (trajectory file, attitude file and time file)

        Screen (dict1): Dictionary that allow to modify some screen parameters:
            resolution (str): ONLY FOR BLENDER. resolution of the screen (1920*1080 by default)
            fps (int): ONLY FOR BLENDER. Number of FPS of the movie or the pre_visualisation (30 by default)
            frame_step (int): Number of frame automatically calculated to ignore during the rendering to have the good
            time for the movie (1 by default)
            samples (int): ONLY FOR BLENDER. Number of samples to render the movie (the more there are the higher the
            quality will be) (64 by default)
            global_ratio (int): ONLY FOR BLENDER. Global ratio for the entire scene (all the objects will be reduce by
            this ratio). Useful because some objects can be blurry if the scene is too big. If global_ratio="None",
            the ratio will be calculated automatically (None by default)
            box_size (int): ONLY FOR BLENDER. Size of the box for the scene to be in this box. It has an impact on the
            calculus of the global_ratio parameter (2000 meters by default)
            movie_duration (int): Duration of the movie, in seconds, to be rendered (60 seconds by default)
            movie_name (int): Name of the movie after it has been saved ("MovieRDV" by default)
            light_orientation (int): ONLY FOR BLENDER. attitude, in degrees, of the light in the scene
            (90, 0, 30 by default)
            initially_paused (bool): If False, the preview (on Blender or VTS) will automatically start. If True,
            the user will have to start it himself (False by default)
            minimize_broker (bool): ONLY FOR VTS. This allow to have the broker (with the time scale, and some options)
            minimized during the visualisation (False by default)
        Camera (dict2): Dictionary that allows to modify some Camera parameters for the visualisation:
            plan (str): ONLY FOR BLENDER. Plan of the scene. Either "XY", "XZ" or "YZ for each close view of the
            satellite or "far" for a view of all the trajectory. Blender and VTS allow to choose the view with the pad.
            centered_object (str): Name of the object in the center of the view (name of one of the object put in the
            scene by the user) (Name of the first satellite added by default)
            close_view (int): ONLY FOR BLENDER. Distance of the camera from the centered object for the close view
            (125 meters by default)
            distant_view (int): ONLY FOR BLENDER. Added distance of the camera from the centered object for the far
            view. The initial distance is automatically calculated to see all the trajectories of the scene in the
            camera. The distant_view parameter will allow to multiply this number by an other one to have a closer or
            further view (1 by default)
            see_inside_constraint (bool): ONLY FOT BLENDER. If True, when inside a constraint (sphere, cone...), The
            constraint disappear until you don't go out of it (True by default)
            not_to_scale (bool): ONLY FOR BLENDER. If True, the satellites of the scene will not be to scale to see them
            all in the camera. The satellites are resize until they arrive to 1 kilometer where the get there initial
            size (False by default)
            scale_ratio (int): ONLY FOR BLENDER. Scale ratio if not_to_scale=True. It will multiply the ratio
            automatically calculated by the scale_ratio to see the satellites bigger or smaller (1 by default)
            time_step (int): ONLY FOR BLENDER. Minimal time step between times in time files (1 second by default)
    """
    def __init__(self, software=config.SOFTWARE, render=config.RENDER, list_sat_visu=None,
                 list_planet=None, list_step=None, config_file_name=config.CONFIGFILE,
                 remove=config.REMOVE, data_path=config.DATAPATH, time_step=config.TIMESTEP):
        self.software = software
        self.render = render
        self.list_sat_visu = list_sat_visu or []
        self.config_file_name = config_file_name
        self.list_planet = list_planet or []
        self.list_step = list_step or []
        self.blendervisu = BlenderVisu(self)
        self.vtsvisu = VTSVisu(self)
        self.remove = remove
        self.data_path = data_path
        self.time_step = time_step

    Screen = {"resolution": config.RESOLUTION, "fps": config.FPS, "frame_step": config.FRAMESTEP,
              "samples": config.SAMPLES, "global_ratio": config.RATIO, "box_size": config.BOXSIZE,
              "movie_duration": config.MOVIEDURATION, "movie_name": config.MOVIENAME,
              "light_orientation": config.LIGHTORIENTATION, "initially_paused": config.PAUSED,
              "minimize_broker": config.MINIMIZE}
    Camera = {"plan": config.PLAN, "centered_object": config.CENTEREDOBJECT,
              "close_view": config.DISTANCECLOSE, "distant_view": config.DISTANCEFAR,
              "see_inside_constraint": config.SEEINSIDE, "not_to_scale": config.NOTTOSCALE,
              "scale_ratio": config.SCALERATIO}

    def add_satellite(self, name, file3dblender, file3dvts, trajectoryfile,
                      attitudefile=config.ATTITUDEFILE, weight=config.WEIGHT, time_file=None,
                      ratio=config.SATELLITERATIO):
        """ This method will be used by the user to add a satellite. It will add a satellite to LIST_SAT_VISU with all
        the given arguments.

        Parameters:
            name (str1): Name given to the satellite
            file3dblender (str2): Name of the Blender 3D file of the satellite
            file3dvts (str3): Name of the VTS 3D file of the satellite
            trajectoryfile (str4 or list): Name of the trajectory file of the satellite
            attitudefile (str5 or list): Name of the attitude file of the satellite (quaternions)
            weight (int1): Weight of the satellite (equal to zero if not given)
            time_file (str6 or list): list of the time step between points of both position and attitude
            ratio (int1): ratio of the satellite for the visualisation

            A satellite is a dictionary with the following parameters:
                satellite (class): with the information about the satellite
                list_cons (list of dict): List of the constraints in attached to the satellite
                list_pos_sat (list): List of the positions of the satellite (size of 3*n)
                list_att_sat (list): List of the attitudes of the satellite (size of 4*n)
                list_time_sat (list): List of the time for the positions and the attitudes of the satellite (size of n)
                parent_path (str): ONLY FOR VTS. "Sol/Earth" by default. Need to be changed if changing of center.
                time_step (int): Atomatically calculated minimal time step between every times in the time file or time
                list. Will allow to place the positions at a good time for the Blender visualisation
        """
        mini_step, count = 1048575, 0
        if file3dblender is not None:
            file3dsplit = file3dblender.split(".")
            if len(file3dsplit) > 2:
                raise NameError("File name must not have point in it")
            if len(file3dsplit) == 1 and self.software.lower() == "blender":
                raise NameError("3D file must be given with its .blend for Blender")
            if file3dsplit[1] != "blend" and self.software.lower() == "blender":
                raise NameError("3D file must be a .blend for Blender")
        if file3dvts is not None:
            file3dsplit = file3dvts.split(".")
            if len(file3dsplit) > 2:
                raise NameError("File name must not have point in it")
            if len(file3dsplit) == 1 and self.software.lower() == "vts":
                raise NameError("3D file must be given with its .obj for VTS")
            if file3dsplit[1] != "obj" and self.software.lower() == "vts":
                raise NameError("3D file must be a .obj for VTS")
        if attitudefile is not None and isinstance(attitudefile, str):
            file_att = attitudefile.split('.')
            if len(file_att) == 1:
                raise NameError("Attitude file must be given with its .txt")
            if len(file_att) > 2:
                raise NameError("File name must not have point in it")
            if file_att[1] != "txt":
                raise NameError("Attitude file must be a .txt")
        if trajectoryfile is not None and isinstance(trajectoryfile, str):
            file_traj = trajectoryfile.split('.')
            if len(file_traj) == 1:
                raise NameError("Trajectory file must be given with its .txt")
            if len(file_traj) > 2:
                raise NameError("File name must not have point in it")
            if file_traj[1] != "txt":
                raise NameError("Trajectory file must be a .txt")
        elif trajectoryfile is None and (file3dblender is not None or file3dvts is not None):
            raise TypeError("You need a trajectory file or list for each satellite. " + name +
                            " does not have trajectory file or list.")
        if time_file is None and (file3dblender is not None or file3dvts is not None):
            raise TypeError("You need a time file or list for each satellite. "
                            + name + " does not have time file or list.")
        sat = Satellite(name, file3dblender, file3dvts, trajectoryfile, attitudefile, ratio, weight)
        list_time_sat = []
        if time_file is not None and isinstance(time_file, str):
            filename = self.data_path + "/" + time_file
            file = open(filename, 'r')
            arr0 = file.readlines()
            file.close()
            if self.software.lower() == "blender":
                list_time_sat = arr0
                if (float(list_time_sat[1])//1)-(float(list_time_sat[0])//1) != (float(list_time_sat[-1])//1)\
                        - (float(list_time_sat[-2])//1):
                    print("WARNING: The time file '" + time_file + "' does not seem to have regular time step."
                                                                   " The number of frames and the time of rendering"
                                                                   " can be very important.")
            elif self.software.lower() == "vts":
                list_time_sat = arr0
            else:
                raise NameError("The software can only be either 'Blender' or 'VTS'")
        elif time_file is not None and isinstance(time_file, list):
            if self.software.lower() == "blender":
                list_time_sat = time_file
                if float(list_time_sat[1])-float(list_time_sat[0]) != float(list_time_sat[-2])-float(list_time_sat[-1]):
                    print("WARNING: The time list does not have regular time step. The number of frames can be very"
                          " important and the time of rendering can be very high")
            elif self.software.lower() == "vts":
                list_time_sat = time_file
            else:
                raise NameError("The software can only be either 'Blender' or 'VTS'")
        list_att_sat = []
        if attitudefile is not None and isinstance(attitudefile, list):
            if self.software.lower() == "blender":
                list_att_sat = attitudefile
            elif self.software.lower() == "vts":
                list_att_sat = attitudefile
            else:
                raise NameError("The software can only be either 'Blender' or 'VTS'")
        elif attitudefile is not None and isinstance(attitudefile, str):
            filename = self.data_path + "/" + attitudefile
            file = open(filename, 'r')
            arr2 = file.readlines()
            file.close()
            for line in arr2:
                item = line.split(' ')
                list_att_sat.append(item[0])
                list_att_sat.append(item[1])
                list_att_sat.append(item[2])
                list_att_sat.append(item[3])
            if self.software.lower() == "blender":
                list_att_sat = list_att_sat
            elif self.software.lower() == "vts":
                pass
            else:
                raise NameError("The software can only be either Blender or VTS")
        list_pos_sat = []
        if trajectoryfile is not None and isinstance(trajectoryfile, list):
            list_pos_sat = trajectoryfile
        elif trajectoryfile is not None and isinstance(trajectoryfile, str):
            filename = self.data_path + "/" + trajectoryfile
            file = open(filename, 'r')
            arr1 = file.readlines()
            file.close()
            for line in arr1:
                item = line.split(' ')
                list_pos_sat.append(item[0])
                list_pos_sat.append(item[1])
                list_pos_sat.append(item[2])
            if self.software.lower() == "blender":
                list_pos_sat = list_pos_sat
            elif self.software.lower() == "vts":
                pass
            else:
                raise NameError("The software can only be either Blender or VTS")
        if len(list_pos_sat)/3 != len(list_time_sat) and (file3dblender is not None or file3dvts is not None):
            raise KeyError("The time file does not have the right amount of data compared to the trajectory file for "
                           + name)
        if len(list_att_sat)/4 != len(list_time_sat) and len(list_att_sat) != 4 and list_att_sat != []:
            raise KeyError("The time file does not have the right amount of data compared to the attitude file for "
                           + name)
        for k in range(len(list_time_sat) - 1):
            if self.time_step <= float(list_time_sat[k + 1]) - float(list_time_sat[count]) < mini_step:
                mini_step = float(list_time_sat[k + 1]) - float(list_time_sat[count])
                count = k + 1
            elif float(list_time_sat[k + 1]) - float(list_time_sat[count]) < self.time_step:
                print("WARNING: Some time step are smaller than the minimum time step put to read the time file. Some "
                      "points are too close in time from each other and will not be read. You can change this by "
                      "changing the TIMESTEP parameter in the default_config.py file")
            elif float(list_time_sat[k + 1]) // 1 - float(list_time_sat[count]) // 1 >= mini_step:
                count += 1
        if attitudefile is not None and trajectoryfile is not None:
            satellite_visu = {"satellite": sat, "list_cons": [], "list_pos_sat": list_pos_sat,
                              "list_att_sat": list_att_sat, "list_time_sat": list_time_sat, "parent_path": "Sol/Earth",
                              "time_step": mini_step}
        elif trajectoryfile is not None and attitudefile is None:
            satellite_visu = {"satellite": sat, "list_cons": [], "list_pos_sat": list_pos_sat, "list_att_sat": None,
                              "list_time_sat": list_time_sat, "parent_path": "Sol/Earth", "time_step": mini_step}
        else:
            satellite_visu = {"satellite": sat, "list_cons": [], "list_time_sat": list_time_sat,
                              "parent_path": "Sol/Earth", "time_step": mini_step}
        self.list_sat_visu.append(satellite_visu)

    def remove_satellite(self, name):
        """
        This method will be used by the user. It will remove a satellite from LIST_SAT_VISU.

        Parameters:
            name (str1): Name of the satellite to delete from the visualisation
        """
        for sat in self.list_sat_visu:
            if sat["satellite"].name == name:
                self.list_sat_visu.remove(sat)

    def add_constraint(self, satellite_name, category, label, display=config.DISPLAY,
                       color=config.COLOR, pos_rot=config.POSROT, size=config.SIZE, file3d=config.FILE3D,
                       file_pos=config.FILEPOS, active=config.ACTIVE):
        """ This method will be used by the user. It will add a constraint to a
        given satellite with the given arguments, in the list_cons of the satellite.

        Parameters:
            satellite_name (str1): Name of the satellite to attach the
            constraint to
            category (str2): Type of constraint
            label (str3): Name of the constraint (must all be different)
            display (bool1): If true, the constraint will appear in the
            visualisation, else, it will not
            color (str4): Color of the constraint (8 choices of color: red,
            orange, yellow, green, sky blue, blue, purple, pink)
            pos_rot (list of 6 int): first 3 = position in the satellite's coordinate system (x, y, z)
                                     last 3 = rotation (rotx, roty, rotz)
            file3d (str5): Name of the 3D file of the constraint
            file_pos (str6): Name of the position file in the global coordinate
            system if needed for some type of constraint
            size (int1): To resize the constraint (in addition to the initial global ratio of the scene)
            active (bool): Set or not the active path (draw the trajectory step by step during the visualisation)

            A constraint is a dictionary with the following parameters:
                category (str): Either trajectory, cone, plume, point or sphere
                label (str): Name of the constraint to find it after in the data or to remove it
                display (bool): If False, the constraint will not be displayed in the scene during the visualisation
                color (str): Color of the constraint (8 choices of color: red,
                orange, yellow, green, sky blue, blue, purple, pink)
                pos_rot (list): first 3 = position in the satellite's coordinate system (x, y, z)
                                last 3 = rotation (rotx, roty, rotz)
                size (int): To resize the constraint (in addition to the initial global ratio of the scene)
                file3d (str): Name of the 3D file of the constraint
                file_pos (str): Name of the position file or a list of the position of the constraint in the global
                coordinate
                active (bool): ONLY FOR THE TRAJECTORIES. If True, the trajectory will draw itself at the same time as
                the position of the satelltie
        """
        inside = False
        for sat in self.list_sat_visu:
            if satellite_name == sat["satellite"].name:
                inside = True
                break
        if not inside:
            raise NameError("The constraint", label, "cannot be attached to", satellite_name,
                            "because this satellite doesn't exist")
        if file3d is not None:
            try:
                file = file3d.split('.')
                if len(file) == 1:
                    raise NameError("3D files must be given with the .blend")
                if len(file) > 2:
                    raise NameError("File must not have points in its name")
                if file[1] != "blend":
                    raise NameError("3D file name must be .blend")
            except NameError:
                print("One of the 3D file name has a problem")
                raise
        else:
            if category == "Cone":
                file3d = config.FILE3DCONEBLENDER
            elif category == "Sphere" and self.software.lower() == "blender":
                file3d = config.FILE3DSPHEREBLENDER
            elif category == "Sphere" and self.software.lower() == "vts":
                file3d = config.FILE3DSPHEREVTS
            elif category == "Plume" and self.software.lower() == "blender":
                file3d = config.FILE3DPLUMEBLENDER
            elif category == "Plume" and self.software.lower() == "vts":
                file3d = config.FILE3DPLUMEVTS
            elif category == "Point":
                file3d = config.FILE3DPOINTBLENDER
        constraint = {"category": category, "label": label, "display": display,
                      "color": color, "pos_rot": pos_rot or [0, 0, 0, 0, 0, 0],
                      "size": size, "file3d": file3d, "file_pos": file_pos, "active": active}
        for sat in self.list_sat_visu:
            if sat["satellite"].name == satellite_name:
                sat["list_cons"].append(constraint)

    def remove_constraint(self, label):
        """
        This method will be used by the user. It will remove a constraint from
        a satellite in LIST_SAT_VISU.

        Parameters:
            label (str1): Name of the constraint to remove from the list
        """
        for sat in self.list_sat_visu:
            for cons in sat["list_cons"]:
                if cons["label"] == label:
                    sat["list_cons"].remove(cons)

    def add_planet(self, name, file3d=None, position_file=None, attitude_file=None):
        """
        This method will be used by the user. It will add a planet to the
        list_planet with the given arguments. For VTS, planet needs just a name ("Earth", "Moon"...).
        For Blender, the planet needs an initial position (a list of 3 ints) and a name. You can also give an initial
        rotation

        Parameters:
            name (str1): Name given to the planet
            file3d (str2): Name of the 3D file of the planet if not in the
            format 'name.blend'
            position_file (str3): Name of the trajectory file of the planet
            attitude_file (str4): Name of the attitude file of the planet
            (quaternions)
        """
        if file3d is None:
            file3d = name + ".blend"
        if self.software.lower() == "blender":
            if position_file is not None:
                planet = {"name": name, "file3d": file3d,
                          "position_file": position_file,
                          "attitude_file": attitude_file}
            else:
                raise KeyError("If the software used is Blender, the planet needs a position file or"
                               " a list of positions")
        else:
            planet = {"name": name}
        self.list_planet.append(planet)

    def add_step(self, label, time, time_ratio=1, camera_angle="XY",
                 constraint_not_displayed=None):
        """
        This method will be used by the user. It will add a steps to the
        list_step with the given arguments.

        Parameters:
            label (str1): label of the step (it will never appear on the render)
            time (int1): time in seconds after the start of the scene when to put the step
            time_ratio (int2): time ratio to apply from the time start
            camera_angle (str2): angle of camera from the time start (choose between: "XY", "XZ", "YZ" and "far")
            constraint_not_displayed (list of constraints' label): list of all
            the constraints label not to display until the next step. If the
            list is empty, all the constraints will be displayed.

            The step is a dictionary with the parameters above
        """
        list_name_sat = []
        empty = True
        for sat in self.list_sat_visu:
            if sat["satellite"].name in list_name_sat:
                raise NameError("Two satellites have the same name " + sat["satellite"].name)
            else:
                list_name_sat.append(sat["satellite"].name)
            if len(sat["list_cons"]) != 0:
                empty = False
        if not empty and constraint_not_displayed is not None:
            for cons in constraint_not_displayed:
                inside = False
                for sat in self.list_sat_visu:
                    for cons_sat in sat["list_cons"]:
                        if cons == cons_sat["label"] and cons_sat["display"]:
                            inside = True
                            break
                if not inside:
                    raise NameError("The constraint", cons, "doesn't exist or is not displayed")
        elif empty and constraint_not_displayed is not None:
            raise NameError("The constraint", constraint_not_displayed[0], "doesn't exist")
        else:
            pass
        constraint_not_displayed = constraint_not_displayed or []
        step = {"label": label, "time": time, "time_ratio": time_ratio,
                "camera_angle": camera_angle, "constraint_not_displayed": constraint_not_displayed}
        if label == "initial step for time ratio":
            self.list_step.insert(0, step)
        else:
            self.list_step.append(step)

    def start(self):
        """
        This is the main method of the code.
        This method will create a visualisation (for Blender or VTS depending on what the user asked for).
        After, it will write the correct lines in the terminal command (either on Windows or on Linux) to open
        automatically the software with the visualisation.

        """
        if not isinstance(self.render, bool):
            raise TypeError("The render parameter needs to be a boolean")
        if self.config_file_name in ("blender_visu", "default_config",
                                     "global_visualisation_corrige2", "red_visualisation",
                                     "vts_visu", "visu_blender"):
            raise NameError("You can't name the configuration file this way because it's already a name of a file")
        if self.software.lower() == "blender":
            self.config_file_name = self.config_file_name + ".py"
            if self.blendervisu.center is not None:
                self.blendervisu = BlenderVisu(self, center=self.blendervisu.center)
            else:
                self.blendervisu = BlenderVisu(self)
            BlenderVisu.startblender(self.blendervisu, self.render)
            if os.name == 'nt':  # Windows laptop
                if self.render:
                    print("The time for the rendering will be approximately:", int(self.Screen["fps"] *
                          self.Screen["movie_duration"]*8/60)//60, "hours and", int(self.Screen["fps"] *
                          self.Screen["movie_duration"]*8/60) % 60, "minutes")
                    subprocess.Popen(["start", "cmd", "/c", "blender visu_blender.blend --background --python " +
                                     self.config_file_name], shell=True)
                    # process.wait()
                else:
                    subprocess.Popen(["start", "cmd", "/c", "blender visu_blender.blend --python " +
                                     self.config_file_name], shell=True)
                    # process.wait()
            else:  # Linux laptop
                if self.render:
                    print("The time for the rendering will be approximately:",
                          int(self.Screen["fps"] * self.Screen["movie_duration"] * 8 / 60) // 60,
                          "hours and", int(self.Screen["fps"] *
                                           self.Screen["movie_duration"] * 8 / 60) % 60, "minutes")
                    print("Do not close the blender file until the scene appears (when the scene appears,"
                          " the movie has finished to render)")
                    subprocess.Popen(["./blender visu_blender.blend --python " +
                                      config.PROJECTPATH + "/" + self.config_file_name], shell=True,
                                     cwd=config.BLENDERPATH)
                    # process.wait()
                else:
                    subprocess.Popen(["./blender visu_blender.blend --python " +
                                     config.PROJECTPATH + "/" + self.config_file_name], shell=True,
                                     cwd=config.BLENDERPATH)
                    # process.wait()
            if self.remove:
                print("BEWARE: The configuration file generated for this visualisation will be deleted after the"
                      " visualisation")
                # os.remove(self.config_file_name)
        elif self.software.lower() == "vts":
            self.config_file_name = self.config_file_name + ".vts"
            self.vtsvisu = VTSVisu(self, initial_time=self.vtsvisu.initial_time)
            VTSVisu.startvts(self.vtsvisu)
            if os.name == 'nt':  # Windows laptop
                if self.render:
                    print("Can't render a VTS movie. Please, if you want to render, choose Blender")
                    subprocess.run(["start", "cmd", "/c",
                                    'startVTS --batch --project "' + config.PROJECTPATH + '/' +
                                    self.config_file_name + '"'], shell=True, cwd=config.VTSPATH)
                else:
                    subprocess.run(["start", "cmd", "/c",
                                    'startVTS --batch --project "' + config.PROJECTPATH + '/' +
                                    self.config_file_name + '"'], shell=True, cwd=config.VTSPATH)
            else:  # Linux laptop
                if self.render:
                    print("Can't render a VTS movie. Please, if you want to render, choose Blender")
                    subprocess.run(['./startVTS --batch --project "' + config.PROJECTPATH + '/' +
                                    self.config_file_name + '"'],
                                   cwd=config.VTSPATH, shell=True)
                else:
                    subprocess.run(['./startVTS --batch --project "' + config.PROJECTPATH + '/' +
                                    self.config_file_name + '"'],
                                   cwd=config.VTSPATH, shell=True)
            if self.remove:
                print("BEWARE: The configuration file generated for this visualisation will be deleted after the"
                      " visualisation")
                # os.remove(self.config_file_name)
        else:
            raise NameError("Please choose between 'Blender' and 'VTS' for the software")
