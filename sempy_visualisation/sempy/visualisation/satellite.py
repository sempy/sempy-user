# -*- coding: utf-8 -*-
# pylint: disable=too-many-arguments
"""
Created on Thu May  7 16:26:43 2020

@author: Valentin Prudhomme
"""

import sempy.visualisation.default_config as config

# _______________________________SATELLITE CLASS________________________________


class Satellite:
    """
    The Satellite object contains lots all the proprieties of a satellite.

    Parameters:
        name (str1):Name given to the satellite
        file3dblender (str2):Name of the Blender 3D file of the satellite.
        file3dvts (str3):Name of the 3D file of the satellite.
        trajectoryfile (str4):Name of the trajectory file of the satellite.
        attitudefile (str5):Name of the attitude file of the satellite (quaternions).
        ratio (int1): ratio of the satellite for the visualisation
        weight (int2):Weight of the satellite (equal zero if not given)
    """
    def __init__(self, name, file3dblender, file3dvts, trajectoryfile, attitudefile, ratio=1,
                 weight=0):
        self.name = name
        self.file3dblender = file3dblender
        self.file3dvts = file3dvts
        self.trajectoryfile = trajectoryfile
        self.attitudefile = attitudefile
        self.ratio = ratio
        self.weight = weight
