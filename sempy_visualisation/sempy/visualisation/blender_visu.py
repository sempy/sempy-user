# -*- coding: utf-8 -*-
# pylint: disable=line-too-long
"""
Created on Wed Apr 22 10:00:09 2020

@author: Valentin Prudhomme
"""

# _________________________________IMPORTS_____________________________________

# import os
from math import pi
import sempy.visualisation.default_config as config

# _________________________BLENDER VISUALISATION CLASS__________________________


def dist_points(vect1, vect2):
    """
        This method allows to return the value of the distance between two vectors in 3D

        Parameters:
            vect1 (List of 3 int): first vector
            vect2 (List of 3 int): second vector

        Return:
            the distance between vect1 and vect2
        """
    return ((float(vect2[0])-float(vect1[0]))**2+(float(vect2[1])-float(vect1[1]))**2 +
            (float(vect2[2])-float(vect1[2]))**2)**0.5


class BlenderVisu:
    """
    The BlenderVisu class contains all the information and methods for the
    Blender visualisation.

    Parameters:
        global_visualisation (obj1): Class with needed objects
        center (str2): Name of the object to put in the center of the visualisation
    """
    def __init__(self, global_visualisation, center=config.CENTER):
        self.global_visualisation = global_visualisation
        self.screen = global_visualisation.Screen
        self.camera = global_visualisation.Camera
        self.center = center

    def init_center(self):
        """
        The init_center method will return the points of the trajectory of
        the object in the center of the scene.

        Return:
            points (list of positions): List of the points
            [x0,y0,z0,x1,y1,z1...] of the trajectory of the object that
            will be in the center of the scene

        """
        length = 3 * len(self.global_visualisation.list_sat_visu[0]["list_pos_sat"])
        points = [0] * length
        lon = 0
        if self.center is not None:
            points[:] = []
            for sat in self.global_visualisation.list_sat_visu:
                if sat["satellite"].name == self.center:
                    points = sat["list_pos_sat"]
        else:
            for sat in self.global_visualisation.list_sat_visu:
                if len(sat["list_time_sat"]) > lon:
                    lon = len(sat["list_time_sat"])
            for sat in self.global_visualisation.list_sat_visu:
                if len(sat["list_time_sat"]) == lon:
                    self.center = sat["satellite"].name
        return points

    def resize_object(self, name, size=1):
        """
        This writes the lines to resize all the objects on the scene
        according to the ratio given by the user.
        It takes the name of the object to resize as parameter.

        Parameters:
            name (str1): Name of the object to resize
            size (int1): Size of the object (multiplied by the ratio)
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
""" + name + """ = bpy.data.objects['""" + name + """']
""" + name + """.select_set(True)
bpy.ops.transform.resize(value=(RATIO*"""+str(size)+""", RATIO*"""+str(size)+""", RATIO*"""+str(size)+"""))\n\n""")
        config_blender.close()

    def rescale_objects(self, name, category, list_time):
        """
            This method will rescale the satellites or the constraint to see them all
            at the start of the visualisation where they arefar away

            Parameters:
                name (str1): Name of the object to rescale
                category (int1): Either 'Satellite' or 'Point' (the other constraint don't need to be rescaled)
                list_time (list of time): list of time of the satellite
        """
        mini_space = 1048575
        config_blender = open(self.global_visualisation.config_file_name, "a")
        vect1, vect2 = [], []
        resc_ref, imax, key, len_max = 0, 0, 0, 0
        for sat in self.global_visualisation.list_sat_visu:
            if sat["time_step"] < mini_space:
                mini_space = sat["time_step"]
            if self.camera["centered_object"] == sat["satellite"].name:
                vect1 = sat["list_pos_sat"]
        for sat in self.global_visualisation.list_sat_visu:
            if sat["satellite"].file3dvts is not None or sat["satellite"].file3dblender is not None:
                vect2 = [sat["list_pos_sat"][0], sat["list_pos_sat"][1], sat["list_pos_sat"][2]]
                dist = dist_points([vect1[0], vect1[1], vect1[2]], vect2)
                if dist > resc_ref:
                    resc_ref = dist*self.camera["scale_ratio"]
        for sat in self.global_visualisation.list_sat_visu:
            if sat["satellite"].file3dvts is not None or sat["satellite"].file3dblender is not None:
                len_max = len(vect1)/3
                i = 0
                vect2 = sat["list_pos_sat"]
                if len(vect2) < len_max:
                    len_max = len(vect2)/3
                while dist_points([vect1[3*i], vect1[3*i+1], vect1[3*i+2]], [vect2[3*i], vect2[3*i+1], vect2[3*i+2]])\
                        > 1 and i < len_max-1:
                    i += 1
                if i > imax:
                    imax = i
        count = 0
        for i in range(len(list_time)):
            if float(list_time[i + 1])//1 - float(list_time[count])//1 >= mini_space:
                key = (float(list_time[i]) / mini_space) // 1
                count = i + 1
                if i == imax:
                    break
        if category == "satellite":
            config_blender.write("""bpy.context.scene.frame_set(0)  # Calculating the ratio to rescale objects to see 
bpy.ops.object.select_all(action='DESELECT')  # them all in a camera
""" + name + """ = bpy.data.objects['""" + name + """']
""" + name + """.select_set(True)
bpy.ops.transform.resize(value=("""+str(resc_ref)+""", """+str(resc_ref)+""", """+str(resc_ref)+"""))
""" + name + """.keyframe_insert(data_path="scale", index =-1)
bpy.context.scene.frame_set(""" + str(key) + """)
bpy.ops.transform.resize(value=("""+str(1/resc_ref)+""", """+str(1/resc_ref)+""", """+str(1/resc_ref)+"""))
""" + name + """.keyframe_insert(data_path="scale", index =-1)\n\n""")
        else:
            config_blender.write("""bpy.context.scene.frame_set(0)  # Calculating the ratio to rescale objects to see 
bpy.ops.object.select_all(action='DESELECT')  # them all in a camera
""" + name + """ = bpy.data.objects['""" + name + """']
""" + name + """.select_set(True)
bpy.ops.transform.resize(value=(""" + str(resc_ref) + """, """ + str(resc_ref) + """, """ + str(resc_ref) + """))
""" + name + """.keyframe_insert(data_path="scale", index =-1)
bpy.context.scene.frame_set(""" + str(key) + """)
bpy.ops.transform.resize(value=(""" + str(1/resc_ref) + """, """ + str(1/resc_ref) + """, """ + str(1/resc_ref) + """))
""" + name + """.keyframe_insert(data_path="scale", index =-1)\n\n""")
        config_blender.close()
        return resc_ref, imax

    def new_step_blender(self, step):
        """
        The new_step_blender method will create a step from the list of
        steps in the list_step in the GlobalVisualisation class
        (where the steps were added with the add_step method).

        Parameters:
            step (dict): Dictionary with the step attributes.
        """
        list_time = []
        time_step, count = -1, 0
        reap_time, mini_space = 0, 1048575
        config_blender = open(self.global_visualisation.config_file_name, "a")
        for sat in self.global_visualisation.list_sat_visu:
            if sat["time_step"] < mini_space:
                mini_space = sat["time_step"]
            if sat["satellite"].file3dvts is not None or sat["satellite"].file3dblender is not None:
                list_time = sat["list_time_sat"]
                for i in range(len(list_time)):
                    if step["time"] > float(list_time[-1]) or step["time"] < 0:
                        raise KeyError("The time of the step:", step["label"], "doesn't fit in the time file"
                                                                               " of the satellite")
                    elif float(list_time[i]) <= step["time"] <= float(list_time[i + 1]):
                        time_step = i
                        break
                key = 0
                for cons in sat["list_cons"]:
                    if cons["display"]:
                        for j in range(len(list_time)-1):
                            if float(list_time[j + 1])//1 - float(list_time[count])//1 >= mini_space:
                                key = (float(list_time[j]) / mini_space) // 1
                                count = j + 1
                            if time_step == -1:
                                key = 0
                                break
                            if j == time_step:
                                break
                        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(""" + str(key) + """)  # display or not objects and to change camera
""" + cons["label"] + """ = bpy.data.objects['""" + cons["label"] + """'] # Setting everything to undisplayed here
""" + cons["label"] + """.select_set(True)
""" + cons["label"] + """.hide_viewport = False
""" + cons["label"] + """.hide_render = False
""" + cons["label"] + """.keyframe_insert(data_path="hide_viewport", index =-1)
""" + cons["label"] + """.keyframe_insert(data_path="hide_render", index =-1)\n\n""")
        for conslabel in step["constraint_not_displayed"]:
            key, count = 0, 0
            for sat in self.global_visualisation.list_sat_visu:
                for cons_sat in sat["list_cons"]:
                    if cons_sat["label"] == conslabel:
                        list_time = sat["list_time_sat"]
            count = 0
            for j in range(len(list_time)-1):
                if float(list_time[j + 1])//1 - float(list_time[count])//1 >= mini_space:
                    key = (float(list_time[j]) / mini_space) // 1
                    count = j + 1
                if j == time_step:
                    reap_time = key
            config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(""" + str(reap_time) + """)  # undisplayed to displayed here
""" + conslabel + """ = bpy.data.objects['""" + conslabel + """']
""" + conslabel + """.select_set(True)
""" + conslabel + """.hide_viewport = True
""" + conslabel + """.hide_render = True
""" + conslabel + """.keyframe_insert(data_path="hide_viewport", index =-1)
""" + conslabel + """.keyframe_insert(data_path="hide_render", index =-1)\n\n""")
        config_blender.close()

    def import_object(self, file_name):
        """
        This method writes the correct lines to import an object
        from its 3D file. Normally, this file is positioned in a <model>
        folder in the same folder as the code and the blender file. This
        can be changed in the configuration file.

        Parameters:
            file_name (str1): Name of the Blender file to import
        """
        file = file_name.split('.')
        if len(file) == 1:
            raise NameError("3D files must be given with the .blend")
        if len(file) > 2:
            raise NameError("File must not have points in its name")
        if file[1] != "blend":
            raise NameError("3D file name must be .blend")
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write('print("importing ' + file_name + '...")\n')
        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '"""+config.PROJECTPATH+"/"+config.FILE3DPATH+"""/"""+str(file_name)+"""'  # satellite or planets)\n""")
        file_name = file_name.split(".")
        config_blender.write("""section = '""" + """\\\\""" + """Object""" + """\\\\'
obj = '""" + file_name[0] + """'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)\n""")
        if not self.camera["see_inside_constraint"]:
            config_blender.write("""ob = bpy.data.objects['""" + file_name[0] + """']
ob.select_set(True)
ob.active_material.use_backface_culling = True\n""")

        config_blender.close()

    def trajectory_color(self, color, satellite_name):
        """
        This method writes the correct line to color the trajectory
        constraint. The user can choose between 8 colors (red, orange,
        yellow, green, sky blue, blue, purple, pink). This method is
        different from the next one because the trajectory does not have a
        3D file like the other constraints.

        Parameters:
            color (str: "red", "orange", "yellow", "sky blue", "blue",
            "pink", "purple" or "green"): Color of the trajectory
            satellite_name (str2): Name of the satellite whose trajectory
            is colored
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""col = bpy.data.materials.new('""" + satellite_name + """_trajectory')  # Changing the
col.roughness = 1  # color and some texture for the trajectories
col.specular_intensity = 0
col.use_backface_culling = True
bpy.context.object.data.twist_smooth = 0.5
bpy.data.materials['""" + satellite_name + """_trajectory'].use_nodes = True
col.node_tree.nodes.new('ShaderNodeEmission')
node_1 = col.node_tree.nodes["Material Output"]
node_2 = col.node_tree.nodes["Emission"]
col.node_tree.links.new(node_1.inputs["Surface"], node_2.outputs[0])\n""")
        if color in ("red", "Red"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)\n""")
        elif color in ("orange", "Orange"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (1, 0.330000, 0, 1)\n""")
        elif color in ("yellow", "Yellow"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (1, 1, 0, 1)\n""")
        elif color in ("green", "Green"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (0, 1, 0, 1)\n""")
        elif color in ("sky blue", "Sky blue"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (0, 1, 1, 1)\n""")
        elif color in ("blue", "Blue"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (0, 0, 1, 1)\n""")
        elif color in ("purple", "Purple"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (0.330000, 0, 1, 1)\n""")
        elif color in ("pink", "Pink"):
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (1, 0, 0.800000, 1)\n""")
        else:
            config_blender.write("""bpy.data.materials['""" + satellite_name + """_trajectory'].node_tree.""" +
                                 """nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)\n""")
        config_blender.write("""mesh=bpy.context.object.data
mesh.materials.clear()
mesh.materials.append(col)\n\n""")

        config_blender.close()

    def constraints_color(self, constraint_category, constraint_label,
                          color):
        """
        This method writes the correct line to change the color of the
        constraint according to the color chosen by the user.
        If the user gives a color that is not available, the
        code will write a line as if the user has chosen the red color.
        There are 8 colors available (red, orange, yellow, green, sky blue,
        blue, purple, pink). This method is used on every constraint except
        the trajectory.

        Parameters:
            constraint_category (str1): Category of the constraint to color
            constraint_label (str2): Label of the constraint to color
            color (str: "red", "orange", "yellow", "sky blue", "blue",
            "pink", "purple" or "green"): Color of the constraint
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == '""" + constraint_category + """':
        material.name = '""" + constraint_label + """'\n""")
        config_blender.close()
        config_blender = open(self.global_visualisation.config_file_name, "a")
        if color in ("red", "Red"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)\n\n""")
        elif color in ("orange", "Orange"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0.330000, 0, 1)\n\n""")
        elif color in ("yellow", "Yellow"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 1, 0, 1)\n\n""")
        elif color in ("green", "Green"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 1, 0, 1)\n\n""")
        elif color in ("sky blue", "Sky blue"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 1, 1, 1)\n\n""")
        elif color in ("blue", "Blue"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 0, 1, 1)\n\n""")
        elif color in ("purple", "Purple"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (0.330000, 0, 1, 1)\n\n""")
        elif color in ("pink", "Pink"):
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0.800000, 1)\n\n""")
        else:
            config_blender.write("""bpy.data.materials['""" + constraint_label +
                                 """'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)\n\n""")
        config_blender.close()

    def rename_object(self, old_name, new_name):
        """
        This method allows to rename an object in the Blender info.
        This allows the code to easily find the object to act on.

        Parameters:
            old_name (str1): Old name of the object to rename
            new_name (str2): New name to give to the object
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        old_name = old_name.split(".")
        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
""" + old_name[0] + """ = bpy.data.objects['""" + old_name[0] + """']
""" + old_name[0] + """.name = '""" + new_name + """'\n\n""")
        config_blender.close()

    def move_constraint_to(self, name, constraint_pos_rot, ratio):
        """
        This method writes the lines to move the object to the location
        given in the constraint attribute.
        This is used on some constraints to position them in the correct
        location with respect to the satellite.

        Parameters:
            name (str1): Name of the object to move
            constraint_pos_rot (list of 6 int): first 3 = position(x, y, z)
                                                last 3 = rotation(rotx,
                                                                roty, rotz)
            ratio (int1): ratio of the satellite fot the visualisation
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Move the constraint and rotate it
bpy.data.objects['""" + name + """'].location.x = """ + str(constraint_pos_rot[0]*ratio) + """*RATIO
bpy.data.objects['""" + name + """'].location.y = """ + str(constraint_pos_rot[1]*ratio) + """*RATIO
bpy.data.objects['""" + name + """'].location.z = """ + str(constraint_pos_rot[2]*ratio) + """*RATIO
""" + name + """ = bpy.data.objects['""" + name + """']
""" + name + """.select_set(True)
bpy.ops.transform.rotate(value=""" + str(int(constraint_pos_rot[3])/180*pi) + """, orient_axis='X')
bpy.ops.transform.rotate(value=""" + str(int(constraint_pos_rot[4])/180*pi) + """, orient_axis='Y')
bpy.ops.transform.rotate(value=""" + str(int(constraint_pos_rot[5])/180*pi) + """, orient_axis='Z')\n\n""")
        config_blender.close()

    def parent_obj(self, child_name, parent_name, category):
        """
        This method writes the lines to parent two objects in Blender.
        This allows the second object to follow the first one's movements.
        This method is used on every constraint to parent them to their
        satellite.

        Parameters:
            child_name (str1): Object name to parent to parent_name
            parent_name (str2): Object name to be parented to child_name
            category (str3): Either Sphere or something else, because the sphere is not parented to the satellite
            but just follow the trajectory of the satellite (to not have the same scale)
        """
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
""" + parent_name + """ = bpy.data.objects['""" + parent_name + """']  # exactly the movement of the parent)
""" + parent_name + """.select_set(True)
""" + child_name + """ = bpy.data.objects['""" + child_name + """']
""" + child_name + """.select_set(True)\n""")
        if category == "Sphere":
            config_blender.write("""bpy.context.view_layer.objects.active = """ + child_name + """
bpy.ops.object.constraint_add(type='COPY_LOCATION')
bpy.context.object.constraints["Copy Location"].target = bpy.data.objects['""" + parent_name + """']\n\n""")
        else:
            config_blender.write("""bpy.context.view_layer.objects.active = """ + parent_name + """
bpy.ops.object.parent_set(type='OBJECT')\n\n""")
        config_blender.close()

    def averaging_position(self):
        """
        This method helped fix a problem of the Blender workspace. It
        takes the trajectory of one of the satellites and calculates a
        "ratio" so that the scene takes place in a smaller box
        (1000m*1000m*1000m by default)
        """
        list_time = [0]
        dist, key, mini_space, count = 0, 0, 1048575, 0
        config_blender = open(self.global_visualisation.config_file_name, "a")
        if self.screen["global_ratio"] is not None:
            for sat in self.global_visualisation.list_sat_visu:
                if sat["time_step"] < mini_space:
                    mini_space = sat["time_step"]
                if float(sat["list_time_sat"][-1]) > float(list_time[-1]):
                    list_time = sat["list_time_sat"]
            for j in range(len(list_time)-1):
                if float(list_time[j + 1])//1 - float(list_time[count])//1 >= mini_space:
                    key = (float(list_time[j]) / mini_space) // 1
                    count = j + 1
            if self.global_visualisation.render:
                self.screen["frame_step"] = (key / self.screen["fps"] / self.screen["movie_duration"]) // 1
            else:
                self.screen["fps"] = (key / self.screen["movie_duration"]) // 1
                config_blender.write("bpy.context.scene.sync_mode = 'FRAME_DROP'\n")
            config_blender.write("bpy.context.scene.frame_end = "+str(key)+"\n")
            config_blender.write("FRAME_END = " + str(key) + "\n")
            config_blender.write("bpy.context.scene.frame_start = 0\n")
            config_blender.write("bpy.context.scene.frame_step = "+str(self.screen["frame_step"])+"\n")
            config_blender.write("RATIO=" + str(self.screen["global_ratio"]) + "\n\n")
        else:
            arr = self.global_visualisation.list_sat_visu[0]["list_pos_sat"]
            mini = [0, 0, 0]
            maxi = [0, 0, 0]
            for i in range(int(len(arr)/3)):
                if i == 0:
                    i += 1
                    mini[0] = float(arr[3*i])
                    mini[1] = float(arr[3*i+1])
                    mini[2] = float(arr[3*i+2])
                if dist < dist_points([float(arr[3*i]), float(arr[3*i+1]), float(arr[3*i+2])],
                                      [mini[0], mini[1], mini[2]]):
                    dist = dist_points([float(arr[3 * i]), float(arr[3 * i + 1]), float(arr[3 * i + 2])],
                                       [mini[0], mini[1], mini[2]])
                    maxi[0] = float(arr[3*i])
                    maxi[1] = float(arr[3*i+1])
                    maxi[2] = float(arr[3*i+2])
            count = 0
            for sat in self.global_visualisation.list_sat_visu:
                if sat["satellite"].file3dblender is not None or sat["satellite"].file3dvts is not None:
                    if sat["time_step"] < mini_space:
                        mini_space = sat["time_step"]
                    if float(sat["list_time_sat"][-1]) > float(list_time[-1]):
                        list_time = sat["list_time_sat"]
            for j in range(len(list_time)-1):
                if float(list_time[j + 1])//1 - float(list_time[count])//1 >= mini_space:
                    key = (float(list_time[j]) / mini_space) // 1
                    count = j + 1
            if self.global_visualisation.render:
                self.screen["frame_step"] = (key / self.screen["fps"] / self.screen["movie_duration"]) // 1
            else:
                self.screen["fps"] = (key / self.screen["movie_duration"]) // 1
                config_blender.write("bpy.context.scene.sync_mode = 'FRAME_DROP'\n")
            config_blender.write("bpy.context.scene.frame_end = " + str(key) + "\n")
            config_blender.write("FRAME_END = " + str(key) + "\n")
            config_blender.write("""bpy.context.scene.frame_start = 0  # set the start and end frame\n""")
            config_blender.write("bpy.context.scene.frame_step = "+str(self.screen["frame_step"])+"\n")
            if self.camera["plan"] == "far":
                self.camera["distant_view"] *= 0.7 * dist_points([mini[0], mini[1], mini[2]],
                                                                 [maxi[0], maxi[1], maxi[2]]) / 4
                for sat in self.global_visualisation.list_sat_visu:
                    sat["satellite"].ratio *= 5 * dist_points([mini[0], mini[1], mini[2]], [maxi[0], maxi[1], maxi[2]])
                    for cons in sat["list_cons"]:
                        if cons["category"] == "Trajectory":
                            cons["size"] *= 17 * dist_points([mini[0], mini[1], mini[2]], [maxi[0], maxi[1], maxi[2]])
            moyenne_x = (mini[0]+maxi[0])/2
            moyenne_y = (mini[1]+maxi[1])/2
            moyenne_z = (mini[2]+maxi[2])/2
            maxi_x = abs(maxi[0]-moyenne_x)
            maxi_y = abs(maxi[1]-moyenne_y)
            maxi_z = abs(maxi[2]-moyenne_z)
            maximum = max(maxi_x, maxi_y, maxi_z)
            self.screen["global_ratio"] = (self.screen["box_size"]/1000)/maximum/4
            config_blender.write("RATIO=" + str(self.screen["global_ratio"]) + " # set the ratio for the scene\n\n")
        config_blender.close()

    def get_point_to_location(self, name, file_pos, list_time):
        """
        This method is only relevant for point constraints or for planets.
        It writes the lines to move the point to the good location
        according to its position file.
        Only used for a satellite if it has a point constraint.

        Parameters:
            name (str1): Name of the object to move in the workspace
            file_pos (str2): Name of the position file to put the point in
            the correct location (with the .txt)
            list_time (list of time): List of time of the satellite
        """
        list_point = []
        posx = 0
        posy = 0
        posz = 0
        mini_space, count = 1048575, 0
        for sat in self.global_visualisation.list_sat_visu:
            if sat["time_step"] < mini_space:
                mini_space = sat["time_step"]
        if isinstance(file_pos, str):
            file_traj = file_pos.split('.')
            if len(file_traj) == 1:
                raise NameError("Trajectory file must be given with the .txt")
            if len(file_traj) > 2:
                raise NameError("Files must not have point in their name")
            if file_traj[1] != "txt":
                raise NameError("Trajectory files must be a .txt")
        points = self.init_center()
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("ob = bpy.data.objects['" + name + "']\n")
        key = 0
        if isinstance(file_pos, str):
            filename = self.global_visualisation.data_path + "/" + file_pos
            file = open(filename, 'r')
            arr = file.readlines()
            file.close()
            for line in arr:
                item = line.split(' ')
                posx = float(item[0])
                list_point.append(posx)
                posy = float(item[1])
                list_point.append(posy)
                posz = float(item[2])
                list_point.append(posz)
        elif isinstance(file_pos, list):
            list_point = file_pos
            posx = file_pos[0]
            posy = file_pos[1]
            posz = file_pos[2]
        else:
            raise TypeError("Position file must be either a List or a String (name of the file)")
        for i in range(int(len(points)/3)):
            list_point.append(posx)
            list_point.append(posy)
            list_point.append(posz)
        i = 0
        while i < len(points)/3:
            config_blender.write("bpy.context.scene.frame_set(" + str(int(key)) + ") # set the points to keyframes\n")
            if i < len(list_time)-1:
                if float(list_time[i + 1])//1 - float(list_time[count])//1 >= mini_space:
                    key = (float(list_time[i]) / mini_space) // 1
                    count = i + 1
            posx = float(list_point[3*i])*1000*self.screen["global_ratio"] - \
                float(points[3*i])*1000*self.screen["global_ratio"]
            posy = float(list_point[3*i+1])*1000*self.screen["global_ratio"] - \
                float(points[3*i+1])*1000*self.screen["global_ratio"]
            posz = float(list_point[3*i+2])*1000*self.screen["global_ratio"] - \
                float(points[3*i+2])*1000*self.screen["global_ratio"]
            i += 1
            config_blender.write("""ob.location = (""" + str(posx) + """, """ + str(posy) + """, """ + str(posz) + """)
ob.keyframe_insert(data_path="location", index =-1)\n\n""")

        config_blender.close()

    def draw_trajectory(self, satellite_name, list_pos_sat, traj_depth, dist_ref, imax, active):
        """
        This method writes the lines to plot the trajectory of each
        satellite. It's based on a series of points joined together.
        This is based on the same trajectory file as the one used for the
        satellite.

        Parameters:
            satellite_name (str1): Name of the satellite whose trajectory
            is drawn
            list_pos_sat (list of positions): List of the positions of the
            trajectory to draw
            traj_depth (int1): Depth of the trajectory in the scene
            dist_ref (int2): Scale of the satellite after rescale it
            to see all the satellites in the scene
            imax (int3): Iteration from which the satellites are distant
            from less than 1 kilometer
            active (bool): Set or not the active path (draw the trajectory step by step)
        """
        list_time, list_pos, list_time_center = [], [], []
        key, mini_space, percent, total_dist = 0, 1048575, 0, 0
        oldx, oldy, oldz = 0, 0, 0
        newcordx, newcordy, newcordz = 0, 0, 0
        points = self.init_center()
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""ops.curve.primitive_bezier_circle_add(enter_editmode=True)  # draw the trajectory
curve = context.active_object
bez_points = curve.data.splines[0].bezier_points
ops.curve.delete()
bpy.context.scene.tool_settings.use_keyframe_insert_auto = True\n""")
        arr = list_pos_sat
        for sat in self.global_visualisation.list_sat_visu:
            if sat["time_step"] < mini_space:
                mini_space = sat["time_step"]
            if sat["satellite"].name == satellite_name:
                list_time = sat["list_time_sat"]
                list_pos = sat["list_pos_sat"]
        for i in range(int(len(arr)/3)):
            posx = (float(arr[i*3]))*1000
            posy = (float(arr[i*3+1]))*1000
            posz = (float(arr[i*3+2]))*1000
            config_blender.write("ops.curve.vertex_add(location=("+str(posx)+", "+str(posy)+", "+str(posz)+"))\n")
        count = 0
        for j in range(len(list_time)):
            if float(list_time[j + 1])//1 - float(list_time[count])//1 >= mini_space:
                key = (float(list_time[j]) / mini_space) // 1
                count = j + 1
            if j == imax:
                break
        config_blender.write("""bpy.context.scene.tool_settings.use_keyframe_insert_auto = False
curve.name='""" + satellite_name + """_trajectory'
ops.object.mode_set(mode='OBJECT')
bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_depth = """ + str(traj_depth*dist_ref) + """/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth", index =-1)
#""" + satellite_name + """ = bpy.data.objects['""" + satellite_name + """_trajectory']
#""" + satellite_name + """.select_set(True)
bpy.ops.transform.resize(value=(RATIO, RATIO, RATIO))
bpy.context.scene.frame_set(""" + str(int(key)) + """)
bpy.context.object.data.bevel_depth = """ + str(traj_depth) + """/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth",index=-1) #give some thickness to the trajectory\n\n""")
        if active:
            key, count = 0, 0
            config_blender.write("bpy.context.object.data.bevel_factor_mapping_end = 'SEGMENTS'\n")
            for j in range(len(list_time)-1):
                total_dist += dist_points([list_pos[3*j], list_pos[3*j+1], list_pos[3*j+2]],
                                          [list_pos[3*j+3], list_pos[3*j+4], list_pos[3*j+5]])
            for i in range(len(list_time)-1):
                config_blender.write("""bpy.context.scene.frame_set(""" + str(key) + """)
bpy.context.object.data.bevel_factor_end = """ + str(percent) + """ 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)\n""")
                percent += dist_points([list_pos[3*i], list_pos[3*i+1], list_pos[3*i+2]],
                                       [list_pos[3*i+3], list_pos[3*i+4], list_pos[3*i+5]]) / total_dist
                if float(list_time[i + 1])//1 - float(list_time[count])//1 >= mini_space:
                    key = (float(list_time[i]) / mini_space) // 1
                    count = i + 1
                    if i == len(list_time)-2:
                        config_blender.write("""bpy.context.scene.frame_set(""" + str(key) + """)
bpy.context.object.data.bevel_factor_end = 1 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)\n""")
        key = -1
        count = 0
        if self.center is not None:
            for sat in self.global_visualisation.list_sat_visu:
                if self.center == sat["satellite"].name:
                    list_time_center = sat["list_time_sat"]
            config_blender.write("""traj = bpy.data.objects['""" + satellite_name + """_trajectory']\n""")
            if float(list_time_center[0]) != 0:
                key = 0
            for i in range(int(len(list_time_center))):
                if i == count:
                    config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
                    if key == 0:
                        trajposx = float(points[i*3])*1000
                        newcordx = float(arr[i*3])*1000*self.screen["global_ratio"]-float(points[i*3])*1000 *\
                            self.screen["global_ratio"]
                        trajposy = float(points[i*3+1])*1000
                        newcordy = float(arr[i*3+1])*1000*self.screen["global_ratio"]-float(points[i*3+1])*1000 *\
                            self.screen["global_ratio"]
                        trajposz = float(points[i*3+2])*1000
                        newcordz = float(arr[i*3+2])*1000*self.screen["global_ratio"]-float(points[i*3+2])*1000 *\
                            self.screen["global_ratio"]
                        config_blender.write("""traj.location = ((-""" + str(trajposx) +
                                             "+traj.location.x)*RATIO, (-" + str(trajposy) +
                                             "+traj.location.y)*RATIO, (-" + str(trajposz) +
                                             """+traj.location.z)*RATIO) 
traj.keyframe_insert(data_path="location", index =-1)\n\n""")
                        oldx = float(arr[i*3])*1000*self.screen["global_ratio"]
                        oldy = float(arr[i*3+1])*1000*self.screen["global_ratio"]
                        oldz = float(arr[i*3+2])*1000*self.screen["global_ratio"]
                    elif key != 0 and i < len(list_time):
                        cordactx = (float(arr[i*3]))*1000*self.screen["global_ratio"] - \
                            float(points[i*3])*1000*self.screen["global_ratio"]
                        cordacty = (float(arr[i*3+1]))*1000*self.screen["global_ratio"]-float(points[i*3+1]) *\
                            1000*self.screen["global_ratio"]
                        cordactz = (float(arr[i*3+2]))*1000*self.screen["global_ratio"]-float(points[i*3+2]) *\
                            1000*self.screen["global_ratio"]
                        trajposx = (float(arr[i*3]))*1000*self.screen["global_ratio"]-oldx-(cordactx-newcordx)
                        trajposy = (float(arr[i*3+1]))*1000*self.screen["global_ratio"]-oldy-(cordacty-newcordy)
                        trajposz = (float(arr[i*3+2]))*1000*self.screen["global_ratio"]-oldz-(cordactz-newcordz)
                        config_blender.write("""traj.location = (traj.location.x-""" + str(trajposx) +
                                             ", traj.location.y-" + str(trajposy) + ", traj.location.z-" +
                                             str(trajposz) + """)
traj.keyframe_insert(data_path="location", index =-1)\n\n""")
                        newcordx = cordactx
                        newcordy = cordacty
                        newcordz = cordactz
                        oldx = float(arr[i*3])*1000*self.screen["global_ratio"]
                        oldy = float(arr[i*3+1])*1000*self.screen["global_ratio"]
                        oldz = float(arr[i*3+2])*1000*self.screen["global_ratio"]
                if count >= len(list_time):
                    config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
                    cordactx = (float(arr[-3])) * 1000 * self.screen["global_ratio"] - \
                        float(points[i * 3]) * 1000 * self.screen["global_ratio"]
                    cordacty = (float(arr[-2])) * 1000 * self.screen["global_ratio"] - float(points[i * 3 + 1]) * \
                        1000 * self.screen["global_ratio"]
                    cordactz = (float(arr[-1])) * 1000 * self.screen["global_ratio"] - float(points[i * 3 + 2]) * \
                        1000 * self.screen["global_ratio"]
                    trajposx = (float(arr[-3])) * 1000 * self.screen["global_ratio"] - oldx - (cordactx - newcordx)
                    trajposy = (float(arr[-2])) * 1000 * self.screen["global_ratio"] - oldy - (
                                cordacty - newcordy)
                    trajposz = (float(arr[-1])) * 1000 * self.screen["global_ratio"] - oldz - (
                                cordactz - newcordz)
                    config_blender.write("""traj.location = (traj.location.x-""" + str(trajposx) +
                                         ", traj.location.y-" + str(trajposy) + ", traj.location.z-" +
                                         str(trajposz) + """)
traj.keyframe_insert(data_path="location", index =-1)\n\n""")
                    newcordx = cordactx
                    newcordy = cordacty
                    newcordz = cordactz
                    oldx = float(arr[-3]) * 1000 * self.screen["global_ratio"]
                    oldy = float(arr[-2]) * 1000 * self.screen["global_ratio"]
                    oldz = float(arr[-1]) * 1000 * self.screen["global_ratio"]
                if i < len(list_time_center)-1:
                    if float(list_time_center[i + 1])//1 - float(list_time_center[count])//1 >= mini_space:
                        key = (float(list_time_center[i]) / mini_space) // 1
                        count = i + 1

        config_blender.close()

    def set_keyframes(self, satellite_name, list_pos_sat, list_att_sat, list_time_sat):
        """
        This method will write the lines to position (location and attitude)
        all the satellites (in LIST_SAT_VISU) with keyframes in the
        timeline of Blender. The "satellite" parameter is the Satellite
        object describing the satellite on which the keyframe and the
        trajectory will be applied. The satellite also contains its
        trajectory and attitude file.
        Used for each satellite in the LIST_SAT_VISU list.

        Parameters:
            satellite_name (str1): Name of the satellite on which to apply keyframes
            list_pos_sat (list of positions): List of the positions of the satellite (x, y, z)
            list_att_sat (list of attitudes): List of the attitudes of the satellite (Quaternions)
            list_time_sat (list of time): List of the time of the trajectory
        """
        mini_space, count = 1048575, 0
        points = self.init_center()
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""ob = bpy.data.objects['""" + satellite_name + """']
ob.rotation_mode = 'QUATERNION' # Set all the points and the attitude for each satellite\n""")
        key = 0
        arr1 = list_pos_sat
        for sat in self.global_visualisation.list_sat_visu:
            if sat["time_step"] < mini_space:
                mini_space = sat["time_step"]
            if self.center == sat["satellite"].name:
                list_time_sat = sat["list_time_sat"]
        if satellite_name == self.center:
            posx, posy, posz = 0, 0, 0
            key = 0
            config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
            config_blender.write("""ob.location = (""" + str(posx) + ", " + str(posy) + ", " + str(posz) + """)
ob.keyframe_insert(data_path="location", index =-1)\n\n""")
        else:
            for i in range(int(len(list_time_sat))):
                if i == count and i < len(arr1)/3-1:
                    config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
                    posx = (float(arr1[i*3]))*1000*self.screen["global_ratio"] - \
                        float(points[i*3])*1000*self.screen["global_ratio"]
                    posy = (float(arr1[i*3+1]))*1000*self.screen["global_ratio"] - \
                        float(points[i*3+1])*1000*self.screen["global_ratio"]
                    posz = (float(arr1[i*3+2]))*1000*self.screen["global_ratio"] - \
                        float(points[i*3+2])*1000*self.screen["global_ratio"]
                if count >= len(arr1)/3:
                    config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
                    posx = (float(arr1[-3])) * 1000 * self.screen["global_ratio"] - \
                        float(points[i * 3]) * 1000 * self.screen["global_ratio"]
                    posy = (float(arr1[-2])) * 1000 * self.screen["global_ratio"] - \
                        float(points[i * 3 + 1]) * 1000 * self.screen["global_ratio"]
                    posz = (float(arr1[-1])) * 1000 * self.screen["global_ratio"] - \
                        float(points[i * 3 + 2]) * 1000 * self.screen["global_ratio"]
                if i < len(list_time_sat)-1:
                    if float(list_time_sat[i + 1])//1 - float(list_time_sat[count])//1 >= mini_space:
                        key = (float(list_time_sat[i]) / mini_space) // 1
                        count = i + 1
                config_blender.write("""ob.location = (""" + str(posx) + ", " + str(posy) + ", " + str(posz) + """)
ob.keyframe_insert(data_path="location", index =-1)\n\n""")
        config_blender.close()
        count = 0
        if list_att_sat is not None:
            config_blender = open(self.global_visualisation.config_file_name, "a")
            arr2 = list_att_sat
            key = 0
            for j in range(int(len(arr2)/4)):
                if j == count:
                    config_blender.write("bpy.context.scene.frame_set(" + str(key) + ")\n")
                    quat_0 = float(arr2[j*4])
                    quat_1 = float(arr2[j*4+1])
                    quat_2 = float(arr2[j*4+2])
                    quat_3 = float(arr2[j*4+3])
                    config_blender.write("""ob.rotation_quaternion = (""" + str(quat_0) + ", " + str(quat_1) + ", " +
                                         str(quat_2) + ", " + str(quat_3) + """)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)\n\n""")
                if j < len(list_time_sat)-1:
                    if float(list_time_sat[j + 1])//1 - float(list_time_sat[count])//1 >= mini_space:
                        key = (float(list_time_sat[j]) / mini_space) // 1
                        count = j + 1

        config_blender.close()

    def initialisation(self):
        """
        This method writes at the very start of the configuration file all
        the parameters to initialise the Blender workspace.
        Used in the Start method.
        """
        clip_start = 0.0001
        if self.camera["plan"] == "far":
            clip_start = 50
        config_blender = open(self.global_visualisation.config_file_name, "w")
        config_blender.write("""import bpy # imports
from bpy import context, data, ops\n
for a in bpy.context.screen.areas: # Initialisation for some parameters of the camera and the light
    if a.type == 'VIEW_3D':
        for s in a.spaces:
            if s.type == 'VIEW_3D':
                s.clip_start = """ + str(clip_start) + """
                s.clip_end = 1000000
ob = bpy.data.objects['Camera']
ob.data.clip_start = """ + str(clip_start) + """
ob.data.clip_end = 1000000
bpy.ops.object.delete(use_global=False)
bpy.ops.object.select_all(action='DESELECT')
lamp1 = bpy.data.objects['Light']
lamp1.select_set(True)
bpy.context.view_layer.objects.active = lamp1
bpy.context.object.data.type = 'SUN'
bpy.context.object.data.energy = 0.15
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][0]) + """*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][1]) + """*3.14/180-3.14, orient_axis='Y')
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][2]) + """*3.14/180-3.14, orient_axis='Z')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.light_add(type='SUN', radius=1, location=(0, 0, 0))
lamp2 = bpy.data.objects['Sun']
lamp2.select_set(True)
bpy.context.view_layer.objects.active = lamp2
bpy.context.object.data.energy = 7
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][0]) + """*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][1]) + """*3.14/180, orient_axis='Y')
bpy.ops.transform.rotate(value=-""" + str(self.screen["light_orientation"][2]) + """*3.14/180, orient_axis='Z')
bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0, 0, 0, 1)
#bpy.context.scene.render.engine = 'CYCLES'
#bpy.context.scene.cycles.device = 'GPU'
bpy.context.scene.cycles.max_bounces = 1
bpy.context.scene.cycles.diffuse_bounces = 1
bpy.context.scene.cycles.glossy_bounces = 1
bpy.context.scene.cycles.transparent_max_bounces = 6
bpy.context.scene.cycles.transmission_bounces = 1
bpy.context.scene.render.tile_x = 512
bpy.context.scene.render.tile_y = 512\n\n""")

        config_blender.close()

    def init_camera(self, dist_ref, imax):
        """
        This method writes the lines to initialise the camera for the
        animation in Blender.
        The camera parameter comes from the self parameter, that calls the
        "BlenderVisu" class and its camera parameter.
        Used at the end of the "Start" method
        Parameters:
            dist_ref (int1): distance between the satellites at the start of the scene
            imax (int2): index of the time from which the satellites are 1 kilometer away
        """
        inside = False
        if self.camera["plan"] not in ("far", "XY", "YZ", "XZ"):
            raise NameError("The plan for the camera does not exist")
        for planet in self.global_visualisation.list_planet:
            if planet["name"] == self.camera["centered_object"]:
                inside = True
        for sat in self.global_visualisation.list_sat_visu:
            if self.camera["centered_object"] == sat["satellite"].name:
                inside = True
            for cons in sat["list_cons"]:
                if cons["label"] == self.camera["centered_object"]:
                    inside = True
        if not inside:
            raise NameError("The object you want to visualise with the camera does not exist")
        list_time = []
        key, ratio, mini_space, count = 0, 0, 1048575, 0
        config_blender = open(self.global_visualisation.config_file_name, "a")
        if self.camera["centered_object"] is None:
            print("BEWARE: The satellite camera was not given. It has been initialized to",
                  self.global_visualisation.list_sat_visu[0]["satellite"].name)
            self.camera["centered_object"] = self.global_visualisation.list_sat_visu[0]["satellite"].name
        if self.camera["centered_object"] is not None:
            for sat in self.global_visualisation.list_sat_visu:
                if sat["time_step"] < mini_space:
                    mini_space = sat["time_step"]
                if sat["satellite"].name == self.camera["centered_object"]:
                    ratio = sat["satellite"].ratio
                    list_time = sat["list_time_sat"]
            for i in range(len(list_time)):
                if i < len(list_time)-1:
                    if float(list_time[i + 1])//1 - float(list_time[count])//1 >= mini_space:
                        key = (float(list_time[i]) / mini_space) // 1
                        count = i + 1
                        if i == imax:
                            break
            config_blender.write("""bpy.ops.object.select_all(action='DESELECT') # Initialisation of the camera
sat = bpy.data.objects['""" + self.camera["centered_object"] + """']
sat.select_set(True)
cam = bpy.data.objects['Camera']
cam.select_set(True)
bpy.context.view_layer.objects.active = """ + self.camera["centered_object"] + """
bpy.ops.object.location_clear(clear_delta=False)
bpy.ops.object.rotation_clear(clear_delta=False)
cam.location.x = sat.location.x 
cam.location.y = sat.location.y 
cam.location.z = sat.location.z
bpy.context.scene.frame_set(0)
if '""" + self.camera["plan"] + """' == 'YZ':
    cam.location.x += """ + str(self.camera["close_view"]) + """*RATIO*""" +
                                 str(dist_ref*ratio) + """
elif '""" + self.camera["plan"] + """' == 'XZ':
    cam.location.y += """ + str(self.camera["close_view"]) + """*RATIO*""" +
                                 str(dist_ref*ratio) + """
elif '""" + self.camera["plan"] + """' == 'XY':
    cam.location.z += """ + str(self.camera["close_view"]) + """*RATIO*""" +
                                 str(dist_ref*ratio) + """
    print(RATIO)
elif '""" + self.camera["plan"] + """' == 'far':
    cam.location.x += """ + str(self.camera["distant_view"]) + """*RATIO*""" +
                                 str(self.camera["distant_view"]) + """
cam.keyframe_insert(data_path="location", index =-1)
bpy.context.scene.frame_set(""" + str(key) + """)
if '""" + self.camera["plan"] + """' == 'YZ':
    cam.location.x = sat.location.x 
    cam.location.x += """ + str(self.camera["close_view"]) + """*RATIO*""" + str(ratio) + """
elif '""" + self.camera["plan"] + """' == 'XZ':
    cam.location.y = sat.location.y 
    cam.location.y += """ + str(self.camera["close_view"]) + """*RATIO*""" + str(ratio) + """
elif '""" + self.camera["plan"] + """' == 'XY':
    cam.location.z = sat.location.z
    cam.location.z += """ + str(self.camera["close_view"]) + """*RATIO*""" + str(ratio) + """
elif '""" + self.camera["plan"] + """' == 'far':
    cam.location.x += """ + str(self.camera["distant_view"]) + """*RATIO*""" +
                                 str(self.camera["distant_view"]) + """
cam.location.x -= sat.location.x 
cam.location.y -= sat.location.y 
cam.location.z -= sat.location.z
cam.keyframe_insert(data_path="location", index =-1)
cons1 = cam.constraints.new(type='COPY_LOCATION')
cons1.target = sat
cam.constraints["Copy Location"].use_offset = True
cons2 = cam.constraints.new(type='TRACK_TO')
cons2.target = sat
cons2.track_axis = 'TRACK_NEGATIVE_Z'
cons2.up_axis = 'UP_Y'
if '""" + self.camera["plan"] + """' == 'XY':
    cons2.track_axis = 'TRACK_Y'\n\n""")

        config_blender.close()

    def init_screen(self):
        """
        This method will write all the useful lines to initialise the
        screen for the animation in Blender.
        The screen parameter comes from the self parameter, that call the
        "BlenderVisu" class and its screen parameter.
        Used at the end of the "Start" method
        """
        res = self.screen["resolution"].split("*")
        config_blender = open(self.global_visualisation.config_file_name, "a")
        config_blender.write("""bpy.context.scene.render.resolution_x = """ + res[0] + """ #initialisation of the screen
bpy.context.scene.render.resolution_y = """ + res[1] + """
#bpy.context.scene.cycles.samples = """ + str(self.screen["samples"]) + """
bpy.context.scene.eevee.taa_render_samples = """ + str(self.screen["samples"]) + """
bpy.context.scene.render.fps = """ + str(self.screen["fps"]) + """
bpy.context.scene.eevee.use_bloom = True
my_areas = bpy.context.workspace.screens[0].areas
my_shading = 'RENDERED'  # 'WIREFRAME' 'SOLID' 'MATERIAL' 'RENDERED'
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.shading.type = my_shading
for area in my_areas:
    if area.type == 'VIEW_3D':
        area.spaces[0].region_3d.view_perspective = 'CAMERA'
        break
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.overlay.show_overlays = False
bpy.context.scene.frame_current = 0
bpy.context.scene.render.filepath = '""" + config.PROJECTPATH + "/" + config.MOVIEPATH + """/""" +
                             self.screen["movie_name"] + """'
bpy.context.scene.render.image_settings.file_format = 'AVI_JPEG'
bpy.ops.object.select_all(action='DESELECT')\n""")

        config_blender.close()

    def startblender(self, render):
        """
        This is the main method of the BlenderVisu class.

        It takes a "rendering" Boolean parameter that will allow the user
        to choose whether or not to render the scene implemented before. If
        rendering=True, there will be a line at the end of the
        configuration file written to render the entire animation.
        The rest of this method will write the lines to append
        the satellites and the constraints attached to and will resize,
        color, and move them in the correct location thanks to all the
        previous methods in the Blender_visu class.

        Parameters:
            render (Bool): If True, the config file of Blender will write
            one more line to render the visualisation in a move.
            If False, it will just open Blender to have a previsualisation.

        This method is called by thr Start() method in the GlobalVisualisation class
        """
        dist_ref = 1
        imax = 0
        list_time_center = []
        if self.camera["not_to_scale"]:
            print("BEWARE: The scene will not be at the correct scale. Satellites will be over-sized so you can"
                  " see them during all the scene from the camera")
        self.init_center()
        inside = False
        for sat in self.global_visualisation.list_sat_visu:
            if sat["satellite"].name == self.center:
                inside = True
                list_time_center = sat["list_time_sat"]
        if not inside:
            raise NameError("The satellite " + self.center + " that you want in the center doesn't exist")
        if len(self.global_visualisation.list_planet) == 0:
            print("BEWARE: There is not any planet in the visualisation")
        file = self.global_visualisation.config_file_name.split('.')
        if len(file) == 1:
            raise NameError("configuration files names must have a file type (.py for Blender)")
        if len(file) > 2:
            raise NameError("A file must not have a point in its name")
        if len(self.global_visualisation.list_sat_visu) == 0:
            raise IndexError("You must at least have one satellite for the visualisation")
        self.initialisation()
        self.averaging_position()
        for plan in self.global_visualisation.list_planet:
            self.import_object(plan["file3d"])
            self.rename_object(plan["file3d"], plan["name"])
            self.resize_object(plan["name"], 1)
            self.get_point_to_location(plan["name"], plan["position_file"], list_time_center)
        for sat in self.global_visualisation.list_sat_visu:
            if sat["satellite"].file3dvts is not None or sat["satellite"].file3dblender is not None:
                self.import_object(sat["satellite"].file3dblender)
                self.rename_object(sat["satellite"].file3dblender, sat["satellite"].name)
                self.resize_object(sat["satellite"].name, sat["satellite"].ratio)
                if self.camera["not_to_scale"] and len(self.global_visualisation.list_sat_visu) >= 2:
                    dist_ref, imax = self.rescale_objects(sat["satellite"].name, "satellite", sat["list_time_sat"])
                for cons in sat["list_cons"]:
                    if cons["display"]:
                        if cons["category"] == "Cone":
                            self.import_object(cons["file3d"])
                            self.constraints_color(cons["category"], cons["label"], cons["color"])
                            self.resize_object(cons["category"], cons["size"]*sat["satellite"].ratio)
                            self.move_constraint_to(cons["category"], cons["pos_rot"], sat["satellite"].ratio)
                            self.parent_obj(cons["category"], sat["satellite"].name, cons["category"])
                            self.rename_object(cons["category"], cons["label"])
                        elif cons["category"] == "Plume":
                            self.import_object(cons["file3d"])
                            self.resize_object(cons["category"], cons["size"]*sat["satellite"].ratio)
                            self.move_constraint_to(cons["category"], cons["pos_rot"], sat["satellite"].ratio)
                            self.parent_obj(cons["category"], sat["satellite"].name, cons["category"])
                            self.rename_object(cons["category"], cons["label"])
                        elif cons["category"] == "Sphere":
                            self.import_object(cons["file3d"])
                            self.constraints_color(cons["category"], cons["label"], cons["color"])
                            self.resize_object(cons["category"], cons["size"])
                            self.parent_obj(cons["category"], sat["satellite"].name, cons["category"])
                            self.rename_object(cons["category"], cons["label"])
                        elif cons["category"] == "Point":
                            self.import_object(cons["file3d"])
                            self.constraints_color(cons["category"], cons["label"], cons["color"])
                            self.get_point_to_location(cons["category"], cons["file_pos"], sat["list_time_sat"])
                            self.resize_object(cons["category"], cons["size"])
                            self.rename_object(cons["category"], cons["label"])
                            if self.camera["not_to_scale"] and len(self.global_visualisation.list_sat_visu) > 2:
                                self.rescale_objects(cons["label"], cons["category"], sat["list_time_sat"])
                        elif cons["category"] == "Trajectory":
                            self.draw_trajectory(sat["satellite"].name, sat["list_pos_sat"], cons["size"], dist_ref,
                                                 imax, cons["active"])
                            self.trajectory_color(cons["color"], sat["satellite"].name)
                            self.rename_object(sat["satellite"].name + "_" + cons["category"].lower(), cons["label"])
                        else:
                            print("Error: Category of constraint not available")
                self.set_keyframes(sat["satellite"].name, sat["list_pos_sat"], sat["list_att_sat"],
                                   sat["list_time_sat"])
            else:
                for cons in sat["list_cons"]:
                    if cons["display"]:
                        if cons["category"] == "Trajectory":
                            self.draw_trajectory(sat["satellite"].name, sat["list_pos_sat"], cons["size"], dist_ref,
                                                 imax, cons["active"])
                            self.trajectory_color(cons["color"], sat["satellite"].name)
                            self.rename_object(sat["satellite"].name + "_" + cons["category"].lower(), cons["label"])
        if len(self.global_visualisation.list_step) == 0 or self.global_visualisation.list_step[0]["time"] != 0:
            self.global_visualisation.add_step("initial step", 0)
        for step in self.global_visualisation.list_step:
            self.new_step_blender(step)
        self.resize_object("Camera")
        self.init_camera(dist_ref, imax)
        self.init_screen()
        if render:
            config_blender = open(self.global_visualisation.config_file_name, "a")
            config_blender.write("bpy.ops.render.render(animation=True)")
            config_blender.close()
        else:
            if not self.screen["initially_paused"]:
                config_blender = open(self.global_visualisation.config_file_name, "a")
                config_blender.write("bpy.ops.screen.animation_play()")
                config_blender.close()
