import os
from importlib.machinery import SourceFileLoader
from setuptools import setup, find_namespace_packages
from glob import glob
import io

name = 'sempy_visualisation'
description = ("Visualisation subpackage of sempy"
              )

# __version__ = SourceFileLoader(
#     'version',
#     os.path.join(os.path.dirname(__file__),
#                  'version.py')).load_module().__version__

__version__ = 1.0

from os.path import (basename, splitext)

here = os.path.abspath(os.path.dirname(__file__))

# get the dependencies and installs
with io.open(os.path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    # Remove flags like "--no-binary=rasterio"
    install_requires = [line.split(' ')[0] for line in f.read().split('\n')]

with open(os.path.join(here, 'README.md')) as readme_file:
    readme = readme_file.read()

setup(name=name,
      description=description,
      version=__version__,
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Emmanuel BLAZQUEZ, Thibault GATEAU',
      author_email='emmanuel.blazquez@isae-supaero.fr, thibault.gateau@isae-supaero.fr',
      url='https://gitlab.isae-supaero.fr/yv.gary/sempy-v1.git',
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: GNU GPL V3 License",
          "Operating System :: OS Independent",
      ],
      include_package_data=True,
      packages=find_namespace_packages(),
      install_requires=install_requires,
)
