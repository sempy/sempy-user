# Visualisation module of SEMpy

## Installation guide:

### I - Configuration and installation:

1.1 Install Blender 2.83 on: https://www.blender.org/download/ (Linux or Windows)

1.2 Install VTS 3.4.2 on: https://timeloop.fr/vts/ (Linux ou Windows)

**WARNING:** Make sure to install the 2.83 version of Blender, otherwise you may run through several problems.