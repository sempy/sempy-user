Dynamics Subpackage
==========================


Cr3bp Dynamics Module
----------------------
.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.dynamics.cr3bp_dynamics
   :members:
   :show-inheritance:
   :inherited-members:

Ephemeris Dynamics Module
-------------------------
.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.dynamics.ephemeris_dynamics
   :members:
   :show-inheritance:
   :inherited-members:
