:orphan:



.. _sphx_glr_examples_orbit_generation:

Orbit Generation Examples.
==========================

This gallery contains examples that demonstrate how to initialize, compute, interpolate and
display a CR3BP orbit.



.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This script demonstrates how to initialize, compute and display a Near Rectilinear Orbit (NRO)....">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_nrho_interpolation_thumb.png
     :alt: Near Rectilinear Orbit interpolation.

     :ref:`sphx_glr_examples_orbit_generation_nrho_interpolation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/nrho_interpolation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_interpolation_thumb.png
     :alt: Halo Orbit Interpolation.

     :ref:`sphx_glr_examples_orbit_generation_halo_interpolation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_interpolation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="If there is available an initial state it can be propagated by following the next steps.">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_state0_propagation2_thumb.png
     :alt: Propagation of an Initial State.

     :ref:`sphx_glr_examples_orbit_generation_state0_propagation2.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/state0_propagation2

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="If there is available an initial state it can be propagated by following the next steps.">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_state0_propagation_thumb.png
     :alt: Propagation of an Initial State.

     :ref:`sphx_glr_examples_orbit_generation_state0_propagation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/state0_propagation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_computation_external_state0_thumb.png
     :alt: Halo Computation from External State.

     :ref:`sphx_glr_examples_orbit_generation_halo_computation_external_state0.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_computation_external_state0

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_computation_thumb.png
     :alt: Halo Orbit Computation.

     :ref:`sphx_glr_examples_orbit_generation_halo_computation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_computation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_genesis_thumb.png
     :alt: Initialization of the Sun-Earth CR3BP system.

     :ref:`sphx_glr_examples_orbit_generation_genesis.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/genesis

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;, Paolo GUARDABASSO">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_dro_computation_thumb.png
     :alt: Distant Retrograde Orbit Computation.

     :ref:`sphx_glr_examples_orbit_generation_dro_computation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/dro_computation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Created on Wed Sep 04 10:47:11 2019">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_family_interpolation_thumb.png
     :alt: Interpolation of an Halo family.

     :ref:`sphx_glr_examples_orbit_generation_halo_family_interpolation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_family_interpolation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Paolo GUARDABASSO">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_dro_interpolation_thumb.png
     :alt: DRO Interpolation.

     :ref:`sphx_glr_examples_orbit_generation_dro_interpolation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/dro_interpolation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Created on Wed Sep 04 11:38:15 2019">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_family_computation_thumb.png
     :alt: Computation of an Halo family.

     :ref:`sphx_glr_examples_orbit_generation_halo_family_computation.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_family_computation

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="@author: Edgar PEREZ, Alberto FOSSA&#x27;">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halos_computation_several_crtbp_thumb.png
     :alt: Halo Computation in several CRTBP systems.

     :ref:`sphx_glr_examples_orbit_generation_halos_computation_several_crtbp.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halos_computation_several_crtbp

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="Example of propagation of an initial state until impact with one of the two primaries or SOI cr...">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_state0_propagation_event_thumb.png
     :alt: Propagation of an Initial State.

     :ref:`sphx_glr_examples_orbit_generation_state0_propagation_event.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/state0_propagation_event

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This script demonstrates how to generate manifold tubes associated to a given Halo in the Earth...">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_halo_manifolds_thumb.png
     :alt: Halo orbit manifolds.

     :ref:`sphx_glr_examples_orbit_generation_halo_manifolds.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/halo_manifolds

.. raw:: html

    <div class="sphx-glr-thumbcontainer" tooltip="This script demonstrate how to generate manifold tubes associated to a given Near Rectilinear H...">

.. only:: html

 .. figure:: /examples/orbit_generation/images/thumb/sphx_glr_nrho_manifolds_thumb.png
     :alt: Near Rectilinear Halo Orbit manifolds.

     :ref:`sphx_glr_examples_orbit_generation_nrho_manifolds.py`

.. raw:: html

    </div>


.. toctree::
   :hidden:

   /examples/orbit_generation/nrho_manifolds
.. raw:: html

    <div class="sphx-glr-clear"></div>



.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-gallery


  .. container:: sphx-glr-download sphx-glr-download-python

    :download:`Download all examples in Python source code: orbit_generation_python.zip </examples/orbit_generation/orbit_generation_python.zip>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

    :download:`Download all examples in Jupyter notebooks: orbit_generation_jupyter.zip </examples/orbit_generation/orbit_generation_jupyter.zip>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
