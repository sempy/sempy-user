
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "examples/orbit_generation/halo_computation_external_state0.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_orbit_generation_halo_computation_external_state0.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_examples_orbit_generation_halo_computation_external_state0.py:


Halo Computation from External State.
=====================================

@author: Edgar PEREZ, Alberto FOSSA'

.. GENERATED FROM PYTHON SOURCE LINES 9-13

A halo orbit can be computed using an external initial state,
we will see that by going throughout this script.

1) The imports:

.. GENERATED FROM PYTHON SOURCE LINES 13-22

.. code-block:: default


    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.orbits.halo import Halo

    import numpy as np
    import matplotlib.pyplot as plt
    from sempy.core.plotting.plotting_module import PlottingOptions, MasterPlotter


.. GENERATED FROM PYTHON SOURCE LINES 23-24

2) CRTBP system of interest:

.. GENERATED FROM PYTHON SOURCE LINES 24-27

.. code-block:: default


    cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)


.. GENERATED FROM PYTHON SOURCE LINES 28-30

3) The external state0, corresponds to a Halo in CRTBP Sun-Earth system about L2,
Family.southern, Azdim bigger than 142000 Km.

.. GENERATED FROM PYTHON SOURCE LINES 30-33

.. code-block:: default


    state0_external = np.array([1.00831832,  0,  7.58154430e-04,  0,  9.96671251e-03, 0])


.. GENERATED FROM PYTHON SOURCE LINES 34-35

4) Orbit initialization and crtbp

.. GENERATED FROM PYTHON SOURCE LINES 35-39

.. code-block:: default


    orbit1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, state0=state0_external, Azdim=12000)
    orbit1.computation()


.. GENERATED FROM PYTHON SOURCE LINES 40-45

Does not matter what Azdim is passed in the constructor if it is passed an external Initial state,
at the end it will take the Azdim computed with the Initial state that we are passing,
in this case state0_external

5) Visualization:

.. GENERATED FROM PYTHON SOURCE LINES 45-48

.. code-block:: default


    plt.ioff()


.. GENERATED FROM PYTHON SOURCE LINES 49-51

Note: The real scale for the primaries is set when we use:
scale_factor=[True, 1] (default is: scale_factor=[True, 3])

.. GENERATED FROM PYTHON SOURCE LINES 51-59

.. code-block:: default


    plot_options = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 10],
                                   sci=True, l3=False, l4=False, l5=False)

    plot1 = MasterPlotter(plot_options, orbit1)
    plot1.scene_TD()
    plot1.scene_twoD()
    plt.show()


.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.000 seconds)


.. _sphx_glr_download_examples_orbit_generation_halo_computation_external_state0.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: halo_computation_external_state0.py <halo_computation_external_state0.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: halo_computation_external_state0.ipynb <halo_computation_external_state0.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
