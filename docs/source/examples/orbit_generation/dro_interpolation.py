"""
DRO Interpolation.
=========================

@author: Paolo GUARDABASSO
"""

# %%
# Imports:
#
# As in the script for dro_computation, to be able to run the script it is necessary to import
# modules and classes needed:
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO

# %%
# Initialization:
#
# This script is to interpolate an orbit from an abacus, which are .json files that contain
# the required data to perform orbit interpolation. Three orbits will be interpolated with three
# different initial parameters and then plotted.
#
# Initialization of the CRTBP system of interest:
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# In a first case, the keyword argument Axdim is used to initialize the orbit with the
# nominal orbit planar extension:
orbit_1 = DRO(cr3bp, cr3bp.m2, Axdim=730000)

# %%
# # For a second case, the orbit is initialised with the nominal orbit Jacobi constant Cjac:
orbit_2 = DRO(cr3bp, cr3bp.m2, Cjac=2.7)

# %%
# The third orbit is initialised with the orbital period T:
orbit_3 = DRO(cr3bp, cr3bp.m2, T=4.5)

# %%
# The fourth orbit is an Earth DRO initialised with the orbital period T:
orbit_4 = DRO(cr3bp, cr3bp.m1, T=4)

# %%
# Interpolation
# The instance method interpolation is used for the three orbits:
orbit_1.interpolation()
orbit_2.interpolation()
orbit_3.interpolation()
orbit_4.interpolation()

# %%
# Visualization:
fig1, ax1 = plt.subplots(1, 1)
ax1.axis('equal')

fig1.suptitle('Several DROs in rotating reference frame')

# Plot Moon and Earth
prim_1 = plt.Circle((cr3bp.m1_pos[0], 0), cr3bp.R1 / cr3bp.L, facecolor='b', edgecolor='k',
                    linewidth=.25)
prim_2 = plt.Circle((cr3bp.m2_pos[0], 0), cr3bp.R2 / cr3bp.L, facecolor='gray', edgecolor='k',
                    linewidth=.25)
ax1.add_artist(prim_1)
ax1.add_artist(prim_2)

ax1.plot(orbit_1.state_vec[:, 0], orbit_1.state_vec[:, 1], color='r', label="orbit_1")
ax1.plot(orbit_2.state_vec[:, 0], orbit_2.state_vec[:, 1], color='g', label="orbit_2")
ax1.plot(orbit_3.state_vec[:, 0], orbit_3.state_vec[:, 1], color='b', label="orbit_3")
ax1.plot(orbit_4.state_vec[:, 0], orbit_4.state_vec[:, 1], color='c', label="orbit_4")

ax1.set_xlabel('x')
ax1.set_ylabel('y')
ax1.legend()

# If you want a quick look into all the attributes of the orbit instance just input the next line
# in the python console:
# print(orbit_1.__dict__.keys())

# %%
# To see a given attribute, e.g. the initial state:
# print(orbit_1.state0)

# %%
# For more details on DRO attributes please refer to the orbit module documentation
