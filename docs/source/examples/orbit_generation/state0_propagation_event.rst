
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "examples/orbit_generation/state0_propagation_event.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_orbit_generation_state0_propagation_event.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_examples_orbit_generation_state0_propagation_event.py:


Propagation of an Initial State.
================================

Example of propagation of an initial state until impact with one of the two primaries or SOI
crossing.

@author: Paolo GUARDABASSO

.. GENERATED FROM PYTHON SOURCE LINES 12-13

Import all necessary modules and classes:

.. GENERATED FROM PYTHON SOURCE LINES 13-33

.. code-block:: default


    import numpy as np
    import matplotlib.pyplot as plt
    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
    from sempy.core.init.constants import DAYS2SEC
    from sempy.core.diffcorr.ode_event import generate_impact_event, generate_soi_event
    from sempy.core.plotting.util import sphere, set_axes_equal


    def set_axes_lim_labels_title(_ax, title):
        """Convenience function to set the axes limits, labels and the figure title. """
        _ax.set_xlabel('x [-]')
        _ax.set_ylabel('y [-]')
        _ax.set_zlabel('z [-]')
        _ax.set_title(title)
        set_axes_equal(_ax)



.. GENERATED FROM PYTHON SOURCE LINES 34-35

Initialize the CRTBP system:

.. GENERATED FROM PYTHON SOURCE LINES 35-37

.. code-block:: default

    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)


.. GENERATED FROM PYTHON SOURCE LINES 38-42

Set the initial state to be propagated:
state0 = np.array([0.1, 0, 0, 0, 0, 0])  # for m1 impact
state0 = np.array([1.0, 0, 0, 0, 0, 0])  # for m2 impact
state0 = np.array([2.5, 0, 0, -1, 0, 0])  # for SOI crossing

.. GENERATED FROM PYTHON SOURCE LINES 42-45

.. code-block:: default

    state0 = np.array([cr3bp.l1.position[0]+0.1, 0, 0, -1, 0, 0])  # for L1 gate crossing
    # state0 = np.array([cr3bp.l2.position[0]-0.1, 0, 0, 1, 0, 0])  # for L2 gate crossing


.. GENERATED FROM PYTHON SOURCE LINES 46-47

Set the time for the propagation:

.. GENERATED FROM PYTHON SOURCE LINES 47-50

.. code-block:: default

    T = 10
    t_span = [0, T]


.. GENERATED FROM PYTHON SOURCE LINES 51-52

Events initialisation

.. GENERATED FROM PYTHON SOURCE LINES 52-63

.. code-block:: default

    m1_impact_event = generate_impact_event(cr3bp, "m1")
    m2_impact_event = generate_impact_event(cr3bp, "m2")
    soi_escape_event = generate_soi_event(cr3bp, "exit")
    soi_enter_event = generate_soi_event(cr3bp, "enter")

    events = (m1_impact_event, m2_impact_event,
              soi_escape_event, soi_enter_event)

    events_name = ["m1 impact", "m2 impact",
                   "soi escape", "soi enter"]


.. GENERATED FROM PYTHON SOURCE LINES 64-65

Initialize the propagator:

.. GENERATED FROM PYTHON SOURCE LINES 65-67

.. code-block:: default

    prop = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=200000)


.. GENERATED FROM PYTHON SOURCE LINES 68-69

Now the class Cr3bpSynodicPropagator can be used to propagate the initial state:

.. GENERATED FROM PYTHON SOURCE LINES 69-71

.. code-block:: default

    t_vec, state_vec, t_events, state_events = prop.propagate(t_span, state0, events=events)


.. GENERATED FROM PYTHON SOURCE LINES 72-73

Plot the results:

.. GENERATED FROM PYTHON SOURCE LINES 73-95

.. code-block:: default


    plt.rcParams['legend.fontsize'] = 10

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # Plot state
    ax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], 'y', label='Propagation state0')
    ax.scatter(state_vec[0, 0], state_vec[0, 1], state_vec[0, 2], color='r', label='State0')

    # Plot Moon
    x_2, y_2, z_2 = sphere(100, cr3bp.R2 / cr3bp.L, cr3bp.m2_pos)
    ax.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)

    # Plot trajectory
    for i, t_event in enumerate(t_events):
        if t_event.size > 0:
            ax.scatter(state_events[i][:, 0], state_events[i][:, 1], state_events[i][:, 2],
                       label=events_name[i])
    ax.legend()
    set_axes_lim_labels_title(ax, 'State 0 propagation with events')


.. GENERATED FROM PYTHON SOURCE LINES 96-97

Print event information

.. GENERATED FROM PYTHON SOURCE LINES 97-100

.. code-block:: default

    for i, t_event in enumerate(t_events):
        if t_event.size > 0:
            print(f"Time to {events_name[i]}: {t_event[0]*cr3bp.T/2/np.pi/DAYS2SEC:.2f} days")


.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.000 seconds)


.. _sphx_glr_download_examples_orbit_generation_state0_propagation_event.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: state0_propagation_event.py <state0_propagation_event.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: state0_propagation_event.ipynb <state0_propagation_event.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
