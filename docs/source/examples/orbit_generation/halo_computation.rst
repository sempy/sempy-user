
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "examples/orbit_generation/halo_computation.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_orbit_generation_halo_computation.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_examples_orbit_generation_halo_computation.py:


Halo Orbit Computation.
=======================

@author: Edgar PEREZ, Alberto FOSSA'

.. GENERATED FROM PYTHON SOURCE LINES 9-14

1) Imports

To run a script it is necessary to import modules and classes needed to perform the required task,
in this script we will compute a halo orbit, therefore the following modules and classes are
imported from the package.

.. GENERATED FROM PYTHON SOURCE LINES 14-22

.. code-block:: default


    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.orbits.halo import Halo

    import matplotlib.pyplot as plt
    from sempy.core.plotting.plotting_module import PlottingOptions, MasterPlotter


.. GENERATED FROM PYTHON SOURCE LINES 23-32

2) Initialization and Computation

To compute an orbit from a first guess, the first step is to create the CRTBP system of interest,
this task is done by using Cr3bp class.
As arguments, the user needs to pass the primaries to the Cr3bp class constructor, m1 and m2,
just by using two instances of Primary class, for example: Primary.SUN and Primary.JUPITER.
The primaries that are available in the package can be found in primary module.

The CRTBP system of interest is contained in an object (cr3bp), as follows:

.. GENERATED FROM PYTHON SOURCE LINES 32-35

.. code-block:: default


    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)


.. GENERATED FROM PYTHON SOURCE LINES 36-46

Second step, initialize the orbit desired to be studied, here the user has to use the class
corresponding to the desired orbit.
Four required arguments must be passed to the class constructor:
The first one is the cr3bp object create before;
the second one is the libration point around which we want our spacecraft to orbit;
the third one is the orbit's family;
and the last one is the nominal Extension parameter or the nominal Jacobi constant,
that the desired orbit has.

For example, if a halo orbit is the focus of research the orbit initialization will be:

.. GENERATED FROM PYTHON SOURCE LINES 46-49

.. code-block:: default


    orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)


.. GENERATED FROM PYTHON SOURCE LINES 50-55

Third step, the crtbp of the Halo orbit.
In the previous step an instance (orbit) of the class Halo was created, it contains the required
information to perform the orbit crtbp by just using the instance method crtbp().

To perform this task, it is only necessary to state the following line of code:

.. GENERATED FROM PYTHON SOURCE LINES 55-58

.. code-block:: default


    orbit.computation()


.. GENERATED FROM PYTHON SOURCE LINES 59-66

3) Visualization

Ultimately, the CRTBP system and the computed orbit can be visualized by using the next lines
of code:

The real scale for the primaries is set when we use:
scale_factor=[True, 1] (default is: scale_factor=[True, 3])

.. GENERATED FROM PYTHON SOURCE LINES 66-76

.. code-block:: default


    plt.ioff()
    plt.close('all')
    plot_options = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 3],
                                   sci=True, l3=True, l4=False, l5=False)
    plot1 = MasterPlotter(plot_options, orbit)
    plot1.scene_TD()
    plot1.scene_twoD()
    plt.show()


.. GENERATED FROM PYTHON SOURCE LINES 77-83

For more details on the visualization options, please refer to the plotting module documentation.

4) Accessing object attributes

If you want a quick look into all the attributes of the orbit instance just input the next
line in the python console:

.. GENERATED FROM PYTHON SOURCE LINES 83-86

.. code-block:: default


    print(orbit.__dict__.keys())


.. GENERATED FROM PYTHON SOURCE LINES 87-88

To see a given attribute, e.g. the initial state:

.. GENERATED FROM PYTHON SOURCE LINES 88-91

.. code-block:: default


    print(orbit.state0)


.. GENERATED FROM PYTHON SOURCE LINES 92-104

For more details on Halo attributes please refer to the orbit module documentation

5) Extra features for the crtbp method:

crtbp() method has as argument: fix_dim

When nothing is passed to the argument fix_dim, a suitable differential correction
procedure is performed, relying on the definition of 'big orbits' (Az_estimate > 0.0520)
that is loosely based on results from the Earth-Moon system.

If fix_dim is equal to Halo.DiffCorrFixDim.z0, z0 dimension is fixed.
Should be used with small Az

.. GENERATED FROM PYTHON SOURCE LINES 104-107

.. code-block:: default


    orbit.computation(Halo.DiffCorrFixDim.z0)


.. GENERATED FROM PYTHON SOURCE LINES 108-110

If fix_dim is equal to Halo.DiffCorrFixDim.x0, x0 dimension is fixed.
Should be used with big Az

.. GENERATED FROM PYTHON SOURCE LINES 110-113

.. code-block:: default


    orbit.computation(Halo.DiffCorrFixDim.x0)


.. GENERATED FROM PYTHON SOURCE LINES 114-116

For more details on the differential correction procedure,
please check Halo and DiffCorr3D classes docstring.


.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.000 seconds)


.. _sphx_glr_download_examples_orbit_generation_halo_computation.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: halo_computation.py <halo_computation.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: halo_computation.ipynb <halo_computation.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
