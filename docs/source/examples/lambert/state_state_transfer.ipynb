{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# State to state direct transfer in the CR3BP model.\n\nThis example demonstrates the use of the CR3BP Lambert solver to compute a direct transfer\nbetween two specified states in the Earth-Moon CR3BP model.\n\n@author: Alberto FOSSA'\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Import statements\n\nAs usual, we will start importing all required modules and classes to generate the selected\ndeparture and arrival orbits and compute the Lambert arc.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport matplotlib.pyplot as plt\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.cr3bp import Cr3bp\nfrom sempy.core.init.constants import DAYS2SEC\nfrom sempy.core.orbits.halo import Halo\nfrom sempy.core.orbits.nrho import NRHO\nfrom sempy.core.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm\nfrom sempy.core.plotting.simple.utils import decorate_3d_axes"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Dynamical model\n\nAt this point, we define the Earth-Moon CR3BP system as the dynamical model in which the\ntransfer is computed.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)\nl_c, t_c = cr3bp.L, cr3bp.T / 2.0 / np.pi  # CR3BP characteristic length [km] and time [s]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Departure and arrival states\n\nThe departure and arrival states are taken from an L1 southern Halo with vertical extension\n``Azdim=50000`` km and an L2 southern NRHO with ``Azdim=70000`` km respectively.\nThe two orbits are firstly initialized, interpolated and corrected as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "halo_dep = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Azdim=50000)\nnrho_arr = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Azdim=70000)\n\nhalo_dep.interpolation()\nnrho_arr.interpolation()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The departure and arrival states are then selected at the aposelene of the two defined orbits:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "state1 = halo_dep.state0\nstate2 = nrho_arr.state_vec[nrho_arr.t_vec.size // 2, 0:6]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Lambert problem object\n\nOnce the endpoint states are defined, a `Cr3bpLambertPbm` object is instantiated specifying the\ndesired force model in which the transfer is computed.\n\nThrough its methods, this object allows the computation of multiple transfer trajectories\nwith different endpoint states and transfer times.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "lamb_pbm = Cr3bpLambertPbm(cr3bp, cr3bp.m2)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Lambert problem solution\n\nIn order to solve for a given transfer arc, the departure position, the arrival position and the\ntime of flight must be chosen and passed as input parameters to the `solve` method of the above\ndefined object. Initial and final positions are given by ``state1`` and ``state2`` defined above\nwhile a ``5`` days time of flight is chosen in this example.\n\nIn addition to the above, other three important parameters must be specified: the number of\ncomplete revolutions about the main attractor, the number of patch point on which the\ndifferential correction procedure is performed and the type of initial guess. All of them are\noptional with their default values described in the method's docstring.\nRegarding the initial guess, two approximation are available: a solution to the Lambert Problem\ncomputed in the Restricted Two-Body Problem (R2BP) neglecting the influence of one of the two\nprimaries or a series of patch points drawn from a number of CR3BP orbits belonging to the same\nfamily of the departure and arrival ones. A choice between the two is made with the input\nparameter `guess` which will assume one of the two allowed values ``r2bp`` or ``stack``\nrespectively. In this example, an initial guess computed with the R2BP approximation will be\nused to generate the required patch points for the subsequent correction procedure.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "sol = lamb_pbm.solve(tof=5 * DAYS2SEC / t_c, nb_revs=1, nb_pts=2, guess='r2bp',\n                     state1=state1, state2=state2)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "After computing the specified initial guess and performing a differential correction of the\napproximated patch points, the following output values are returned by the aforementioned `solve`\nmethod: time of flight, corrected time and states at patch points, first and second maneuvers\n(dV vectors), maneuvers magnitude (first, second and total dV), departure and arrival states\n``state1`` and ``state2``. More information on both input and output parameters might be found\nin the method's docstring.\n\nThe impulsive dVs required to perform such transfer might be then retrieved from the fifth\nelement of the ``sol`` tuple above.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "dv_mag = sol[5] * l_c / t_c * 1e3  # dV1, dV2 and total dV magnitude [m/s]\n\nprint('\\nImpulsive maneuvers:\\n')\nfor i, m in enumerate(('dV1', 'dV2', 'dVt')):\n    print(f\"{m:5s}: {dv_mag[i]} [m/s]\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Patch point propagation\n\nOnce the Lambert Problem has been solved, a continuous transfer trajectory is obtained\npropagating the corrected patch points returned by the aforementioned `solve` method.\nThe propagation is accomplished with the `Cr3bpLambertPbm` method `propagate` which ensure the\nsame dynamical model and scaling parameters are used for the explicit integration of the\ncorrected transfer arc.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_vec, state_vec = lamb_pbm.propagate(sol[1], sol[2])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Three-dimensional trajectory plot\n\nFinally, the departure orbit, the arrival orbit and the corresponding transfer trajectory are\nplotted on a three-dimensional figure for a visual inspection of the computed results.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = plt.figure()\nax = fig.add_subplot(projection='3d')\nax.plot(halo_dep.state_vec[:, 0], halo_dep.state_vec[:, 1], halo_dep.state_vec[:, 2],\n        label='Departure L1 Halo')\nax.plot(nrho_arr.state_vec[:, 0], nrho_arr.state_vec[:, 1], nrho_arr.state_vec[:, 2],\n        label='Arrival L2 NRHO')\nax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], label='Transfer arc')\nax.quiver(state_vec[0, 0], state_vec[0, 1], state_vec[0, 2],\n          state_vec[0, 3], state_vec[0, 4], state_vec[0, 5], color='k', length=0.1)\nax.quiver(state_vec[-1, 0], state_vec[-1, 1], state_vec[-1, 2],\n          state_vec[-1, 3], state_vec[-1, 4], state_vec[-1, 5], color='k', length=0.1)\ndecorate_3d_axes(ax, 'Direct transfer between L1/L2 southern Halo orbits', '-')\nax.view_init(azim=32.0, elev=22.0)\nplt.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}