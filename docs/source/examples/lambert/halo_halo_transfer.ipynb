{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Halo to Halo direct transfer in the CR3BP model.\n\nThis example demonstrates the use of the CR3BP Lambert solver to compute a direct transfer\nbetween two specified Earth-Moon Halo orbits. Boundary conditions and time of flight are taken\nfrom Davis et al. [1] and summarized below:\n\nDeparture and arrival orbits:\n\n+-----------------+-----------------+-----------------+-----------------+-----------------+\n| Orbit           | Family          | Type            | Jacobi constant | Period          |\n+=================+=================+=================+=================+=================+\n| Departure       | L2 southern     | Halo            | 3.0327          | 7.5 days        |\n+-----------------+-----------------+-----------------+-----------------+-----------------+\n| Arrival         | L2 southern     | Halo            | 3.06            | 13.6 days       |\n+-----------------+-----------------+-----------------+-----------------+-----------------+\n\nTwo-impulses direct transfer trajectory:\n\n+-----------------+-----------------+-----------------+-----------------+-----------------+\n| Departure       | Arrival         | First impulse   | Second impulse  | Time of Flight  |\n+=================+=================+=================+=================+=================+\n| 1.07 days coast | 4.51 days coast | 48.00 m/s       | 69.01 m/s       | 12.68 days      |\n+-----------------+-----------------+-----------------+-----------------+-----------------+\n\n[1] Davis et al., *Locally Optimal Transfers Between Libration Point Orbits Using Invariant\nManifolds*, Advances in the Astronautical Sciences, vol. 135, 2009.\n\n@author: Alberto FOSSA'\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Import statements\n\nAs usual, we will start importing all required modules and classes to generate the selected\ndeparture and arrival orbits and compute the Lambert arc.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport matplotlib.pyplot as plt\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.cr3bp import Cr3bp\nfrom sempy.core.init.constants import DAYS2SEC\nfrom sempy.core.orbits.halo import Halo\nfrom sempy.core.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm\nfrom sempy.core.plotting.simple.utils import decorate_3d_axes"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Dynamical model\n\nAt this point, we define the Earth-Moon CR3BP system as the dynamical model in which the\ntransfer is computed.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)\nl_c, t_c = cr3bp.L, cr3bp.T / 2.0 / np.pi  # CR3BP characteristic length [km] and time [s]"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Departure and arrival orbits\n\nThen, the departure and arrival L2 southern Halo orbits are instantiated starting from the\nvalues of their respective periods reported in the first table above.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "halo_dep = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=7.5 * DAYS2SEC / t_c)\nhalo_arr = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=13.6 * DAYS2SEC / t_c)\n\nhalo_dep.interpolation()\nhalo_arr.interpolation()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Lambert problem object\n\nOnce the endpoint orbits are defined, a `Cr3bpLambertPbm` object is instantiated specifying the\ndesired force model, departure orbit and arrival orbit for the sought transfer arc.\n\nThrough its methods, this object allows the computation of multiple transfer trajectories\nbetween the specified orbits characterized by different departure positions, arrival positions\nand time of flights.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "lamb_pbm = Cr3bpLambertPbm(cr3bp, cr3bp.m2, halo_dep, halo_arr)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Lambert problem solution\n\nIn order to solve for a given transfer arc, the departure position on the first orbit, the\narrival position on the second one and the time of flight must be chosen and passed as input\nparameters to the `solve` method of the above defined object.\nPositions along the orbits must be specified as a fraction of their respective orbital periods\nin the interval ``[0, 1]``. Moreover, the time of flight must be either a positive value or equal\nto zero. In the last case, an approximation based on a weighted average of the departure and\narrival orbits periods will be computed and set as required transfer time.\nIn this examples, data in the second table above are used as boundary conditions and time of\nflight for the sought transfer.\n\nIn addition to the above, other three important parameters must be specified: the number of\ncomplete revolutions about the main attractor, the number of patch point on which the\ndifferential correction procedure is performed and the type of initial guess. All of them are\noptional with their default values described in the method's docstring.\nRegarding the initial guess, two approximation are available: a solution to the Lambert Problem\ncomputed in the Restricted Two-Body Problem (R2BP) neglecting the influence of one of the two\nprimaries or a series of patch points drawn from a number of CR3BP orbits belonging to the same\nfamily of the departure and arrival ones. A choice between the two is made with the input\nparameter `guess` which will assume one of the two allowed values ``r2bp`` or ``stack``\nrespectively. In this example, an initial guess computed with the trajectory stacking\napproximation will be used to generate the required patch points for the subsequent correction\nprocedure.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "sol = lamb_pbm.solve(tof=12.68 * DAYS2SEC / t_c, nb_revs=1, nb_pts=5, guess='stack',\n                     theta1=1.07 / 7.5, theta2=4.51 / 13.6)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "After computing the specified initial guess and performing a differential correction of the\napproximated patch points, the following output values are returned by the aforementioned `solve`\nmethod: time of flight, corrected time and states at patch points, first and second maneuvers\n(dV vectors), maneuvers magnitude (first, second and total dV), departure and arrival states\ncorresponding to the endpoints positions ``theta1`` and ``theta2`` given as input.\nMore information on both input and output parameters might be found in the method's docstring.\n\nThe impulsive dVs required to perform such transfer might be then retrieved from the fifth\nelement of the ``sol`` tuple above.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "dv_mag = sol[5] * l_c / t_c * 1e3  # dV1, dV2 and total dV magnitude [m/s]\n\nprint('\\nImpulsive maneuvers:\\n')\nfor i, m in enumerate(('dV1', 'dV2', 'dVt')):\n    print(f\"{m:5s}: {dv_mag[i]} [m/s]\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Patch point propagation\n\nOnce the Lambert Problem has been solved, a continuous transfer trajectory is obtained\npropagating the corrected patch points returned by the aforementioned `solve` method.\nThe propagation is accomplished with the `Cr3bpLambertPbm` method `propagate` which ensure the\nsame dynamical model and scaling parameters are used for the explicit integration of the\ncorrected transfer arc.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_vec, state_vec = lamb_pbm.propagate(sol[1], sol[2])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Three-dimensional trajectory plot\n\nFinally, the departure orbit, the arrival orbit and the corresponding transfer trajectory are\nplotted on a three-dimensional figure for a visual inspection of the computed results.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = plt.figure()\nax = fig.add_subplot(projection='3d')\nax.plot(halo_dep.state_vec[:, 0] * l_c, halo_dep.state_vec[:, 1] * l_c,\n        halo_dep.state_vec[:, 2] * l_c, label='Dep. Halo')\nax.plot(halo_arr.state_vec[:, 0] * l_c, halo_arr.state_vec[:, 1] * l_c,\n        halo_arr.state_vec[:, 2] * l_c, label='Arr. Halo')\nax.plot(state_vec[:, 0] * l_c, state_vec[:, 1] * l_c, state_vec[:, 2] * l_c, label='Transfer')\ndecorate_3d_axes(ax, '', 'km')\nax.grid(False)\nplt.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}