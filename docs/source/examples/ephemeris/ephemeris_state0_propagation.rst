
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "examples/ephemeris/ephemeris_state0_propagation.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_ephemeris_ephemeris_state0_propagation.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_examples_ephemeris_ephemeris_state0_propagation.py:


Propagation of an initial state in the ephemeris model.
=======================================================

This example demonstrates how it is possible to propagate a known initial state in the ephemeris
force model. It makes use of the `Ephemeris` class to define an object that contains the celestial
bodies included in the model and the `EphemerisPropagator` class to propagate an available state
between two given epochs.

@author: Alberto FOSSA'

.. GENERATED FROM PYTHON SOURCE LINES 14-18

1) Import statements:

we will start importing all necessary modules and classes to define our `Ephemeris` and
`EphemerisPropagator` objects and compute the initial and final epochs.

.. GENERATED FROM PYTHON SOURCE LINES 18-32

.. code-block:: default


    import numpy as np
    import spiceypy as sp
    import matplotlib.pyplot as plt

    from sempy.core.init.primary import Primary
    from sempy.core.init.ephemeris import Ephemeris
    from sempy.core.propagation.ephemeris_propagator import EphemerisPropagator
    from sempy.core.init.constants import MOON_SMA, MOON_OMEGA_MEAN
    from sempy.core.plotting.simple.utils import decorate_3d_axes
    from sempy.core.init.load_kernels import load_kernels

    load_kernels()


.. GENERATED FROM PYTHON SOURCE LINES 33-45

2) Creation of an `Ephemeris` object:

Our instance of `Ephemeris` class will contain information on the celestial bodies to be
included in the force model. The object is instantiated passing as argument a tuple of `Primary`
objects corresponding to the above mentioned bodies.
It will then store information on their names, NAIF IDs and standard gravitational parameters.

Note: for bodies other than the Sun, the Earth and the Moon the body is substituted with the
barycenter of the corresponding planetary system (e.g. Jupiter is replaced with Jupiter
barycenter and so on). See the class documentation for details on why it is needed.

In this example, we will add the Earth, the Moon, the Sun and Jupiter to our model:

.. GENERATED FROM PYTHON SOURCE LINES 45-48

.. code-block:: default


    eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN, Primary.JUPITER))


.. GENERATED FROM PYTHON SOURCE LINES 49-83

3) Creation of an `EphemerisPropagator` object:

This object contains information on the ephemeris model (i.e. celestial bodies to be taken into
account) to be used to integrate the equations of motion as well as on the reference frame in
which the input and output states are expressed.

Regarding the reference, the inertial frame J2000 (or EME2000) is selected as default. This
choice can be overridden passing a different value for the optional parameter `ref` to the class
constructor. For the origin, by default it will coincide with the center of the body that has
been passed first to the `Ephemeris` class constructor. This choice can be overridden passing
the optional input argument `obs` to the class constructor. Its value must be a `Primary` object
contained in the `Ephemeris` object. In the following we will pass `Primary.EARTH`. This will
not modify the default choice but demonstrates how to do so if needed.

`EphemerisPropagator` allows also to select between 6-dimensional equations, in which only the
initial state is propagated, and 42-dimensional equations, in which the State Transition Matrix
(STM) is propagated together with the former state. Unless explicitly set with the optional
input argument `with_stm=True`, 6-dimensional equations are chosen.

Going into more technical details, the propagator supports integration in both dimensional and
non-dimensional units of time, length and velocity. By default, dimensional units consistent
with the ones used by the SPICE Toolkit are used. They are seconds for time and kilometers
for length.

If the propagation (and thus the initial state and epoch passed to the propagate method) has
to be performed in non-dimensional units, optional input parameters must be passed to the class
constructor to define the characteristic time and length for adimensionalization.

In this example we will scale the equations using the semi-major axis and mean motion of the
Moon's orbit about the Earth to obtain the characteristic time and length `t_c`, `l_c`.

We will finally ask to compute the states time series on 2000 discrete points equally spaced in
time between the specified initial and final epochs. This is achieved passing the extra argument
`time_steps=2000`.

.. GENERATED FROM PYTHON SOURCE LINES 83-87

.. code-block:: default


    prop = EphemerisPropagator(eph, obs=Primary.EARTH, t_c=1. / MOON_OMEGA_MEAN, l_c=MOON_SMA,
                               time_steps=2000)


.. GENERATED FROM PYTHON SOURCE LINES 88-96

As described above, `prop` object will propagate an initial state properly scaled in the inertial
frame J2000 centered at the Earth center. Gravitational attraction of the Earth, Moon, Sun and
Jupiter are taken into account to compute the instantaneous acceleration at each propagation
step. Their position relative to the frame origin (the Earth) is retrieved from the JPL
Planetary and Lunar Ephemerides DE430 using the CSPICE function `spkgps_c`.

If we are interested in propagating not only the state but also its STM, we can create a second
instance of `EphemerisPropagator` in which we explicitly pass `with_stm=True`:

.. GENERATED FROM PYTHON SOURCE LINES 96-100

.. code-block:: default


    prop42 = EphemerisPropagator(eph, with_stm=True, t_c=1. / MOON_OMEGA_MEAN, l_c=MOON_SMA,
                                 time_steps=2000)


.. GENERATED FROM PYTHON SOURCE LINES 101-127

4) Initial conditions and integration time span:

Once the propagator has been instantiated, an initial state is propagated simply invoking its
`propagate` method whose input parameters are the aforementioned state and the time span over
which the integration of the equation of motion has to be performed.

Since no scaling of the initial conditions is performed by this method, both initial state and
time span must be expressed either in dimensional units of `km`, `km/s` and `s` or in
non-dimensional ones to be consistent with the previously defined characteristic time `t_c`
and length `l_c`.

By convention, SPICE kernels providing information on the Planetary and Lunar Ephemerides express
time in Ephemeris Time (ET) also referred to as Barycentric Dynamical Time (TDB). ET corresponds
to the number of ephemeris seconds past J2000, i.e. past 01 January 2000 12:00:00.000 TDB.
If using dimensional equations, the `propagate` method takes as input a time span expressed in
the same units (i.e. ephemeris seconds past J2000) which must be scaled by `t_c` if
non-dimensional equations are selected.

Conversion between ET and Coordinated Universal Time (UTC) expressed in calendar format
(and vice-versa) is performed with the CSPICE functions `str2et` and `et2utc` respectively.

In this example, the initial and final epochs are set equal to 01 June 2020 12:00:00.000 UTC
and 28 June 2020 19:00:00.000 UTC to cover roughly one sidereal period of the Moon.
The corresponding epochs in ET are then computed as follows. Note that since we want to
propagate the equations of motion in non-dimensional units, the outputs of `str2et`
(ET in seconds) must be divided by `t_c` (or multiplied by its reciprocal `MOON_OMEGA_MEAN`).

.. GENERATED FROM PYTHON SOURCE LINES 127-131

.. code-block:: default


    et0 = sp.str2et('2020 JUN 01 12:00:00.000') * MOON_OMEGA_MEAN
    et_final = sp.str2et('2020 JUN 28 19:00:00.000') * MOON_OMEGA_MEAN


.. GENERATED FROM PYTHON SOURCE LINES 132-136

The initial state `state0` has been obtained performing a differential correction procedure on a
series of patch points belonging to an L2 Southern Halo orbit of vertical extension 30000 km
initially computed in the Circular Restricted Three-Body Problem approximation. Its components
are already scaled by `t_c` and `l_c`.

.. GENERATED FROM PYTHON SOURCE LINES 136-140

.. code-block:: default


    state0 = np.array([-1.0267983304165509, -0.2609212204625536, 0.0422502664880359,
                       0.3251963484652335, -1.2457346349152956, -0.5581757509858384])


.. GENERATED FROM PYTHON SOURCE LINES 141-149

5) Propagation of the initial state:

After defining integration time span and initial conditions, the `propagate` method must be
called to propagate the equations of motion. Here we will propagate both 6-dimensional and
42-dimensional equations starting from the same values for `state0` and `t_span`. Note that in
the second case, if a 6-dimensional state is passed to the `propagate` method, the last is
automatically concatenated with the initial STM (flattened 6x6 identity matrix) to obtain a
42-dimensional state consistent with the chosen equations.

.. GENERATED FROM PYTHON SOURCE LINES 149-153

.. code-block:: default


    t_vec, state_vec, _, _ = prop.propagate([et0, et_final], state0)
    t_vec42, state_vec42, _, _ = prop42.propagate([et0, et_final], state0)


.. GENERATED FROM PYTHON SOURCE LINES 154-156

At this point we can verify that `state_vec` coincides with the first 6 rows of `state_vec42`,
`t_vec` coincides with `t_vec42` and the final epochs matches the ones defined above:

.. GENERATED FROM PYTHON SOURCE LINES 156-166

.. code-block:: default


    np.testing.assert_allclose(state_vec, state_vec42[:, :6], rtol=0.0, atol=2e-6)
    np.testing.assert_array_equal(t_vec, t_vec42)

    utc_final = sp.et2utc(t_vec[-1] / MOON_OMEGA_MEAN, 'C', 3)
    utc_final42 = sp.et2utc(t_vec42[-1] / MOON_OMEGA_MEAN, 'C', 3)

    np.testing.assert_equal(utc_final, '2020 JUN 28 19:00:00.000')
    np.testing.assert_equal(utc_final42, '2020 JUN 28 19:00:00.000')


.. GENERATED FROM PYTHON SOURCE LINES 167-176

6) Plot:

Finally, we can directly plot the obtained trajectory in the J2000 inertial frame centered at
the Earth center in which initial conditions and states time series are expressed.

In this example we will also retrieve the position of the Moon with respect to the frame origin
with an explicit call to the CSPICE function spkpos_c and display its trajectory together with
the propagated state. The function will return position time series expressed in `km` that must
be scaled by `MOON_SMA` to be consistent with the units of `state_vec` and `state_vec42`.

.. GENERATED FROM PYTHON SOURCE LINES 176-180

.. code-block:: default


    state_moon, _ = sp.spkpos('MOON', t_vec / MOON_OMEGA_MEAN, 'J2000', 'NONE', 'EARTH')
    state_moon /= MOON_SMA


.. GENERATED FROM PYTHON SOURCE LINES 181-183

Once the position time series of the Moon w.r.t. the Earth have been computed, we can proceed
plotting both trajectories in the same figure:

.. GENERATED FROM PYTHON SOURCE LINES 183-191

.. code-block:: default


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], color='r', label='propagated state')
    ax.plot(state_moon[:, 0], state_moon[:, 1], state_moon[:, 2], color='b', label='Moon\'s orbit')
    ax.scatter(0., 0., 0., color='k', label='Earth')
    decorate_3d_axes(ax, 'Propagated state and Moon\'s orbit in J2000 frame centered at the Earth', '-')
    plt.show()


.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.000 seconds)


.. _sphx_glr_download_examples_ephemeris_ephemeris_state0_propagation.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: ephemeris_state0_propagation.py <ephemeris_state0_propagation.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: ephemeris_state0_propagation.ipynb <ephemeris_state0_propagation.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
