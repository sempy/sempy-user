{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Propagation of an initial state in the ephemeris model.\n\nThis example demonstrates how it is possible to propagate a known initial state in the ephemeris\nforce model. It makes use of the `Ephemeris` class to define an object that contains the celestial\nbodies included in the model and the `EphemerisPropagator` class to propagate an available state\nbetween two given epochs.\n\n@author: Alberto FOSSA'\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "1) Import statements:\n\nwe will start importing all necessary modules and classes to define our `Ephemeris` and\n`EphemerisPropagator` objects and compute the initial and final epochs.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport spiceypy as sp\nimport matplotlib.pyplot as plt\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.ephemeris import Ephemeris\nfrom sempy.core.propagation.ephemeris_propagator import EphemerisPropagator\nfrom sempy.core.init.constants import MOON_SMA, MOON_OMEGA_MEAN\nfrom sempy.core.plotting.simple.utils import decorate_3d_axes\nfrom sempy.core.init.load_kernels import load_kernels\n\nload_kernels()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "2) Creation of an `Ephemeris` object:\n\nOur instance of `Ephemeris` class will contain information on the celestial bodies to be\nincluded in the force model. The object is instantiated passing as argument a tuple of `Primary`\nobjects corresponding to the above mentioned bodies.\nIt will then store information on their names, NAIF IDs and standard gravitational parameters.\n\nNote: for bodies other than the Sun, the Earth and the Moon the body is substituted with the\nbarycenter of the corresponding planetary system (e.g. Jupiter is replaced with Jupiter\nbarycenter and so on). See the class documentation for details on why it is needed.\n\nIn this example, we will add the Earth, the Moon, the Sun and Jupiter to our model:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN, Primary.JUPITER))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "3) Creation of an `EphemerisPropagator` object:\n\nThis object contains information on the ephemeris model (i.e. celestial bodies to be taken into\naccount) to be used to integrate the equations of motion as well as on the reference frame in\nwhich the input and output states are expressed.\n\nRegarding the reference, the inertial frame J2000 (or EME2000) is selected as default. This\nchoice can be overridden passing a different value for the optional parameter `ref` to the class\nconstructor. For the origin, by default it will coincide with the center of the body that has\nbeen passed first to the `Ephemeris` class constructor. This choice can be overridden passing\nthe optional input argument `obs` to the class constructor. Its value must be a `Primary` object\ncontained in the `Ephemeris` object. In the following we will pass `Primary.EARTH`. This will\nnot modify the default choice but demonstrates how to do so if needed.\n\n`EphemerisPropagator` allows also to select between 6-dimensional equations, in which only the\ninitial state is propagated, and 42-dimensional equations, in which the State Transition Matrix\n(STM) is propagated together with the former state. Unless explicitly set with the optional\ninput argument `with_stm=True`, 6-dimensional equations are chosen.\n\nGoing into more technical details, the propagator supports integration in both dimensional and\nnon-dimensional units of time, length and velocity. By default, dimensional units consistent\nwith the ones used by the SPICE Toolkit are used. They are seconds for time and kilometers\nfor length.\n\nIf the propagation (and thus the initial state and epoch passed to the propagate method) has\nto be performed in non-dimensional units, optional input parameters must be passed to the class\nconstructor to define the characteristic time and length for adimensionalization.\n\nIn this example we will scale the equations using the semi-major axis and mean motion of the\nMoon's orbit about the Earth to obtain the characteristic time and length `t_c`, `l_c`.\n\nWe will finally ask to compute the states time series on 2000 discrete points equally spaced in\ntime between the specified initial and final epochs. This is achieved passing the extra argument\n`time_steps=2000`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "prop = EphemerisPropagator(eph, obs=Primary.EARTH, t_c=1. / MOON_OMEGA_MEAN, l_c=MOON_SMA,\n                           time_steps=2000)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "As described above, `prop` object will propagate an initial state properly scaled in the inertial\nframe J2000 centered at the Earth center. Gravitational attraction of the Earth, Moon, Sun and\nJupiter are taken into account to compute the instantaneous acceleration at each propagation\nstep. Their position relative to the frame origin (the Earth) is retrieved from the JPL\nPlanetary and Lunar Ephemerides DE430 using the CSPICE function `spkgps_c`.\n\nIf we are interested in propagating not only the state but also its STM, we can create a second\ninstance of `EphemerisPropagator` in which we explicitly pass `with_stm=True`:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "prop42 = EphemerisPropagator(eph, with_stm=True, t_c=1. / MOON_OMEGA_MEAN, l_c=MOON_SMA,\n                             time_steps=2000)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "4) Initial conditions and integration time span:\n\nOnce the propagator has been instantiated, an initial state is propagated simply invoking its\n`propagate` method whose input parameters are the aforementioned state and the time span over\nwhich the integration of the equation of motion has to be performed.\n\nSince no scaling of the initial conditions is performed by this method, both initial state and\ntime span must be expressed either in dimensional units of `km`, `km/s` and `s` or in\nnon-dimensional ones to be consistent with the previously defined characteristic time `t_c`\nand length `l_c`.\n\nBy convention, SPICE kernels providing information on the Planetary and Lunar Ephemerides express\ntime in Ephemeris Time (ET) also referred to as Barycentric Dynamical Time (TDB). ET corresponds\nto the number of ephemeris seconds past J2000, i.e. past 01 January 2000 12:00:00.000 TDB.\nIf using dimensional equations, the `propagate` method takes as input a time span expressed in\nthe same units (i.e. ephemeris seconds past J2000) which must be scaled by `t_c` if\nnon-dimensional equations are selected.\n\nConversion between ET and Coordinated Universal Time (UTC) expressed in calendar format\n(and vice-versa) is performed with the CSPICE functions `str2et` and `et2utc` respectively.\n\nIn this example, the initial and final epochs are set equal to 01 June 2020 12:00:00.000 UTC\nand 28 June 2020 19:00:00.000 UTC to cover roughly one sidereal period of the Moon.\nThe corresponding epochs in ET are then computed as follows. Note that since we want to\npropagate the equations of motion in non-dimensional units, the outputs of `str2et`\n(ET in seconds) must be divided by `t_c` (or multiplied by its reciprocal `MOON_OMEGA_MEAN`).\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "et0 = sp.str2et('2020 JUN 01 12:00:00.000') * MOON_OMEGA_MEAN\net_final = sp.str2et('2020 JUN 28 19:00:00.000') * MOON_OMEGA_MEAN"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The initial state `state0` has been obtained performing a differential correction procedure on a\nseries of patch points belonging to an L2 Southern Halo orbit of vertical extension 30000 km\ninitially computed in the Circular Restricted Three-Body Problem approximation. Its components\nare already scaled by `t_c` and `l_c`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "state0 = np.array([-1.0267983304165509, -0.2609212204625536, 0.0422502664880359,\n                   0.3251963484652335, -1.2457346349152956, -0.5581757509858384])"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "5) Propagation of the initial state:\n\nAfter defining integration time span and initial conditions, the `propagate` method must be\ncalled to propagate the equations of motion. Here we will propagate both 6-dimensional and\n42-dimensional equations starting from the same values for `state0` and `t_span`. Note that in\nthe second case, if a 6-dimensional state is passed to the `propagate` method, the last is\nautomatically concatenated with the initial STM (flattened 6x6 identity matrix) to obtain a\n42-dimensional state consistent with the chosen equations.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_vec, state_vec, _, _ = prop.propagate([et0, et_final], state0)\nt_vec42, state_vec42, _, _ = prop42.propagate([et0, et_final], state0)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "At this point we can verify that `state_vec` coincides with the first 6 rows of `state_vec42`,\n`t_vec` coincides with `t_vec42` and the final epochs matches the ones defined above:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "np.testing.assert_allclose(state_vec, state_vec42[:, :6], rtol=0.0, atol=2e-6)\nnp.testing.assert_array_equal(t_vec, t_vec42)\n\nutc_final = sp.et2utc(t_vec[-1] / MOON_OMEGA_MEAN, 'C', 3)\nutc_final42 = sp.et2utc(t_vec42[-1] / MOON_OMEGA_MEAN, 'C', 3)\n\nnp.testing.assert_equal(utc_final, '2020 JUN 28 19:00:00.000')\nnp.testing.assert_equal(utc_final42, '2020 JUN 28 19:00:00.000')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "6) Plot:\n\nFinally, we can directly plot the obtained trajectory in the J2000 inertial frame centered at\nthe Earth center in which initial conditions and states time series are expressed.\n\nIn this example we will also retrieve the position of the Moon with respect to the frame origin\nwith an explicit call to the CSPICE function spkpos_c and display its trajectory together with\nthe propagated state. The function will return position time series expressed in `km` that must\nbe scaled by `MOON_SMA` to be consistent with the units of `state_vec` and `state_vec42`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "state_moon, _ = sp.spkpos('MOON', t_vec / MOON_OMEGA_MEAN, 'J2000', 'NONE', 'EARTH')\nstate_moon /= MOON_SMA"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once the position time series of the Moon w.r.t. the Earth have been computed, we can proceed\nplotting both trajectories in the same figure:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = plt.figure()\nax = fig.add_subplot(111, projection='3d')\nax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], color='r', label='propagated state')\nax.plot(state_moon[:, 0], state_moon[:, 1], state_moon[:, 2], color='b', label='Moon\\'s orbit')\nax.scatter(0., 0., 0., color='k', label='Earth')\ndecorate_3d_axes(ax, 'Propagated state and Moon\\'s orbit in J2000 frame centered at the Earth', '-')\nplt.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}