
.. DO NOT EDIT.
.. THIS FILE WAS AUTOMATICALLY GENERATED BY SPHINX-GALLERY.
.. TO MAKE CHANGES, EDIT THE SOURCE PYTHON FILE:
.. "examples/ephemeris/halo_orbit_correction_2.py"
.. LINE NUMBERS ARE GIVEN BELOW.

.. only:: html

    .. note::
        :class: sphx-glr-download-link-note

        Click :ref:`here <sphx_glr_download_examples_ephemeris_halo_orbit_correction_2.py>`
        to download the full example code

.. rst-class:: sphx-glr-example-title

.. _sphx_glr_examples_ephemeris_halo_orbit_correction_2.py:


Correction of an Earth-Moon L2 southern Halo orbit in the Sun-Earth-Moon ephemeris model.
=========================================================================================

This example demonstrate how to transition a periodic orbit generated in the CR3BP model into a
higher fidelity N-body ephemeris model using differential correction techniques based on multiple
shooting procedure.

The target orbit is an Earth-Moon L2 southern Halo orbit defined in the Earth-Moon CR3BP and
transitioned into the Sun-Earth-Moon ephemeris model. The following steps are required:

1. | Initialization of the CR3BP structure and the Halo orbit.
2. | Interpolation and sampling of the Halo orbit in the CR3BP force model.
3. | Coordinate transformation from CR3BP synodic to J2000 inertial frame centered
   | at the Earth's center.
4. | Multiple shooting procedure in J2000 to obtain a continuous trajectory in
   | the Sun-Earth-Moon ephemeris model.
5. | Coordinate transformation from J2000 inertial to instantaneous Earth-Moon barycenter
   | centered rotating frame.
6. | Display of the initial CR3BP orbit and corrected trajectory in a rotating frame.

@author: Alberto FOSSA'

.. GENERATED FROM PYTHON SOURCE LINES 27-29

We will start importing all the modules and classes needed to complete the aforementioned steps
and the SPICE kernels required by the change of coordinates and differential correction routines.

.. GENERATED FROM PYTHON SOURCE LINES 30-48

.. code-block:: default


    import numpy as np
    import spiceypy as sp

    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.init.ephemeris import Ephemeris
    from sempy.core.orbits.halo import Halo
    from sempy.core.init.load_kernels import load_kernels

    from sempy.core.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
    from sempy.core.diffcorr.multiple_shooting import MultipleShooting

    import matplotlib.pyplot as plt
    from sempy.core.plotting.simple.utils import decorate_3d_axes

    load_kernels()


.. GENERATED FROM PYTHON SOURCE LINES 49-51

Once the import step is completed, we can move on defining the dynamical models of interest:
Earth-Moon CR3BP and Sun-Earth-Moon ephemeris model.

.. GENERATED FROM PYTHON SOURCE LINES 52-56

.. code-block:: default


    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))


.. GENERATED FROM PYTHON SOURCE LINES 57-63

The third step consist in the initialization, interpolation and sampling of the target Halo
orbit in the Earth-Moon CR3BP. Cr3bpOrbit class methods are available for all above
mentioned steps. An Earth-Moon L2 southern Halo orbit with vertical extension of 30000 km
is chosen in this case. After the initial interpolation, samples are collected over 10
revolutions of the CR3BP orbit. A uniform period fraction sampling with 50 samples per orbit
is selected in this example.

.. GENERATED FROM PYTHON SOURCE LINES 64-72

.. code-block:: default


    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=60e3)
    halo.interpolation()

    nb_revs = 10
    nb_patch_rev = 50
    t_patch, state_patch = halo.sampling(nb_revs, nb_patch_rev)


.. GENERATED FROM PYTHON SOURCE LINES 73-87

After computing the required patch points in the CR3BP synodic frame, the last must be
expressed in an Earth-centered J2000 inertial frame to perform the correction in the
Sun-Earth-Moon ephemeris model.

Transformation from synodic to inertial J2000 requires first the definition of an initial
epoch, selected to be on June 18, 2020 at 12:00:00.000 UTC. The calendar date must be
converted into the corresponding ephemeris time ET (ephemeris seconds past J2000) to be used
as input parameter for the coordinate transformation functions. This is done invoking the
SPICE routine `str2et`.

Secondly, the output ephemeris time and patch points states expressed in the inertial frame
are adimensionalized using the characteristic time and length of the initial CR3BP structure,
defined as the reciprocal of the mean motion of the primary and their corresponding distance
expressed in seconds and kilometers respectively. These quantities are defined below.

.. GENERATED FROM PYTHON SOURCE LINES 88-99

.. code-block:: default


    t_c = cr3bp.T / 2.0 / np.pi  # characteristic time in seconds
    l_c = cr3bp.L  # characteristic length in kilometers

    et0 = sp.str2et('2025 JUN 21 11:00:06.000')  # initial epoch in ephemeris seconds
    et0 -= halo.T * t_c #initial pruning


    t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,
                                                        adim=True, bary_from_spice=True)


.. GENERATED FROM PYTHON SOURCE LINES 100-109

The differential correction routines based on multiple shooting techniques and implemented
in SEMPY are made available to the end user by the MultipleShooting class.
To instantiate a new MultipleShooting object, it is required to specify the ephemeris model
in which the correction will be performed and possibly several optional input parameters
such as characteristic quantities for adimensionalization of the Equations of Motion,
propagator settings and acceptable tolerance under which the iterative procedure is stopped.
An additional flags determines if the constraint on epoch continuity must be added to the
problem formulation to improve converge. If set to True, the correct method must be called
with the variable time options set also to True (the default in this case).

.. GENERATED FROM PYTHON SOURCE LINES 110-113

.. code-block:: default


    multiple_shooting = MultipleShooting(ephemeris, epoch_constr=True, t_c=t_c, l_c=l_c)


.. GENERATED FROM PYTHON SOURCE LINES 114-119

Once a MultipleShooting object has been instantiated, the initial patch points expressed
in the J2000 inertial frame can be corrected using the correct method of the former object.
If only patch points time and states are passed as inputs, no additional constraints other
than states continuity and (optionally) epoch continuity are enforced during the multiple
shooting procedure. An example call is demonstrated below.

.. GENERATED FROM PYTHON SOURCE LINES 120-126

.. code-block:: default


    print('\nStart of the multiple shooting procedure\n')
    t_corr_j2000, state_corr_j2000, _ = \
        multiple_shooting.correct(t_patch_j2000, state_patch_j2000, t_fix=nb_patch_rev)
    print('\nEnd of the multiple shooting procedure')


.. GENERATED FROM PYTHON SOURCE LINES 127-134

After the differential correction procedure has ended, the resulting patch points time and
states are transitioned back into an instantaneous Earth-Moon barycenter centered rotating
frame to be more easily visualized together with the initial reference orbit. This is
accomplished in a similar way as for the inverse transformation.

Moreover, if a variable time correction procedure has been chosen, an updated initial
epoch UTC and calendar format can be retrieved as well using the SPICE routine `et2utc`.

.. GENERATED FROM PYTHON SOURCE LINES 135-147

.. code-block:: default


    # pruning of state and time vectors (to delete first and last revolution)
    t_corr_j2000 = t_corr_j2000[nb_patch_rev:-nb_patch_rev]
    state_corr_j2000 = state_corr_j2000[nb_patch_rev:-nb_patch_rev]

    et0_corr = t_corr_j2000[0] * t_c  # corrected initial epoch in ephemeris seconds
    utc0_corr = sp.et2utc(et0_corr, 'C', 5)  # corrected initial epoch in UTC and calendar format
    print(f"\nCorrected initial epoch: {utc0_corr} UTC")

    t_corr_syn, state_corr_syn = j2000_to_synodic(t_corr_j2000, state_corr_j2000, et0_corr, cr3bp,
                                                  cr3bp.m1, adim=True, bary_from_spice=True)


.. GENERATED FROM PYTHON SOURCE LINES 148-150

Finally, the initial reference orbit defined in the CR3BP model and the corrected patch
points can be displayed together in a rotating frame that approximates the CR3BP synodic one.

.. GENERATED FROM PYTHON SOURCE LINES 151-169

.. code-block:: default


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2], color='b',
            label='Halo orbit')
    ax.scatter(cr3bp.m2_pos[0], 0., 0., color='k', label='Moon')
    ax.scatter(cr3bp.l2.position[0], 0., 0., color='m', label='L2')
    ax.plot(state_corr_syn[:, 0],
            state_corr_syn[:, 1],
            state_corr_syn[:, 2],
            color='r',
            label='Corrected trajectory')
    decorate_3d_axes(ax, 'EM L2 southern Halo corrected in the Sun-Earth-Moon ephemeris model, '
                         'synodic frame', '-')
    plt.show()




.. GENERATED FROM PYTHON SOURCE LINES 171-203

.. code-block:: default


    # saving the ephemerids in a file
    t_corr_j2000_dim = t_corr_j2000 * t_c
    t_corr_j2000_UTC = np.transpose(sp.et2utc(t_corr_j2000_dim, 'ISOC', 5))
    print(f"\nCut initial epoch: {t_corr_j2000_UTC[nb_patch_rev]} UTC")


    state_corr_j2000_dim = state_corr_j2000
    state_corr_j2000_dim[:, 0:3] = state_corr_j2000[:, 0:3]*cr3bp.L
    state_corr_j2000_dim[:, 3:6] = state_corr_j2000[:, 3:6]*cr3bp.L/t_c

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    ax2.scatter(0, 0., 0., color='k', label='Earth')
    # ax2.scatter(cr3bp.l2.position[0], 0., 0., color='m', label='L2')
    ax2.plot(state_corr_j2000_dim[:, 0],
             state_corr_j2000_dim[:, 1],
             state_corr_j2000_dim[:, 2],
             color='r',
             label='Corrected trajectory')
    decorate_3d_axes(ax2, 'EM L2 southern Halo corrected in the Sun-Earth-Moon ephemeris model, '
                          'inertial frame', '-')
    plt.show()

    data = np.c_[t_corr_j2000_UTC, t_corr_j2000_dim, state_corr_j2000_dim]

    datafile_path = "ephemerids_HaloL2_2025.txt"
    with open(datafile_path, 'w+') as datafile_id:
        np.savetxt(datafile_id, data,  fmt="%s")

    # We will conclude unloading the kernel pool using the SPICE routine kclear.
    sp.kclear()


.. rst-class:: sphx-glr-timing

   **Total running time of the script:** ( 0 minutes  0.000 seconds)


.. _sphx_glr_download_examples_ephemeris_halo_orbit_correction_2.py:


.. only :: html

 .. container:: sphx-glr-footer
    :class: sphx-glr-footer-example



  .. container:: sphx-glr-download sphx-glr-download-python

     :download:`Download Python source code: halo_orbit_correction_2.py <halo_orbit_correction_2.py>`



  .. container:: sphx-glr-download sphx-glr-download-jupyter

     :download:`Download Jupyter notebook: halo_orbit_correction_2.ipynb <halo_orbit_correction_2.ipynb>`


.. only:: html

 .. rst-class:: sphx-glr-signature

    `Gallery generated by Sphinx-Gallery <https://sphinx-gallery.github.io>`_
