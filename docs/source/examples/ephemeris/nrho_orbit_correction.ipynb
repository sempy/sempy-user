{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Correction of an Earth-Moon L2 southern NRHO orbit in the Sun-Earth-Moon ephemeris model.\n\nThis example demonstrate how to transition a periodic orbit generated in the CR3BP model into a\nhigher fidelity N-body ephemeris model using differential correction techniques based on multiple\nshooting procedure.\n\nThe target orbit is an Earth-Moon L2 southern NRHO orbit defined in the Earth-Moon CR3BP and\ntransitioned into the Sun-Earth-Moon ephemeris model. The following steps are required:\n\n1. | Initialization of the CR3BP structure and the NRHO orbit.\n2. | Interpolation and sampling of the NRHO orbit in the CR3BP force model.\n3. | Coordinate transformation from CR3BP synodic to J2000 inertial frame centered\n   | at the Earth's center.\n4. | Multiple shooting procedure in J2000 to obtain a continuous trajectory in\n   | the Sun-Earth-Moon ephemeris model.\n5. | Propagation of the previously corrected patch points.\n6. | Coordinate transformation from J2000 inertial to instantaneous Earth-Moon barycenter\n   | centered rotating frame.\n7. | Display of the initial CR3BP orbit and corrected trajectory in a rotating frame.\n\n@author: Alberto FOSSA'\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We will start importing all the modules and classes needed to complete the aforementioned steps\nand the SPICE kernels required by the change of coordinates and differential correction routines.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport spiceypy as sp\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.cr3bp import Cr3bp\nfrom sempy.core.init.ephemeris import Ephemeris\nfrom sempy.core.init.load_kernels import load_kernels\nfrom sempy.core.orbits.nrho import NRHO\n\nfrom sempy.core.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic\nfrom sempy.core.diffcorr.multiple_shooting import MultipleShooting\nfrom sempy.core.propagation.patch_points_propagator import PatchPointsPropagator\n\nimport matplotlib.pyplot as plt\nfrom sempy.core.plotting.simple.utils import decorate_3d_axes\n\nload_kernels()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once the import step is completed, we can move on defining the dynamical models of interest:\nEarth-Moon CR3BP and Sun-Earth-Moon ephemeris model.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)\nephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The third step consist in the initialization, interpolation and sampling of the target\nNRHO orbit in the Earth-Moon CR3BP.\nCr3bpOrbit class methods are available for all above mentioned steps.\nAn Earth-Moon L2 southern NRHO orbit with periselene radius of 5930 km is chosen in this case.\nAfter the initial interpolation, samples are collected over 10 revolutions of the CR3BP orbit.\nA uniform period fraction sampling with 50 samples per orbit is selected in this example.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Rpdim=5930)\nnrho.interpolation()\n\nnb_revs = 10\nnb_patch_rev = 50\nt_patch, state_patch = nrho.sampling(nb_revs, nb_patch_rev)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "After computing the required patch points in the CR3BP synodic frame, the last must be\nexpressed in an Earth-centered J2000 inertial frame to perform the correction in the\nSun-Earth-Moon ephemeris model.\n\nTransformation from synodic to inertial J2000 requires first the definition of an initial\nepoch, selected to be on June 18, 2020 at 12:00:00.000 UTC. The calendar date must be\nconverted into the corresponding ephemeris time ET (ephemeris seconds past J2000) to be used\nas input parameter for the coordinate transformation functions. This is done invoking the\nSPICE routine `str2et`.\n\nSecondly, the output ephemeris time and patch points states expressed in the inertial\nframe are adimensionalized using the characteristic time and length of the initial CR3BP\nstructure, defined as the reciprocal of the mean motion of the primary and their\ncorresponding distance expressed in seconds and kilometers respectively.\nThese quantities are defined below.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "et0 = sp.str2et('2020 JUN 18 12:00:00.000')  # initial epoch in ephemeris seconds\n\nt_c = cr3bp.T / 2.0 / np.pi  # characteristic time in seconds\nl_c = cr3bp.L  # characteristic length in kilometers\n\nt_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,\n                                                    adim=True, bary_from_spice=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The differential correction routines based on multiple shooting techniques and implemented\nin SEMPY are made available to the end user by the MultipleShooting class.\nTo instantiate a new MultipleShooting object, it is required to specify the ephemeris model\nin which the correction will be performed and possibly several optional input parameters\nsuch as characteristic quantities for adimensionalization of the Equations of Motion,\npropagator settings and acceptable tolerance under which the iterative procedure is stopped.\nAn additional flags determines if the constraint on epoch continuity must be added to the\nproblem formulation to improve converge. If set to True, the correct method must be called\nwith the variable time options set also to True (the default in this case).\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "multiple_shooting = MultipleShooting(ephemeris, epoch_constr=True, t_c=t_c, l_c=l_c,\n                                     precision=1e-6, maxiter=20)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once a MultipleShooting object has been instantiated, the initial patch points expressed\nin the J2000 inertial frame can be corrected using the correct method of the former object.\nIf only patch points time and states are passed as inputs, no additional constraints other\nthan states continuity and (optionally) epoch continuity are enforced during the multiple\nshooting procedure. An example call is demonstrated below.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "print('\\nStart of the multiple shooting procedure\\n')\nt_corr_j2000, state_corr_j2000, _ = multiple_shooting.correct(t_patch_j2000, state_patch_j2000)\nprint('\\nEnd of the multiple shooting procedure')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "After the differential correction procedure has ended, a smooth trajectory is computed via\nexplicit propagation of the corrected patch point states across the corresponding time\nintervals. This is accomplished instantiating a new PatchPointPropagator object with the\ndynamical model and characteristic quantities already defined for the multiple shooting\nprocedure.\nIts propagate method takes as input parameters a series of patch point times and states and\nreturns a smooth trajectory obtained via explicit integration of the associated Equations of\nMotion between subsequent nodes. Optional parameters expose to the end user the possibility\nto prune from the final solution a given number of initial and final patch points as well as\ndetermine the number of discrete points in which the solution is returned.\n\nIn this example, the first and last revolutions are cut from the integrated trajectory and\na sampling grid with 100 nodes between subsequents patch points is chosen.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "patch_points_prop = PatchPointsPropagator(ephemeris, t_c=t_c, l_c=l_c, time_steps=100)\nt_vec_j2000, state_vec_j2000 = \\\n    patch_points_prop.propagate(t_corr_j2000, state_corr_j2000, prune_first=nb_patch_rev,\n                                prune_last=nb_patch_rev)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Once the corrected patch points have been propagated to obtain a smooth trajectory,\nthe last is transitioned back into an instantaneous Earth-Moon barycenter centered rotating\nframe to be more easily visualized together with the initial reference orbit.\nThis is accomplished in a similar way as for the inverse transformation.\n\nMoreover, if a variable time correction procedure has been chosen, an updated initial\nepoch UTC and calendar format can be retrieved as well using the SPICE routine `et2utc`.\nNote that its value will be different for the first corrected patch point and the first state\nin the propagated trajectory if a positive integer has been passed as input to the\nprune_first argument of the PatchPointsPropagator propagate method.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "et0_corr = t_vec_j2000[0] * t_c  # corrected initial epoch in ephemeris seconds\nutc0_corr = sp.et2utc(et0_corr, 'C', 5)  # corrected initial epoch in UTC and calendar format\nprint(f\"\\nCorrected initial epoch: {utc0_corr} UTC\")\n\nt_vec_syn, state_vec_syn = j2000_to_synodic(t_vec_j2000, state_vec_j2000, et0_corr, cr3bp,\n                                            cr3bp.m1, adim=True, bary_from_spice=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Finally, the initial reference orbit defined in the CR3BP model and the corrected patch\npoints can be displayed together in a rotating frame that approximates the CR3BP synodic one.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = plt.figure()\nax = fig.add_subplot(111, projection='3d')\nax.plot(nrho.state_vec[:, 0], nrho.state_vec[:, 1], nrho.state_vec[:, 2], color='b',\n        label='NRHO orbit')\nax.plot(state_vec_syn[:, 0], state_vec_syn[:, 1], state_vec_syn[:, 2], color='r',\n        label='Corrected trajectory', linewidth = 0.5)\nax.scatter(cr3bp.m2_pos[0], 0., 0., color='k', label='Moon')\nax.scatter(cr3bp.l2.position[0], 0., 0., color='m', label='L2')\ndecorate_3d_axes(ax, 'EM L2 southern NRHO corrected in the Sun-Earth-Moon ephemeris model', '-')\nplt.rc('legend', fontsize = 6)\n\nplt.legend()\nplt.show()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We will conclude unloading the kernel pool using the SPICE routine kclear.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "sp.kclear()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}