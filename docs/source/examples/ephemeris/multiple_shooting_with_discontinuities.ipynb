{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Multiple Shooting procedure with velocity discontinuities.\n\nThis example demonstrates how to include velocity discontinuities at patch points while correcting\na given trajectory in the N-body ephemeris model.\n\nFor a more in-depth introduction on the initialization and exploitation of multiple shooting\nprocedures see also the other example scripts in this folder.\n\n@author: Alberto FOSSA'\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Import statements\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import numpy as np\nimport spiceypy as sp\nimport matplotlib.pyplot as plt\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.cr3bp import Cr3bp\nfrom sempy.core.init.ephemeris import Ephemeris\nfrom sempy.core.init.load_kernels import load_kernels\nfrom sempy.core.orbits.halo import Halo\nfrom sempy.core.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic\nfrom sempy.core.diffcorr.multiple_shooting import MultipleShooting\nfrom sempy.core.propagation.patch_points_propagator import PatchPointsPropagator\nfrom sempy.core.plotting.simple.utils import decorate_3d_axes\n\nload_kernels()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Definition of the dynamical models\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # CR3BP structure for the Earth-Moon system\nt_c = cr3bp.T / 2.0 / np.pi  # characteristic time [s]\nl_c = cr3bp.L  # characteristic length [km]\n\n# N-body ephemeris model for the Sun-Earth-Moon system\n\neph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Initialization and interpolation of an Earth-Moon L2 southern Halo orbit in the CR3BP\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)  # Az of 30000 km\nhalo.interpolation()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Orbit sampling\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "nb_revs = 10  # total number of revolutions\nnb_patch_rev = 20  # number of patch points per revolution\nt_patch, state_patch = halo.sampling(nb_revs, nb_patch_rev)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Change of coordinates from synodic to inertial J2000\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "et0 = sp.str2et('01 JUL 2020 12:00:00.000')  # start epoch in ephemeris time [s]\nt_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,\n                                                    adim=True, bary_from_spice=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Definition of impulsive manoeuvres\n\nWhile correcting a series of patch points to obtain a continuous trajectory, impulsive\nmanoeuvres can be taken into account as velocity discontinuities allowed at one or more\npatch points. The `correct` method of the `MultipleShooting` class accepts to different\nkeywords to specify their location: `delta_v` and `constr_mask`.\n\n`delta_v` must be a single integer or a tuple of integers, each of them specifying the\nindex or indexes of the patch points on which the continuity constraint on the three\nvelocity components has to be removed. For example, passing `delta_v=(5, 10)` will result\nin two impulsive manoeuvres carried out at the 6th and 11th patch points respectively\n(remember that in Python indexes start from zero and not one).\n\n`constr_mask` must be a boolean mask (e.g. an array of zeros and ones) with size\n`6 * (number of patch points - 1)` which allows a more fine tuning of the enforced and\nreleased continuity constraints. In fact, the mask allows to control each component of the\nerror vector independently, thus potentially preventing impulsive manoeuvres to be carried\nout in a given direction. Zeros correspond to imposed constraints and ones to released ones\n(impulsive manoeuvres). Be aware that in principle this second option allows to specify\nposition discontinuities if ones are passed for the corresponding constraints.\n\nAs a first example, impulsive manoeuvres carried out at the 21st and 41st patch points\n(second and fourth periselene passage) can be specified as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "delta_v = (20, 40)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Then, using the second available keyword the same manoeuvres can be declared with the boolean\nmask defined below. Care must be taken since continuity constraints applied at the ith patch\npoint correspond to rows `[6 * (i - 1), 6 * i - 1]` in the error vector `fx_vec`.\nAs a consequence, indexes `(117, 118, 119)` and `(237, 238, 239)` must be fixed in this example.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "constr_mask = np.zeros(6 * (nb_revs * nb_patch_rev - 1), dtype=np.bool)\nconstr_mask[6 * 19 + np.arange(3, 6)] = 1\nconstr_mask[6 * 39 + np.arange(3, 6)] = 1"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Differential correction\n\nOnce the manoeuvres has been defined, the multiple shooting algorithm is initialized as usual\nand the two keywords has just to be passed to its `correct` method. Note that `delta_v` and\n`constr_mask` are mutually exclusive.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "multi_shoot = MultipleShooting(eph, epoch_constr=True, t_c=t_c, l_c=l_c)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "A first solution is computed with the `delta_v` keyword as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_corr1_j2000, state_corr1_j2000, sol1 = \\\n    multi_shoot.correct(t_patch_j2000, state_patch_j2000, delta_v=delta_v)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The second one is then computed with the `constr_mask` keyword as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_corr2_j2000, state_corr2_j2000, sol2 = \\\n    multi_shoot.correct(t_patch_j2000, state_patch_j2000, constr_mask=constr_mask)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Maneuvers analysis\n\nOnce the differential correction has ended, it is possible to retrieve the components and\nmagnitude of the specified manoeuvres from the error vector included in the solution\ndictionaries `sol1` and `sol2`.\nIn order to do so, it is sufficient to extract the array components corresponding to the\ncontinuity constraints on the velocities that have not been enforced during the correction\nprocedure. The velocity components are then given by the opposite of the former values.\nThe corresponding indexes are also stored in the same dictionaries with keywords\n`fx_free_idx`. It is then convenient to reshape the output array as a three column matrix\nsuch that each column correspond to a different velocity component (vx, vy, vz) and each\nrow to a different manoeuvre.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "dv_comps1_j2000 = - sol1['fx_vec'][sol1['fx_free_idx']].reshape(sol1['fx_free_idx'].size // 3, 3)\ndv_comps2_j2000 = - sol2['fx_vec'][sol2['fx_free_idx']].reshape(sol2['fx_free_idx'].size // 3, 3)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The dV magnitude applied at each patch point is then retrieved computing the norm of the\nprevious vectors by rows.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "dv_mag1 = np.linalg.norm(dv_comps1_j2000, axis=1)\ndv_mag2 = np.linalg.norm(dv_comps2_j2000, axis=1)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Finally, a visual check verifies that the two problem formulations described above are\nequivalent and lead to the same results\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "v_c = 1e3 * l_c / t_c  # characteristic speed [m/s]\n\nprint(f\"\\n{'Computed impulsive manoeuvres:':^50s}\\n\")\nprint(f\"{'dV keyword':^20s}{'':5s}{'boolean mask':^20s}\")\nfor i in range(dv_mag1.size):\n    print(f\"{dv_mag1[i] * v_c:20.15f}{'m/s':^5s}{dv_mag2[i] * v_c:20.15f}{'m/s':^5s}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Propagation of the corrected patch points\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "prop = PatchPointsPropagator(eph, t_c=t_c, l_c=l_c, time_steps=100)\nt_prop1_j2000, state_prop1_j2000 = prop.propagate(t_corr1_j2000, state_corr1_j2000)\nt_prop2_j2000, state_prop2_j2000 = prop.propagate(t_corr2_j2000, state_corr2_j2000)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Change of coordinates from J2000 inertial to synodic\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "t_prop1, state_prop1 = j2000_to_synodic(t_prop1_j2000, state_prop1_j2000, t_prop1_j2000[0] * t_c,\n                                        cr3bp, cr3bp.m1, True, True)\nt_prop2, state_prop2 = j2000_to_synodic(t_prop2_j2000, state_prop2_j2000, t_prop2_j2000[0] * t_c,\n                                        cr3bp, cr3bp.m1, True, True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Plots\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig = plt.figure()\nax = fig.add_subplot(111, projection='3d')\nax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2],\n        color='k', label='Halo')\nax.plot(state_prop1[:, 0], state_prop1[:, 1], state_prop1[:, 2],\n        color='b', label='corrected dV key')\nax.plot(state_prop2[:, 0], state_prop2[:, 1], state_prop2[:, 2], '--',\n        color='r', label='corrected mask')\ndecorate_3d_axes(ax, 'Corrected Halo orbit with velocity discontinuities', '-')\nplt.show()\n\nsp.kclear()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}