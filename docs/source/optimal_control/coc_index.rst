Change of Coordinates Module
============================

.. toctree::
   :maxdepth: 1

.. automodule:: sempy.optimal_control.coc
   :members:

Obital elements to state vector
-------------------------------
.. automodule:: sempy.optimal_control.coc.coe2svvec
   :members:
   :show-inheritance:
   :inherited-members:


Rotation Matrices
------------------
.. automodule:: sempy.optimal_control.coc.rotmat
   :members:
   :show-inheritance:
   :inherited-members:


Rotation Matrix Vector
----------------------
.. automodule:: sempy.optimal_control.coc.rotmatvec
   :members:
   :show-inheritance:
   :inherited-members:
