Optimization Module
===================

.. toctree::
   :maxdepth: 1

.. automodule:: sempy.optimal_control.src
   :members:

Collocation Script
------------------
.. automodule:: sempy.optimal_control.src.collocation
   :members:
   :show-inheritance:
   :inherited-members:


Constraints Script
------------------
.. automodule:: sempy.optimal_control.src.constraints
   :members:
   :show-inheritance:
   :inherited-members:


Cost Script
-----------
.. automodule:: sempy.optimal_control.src.cost
   :members:
   :show-inheritance:
   :inherited-members:

Optimization Script
-------------------
.. automodule:: sempy.optimal_control.src.optimization
   :members:
   :show-inheritance:
   :inherited-members:

Problem Script
--------------
.. automodule:: sempy.optimal_control.src.problem
   :members:
   :show-inheritance:
   :inherited-members:

Pseudospectrals Script
----------------------
.. automodule:: sempy.optimal_control.src.pseudospectrals
   :members:
   :show-inheritance:
   :inherited-members:

Scaling Script
--------------
.. automodule:: sempy.optimal_control.src.scaling
   :members:
   :show-inheritance:
   :inherited-members:

Solver Script
-------------
.. automodule:: sempy.optimal_control.src.solver
   :members:
   :show-inheritance:
   :inherited-members:

Transcription Script
--------------------
.. automodule:: sempy.optimal_control.src.transcription
   :members:
   :show-inheritance:
   :inherited-members:

Utils Script
------------
.. automodule:: sempy.optimal_control.src.utils
   :members:
   :show-inheritance:
   :inherited-members:

