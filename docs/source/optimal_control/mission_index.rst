Mission Module
==============

.. toctree::
   :maxdepth: 1

.. automodule:: sempy.optimal_control.mission
   :members:

Class Mission
-------------
.. automodule:: sempy.optimal_control.mission.mission
   :members:
   :show-inheritance:
   :inherited-members:


Halo to NRHO Transfer
---------------------
.. automodule:: sempy.optimal_control.mission.HALO2NRHO
   :members:
   :show-inheritance:
   :inherited-members:


CR3BP Propagation Function
--------------------------
.. automodule:: sempy.optimal_control.mission.LEO2NRHO.cr3bp_propagation
   :members:
   :show-inheritance:
   :inherited-members:

Earth to Manifold Transfer
--------------------------
.. automodule:: sempy.optimal_control.mission.LEO2NRHO.earth2manifold
   :members:
   :show-inheritance:
   :inherited-members:

Earth to Manifold Initial Guess Trajectory
------------------------------------------
.. automodule:: sempy.optimal_control.mission.LEO2NRHO.earth2manifold_guess
   :members:
   :show-inheritance:
   :inherited-members:

LEO to GEO Transfer
-------------------
.. automodule:: sempy.optimal_control.mission.LEO2NRHO.LEO2GEO
   :members:
   :show-inheritance:
   :inherited-members:

Orbit Raising Function
----------------------
.. automodule:: sempy.optimal_control.mission.LEO2NRHO.orbit_raising_cr3bp
   :members:
   :show-inheritance:
   :inherited-members:

