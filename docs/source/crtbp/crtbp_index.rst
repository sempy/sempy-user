Crtbp Subpackage
================


Jacobi Module
---------------

.. toctree::
   :maxdepth: 1
.. automodule:: sempy.core.crtbp.jacobi
   :members:
   :show-inheritance:
   :inherited-members:

Richardson Coefficients Module
------------------------------

.. toctree::
   :maxdepth: 1
.. automodule:: sempy.core.crtbp.richardson_coefficients
   :members:
   :show-inheritance:
   :inherited-members:
