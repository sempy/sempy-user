Propagation Subpackage
=======================

Propagator Module
------------------
.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.propagation.propagator
   :members:
   :show-inheritance:
   :inherited-members:

.. automodule:: sempy.core.propagation.patch_points_propagator
   :members:
   :show-inheritance:
   :inherited-members:
