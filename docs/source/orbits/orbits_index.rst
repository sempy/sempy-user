Orbits Subpackage
=================
.. toctree::
   :maxdepth: 1

Orbits Inheritance Diagram
----------------------------

.. inheritance-diagram:: sempy.core.orbits.halo.Halo sempy.core.orbits.nrho.NRHO sempy.core.orbits.dro.Dro
    sempy.core.orbits.vlyap.Vlyap sempy.core.orbits.plyap.Plyap sempy.core.orbits.thirdorder_orbit.ThirdOrderOrbit
   :parts: 1

Orbit Module
-----------------

.. automodule:: sempy.core.orbits.orbit
   :members:
   :show-inheritance:
   :inherited-members:

CrtbpOrbit Module
------------------

.. automodule:: sempy.core.orbits.crtbporbit
   :members:
   :show-inheritance:
   :inherited-members:

Halo Orbit Module
------------------

.. automodule:: sempy.core.orbits.halo
   :members:
   :show-inheritance:
   :inherited-members:

Near Rectilinear Halo Orbit Module
----------------------------------

.. automodule:: sempy.core.orbits.nrho
   :members:
   :show-inheritance:
   :inherited-members:

Distant Retrograde Orbit Module
--------------------------------

.. automodule:: sempy.core.orbits.dro
   :members:
   :show-inheritance:
   :inherited-members:

Vertical Lyapunov Orbit Module
--------------------------------

.. automodule:: sempy.core.orbits.vlyap
   :members:
   :show-inheritance:
   :inherited-members:

Planar Lyapunov Orbit Module
--------------------------------

.. automodule:: sempy.core.orbits.plyap
   :members:
   :show-inheritance:
   :inherited-members:

Third-Order Orbit Module
--------------------------------

.. automodule:: sempy.core.orbits.thirdorder_orbit
   :members:
   :show-inheritance:
   :inherited-members:

Butterfly Orbit Module
----------------------

.. automodule:: sempy.core.orbits.butterfly
   :members:
   :show-inheritance:
   :inherited-members:
