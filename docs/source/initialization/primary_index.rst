Primary Module
==================
.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.init.primary
    :members:
    :show-inheritance:
    :inherited-members:
