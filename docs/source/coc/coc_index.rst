Change of Coordinates Subpackage
================================
.. toctree::
   :maxdepth: 1

Change from intertial frame to local frames
-------------------------------------------

.. automodule:: sempy.core.coc.local_orbital_frames
   :members:
   :show-inheritance:
   :inherited-members:

Change Synodic/Inertial frame
-----------------------------

.. automodule:: sempy.core.coc.synodic_inertial
   :members:
   :show-inheritance:
   :inherited-members:

Change from Synodic frame to J2000 Instantaneous Inertial frames
----------------------------------------------------------------

.. automodule:: sempy.core.coc.synodic_j2000
   :members:
   :show-inheritance:
   :inherited-members:

Translation between J2000 Instantaneous Inertial frames
-------------------------------------------------------

.. automodule:: sempy.core.coc.translation_j2000
   :members:
   :show-inheritance:
   :inherited-members:

Two vector frame
----------------

.. automodule:: sempy.core.coc.two_vectors_frame
   :members:
   :show-inheritance:
   :inherited-members:
