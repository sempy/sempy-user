"""
Halo orbit manifolds.
======================================

This script demonstrates how to generate manifold tubes associated to a given Halo in the
Earth-Moon system.

Allowed tubes are all possible combinations of stability (stable/unstable) and way
(interior/exterior) and can be computed one by one or all together in a single function call.
For each tube, several positions along the orbit must be specified to compute a discrete set of
manifold branches that approximates the continuous tube.

Each branch is described by an object of the `CrtbpOrbit` inner class `ManifoldBranch` and
automatically appended to the orbit's attribute `manifolds` when one of the two methods
`CrtbpOrbit.all_manifolds_computation()` or `CrtbpOrbit.single_manifold_computation()` is called.

@author: Alberto FOSSA', Paolo GUARDABASSO
"""

# %%
# Firstly, all required modules and classes must be imported:
import matplotlib.pyplot as plt
import numpy as np

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.plotting.util import sphere, set_axes_equal
from sempy.core.diffcorr.ode_event import generate_impact_event


def set_axes_lim_labels_title(ax, title):
    """Convenience function to set the axes limits, labels and the figure title. """
    ax.set_xlabel('x [-]')
    ax.set_ylabel('y [-]')
    ax.set_zlabel('z [-]')
    ax.set_title(title)
    set_axes_equal(ax)


# %%
# Initialize the CR3BP
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# Generate the terminal impact event function in a format compatible with the propagation solver
# "solve_ivp":
m2_impact_event = generate_impact_event(cr3bp, "m2")

# %%
# Then, the reference Halo orbit is instantiated and interpolated as described in
# `Halo interpolation` example script:

halo_1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=60e3)
halo_1.interpolation()

# %%
# To compute all manifolds tubes at once, we make use of the `CrtbpOrbit` class method
# `all_manifolds_computation()`. Its usage is as simple as:

halo_1.all_manifolds_computation(11, 20., max_internal_steps=2_000_000)

# Here, it is specified to compute 11 branches for each manifold equally spaced along the orbit.
# Moreover, each branch will be propagated for an amount of time equal to 20 in non-dimensional
# units.
#
# If a long propagation time is sought, numerical difficulties might be encountered by the
# integration routines due to an imposed upper limit on the available integrator time steps.
# To increase this limit, the `max_internal_steps` parameter should be set equal to an
# higher value w.r.t. the default one specified in `default.ini`.
#
# If specific departure states are sought for the branches, the first positional argument can be
# replaced by a Numpy array containing those positions expressed as fractions of the orbit's
# period between 0 and 1. For example, passing ``numpy.array([0.25, 0.75])`` will compute two
# branches per tube that depart the orbit at 1/4 and 3/4 of its period.
#
# A single manifold tube can also be computed with a function call similar to the above where
# two positional arguments must be prepended to specify the manifold stability
# (stable/unstable) and way (interior/exterior). In this example, a Lunar impact event is passed
# to the propagator, so that manifolds that impact the surface are not propagated further. Also,
# the state is propagated together with the State Transition Matrix through the parameter
# 'with_stm'.

halo_2 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=50)
halo_2.interpolation()

halo_2.single_manifold_computation('unstable', 'interior', 11, 20., time_steps=20000,
                                   events=m2_impact_event, with_stm=True)

# %%
# Finally, if we are interested in a specific branch we can make use of the inner class
# `ManifoldBranch` to compute it directly.
#
# Nota: differently as before, the computed branch is not automatically stored in the
# orbit class attribute `manifolds`.

manifold_branch = halo_2.ManifoldBranch(halo_2, 'stable', 'exterior')
manifold_branch.computation(0.25, 20.)

# where the two positional arguments in `ManifoldBranch.crtbp()` refer to the branch
# position and propagation time respectively.


# %% Plots
# The obtained trajectories are then plotted as follows:
fig1 = plt.figure()
ax1 = fig1.add_subplot(111, projection='3d')

# Plot Moon
x_2, y_2, z_2 = sphere(100, cr3bp.R2 / cr3bp.L, cr3bp.m2_pos)
ax1.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)

# Plot libration point
pos_l2 = np.array(cr3bp.l2.position)
ax1.scatter(pos_l2[0], pos_l2[1], pos_l2[2], marker='*', color='g')


ax1.plot(halo_1.state_vec[:, 0], halo_1.state_vec[:, 1], halo_1.state_vec[:, 2], 'k')
for kind in halo_1.manifolds:
    for branch in halo_1.manifolds[kind]:
        COLOR = 'r' if branch.stability == 'unstable' else 'g'
        ax1.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2],
                 color=COLOR)
set_axes_lim_labels_title(ax1, 'Stable/unstable, interior/exterior manifold tubes')

# %%
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')

# Plot Moon
ax2.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)

ax2.plot(halo_2.state_vec[:, 0], halo_2.state_vec[:, 1], halo_2.state_vec[:, 2], color='b')
for branch in halo_2.manifolds['unstable_interior']:
    ax2.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2], color='r')
ax2.plot(manifold_branch.state_vec[:, 0], manifold_branch.state_vec[:, 1],
         manifold_branch.state_vec[:, 2], color='g')
set_axes_lim_labels_title(ax2, 'Unstable interior tube, stable exterior branch')
plt.show()
