"""
Initialization of the Sun-Earth CR3BP system.
=============================================

@author: Edgar PEREZ, Alberto FOSSA'
"""

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.plyap import Plyap
import time

start_time = time.perf_counter()

cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)

orbit1 = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)

orbit2 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.12)

cr3bp_nro = Cr3bp(Primary.EARTH, Primary.MOON)
orbit3 = NRHO(cr3bp_nro, cr3bp_nro.l2, NRHO.Family.northern, Azdim=12000)

orbit4 = Plyap(cr3bp_nro, cr3bp_nro.l2, Plyap.Family.planar, Axdim=12000)

print('\n', '                   New CRTBP                    ', '\n')
classVar = vars(cr3bp)
print('\n\n'.join("%s = %s" % item for item in classVar.items()))
print("--- %s seconds ---" % (time.perf_counter() - start_time))
