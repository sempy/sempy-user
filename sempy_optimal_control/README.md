# Optimal Control module of SEMpy

In order to run the Optimal Control Module of SEMpy, you will have to download manually the following libraries: IPOPT, BLAS, Lapack, ASL, Metis, Mumps, IPyOPT, Cppad_py, and HSL (optionnal).

Here are the related links:

- IPOPT : https://coin-or.github.io/Ipopt/INSTALL.html
- BLAS : https://github.com/coin-or-tools/ThirdParty-Blas/blob/master/INSTALL.BLAS
- Lapack : https://github.com/coin-or-tools/ThirdParty-Lapack/blob/master/INSTALL.LAPACK
- ASL : https://github.com/coin-or-tools/ThirdParty-ASL
- Metis : https://github.com/coin-or-tools/ThirdParty-Metis/blob/stable/2.0/INSTALL.Metis
- Mumps : https://github.com/coin-or-tools/ThirdParty-Mumps
- IPyOPT : https://gitlab.com/g-braeunlich/ipyopt
- Cppad_py : https://bradbell.github.io/cppad_py/doc/xsrst/setup_py.html
- HSL : https://www.hsl.rl.ac.uk/ipopt/