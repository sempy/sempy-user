import numpy as np
import cppad_py

import matplotlib.pyplot as plt

from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.mission.mission import Mission

from sempy.optimal_control.src.optimization import Optimization



class HALO2NRHO(Mission):

    def __init__(self, spacecraft_prm, halo_prm, nrho_prm, **kwargs):
        """ `HALO2NRHO` class implements a low-thrust transfer between a Halo and a NRHO orbits
            using trajectory stacking method to generate the initial guess.
            
            cf. Robert Pritchett, Kathleen Howell, and Daniel Grebow. “Low-Thrust Transfer Design Based 
            on Collocation Techniques: Applications in the Restricted Three-Body Problem”. 

            Parameters
            ----------
            spacecraft_prm : dict
                Spacecraft parameters dictionnary
            halo_prm : dict
                Halo orbit parameters namely : library point (l1 or l2), family (southern or northern) and
                either Orbit extension or Jacobi constant
            nrho_prm : dict
                NRHO orbit parameters namely : library point (l1 or l2), family (southern or northern) and
                either Periselene radius, altitude, vertical extension or Jacobi constant

            Attributs
            ---------
            halo : Halo orbit object
                Departure Halo orbit object
            nrho : NRHO orbit object
                Arrival NRHO orbit object


        """

        Mission.__init__(self, spacecraft_prm, n_states=7, n_controls=4, n_path_con=2, n_event_con=13,
                         n_f_par=0, **kwargs)

        # Generation of the departure and arrival orbits
        self.halo_generation(halo_prm)
        self.nrho_generation(nrho_prm)

    def halo_generation(self, halo_prm):
        """ Generation of the departure Halo orbit using the parameters given  
            by the user.

            Parameters
            ----------
            halo_prm : dict
                Halo orbit parameters namely : library point (l1 or l2), family (southern or northern) and
                either Orbit extension or Jacobi constant

        """

        # Libration point definition
        libration_point = self.cr3bp.l1 if halo_prm['libration_point'] == 'l1' else self.cr3bp.l2

        # Halo family
        family = Halo.Family.southern if halo_prm['family'] == 'southern' else Halo.Family.northern

        if hasattr(halo_prm, 'Azdim'):
            self.halo = Halo(cr3bp, libration_point, family, Azdim=halo_prm['Azdim'])
        elif hasattr(halo_prm, 'Cjac'):
            self.halo = Halo(cr3bp, libration_point, family, Cjac=halo_prm['Cjac'])
        else:
            raise ValueError("Neither orbit extension or Jacobi constant has been setted")

    def nrho_generation(self, nrho_prm):
        """ Generation of the arrival NRHO orbit using the parameters given  
            by the user.

            Parameters
            ----------
            nrho_prm : dict
                NRHO orbit parameters namely : library point (l1 or l2), family (southern or northern) and
                either Periselene radius, altitude, vertical extension or Jacobi constant

        """

        # Libration point definition
        libration_point = self.cr3bp.l1 if nrho_prm['libration_point'] == 'l1' else self.cr3bp.l2

        # NRHO family
        family = NRHO.Family.southern if nrho_prm['family'] == 'southern' else NRHO.Family.northern

        if hasattr(nrho_prm, 'Azdim'):
            self.nrho = NRHO(cr3bp, libration_point, family, Azdim=nrho_prm['Azdim'])
        elif hasattr(nrho_prm, 'Cjac'):
            self.nrho = Halo(cr3bp, libration_point, family, Cjac=halo_prm['Cjac'])
        elif hasattr(nrho_prm, 'Rpdim'):
            self.nrho = Halo(cr3bp, libration_point, family, Rpdim=halo_prm['Rpdim'])
        elif hasattr(nrho_prm, 'Altpdim'):
            self.nrho = Halo(cr3bp, libration_point, family, Altpdim=halo_prm['Altpdim'])
        else:
            raise ValueError("Neither periselene radius or altitude, vertical extension, \
                                Jacobi constant or orbit's period")

    def set_boundaries(self):
        """ Setting of the states, controls, free-parameters, initial and final times
                boundaries """

        # X (syn. fram) [-]
        self.low_bnd.states[0] = 1 - self.cr3bp.mu - 1.0
        self.upp_bnd.states[0] = 1 - self.cr3bp.mu + 1.0

        # Y (syn. fram) [-]
        self.low_bnd.states[1] = - 1.0
        self.upp_bnd.states[1] = 1.0

        # Z (syn. fram) [-]
        self.low_bnd.states[2] = - 1.0
        self.upp_bnd.states[2] = 1.0

        # Vx (syn. fram) [-]
        self.low_bnd.states[3] = - 2
        self.upp_bnd.states[3] = 2

        # Vy (syn. fram) [-]
        self.low_bnd.states[4] = - 2
        self.upp_bnd.states[4] = 2

        # Vz (syn. fram) [-]
        self.low_bnd.states[5] = - 2
        self.upp_bnd.states[5] = 2

        # Set the mass limits [-]
        self.low_bnd.states[6] = 0
        self.upp_bnd.states[6] = self.spacecraft.mass0

        # Set the thrust limits [-]
        self.low_bnd.controls[0] = self.spacecraft.thrust_min 
        self.upp_bnd.controls[0] = self.spacecraft.thrust_max 

        # Set the limits for controls ux, uy, uz
        for i in [1, 2, 3]:
            self.low_bnd.controls[i] = -1
            self.upp_bnd.controls[i] = 1


        # Set the times limits
        t_init  = self.initial_guess.time[0]
        t_final = self.initial_guess.time[-1]

        self.low_bnd.ti = self.upp_bnd.ti = t_init

        self.low_bnd.tf = 0.5 * t_final
        self.upp_bnd.tf = 1.5 * t_final

    def path_constraints(self, states, controls, f_par):
        """ Computation of the path constraints 

            Parameters
            ----------
            states : ndarray
                Matrix of the states
            controls : ndarray 
                Matrix of the controls
            f_par : array
                Array of the free parameters

            Returns
            -------
            constraints : ndarray
                Path constraints matrix
        """
        constraints = np.ndarray(
            (self.prm['n_path_con'], self.prm['n_nodes']), dtype=cppad_py.a_double)

        x, y, z = states[:3]
        ux, uy, uz = controls[1:]

        u2 = ux*ux + uy*uy + uz*uz

        r_nrm = np.sqrt( (x-(1-self.cr3bp.mu))*(x-(1-self.cr3bp.mu)) + y*y + z*z )

        constraints[0] = u2
        constraints[1] = r_nrm

        return constraints

    def set_path_constraints_boundaries(self):
        """ Setting of the path constraints boundaries """
        self.low_bnd.path[0] = self.upp_bnd.path[0] = 1

        self.low_bnd.path[1] = 0.0007804
        self.upp_bnd.path[1] = 2


    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Computation of the end point cost (Mayer term) 

            Parameters
            ----------
            ti : float
                Initial time value
            xi : array
                Initial states array
            tf : float
                Final time value
            xf : array
                Final states array
            f_prm : array
                Free parameters array

            Returns
            -------
            float
                Mayer term value

        """
        mf = xf[-1]
        return - mf / 1000

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
        """ Computation of the events constraints 

            Parameters
            ----------
            xi : array
                Array of states at initial time
            ui : array
                Array of controls at initial time
            xf : array
                Array of states at final time
            uf : array
                Array of controls at final time
            ti : float
                Value of initial time
            tf : float
                Value of final time
            f_prm : array
                Free parameters array

            Returns
            -------
            constraints : array
                Array of the event constraints

        """

        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial states
        x0, y0, z0, vx0, vy0, vz0, m0 = self.initial_guess.states[:, 0]

        # Final states (minus mass)
        x_f, y_f, z_f, vx_f, vy_f, vz_f = self.initial_guess.states[:-1, -1]

        # Position (init) [-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # Velocity (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # Mass (init) [-]
        constraints[6] = m0 - xi[6]

        # Position (final) [-]
        constraints[7] = x_f - xf[0]
        constraints[8] = y_f - xf[1]
        constraints[9] = z_f - xf[2]

        # Velocity (final) [-]
        constraints[10] = vx_f - xf[3]
        constraints[11] = vy_f - xf[4]
        constraints[12] = vz_f - xf[5]
        

        return constraints

    def set_events_constraints_boundaries(self):
        """ Setting of the events constraints boundaries """

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

        # Position (final) [-]
        self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
        self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
        self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

        # Velocity (final) [-]
        self.low_bnd.event[10] = self.upp_bnd.event[10] = 0
        self.low_bnd.event[11] = self.upp_bnd.event[11] = 0
        self.low_bnd.event[12] = self.upp_bnd.event[12] = 0

    def set_initial_guess(self):
        """ Setting of the initial guess for the states, controls, free-parameters
                and time grid """

        # Shifts for both the Halo and the NRHO orbits
        shift_h = 750
        shift_n = 100
        
        # Initialization of the states, controls and time
        states = np.ndarray(shape=(7, self.prm['n_nodes']))
        controls = np.ndarray(shape=(4, self.prm['n_nodes']))
        time = np.zeros(self.prm['n_nodes'])

        halo_st = self.halo.state_vec.transpose()[:6]
        nrho_st = self.nrho.state_vec.transpose()[:6]

        halo_st = np.roll(halo_st, shift_h, axis=1)
        nrho_st = np.roll(nrho_st, shift_n, axis=1)

        n_pts = len(self.halo.t_vec)
        step = int(n_pts / (self.prm['n_nodes'] / 2))

        # Set the states
        states[:6, :int(self.prm['n_nodes']/2)] = halo_st[:, 0:-1:step]
        states[:6, int(self.prm['n_nodes']/2):] = nrho_st[:, 0:-1:step]

        states[6] = self.spacecraft.mass0 * np.ones(self.prm['n_nodes'])

        # Set the controls
        controls[0] = np.zeros(self.prm['n_nodes'])

        v_norm = np.sqrt( states[3]*states[3] + states[4]*states[4] + states[5]*states[5])
        controls[1] = states[3] / v_norm
        controls[2] = states[4] / v_norm
        controls[3] = states[5] / v_norm

        # Set the time
        time = np.linspace(0, 4, self.prm['n_nodes'])

        # Set the initial guess
        self.initial_guess.states = states
        self.initial_guess.controls = controls
        self.initial_guess.time = time

if __name__ == '__main__':

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'mass_dry': m_dry, 'thrust_max': thrust_max, 'thrust_min': thrust_min, 'isp': Isp}


    # Departure orbit
    halo_prm = {'libration_point': 'l1', 'family': 'southern', 'Cjac': 3.05}

    # Final orbit
    nrho_prm = {'libration_point': 'l2', 'family': 'southern', 'Rpdim': 3000}


    # Number of nodes
    n_nodes = 200

    # Instantiation of the problem
    problem = NRHO2NRHO(spacecraft_prm, halo_prm, nrho_prm, n_nodes=n_nodes)

    # Instantiation of the optimization
    optimization = Optimization(problem=problem)

    # Launch of the optimization
    optimization.launch_optimization()
    