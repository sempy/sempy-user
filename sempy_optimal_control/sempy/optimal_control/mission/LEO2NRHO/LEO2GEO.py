import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.mission.mission import Mission
from sempy.optimal_control.coc.rotmat import lci2syn, syn2lci
from sempy.optimal_control.coc.rotmatvec import polar2eq_vec
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.src import transcription, solver, utils
import sempy.core.init.constants as cst 

from sempy.optimal_control.mission.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising

class LEO2GEO(Mission):

    def __init__(self, spc_prm, orb_prm, orbit_raising_dt, bounds_prm, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Mission.__init__(self, spc_prm, bounds_prm, n_event_con=7, n_path_con=1, n_f_par=0, **kwargs)

        self.orbit_raising_dt = orbit_raising_dt
        self.orb_prm = orb_prm

        # Computation of the initial guess
        self.set_initial_guess()

        # Computation of states and controls boundaries
        self.set_states_controls_bnd()

        # Event constraints boundaries
        self.set_events_con_bnd()

        # Path constraints boundaries
        self.set_path_con_bnd()

        # User-defined functions
        self.set_user_defined_func()

    def set_user_defined_func(self):
        """ Sets the user_defined function and Variables them to the 
            optimization (Problem) class """

        self.pbm.end_point_cost = self.cost_mayer  # Cost function
        self.pbm.event_constraints = self.event_con  # Event constraints function
        self.pbm.path_constraints = self.path_con  # Path constraints function
        self.pbm.dynamics = self.dynamics  # Dynamics function

    def cost_mayer(self, ti, xi, tf, xf, f_prm):
        """ Compute the Mayer term of the cost function """
        d_v = xf[0:3]-self.target_state[0:3]
        # Maximization of the final mass
        return np.linalg.norm(d_v)

    def path_con(self, states, controls, f_prm):
        constraints = np.ndarray(
            (self.pbm.prm['n_path_con'], self.pbm.prm['n_nodes']), dtype=cppad_py.a_double)

        ux, uy, uz = controls[1:]

        u2 = ux*ux + uy*uy + uz*uz

        constraints[0] = u2

        return constraints

    def set_path_con_bnd(self):
        """ Sets the path constraints boundaries """
        self.pbm.low_bnd.path[0] = self.pbm.upp_bnd.path[0] = 1

    def event_con(self, xi, ui, xf, uf, f_prm, ti, tf):
        constraints = np.ndarray((self.pbm.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        x0, y0, z0, vx0, vy0, vz0 = self.pbm.initial_guess.states[:-1, 0]
        x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.target_state

        # x, y, z (init)[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = xi[6]

        return constraints

    def set_events_con_bnd(self):
        """ Sets the events constraints """

        xi, yi, zi, vxi, vyi, vzi, mi = self.pbm.initial_guess.states[:, 0]

        # Position (init) [-]
        self.pbm.low_bnd.event[0] = self.pbm.upp_bnd.event[0] = 0
        self.pbm.low_bnd.event[1] = self.pbm.upp_bnd.event[1] = 0
        self.pbm.low_bnd.event[2] = self.pbm.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.pbm.low_bnd.event[3] = self.pbm.upp_bnd.event[3] = 0
        self.pbm.low_bnd.event[4] = self.pbm.upp_bnd.event[4] = 0
        self.pbm.low_bnd.event[5] = self.pbm.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.pbm.low_bnd.event[6] = self.pbm.upp_bnd.event[6] = mi

        

    def set_initial_guess(self):
        """ Set the initial guess using the earth2manifold_guess classes
            `Earth2NRHOFixedPositionGuess` and `Earth2NRHOFixedVelocityGuess` """

        prop_time = self.orbit_raising_dt['t_prop']

        # Propagation time [-]
        self.time = np.arange(0, prop_time, prop_time / self.pbm.prm['n_nodes'])

        # Orbit raising object  
        orbit_raising = OrbitRaising(self.cr3bp, self.spacecraft, self.orb_prm, self.time)

        # Initial guess recovery 
        states, controls, time = orbit_raising.get_initial_guess()
        self.pbm.initial_guess.states = states
        self.pbm.initial_guess.controls = controls
        self.pbm.initial_guess.time = time 

if __name__ == '__main__':

    # ____________________________________ Optimization / Transcription Parameters ______________________________________________________

    # Bounds parameters
    bounds_prm = {'x_fct'  : 10, 'y_fct' : 5, 'z_fct' : 5, 
                  'vx_fct' : 5, 'vy_fct': 5, 'vz_fct': 5, 
                  'fuel_prc': 0.0, 'time_bnd': (0, 2)}

    host = 'laptop'

    n_nodes = 200

    linear_solver = 'ma86'
    tr_method = 'hermite-simpson'

    plot_results = True

    explicit_integration = False
    plot_physical_err = False

    options = {'tr_method': tr_method, 'explicit_integration': explicit_integration, 'host': host, 'linear_solver': linear_solver,
                'plot_results': plot_results, 'plot_physical_err': plot_physical_err, 'n_nodes': n_nodes, 'pickle_results': True}

    # ____________________________________ Spacecraft Parameters __________________________________________

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'mass_dry': m_dry, 'thrust_max': thrust_max, 'thrust_min': thrust_min, 'isp': Isp}

    # ____________________________________ Initial Orbit Parameters __________________________________________

    # Departure orbit
    a = 20000   # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58  # inclination [deg]
    W = -300    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 0     # initial true anomaly [deg]

    orbital_prm = {'a': a, 'e': e, 'i': i*np.pi/360, 'W': W*np.pi/360, 'w': w*np.pi/360, 'ta': ta*np.pi/360}

    # ____________________________________ Initial Guess Trajectory __________________________________________

    # Time of propagation of the initial trajectory [-]
    propagation_time = 2.20

    # Data needed for the construction of the initial trajectory
    # (orbit raising)
    orbit_raising_dt = {'t_prop': propagation_time}

    # ____________________________________ Class Instantiation _______________________________________________
    
    leo2geo = LEO2GEO(spacecraft_prm, orbital_prm, orbit_raising_dt, bounds_prm, **options)

    tran = transcription.Transcription(leo2geo.pbm)
    tran.launch()
    
    solv = solver.IPOPT(tran)
    solv.solve(max_iter=10000)