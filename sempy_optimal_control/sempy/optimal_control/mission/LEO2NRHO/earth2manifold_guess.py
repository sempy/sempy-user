import numpy as np
import cppad_py

import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.mission.mission import Mission
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.coc.rotmat import eci2syn
import sempy.core.init.constants as cst 

from sempy.optimal_control.mission.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising


class Earth2NRHOGuess(Mission):

    def __init__(self, spacecraft_prm, target_state, orbit_prm, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Mission.__init__(self, spacecraft_prm, **kwargs)

        # Stocking orbital parameters and target states as Variables
        self.orbit_prm = orbit_prm
        self.target_state = target_state

        # Computation of free parameters boundaries
        self.set_free_parameters_bnd()

    def set_free_parameters_bnd(self):
        """ Sets the free parameters boundaries """

        # True anomaly [rad]
        self.low_bnd.f_par[0] = -2*np.pi 
        self.upp_bnd.f_par[0] = 2*np.pi

    def path_constraints(self, states, controls, f_par):
        """ Computation of the path constraints 

            Parameters
            ----------
            states : ndarray
                Matrix of the states
            controls : ndarray 
                Matrix of the controls
            f_par : array
                Array of the free parameters

            Returns
            -------
            constraints : ndarray
                Path constraints matrix
        """
        constraints = np.ndarray(
            (self.prm['n_path_con'], self.prm['n_nodes']), dtype=cppad_py.a_double)

        ux, uy, uz = controls[1:]

        u2 = ux*ux + uy*uy + uz*uz

        constraints[0] = u2

        return constraints

    def set_path_constraints_boundaries(self):
        """ Setting of the path constraints boundaries """
        self.low_bnd.path[0] = self.upp_bnd.path[0] = 1


class Earth2NRHOFixedPositionGuess(Earth2NRHOGuess):

    def __init__(self, spacecraft_prm, target_state, orbit_prm, prop_time, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Earth2NRHOGuess.__init__(self, spacecraft_prm, target_state, orbit_prm, \
            n_states=7, n_controls=4, n_event_con=7, n_path_con=1,  n_f_par=1, **kwargs)

        # Storage of the propagation time for the initial guess
        self.prop_time = prop_time


    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Computation of the end point cost (Mayer term) 

            Parameters
            ----------
            ti : float
                Initial time value
            xi : array
                Initial states array
            tf : float
                Final time value
            xf : array
                Final states array
            f_prm : array
                Free parameters array

            Returns
            -------
            float
                Mayer term value

        """

        # Target position in Synodic frame
        x_tgt, y_tgt, z_tgt = self.target_state[:3]

        # Final position
        x_f, y_f, z_f = xf[:3]

        return np.sqrt((x_f-x_tgt)*(x_f-x_tgt) + (y_f-y_tgt)*(y_f-y_tgt) + (z_f-z_tgt)*(z_f-z_tgt))

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
        """ Computation of the events constraints 

            Parameters
            ----------
            xi : array
                Array of states at initial time
            ui : array
                Array of controls at initial time
            xf : array
                Array of states at final time
            uf : array
                Array of controls at final time
            ti : float
                Value of initial time
            tf : float
                Value of final time
            f_prm : array
                Free parameters array

            Returns
            -------
            constraints : array
                Array of the event constraints

        """
        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial orbit free parameters
        ta = f_prm[0]

        # States in ECI
        r0_eci, v0_eci = coe2sv_vec(self.orbit_prm['a'], self.orbit_prm['e'], self.orbit_prm['i'], self.orbit_prm['W'], self.orbit_prm['w'], [ta], cst.GM_EARTH)
        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0 = eci2syn(np.hstack((r0_eci[0], v0_eci[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

        # x, y, z (init )[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = xi[6]

        return constraints

    def set_events_constraints_boundaries(self):
        """ Setting of the events constraints boundaries """
        xi, yi, zi, vxi, vyi, vzi, mi = self.initial_guess.states[:, 0]

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = mi

    def set_initial_guess(self):
        """ Setting of the initial guess for the states, controls, free-parameters
            and time grid """
        
        # Propagation time [-]
        self.time = np.arange(0, self.prop_time, self.prop_time / self.prm['n_nodes'])

        # Orbit raising object  
        orbit_raising = OrbitRaising(self.cr3bp, self.spacecraft, self.orbit_prm, self.time)

        # Initial guess recovery 
        states, controls, time = orbit_raising.get_initial_guess()
        self.initial_guess.states = states
        self.initial_guess.controls = controls
        self.initial_guess.time = time 
        self.initial_guess.f_prm = np.array([self.orbit_prm['ta']])

class Earth2NRHOFixedVelocityGuess(Earth2NRHOGuess):

    def __init__(self, spacecraft_prm, target_state, orbit_prm, initial_guess, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Earth2NRHOGuess.__init__(self, spacecraft_prm, target_state, orbit_prm, \
            n_states=7, n_controls=4, n_event_con=10, n_path_con=1,  n_f_par=1, **kwargs)

        # Storage of the target states 
        self.target_state = target_state

        # Storage of the initial guess as an attribut
        self.initial_guess = initial_guess

    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Computation of the end point cost (Mayer term) 

            Parameters
            ----------
            ti : float
                Initial time value
            xi : array
                Initial states array
            tf : float
                Final time value
            xf : array
                Final states array
            f_prm : array
                Free parameters array

            Returns
            -------
            float
                Mayer term value

        """
        # Target velocity in Synodic frame
        vx_tgt, vy_tgt, vz_tgt = self.target_state[3:]

        # Final velocity
        vx_f, vy_f, vz_f = xf[3:-1]

        return np.sqrt((vx_f-vx_tgt)*(vx_f-vx_tgt) + (vy_f-vy_tgt)*(vy_f-vy_tgt) + (vz_f-vz_tgt)*(vz_f-vz_tgt))

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
        """ Computation of the events constraints 

            Parameters
            ----------
            xi : array
                Array of states at initial time
            ui : array
                Array of controls at initial time
            xf : array
                Array of states at final time
            uf : array
                Array of controls at final time
            ti : float
                Value of initial time
            tf : float
                Value of final time
            f_prm : array
                Free parameters array

            Returns
            -------
            constraints : array
                Array of the event constraints

        """
        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial orbit free parameters
        ta = f_prm[0]

        # States in ECI
        r0_eci, v0_eci = coe2sv_vec(self.orbit_prm['a'], self.orbit_prm['e'], self.orbit_prm['i'], self.orbit_prm['W'], self.orbit_prm['w'], [ta], cst.GM_EARTH)
        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0 = eci2syn(np.hstack((r0_eci[0], v0_eci[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

        # x, y, z (init )[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = xi[6]

        # x, y, z (final) [-]
        constraints[7] = self.target_state[0] - xf[0]
        constraints[8] = self.target_state[1] - xf[1]
        constraints[9] = self.target_state[2] - xf[2]

        return constraints

    def set_events_constraints_boundaries(self):
        """ Setting of the events constraints boundaries """

        xi, yi, zi, vxi, vyi, vzi, mi = self.initial_guess.states[:, 0]

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = mi

        # Position (final) [-]
        self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
        self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
        self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

    def set_initial_guess(self):
        """ Setting of the initial guess for the states, controls, free-parameters
            and time grid """
        
        self.initial_guess.states = self.initial_guess['states']
        self.initial_guess.controls = self.initial_guess['controls']
        self.initial_guess.time = self.initial_guess['time']
        self.initial_guess.f_prm = np.array([self.orbit_prm['ta']])

if __name__ == '__main__':

    # ____________________________________ Spacecraft Parameters __________________________________________

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'mass_dry': m_dry, 'thrust_max': thrust_max, 'thrust_min': thrust_min, 'isp': Isp}

    # ____________________________________ Initial Orbit Parameters __________________________________________

    # Departure orbit
    a = 42200  # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58 # inclination [deg]
    W = 90      # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 180     # initial true anomaly [deg]

    orbital_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180, 'ta': ta*np.pi/180}

    # ______________________________________ Manifolds Generation _______________________________________________

    # Manifolds 
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    nrho = NRHO(cr3bp, cr3bp.l1, NRHO.Family.southern, Altpdim=6e3)
    nrho.interpolation()

    nrho.single_manifold_computation(stability='stable', way='exterior', theta=[0.48], t_prop=15.15, max_internal_steps=2_000_000)

    # Useful part of the manifold (in percentage)
    useful_prt = 0.8

    # The target state is the beginning of the useful part of the manifold
    target_state = nrho.manifolds['stable_exterior'][0].state_vec[-1, :]


    # ______________________________________ Orbit Raising generation _______________________________________________

    prop_time = 1.93
    time = np.arange(0, prop_time, prop_time / 250)

    # Orbit raising object  
    orbit_raising = OrbitRaising(cr3bp, Spacecraft(**spacecraft_prm), orbital_prm, time)


    plot_trajectory_manifold(orbit_raising.states[0], orbit_raising.states[1], orbit_raising.states[2],
        nrho.manifolds['stable_exterior'][0].state_vec[:, 0],nrho.manifolds['stable_exterior'][0].state_vec[:, 1], 
        nrho.manifolds['stable_exterior'][0].state_vec[:, 2], target_state[0], target_state[1], target_state[2])

    # ________________________________ Fixed Position Initial Guess Generation _______________________________________________

    n_nodes = 250

    problem = Earth2NRHOFixedPositionGuess(spacecraft_prm, target_state, orbital_prm, n_nodes=n_nodes)

    # Instantiation of the optimization
    optimization = Optimization(problem=problem)

    # Launch of the optimization
    optimization.launch_optimization()

    # Recovery of the previous optimization results
    intermediate_states = optimization.results['opt_st']
    intermediate_controls = optimization.results['opt_ct']
    intermediate_time = optimization.results['opt_tm']
    intermediate_par = optimization.results['opt_pr']

    # Updated orbital parameters for initial orbit
    orbital_prm = {'a': a, 'e': e, 'i': intermediate_par[0], 'W': intermediate_par[1], 'w': w*np.pi/180, 'ta': intermediate_par[0]}

    # Optimized initial guess
    initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}

    # ________________________________ Fixed Position & Velocity Initial Guess Generation _______________________________________________

    problem = Earth2NRHOFixedVelocityGuess(spacecraft_prm, target_state, orbital_prm, initial_guess, n_nodes=n_nodes)

    # Instantiation of the optimization
    optimization = Optimization(problem=problem)

    # Launch of the optimization
    optimization.launch_optimization()