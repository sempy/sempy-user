import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.mission.mission import Mission
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.coc.rotmat import eci2syn
from sempy.optimal_control.src import transcription, solver, utils
import sempy.core.init.constants as cst 

from sempy.optimal_control.mission.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising
from sempy.optimal_control.src.manifold_approximation import manifold_approximation
from sempy.optimal_control.mission.LEO2NRHO.earth2manifold_guess import Earth2NRHOFixedPositionGuess, Earth2NRHOFixedVelocityGuess, Earth2NRHOEnergyAdjustement


class Earth2NRHO(Mission):

    def __init__(self, spc_prm, orb_prm, orbit_raising_dt, manifolds_dt, bounds_prm, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Mission.__init__(self, spc_prm, bounds_prm, n_event_con=13, n_path_con=1, n_f_par=3, **kwargs)

        # Storing some parameters
        self.orb_prm = orb_prm                      # Initial orbit parameters
        self.orbit_raising_dt = orbit_raising_dt    # Parameters destinated to the OrbitRaising class
        self.manifolds_dt = manifolds_dt            # Parameters destinated to the cpt_interpolation_coefficients method

        # ==================================================
        # self.mission_analysis()
        # ==================================================

        # Computation of the initial guess
        self.set_initial_guess(spc_prm, bounds_prm, **kwargs)

        # Computation of states and controls boundaries
        self.set_states_controls_bnd()

        # Computation of free parameters boundaries
        self.set_free_parameters_bnd()

        # Event constraints boundaries
        self.set_events_con_bnd()

        # Path constraints boundaries
        self.set_path_con_bnd()

        # User-defined functions
        self.set_user_defined_func()

    def approximated_manifold(self, theta, tau):
        """ Return the states [x, y, z, vx, vy, vz] on the manifold corresponding to the two parameters
            theta and tau """

        # Inputs of the manifold_approximation() function
        cr3bp = self.manifolds_dt['halo'].cr3bp
        C_jac = self.manifolds_dt['halo'].C
        grid = self.manifolds_dt['halo'].manifolds['stable_interior']
        theta_array = self.manifolds_dt['theta_arr']
        tau_array = self.manifolds_dt['tau_arr']

        approx_states = manifold_approximation(cr3bp, C_jac, grid, theta_array, tau_array, theta, tau)

        return approx_states

    def set_user_defined_func(self):
        """ Sets the user_defined function and Variables them to the 
            optimization (Problem) class """

        self.pbm.end_point_cost = self.cost_mayer  # Cost function
        self.pbm.event_constraints = self.event_con  # Event constraints function
        self.pbm.path_constraints = self.path_con  # Path constraints function
        self.pbm.dynamics = self.dynamics  # Dynamics function

    def set_free_parameters_bnd(self):
        """ Sets the free parameters boundaries """
        
        # True anomaly [rad]
        self.pbm.low_bnd.f_par[0] = -2*np.pi 
        self.pbm.upp_bnd.f_par[0] = 2*np.pi

        # Theta [-]
        self.pbm.low_bnd.f_par[1] = min([man_.theta for man_ in self.manifolds_dt['halo'].manifolds['stable_interior']])
        self.pbm.upp_bnd.f_par[1] = max([man_.theta for man_ in self.manifolds_dt['halo'].manifolds['stable_interior']])

        beg_ind = int(self.manifolds_dt['useful_part']*len(self.manifolds_dt['halo'].manifolds['stable_interior'][0].t_vec))

        # Tau [-]
        self.pbm.low_bnd.f_par[2] = min(self.manifolds_dt['tau_arr'][beg_ind:])
        self.pbm.upp_bnd.f_par[2] = max(self.manifolds_dt['tau_arr'][beg_ind:])

    def cost_mayer(self, ti, xi, tf, xf, f_prm):
        """ Compute the Mayer term of the cost function """

        # Maximization of the final mass
        return -xf[-1] / 1000

    def path_con(self, states, controls, f_prm):
        constraints = np.ndarray(
            (self.pbm.prm['n_path_con'], self.pbm.prm['n_nodes']), dtype=cppad_py.a_double)

        ux, uy, uz = controls[1:]

        u2 = ux*ux + uy*uy + uz*uz

        constraints[0] = u2

        return constraints

    def set_path_con_bnd(self):
        """ Sets the path constraints boundaries """
        self.pbm.low_bnd.path[0] = self.pbm.upp_bnd.path[0] = 1

    def event_con(self, xi, ui, xf, uf, f_prm, ti, tf):

        constraints = np.ndarray((self.pbm.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial orbit free parameters
        ta, theta, tau = f_prm

        # States in ECI
        r0_eci, v0_eci = coe2sv_vec(self.orb_prm['a'], self.orb_prm['e'], self.orb_prm['i'], self.orb_prm['W'], self.orb_prm['w'], [ta], cst.GM_EARTH)
        
        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0 = eci2syn(np.hstack((r0_eci[0], v0_eci[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

        # Targeted position, function of theta and tau parameters
        x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.interpolated_manifold(theta, tau)

        # x, y, z (init)[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = xi[6]

        # x, y, z (final)[-]
        constraints[7] = x_tgt - xf[0]
        constraints[8] = y_tgt - xf[1]
        constraints[9] = z_tgt - xf[2]

        # vx, vy, vz (final) [-]
        constraints[10] = vx_tgt - xf[3]
        constraints[11] = vy_tgt - xf[4]
        constraints[12] = vz_tgt - xf[5]

        return constraints

    def set_events_con_bnd(self):
        """ Sets the events constraints """

        xi, yi, zi, vxi, vyi, vzi, mi = self.pbm.initial_guess.states[:, 0]

        # Position (init) [-]
        self.pbm.low_bnd.event[0] = self.pbm.upp_bnd.event[0] = 0
        self.pbm.low_bnd.event[1] = self.pbm.upp_bnd.event[1] = 0
        self.pbm.low_bnd.event[2] = self.pbm.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.pbm.low_bnd.event[3] = self.pbm.upp_bnd.event[3] = 0
        self.pbm.low_bnd.event[4] = self.pbm.upp_bnd.event[4] = 0
        self.pbm.low_bnd.event[5] = self.pbm.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.pbm.low_bnd.event[6] = self.pbm.upp_bnd.event[6] = mi

        # Position (init) [-]
        self.pbm.low_bnd.event[7] = self.pbm.upp_bnd.event[7] = 0
        self.pbm.low_bnd.event[8] = self.pbm.upp_bnd.event[8] = 0
        self.pbm.low_bnd.event[9] = self.pbm.upp_bnd.event[9] = 0

        # Velocity (init) [-]
        self.pbm.low_bnd.event[10] = self.pbm.upp_bnd.event[10] = 0
        self.pbm.low_bnd.event[11] = self.pbm.upp_bnd.event[11] = 0
        self.pbm.low_bnd.event[12] = self.pbm.upp_bnd.event[12] = 0

    def set_initial_guess(self, spc_prm, bounds_prm, **kwargs):
        """ Set the initial guess using the earth2manifold_guess classes
            `Earth2NRHOFixedPositionGuess` and `Earth2NRHOFixedVelocityGuess` """

        # ________________________________ Fixed Position Initial Guess Generation _______________________________________________

        target_state = self.interpolated_manifold(self.manifolds_dt['target_theta'], self.manifolds_dt['target_tau'])

        halo = self.manifolds_dt['halo']

        earth2nrho_fixed_pos = Earth2NRHOFixedPositionGuess(spc_prm, target_state, self.orb_prm, self.orbit_raising_dt['t_prop'], bounds_prm, **kwargs)

        v_f = np.linalg.norm(earth2nrho_fixed_pos.pbm.initial_guess.states[3:6, -1])
        v_f_tgt = np.linalg.norm(target_state[3:])

        print("Init vel. : {} km/s".format(np.linalg.norm(earth2nrho_fixed_pos.pbm.initial_guess.states[3:6, 0])*self.v_ref))
        input()

        print("Final velocity : {}".format(v_f))
        print("Target velocity : {}".format(v_f_tgt))
        # input()

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # plt.plot(earth2nrho_fixed_pos.pbm.initial_guess.states[0], earth2nrho_fixed_pos.pbm.initial_guess.states[1], earth2nrho_fixed_pos.pbm.initial_guess.states[2])
        # for i in range(len(nrho.manifolds['stable_interior'])):
        #     plt.plot(nrho.manifolds['stable_interior'][i].state_vec[:, 0], nrho.manifolds['stable_interior'][i].state_vec[:, 1], nrho.manifolds['stable_interior'][i].state_vec[:, 2])
        # plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')
        # plt.show()

        tran = transcription.Transcription(earth2nrho_fixed_pos.pbm)
        tran.launch()

        solv = solver.IPOPT(tran)
        solv.solve(max_iter=1000)

        # # JACOBI ADJUSTEMENT
        # # ==================

        # # Recovery of the previous optimization results
        # intermediate_states = tran.problem.optimal_states
        # intermediate_controls = tran.problem.optimal_controls
        # intermediate_time = tran.problem.optimal_time
        # intermediate_par = tran.problem.optimal_par

        # vx_f = intermediate_states[3, -1]
        # vy_f = intermediate_states[4, -1]
        # vz_f = intermediate_states[5, -1]
        # v_norm_f = np.sqrt(vx_f*vx_f + vy_f*vy_f + vz_f*vz_f)
        # v_tgt_norm = np.sqrt(target_state[3]*target_state[3] + target_state[4]*target_state[4]+target_state[5]*target_state[5])

        # # print("Final velocity : {}".format(v_norm_f))
        # # print("Target velocity : {}".format(v_tgt_norm))

        # # input()

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # plt.plot(intermediate_states[0], intermediate_states[1], intermediate_states[2])
        # for i in range(len(nrho.manifolds['stable_interior'])):
        #     plt.plot(nrho.manifolds['stable_interior'][i].state_vec[:, 0], nrho.manifolds['stable_interior'][i].state_vec[:, 1], nrho.manifolds['stable_interior'][i].state_vec[:, 2])
        # plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')
        # plt.show()

        # # Updated orbital parameters for initial orbit
        # self.orb_prm['ta'] = intermediate_par[0]

        # # Optimized initial guess
        # initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}

        # # ________________________________ Fixed Position & Velocity Initial Guess Generation _______________________________________________

        # earth2nrho_jac_adj = Earth2NRHOEnergyAdjustement(spc_prm, target_state, self.orb_prm, bounds_prm, initial_guess, **kwargs)

        # tran = transcription.Transcription(earth2nrho_jac_adj.pbm)
        # tran.launch()

        # solv = solver.IPOPT(tran)
        # solv.solve(max_iter=2500)


        # # =========================

        # Recovery of the previous optimization results
        intermediate_states = tran.problem.optimal_states
        intermediate_controls = tran.problem.optimal_controls
        intermediate_time = tran.problem.optimal_time
        intermediate_par = tran.problem.optimal_par

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        plt.plot(intermediate_states[0], intermediate_states[1], intermediate_states[2])
        for i in range(len(halo.manifolds['stable_interior'])):
            plt.plot(halo.manifolds['stable_interior'][i].state_vec[:, 0], halo.manifolds['stable_interior'][i].state_vec[:, 1], halo.manifolds['stable_interior'][i].state_vec[:, 2])
        plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')
        plt.show()

        # Updated orbital parameters for initial orbit
        self.orb_prm['ta'] = intermediate_par[0]

        # Optimized initial guess
        initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}

        # ________________________________ Fixed Position & Velocity Initial Guess Generation _______________________________________________

        earth2nrho_fixed_vel = Earth2NRHOFixedVelocityGuess(spc_prm, target_state, self.orb_prm, bounds_prm, initial_guess, **kwargs)

        tran = transcription.Transcription(earth2nrho_fixed_vel.pbm)
        tran.launch()

        solv = solver.IPOPT(tran)
        solv.solve(max_iter=2000)

        # Recovery of the states, controls, time and free parameters values
        states_ = tran.problem.optimal_states
        controls_ = tran.problem.optimal_controls
        time_ = tran.problem.optimal_time
        par_ = tran.problem.optimal_par

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        plt.plot(states_[0], states_[1], states_[2])
        for i in range(len(halo.manifolds['stable_interior'])):
            plt.plot(halo.manifolds['stable_interior'][i].state_vec[:, 0], halo.manifolds['stable_interior'][i].state_vec[:, 1], halo.manifolds['stable_interior'][i].state_vec[:, 2])
        plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')
        plt.show()

        # Set of the initial guess
        self.pbm.initial_guess.states = states_
        self.pbm.initial_guess.controls = controls_
        self.pbm.initial_guess.time = time_
        self.pbm.initial_guess.f_prm[0] = par_

        self.pbm.initial_guess.f_prm[1] = self.manifolds_dt['target_theta']
        self.pbm.initial_guess.f_prm[2] = self.manifolds_dt['target_tau']

        

    # def test_interp(self):
    #     theta = 0.484   
    #     tau = 14.955013274090698

    #     x, y, z, vx, vy, vz = self.interpolated_manifold(theta, tau)

    #     fig = plt.figure()
    #     ax = fig.add_subplot(111, projection='3d')
    #     plt.plot([x], [y], [z], 'ro')
        
    #     for i in range(len(self.manifolds_dt['nrho'].manifolds['stable_interior'])):
    #         plt.plot(self.manifolds_dt['nrho'].manifolds['stable_interior'][i].state_vec[:, 0], self.manifolds_dt['nrho'].manifolds['stable_interior'][i].state_vec[:, 1], 
    #             self.manifolds_dt['nrho'].manifolds['stable_interior'][i].state_vec[:, 2], color='blue')

    #     plt.show()

    def mission_analysis(self):

        ta = [-180]
        # ta = [-360, -180, 0, 180, 200, 220, 250, 300, 360] # MODIFIER AUSSI EN DESSOUS !!!!!! /!\
        # ta = np.arange(-360, -340, 5)

        # Propagation time [-]
        time = np.arange(0, self.orbit_raising_dt['t_prop'], self.orbit_raising_dt['t_prop'] / self.pbm.prm['n_nodes'])

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        for ta_ in ta:

            orb_prm = self.orb_prm
            orb_prm['ta'] = ta_ * np.pi / 360

            # Orbit raising object  
            orbit_raising = OrbitRaising(self.cr3bp, self.spacecraft, orb_prm, time)

            states, controls, time = orbit_raising.get_initial_guess()

            ax.plot(states[0], states[1], states[2], '-', label=str(ta_))

        for i in range(len(self.manifolds_dt['halo'].manifolds['stable_interior'])):
            ax.plot(self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 0], self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 1], 
                self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 2], '-')

        target_state = self.interpolated_manifold(self.manifolds_dt['target_theta'], self.manifolds_dt['target_tau'])
        plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')
        plt.legend()
        plt.show()

if __name__ == '__main__':

    # ____________________________________ Optimization / Transcription Parameters ______________________________________________________

    # Bounds parameters
    bounds_prm = {'x_fct'  : 10, 'y_fct' : 5, 'z_fct' : 5, 
                  'vx_fct' : 5, 'vy_fct': 5, 'vz_fct': 5, 
                  'fuel_prc': 0.0, 'time_bnd': (0, 2)}

    host = 'laptop'

    n_nodes = 300

    linear_solver = 'ma86'
    tr_method = 'hermite-simpson'

    plot_results = True

    explicit_integration = False
    plot_physical_err = False

    options = {'tr_method': tr_method, 'explicit_integration': explicit_integration, 'host': host, 'linear_solver': linear_solver,
                'plot_results': plot_results, 'plot_physical_err': plot_physical_err, 'n_nodes': n_nodes, 'pickle_results': True}

    # ____________________________________ Spacecraft Parameters __________________________________________

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'mass_dry': m_dry, 'thrust_max': thrust_max, 'thrust_min': thrust_min, 'isp': Isp}

    # ____________________________________ Initial Orbit Parameters __________________________________________

    # Departure orbit
    a = 42164  # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 7  # inclination [deg]
    W = 90    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 220     # initial true anomaly [deg]

    orbital_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180, 'ta': ta*np.pi/180}

    # ____________________________________ Initial Guess Trajectory __________________________________________

    # Time of propagation of the initial trajectory [-]
    propagation_time = 3.5

    # Data needed for the construction of the initial trajectory
    # (orbit raising)
    orbit_raising_dt = {'t_prop': propagation_time}

    # ____________________________________ Manifolds Generation _______________________________________________

    # Manifolds 
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10)
    halo.interpolation()

    # Manifold branch to compute (0 <= theta <= 1)
    theta2cpt = np.arange(0.15, 0.25, 0.01)
    # theta2cpt = [0.09, 0.5, 0.6, 0.7]
    tau2cpt = 15

    halo.single_manifold_computation(stability='stable', way='interior', theta=theta2cpt, t_prop=tau2cpt, max_internal_steps=2_000_000)

    # Inverse the tau array
    tau_arr = np.array([-tau for tau in halo.manifolds['stable_interior'][0].t_vec])

    # Useful portion of the manifold (in percentage)
    # Only this part will be interpolated to save computation time
    useful_prt = 0.0

    # Point of the manifold targeted, parametrized by tau and theta 
    target_tau = 10.45
    target_theta = 0.53

    # Manifolds data
    manifolds_dt = {'halo': halo, 'target_tau': target_tau, 'target_theta': target_theta, 'useful_part': useful_prt, 'theta_arr' : theta2cpt, 'tau_arr': tau_arr}


    # ____________________________________ Class Instantiation _______________________________________________
    
    earth2nrho = Earth2NRHO(spacecraft_prm, orbital_prm, orbit_raising_dt, manifolds_dt, bounds_prm, **options)

    tran = transcription.Transcription(earth2nrho.pbm)
    tran.launch()
    
    solv = solver.IPOPT(tran)
    solv.solve(max_iter=10000)