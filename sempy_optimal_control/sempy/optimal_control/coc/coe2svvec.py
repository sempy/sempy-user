#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 10:47:12 2019

@author: Alberto FOSSA'
"""

import numpy as np

from sempy.optimal_control.coc.rotmatvec import per2eq_vec


def coe2sv_vec(sma, ecc, inc, raan, aop, theta, gm_r2bp):
    """Change of Coordinates from Classical Orbital Elements to State Vector in body-centred
    inertial reference frame.

    Parameters
    ----------
    sma : float
        Semi-major axis [km]
    ecc : float
        Eccentricity [-]
    inc : float
        Inclination [rad]
    raan : float
        Right Ascension of the Ascending Node [rad]
    aop : float
        Argument of Periapsis [rad]
    theta : ndarray
        True anomalies [rad]
    gm_r2bp : float
        Central body standard gravitational parameter for the
        Restricted Two-Body Problem (R2BP) [km^3/s^2]

    Returns
    -------
    r_vec : ndarray
        Position vector [km]
    v_vec : ndarray
        Velocity vector [km/s]

    """

    theta = np.reshape(theta, (len(theta), 1))  # column vector (n, 1)

    h_mag = (gm_r2bp * sma * (1 - ecc ** 2)) ** 0.5  # specific angular momentum magnitude
    r_mag = (h_mag ** 2 / gm_r2bp) / (1 + ecc * np.cos(theta))  # distance from the central body

    # position and velocity components in perifocal frame
    x_perifocal = r_mag * np.cos(theta)
    y_perifocal = r_mag * np.sin(theta)
    vx_perifocal = - (gm_r2bp / h_mag) * np.sin(theta)
    vy_perifocal = (gm_r2bp / h_mag) * (ecc + np.cos(theta))

    # conversion from perifocal to equatorial reference frame
    r_vec, v_vec = \
        per2eq_vec(x_perifocal, y_perifocal, vx_perifocal, vy_perifocal, raan, inc, aop)

    return r_vec, v_vec
