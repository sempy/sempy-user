#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 11:12:19 2019

@author: Alberto FOSSA'
"""

import numpy as np


def rot2irf(time):
    """Rotation matrix between rotating and inertial reference frames whose z axes coincides
    assuming that the x axes are aligned at ``t=0`` and the angular rate is ``n=1``.

    Parameters
    ----------
    time : float
        Time at which the rotation matrix is computed expressed in non dimensional units [-]

    Returns
    -------
    rot_mat : ndarray
        6x6 rotation matrix between rotating and inertial reference frames

    """

    cos_time = np.cos(time)
    sin_time = np.sin(time)

    r3t = np.array([[cos_time, -sin_time, 0], [sin_time, cos_time, 0], [0, 0, 1]])
    r3t_dot = np.array([[-sin_time, -cos_time, 0], [cos_time, -sin_time, 0], [0, 0, 0]])

    row1 = np.hstack((r3t, np.zeros((3, 3))))
    row2 = np.hstack((r3t_dot, r3t))

    rot_mat = np.vstack((row1, row2))

    return rot_mat

def eci2syn(state_eci, time, mu_cr3bp, lc_cr3bp, tc_cr3bp):
    """Converts the State Vector at time t from inertial reference frame centred on the
    first primary to synodic reference frame.

    Parameters
    ----------
    state_eci : ndarray
        State Vector in inertial reference frame and dimensional units
        as ``[X, Y, Z, Vx, Vy, Vz]`` [km, km/s]
    time : float
        Time in dimensional unit [s]
    mu_cr3bp : float
        CR3BP mass parameter [-]
    lc_cr3bp : float
        CR3BP characteristic length [km]
    tc_cr3bp : float
        CR3BP characteristic time [s]

    Returns
    -------
    state : ndarray
        State Vector in synodic reference frame and non dimensional units
        as ``[x, y, z, vx, vy, vx]`` [-]

    """

    vc_cr3bp = lc_cr3bp / tc_cr3bp  # characteristic velocity [km/s]
    time_adim = time / tc_cr3bp  # time in non dimensional units

    state_eci_adim = state_eci / np.hstack((np.ones(3) * lc_cr3bp, np.ones(3) * vc_cr3bp))
    state_m1 = np.hstack(([-mu_cr3bp], np.zeros(5)))  # state for m1 in synodic frame
    # state = np.linalg.solve(rot2irf(time_adim), state_eci_adim) + state_m1 # doesn't work with cppad_py 
    state = np.linalg.inv(rot2irf(time_adim)).dot(state_eci_adim) + state_m1

    return state



def syn2lci(state, time, mu_cr3bp, lc_cr3bp, tc_cr3bp):
    """Converts the State Vector at time t from synodic reference frame to inertial reference frame
    centred on the second primary.

    Parameters
    ----------
    state : ndarray
        State Vector in synodic reference frame and non dimensional units
        as ``[x, y, z, vx, vy, vx]`` [-]
    time : float
        Time in non dimensional units [-]
    mu_cr3bp : float
        CR3BP mass parameter [-]
    lc_cr3bp : float
        CR3BP characteristic length [km]
    tc_cr3bp : float
        CR3BP characteristic time [s]

    Returns
    -------
    state_lci : ndarray
        State Vector in inertial reference frame and dimensional units
        as ``[X, Y, Z, Vx, Vy, Vz]`` [km, km/s]

    """

    vc_cr3bp = lc_cr3bp / tc_cr3bp  # characteristic velocity [km/s]
    state_m2 = np.hstack(([1 - mu_cr3bp], np.zeros(5)))  # state for m2 in synodic frame

    state_adim = np.matmul(rot2irf(time), (state - state_m2))
    state_lci = state_adim * np.hstack((np.ones(3) * lc_cr3bp, np.ones(3) * vc_cr3bp))

    return state_lci


def lci2syn(state_lci, time, mu_cr3bp, lc_cr3bp, tc_cr3bp):
    """Converts the State Vector at time t from inertial reference frame centred on the
    second primary to synodic reference frame.

    Parameters
    ----------
    state_lci : ndarray
        State Vector in inertial reference frame and dimensional units
        as ``[X, Y, Z, Vx, Vy, Vz]`` [km, km/s]
    time : float
        Time in non dimensional units [-]
    mu_cr3bp : float
        CR3BP mass parameter [-]
    lc_cr3bp : float
        CR3BP characteristic length [km]
    tc_cr3bp : float
        CR3BP characteristic time [s]

    Returns
    -------
    state : ndarray
        State Vector in synodic reference frame and non dimensional units
        as ``[x, y, z, vx, vy, vx]`` [-]

    """

    vc_cr3bp = lc_cr3bp / tc_cr3bp  # characteristic velocity [km/s]
    time_adim = time / tc_cr3bp  # time in non dimensional units

    state_lci_adim = state_lci / np.hstack((np.ones(3) * lc_cr3bp, np.ones(3) * vc_cr3bp))
    state_m2 = np.hstack(([1 - mu_cr3bp], np.zeros(5)))  # state for m2 in synodic frame
    state = np.linalg.solve(rot2irf(time_adim), state_lci_adim) + state_m2

    return state


def rot1(theta):
    """Elementary rotation matrix around X axis.

    Parameters
    ----------
    theta : float
        Rotation angle [rad]

    Returns
    -------
    rot_mat : ndarray
        3x3 rotation matrix around X axis

    """

    rot_mat = np.array([[1, 0, 0], [0, np.cos(theta), np.sin(theta)],
                        [0, -np.sin(theta), np.cos(theta)]])

    return rot_mat


def rot3(theta):
    """Elementary rotation matrix around Z axis.

    Parameters
    ----------
    theta : float
        Rotation angle [rad]

    Returns
    -------
    rot_mat : ndarray
        3x3 rotation matrix around Z axis

    """

    rot_mat = np.array([[np.cos(theta), np.sin(theta), 0],
                        [-np.sin(theta), np.cos(theta), 0], [0, 0, 1]])

    return rot_mat


def eq2per(raan, inc, aop):
    """Rotation matrix from inertial, body-centred equatorial reference frame to
    perifocal reference frame.

    Parameters
    ----------
    raan : float
        Right Ascension of the Ascending Node [rad]
    inc : float
        Inclination [rad]
    aop : float
        Argument of Periapsis [rad]

    Returns
    -------
    rot_mat : ndarray
        3x3 rotation matrix from equatorial to perifocal reference frames

    """

    rot_mat = np.matmul(np.matmul(rot3(aop), rot1(inc)), rot3(raan))
    # rot_mat = rot3(aop) @ rot1(inc) @ rot3(raan)

    return rot_mat


def per2eq(raan, inc, aop):
    """Rotation matrix from perifocal reference frame to inertial, body-centred equatorial
    reference frame.

    Parameters
    ----------
    raan : float
        Right Ascension of the Ascending Node [rad]
    inc : float
        Inclination [rad]
    aop : float
        Argument of Periapsis [rad]

    Returns
    -------
    rot_mat : ndarray
        3x3 rotation matrix from perifocal to equatorial reference frames

    """

    rot_mat = np.matrix.transpose(eq2per(raan, inc, aop))

    return rot_mat
