#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 11:08:29 2019

@author: Alberto FOSSA'
"""

import numpy as np

from sempy.optimal_control.coc.rotmat import per2eq


def polar2per_vec(r_vec, theta_vec, u_vec, v_vec):
    """Transformation from polar coordinates to perifocal reference frame.

    Parameters
    ----------
    r_vec : ndarray
        Radius time series [km]
    theta_vec : ndarray
        Angle time series [rad]
    u_vec : ndarray
        Radial velocity time series [km/s]
    v_vec : ndarray
        Tangential velocity time series [km/s]

    Returns
    -------
    x_vec : ndarray
        Position along x axis [km]
    y_vec : ndarray
        Position along y axis [km]
    vx_vec : ndarray
        Velocity component along x axis [km/s]
    vy_vec : ndarray
        Velocity component along y axis [km/s]

    """

    x_vec = r_vec * np.cos(theta_vec)
    y_vec = r_vec * np.sin(theta_vec)

    vx_vec = u_vec * np.cos(theta_vec) - v_vec * np.sin(theta_vec)
    vy_vec = u_vec * np.sin(theta_vec) + v_vec * np.cos(theta_vec)

    return x_vec, y_vec, vx_vec, vy_vec


def per2eq_vec(x_per, y_per, vx_per, vy_per, raan, inc, aop):
    """Transformation from perifocal reference frame to inertial, body-centred equatorial
    reference frame.

    Parameters
    ----------
    x_per : ndarray
        Position along x axis [km]
    y_per : ndarray
        Position along y axis [km]
    vx_per : ndarray
        Velocity component along x axis [km/s]
    vy_per : ndarray
        Velocity component along y axis [km/s]
    raan : float
        Right Ascension of the Ascending Node [rad]
    inc : float
        Inclination [rad]
    aop : float
        Argument of Periapsis [rad]

    Returns
    -------
    r_eq : ndarray
        Position vector [km]
    v_eq : ndarray
        Velocity vector [km/s]

    """

    # compute sin and cos of RAAN, inclination and AOP
    cos_raan = np.cos(raan)
    sin_raan = np.sin(raan)

    cos_inc = np.cos(inc)
    sin_inc = np.sin(inc)

    cos_aop = np.cos(aop)
    sin_aop = np.sin(aop)

    # compute coefficients of the rotation matrix
    q11 = cos_raan * cos_aop - sin_raan * cos_inc * sin_aop
    q12 = -cos_raan * sin_aop - sin_raan * cos_inc * cos_aop
    q21 = sin_raan * cos_aop + cos_raan * cos_inc * sin_aop
    q22 = cos_raan * cos_inc * cos_aop - sin_raan * sin_aop
    q31 = sin_inc * sin_aop
    q32 = cos_aop * sin_inc

    # compute the position and velocity vectors in equatorial reference frame
    x_eq = q11 * x_per + q12 * y_per
    y_eq = q21 * x_per + q22 * y_per
    z_eq = q31 * x_per + q32 * y_per

    vx_eq = q11 * vx_per + q12 * vy_per
    vy_eq = q21 * vx_per + q22 * vy_per
    vz_eq = q31 * vx_per + q32 * vy_per

    # stack together the x_eq, y_eq, z_eq components of the position and velocity vectors
    r_eq = np.hstack((x_eq, y_eq, z_eq))
    v_eq = np.hstack((vx_eq, vy_eq, vz_eq))

    return r_eq, v_eq


def polar2eq_vec(r_vec, theta_vec, u_vec, v_vec, raan, inc, aop):
    """Transformation from polar coordinates to inertial, body-centred equatorial reference frame.

    Parameters
    ----------
    r_vec : ndarray
        Radius time series [km]
    theta_vec : ndarray
        Angle time series [rad]
    u_vec : ndarray
        Radial velocity time series [km/s]
    v_vec : ndarray
        Tangential velocity time series [km/s]
    raan : float
        Right Ascension of the Ascending Node [rad]
    inc : float
        Inclination [rad]
    aop : float
        Argument of Periapsis [rad]

    Returns
    -------
    r_eq : ndarray
        Position vector [km]
    v_eq : ndarray
        Velocity vector [km/s]

    """

    x_per, y_per, vx_per, vy_per = polar2per_vec(r_vec, theta_vec, u_vec, v_vec)
    r_eq, v_eq, = per2eq_vec(x_per, y_per, vx_per, vy_per, raan, inc, aop)

    return r_eq, v_eq


def per2eq_loop(x_per, y_per, vx_per, vy_per, raan, inc, aop):
    """Transformation from perifocal reference frame to inertial, body-centred equatorial
    reference frame. To be used only for testing purposes.

    Parameters
    ----------
    x_per : ndarray
        Position along x axis [km]
    y_per : ndarray
        Position along y axis [km]
    vx_per : ndarray
        Velocity component along x axis [km/s]
    vy_per : ndarray
        Velocity component along y axis [km/s]
    raan : float
        Right Ascension of the Ascending Node [rad]
    inc : float
        Inclination [rad]
    aop : float
        Argument of Periapsis [rad]

    Returns
    -------
    r_eq : ndarray
        Position vector [km]
    v_eq : ndarray
        Velocity vector [km/s]

    """

    nb_states = len(x_per)
    rot_mat = per2eq(raan, inc, aop)
    r_eq = np.zeros((nb_states, 3))
    v_eq = np.zeros((nb_states, 3))

    for k in range(nb_states):
        r_per = np.array([x_per[k], y_per[k], 0.0])
        v_per = np.array([vx_per[k], vy_per[k], 0.0])
        r_eq[k, :] = np.matmul(rot_mat, r_per)
        v_eq[k, :] = np.matmul(rot_mat, v_per)

    return r_eq, v_eq
