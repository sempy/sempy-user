#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  01 10:40:21 2021

@author: SEMBLANET Tom

"""

import numpy as np
import cppad_py

from sempy.optimal_control.problem import Problem
from sempy.optimal_control.optimization import Optimization

class User(Problem):
	""" Pattern of an optimal control problem definition """

	def __init__(self):
		""" Initialization of the `User` class """
		n_states = 3
		n_controls = 2
		n_path_con = 1
		n_event_con = 4
		n_f_par = 0
		n_nodes = 200

		Problem.__init__(self, n_states, n_controls, n_path_con,
						 n_event_con, n_f_par, n_nodes)

	def set_constants(self):
		""" Setting the problem constants """
		self.final_time = 2
		self.g = 1
		self.a = 0.5 * self.g

	def set_boundaries(self):
		""" Setting of the states, controls, free-parameters, initial and final times
						boundaries """
	   	
		# Boundaries on state #1
		self.low_bnd.states[0] = -10
		self.upp_bnd.states[0] = 10

		# Boundaries on state #2
		self.low_bnd.states[1] = -10
		self.upp_bnd.states[1] = 10

		# Boundaries on state #3
		self.low_bnd.states[2] = -10
		self.upp_bnd.states[2] = 10

		# Boundaries on control #1
		self.low_bnd.controls[0] = -10
		self.upp_bnd.controls[0] = 10

		# Boundaries on control #2
		self.low_bnd.controls[1] = -10
		self.upp_bnd.controls[1] = 10

		# Boundaries on initial time
		self.low_bnd.ti = 0
		self.upp_bnd.ti = 0

		# Boundaries on initial time
		self.low_bnd.tf = self.final_time
		self.upp_bnd.tf = self.final_time

	def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
		""" Computation of the events constraints """
		events = np.ndarray((self.prm['n_event_con'], 1),
							   dtype=cppad_py.a_double)

		events[0] = xi[0]
		events[1] = xi[1]
		events[2] = xi[2]
		events[3] = xf[1]

		return events

	def set_events_constraints_boundaries(self):
		""" Setting of the events constraints boundaries """
		
		self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
		self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
		self.low_bnd.event[2] = self.upp_bnd.event[2] = 0
		self.low_bnd.event[3] = self.upp_bnd.event[3] = 0.1

	def path_constraints(self, states, controls, f_par):
		""" Computation of the path constraints """
		paths = np.ndarray((self.prm['n_path_con'],
							self.prm['n_nodes']), dtype=cppad_py.a_double)

		paths[0] = controls[0]*controls[0] + controls[1]*controls[1]

		return paths

	def set_path_constraints_boundaries(self):
		""" Setting of the path constraints boundaries """
		
		self.low_bnd.path[0] = self.upp_bnd.path[0] = 1

	def dynamics(self, states, controls, f_prm, expl_int=False):
		""" Computation of the states derivatives """
		if expl_int == False:
			dynamics = np.ndarray(
				(states.shape[0], states.shape[1]), dtype=cppad_py.a_double)
		else:
			dynamics = np.zeros(len(states))

		dynamics[0] = states[2] * controls[0]
		dynamics[1] = states[2] * controls[1]
		dynamics[2] = self.a - self.g * controls[1]

		return dynamics

	def end_point_cost(self, ti, xi, tf, xf, f_prm):
		""" Computation of the end point cost (Mayer term) """
		return -xf

	def set_initial_guess(self):
		""" Setting of the initial guess for the states, controls, free-parameters
						and time grid """
		
		# Time
		self.initial_guess.time = np.linspace(0, self.final_time, self.prm['n_nodes'])

		# States
		self.initial_guess.states[0] = np.array(
			[5 for _ in self.initial_guess.time])
		self.initial_guess.states[1] = np.array(
			[-3 for _ in self.initial_guess.time])
		self.initial_guess.states[2] = np.array(
			[1.2 for _ in self.initial_guess.time])

		# Controls
		self.initial_guess.controls[0] = np.array(
			[2 for _ in self.initial_guess.time])
		self.initial_guess.controls[1] = np.array(
			[3.4 for _ in self.initial_guess.time])

if __name__ == '__main__':

	# options = {'tr_method': 'hermite-simpson', 'linear_solver': 'ma27'}

	# Instantiation of the problem
	problem = User()

	# Instantiation of the optimization
	optimization = Optimization(problem=problem)

	# Launch of the optimization
	optimization.run()