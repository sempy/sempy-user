import matplotlib.pyplot as plt
from scipy import interpolate
import numpy as np

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO

from sempy.core.bspline.bispev import bispev

def manifolds_interpolation(orbit, beg_prc=0, end_prc=1):
	""" Interpolates the Orbit's manifolds, computing a function f: R²->R⁶ 
		mapping two parameters theta, tau and x, y, z, vx, vy, vz. 
		Parameters `tau_beg` and `tau_end` indicate which part of the manifolds 
		will be interpolated. (interpolation from tau_beg*tau to tau_end*tau) 

	"""

	# Percentage of manifolds branches that will be interpolated
	beg_ind = int(beg_prc*len(orbit.manifolds['stable_interior'][0].t_vec))
	end_ind = int(end_prc*len(orbit.manifolds['stable_interior'][0].t_vec))

	# Construction of arrays of tau and theta (Flip the theta array so it's in an ascending order 
	# for RectBivariateSpline function)
	theta_arr = np.array([manifold.theta for manifold in orbit.manifolds['stable_interior'][:]])
	tau_arr   = np.array([-tau for tau in orbit.manifolds['stable_interior'][0].t_vec[beg_ind:end_ind]])

	# Computation of discrete fx, fy, fz, fvx, fvy, fvz functions which will be
	# interpolated
	x  = np.array([manifold.state_vec[beg_ind:end_ind, 0] for manifold in orbit.manifolds['stable_interior'][:]])
	y  = np.array([manifold.state_vec[beg_ind:end_ind, 1] for manifold in orbit.manifolds['stable_interior'][:]])
	z  = np.array([manifold.state_vec[beg_ind:end_ind, 2] for manifold in orbit.manifolds['stable_interior'][:]])
	vx = np.array([manifold.state_vec[beg_ind:end_ind, 3] for manifold in orbit.manifolds['stable_interior'][:]])
	vy = np.array([manifold.state_vec[beg_ind:end_ind, 4] for manifold in orbit.manifolds['stable_interior'][:]])
	vz = np.array([manifold.state_vec[beg_ind:end_ind, 5] for manifold in orbit.manifolds['stable_interior'][:]])

	# Interpolation
	f_x = interpolate.RectBivariateSpline(theta_arr, tau_arr, x)
	f_y = interpolate.RectBivariateSpline(theta_arr, tau_arr, y)
	f_z = interpolate.RectBivariateSpline(theta_arr, tau_arr, z)
	f_vx = interpolate.RectBivariateSpline(theta_arr, tau_arr, vx)
	f_vy = interpolate.RectBivariateSpline(theta_arr, tau_arr, vy)
	f_vz = interpolate.RectBivariateSpline(theta_arr, tau_arr, vz)

	# Storage of the spline coefficient and interpolation function information
	f_x_dt = {'tx': f_x.tck[0], 'nx': len(f_x.tck[0]), 'ty': f_x.tck[1],  'ny': len(f_x.tck[1]), 'c': f_x.get_coeffs()}
	f_y_dt = {'tx': f_y.tck[0], 'nx': len(f_y.tck[0]), 'ty': f_y.tck[1],  'ny': len(f_y.tck[1]), 'c': f_y.get_coeffs()}
	f_z_dt = {'tx': f_z.tck[0], 'nx': len(f_z.tck[0]), 'ty': f_z.tck[1],  'ny': len(f_z.tck[1]), 'c': f_z.get_coeffs()}
	f_vx_dt = {'tx': f_vx.tck[0], 'nx': len(f_vx.tck[0]), 'ty': f_vx.tck[1],  'ny': len(f_vx.tck[1]), 'c': f_vx.get_coeffs()}
	f_vy_dt = {'tx': f_vy.tck[0], 'nx': len(f_vy.tck[0]), 'ty': f_vy.tck[1],  'ny': len(f_vy.tck[1]), 'c': f_vy.get_coeffs()}
	f_vz_dt = {'tx': f_vz.tck[0], 'nx': len(f_vz.tck[0]), 'ty': f_vz.tck[1],  'ny': len(f_vz.tck[1]), 'c': f_vz.get_coeffs()}

	return f_x_dt, f_y_dt, f_z_dt, f_vx_dt, f_vy_dt, f_vz_dt


if __name__ == '__main__':

	# Construction of the CR3BP problem and NRHO orbit
	cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
	nrho = NRHO(cr3bp, cr3bp.l1, NRHO.Family.southern, Altpdim=6e3)
	nrho.interpolation()

	# Array of branches which will be computed
	theta2cpt = np.arange(0.00, 1.00, 0.01)

	# Length of branches
	tau2cpt = 15

	nrho.all_manifolds_computation(theta2cpt, tau2cpt, max_internal_steps=2_000_000)

	# Percentage of manifolds branches that will be interpolated
	# Used to only catch the usefull part of the NRHO's manifolds 
	# Manifolds will be interpolated over [beg_prc * tau2cpt ; end_prc * tau2cpt]
	beg_prc = 0.75
	end_prc = 1

	f_x_dt, f_y_dt, f_z_dt, f_vx_dt, f_vy_dt, f_vz_dt = manifolds_interpolation(nrho, beg_prc, end_prc)

	# Exploitation of the interpolation
	theta_interp = 0.51
	tau_interp = -14

	x_ = bispev(**f_x_dt, x=[theta_interp], y=[tau_interp])
	y_ = bispev(**f_y_dt, x=[theta_interp], y=[tau_interp])
	z_ = bispev(**f_z_dt, x=[theta_interp], y=[tau_interp])
	vx_ = bispev(**f_vx_dt, x=[theta_interp], y=[tau_interp])
	vy_ = bispev(**f_vy_dt, x=[theta_interp], y=[tau_interp])
	vz_ = bispev(**f_vz_dt, x=[theta_interp], y=[tau_interp])



