import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle
import math

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.coc.rotmat import eci2syn
import sempy.core.init.constants as cst 

from sempy.optimal_control.spacecraft_trajectories.mission import Mission
from sempy.optimal_control.src.optimization import Optimization
from sempy.optimal_control.src import utils

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.Earth2Halo_guess import set_Earth2Halo_guess

# For explicit integration
from sempy.optimal_control.src.explicit_integration import explicit_integration


#from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_guess import Earth2NRHOFixedPositionGuess, Earth2NRHOFixedVelocityGuess


class Earth2Halo(Mission):

    def __init__(self, cr3bp, spc_prm, departure_orb_prm, arrival_orb, n_nodes): # bounds removed
        """ Initialization of the LLO2LLO class."""

        # Storing some parameters
        self.cr3bp = cr3bp
        self.departure_orb_prm = departure_orb_prm
        self.arrival_orb = arrival_orb
        self.spc_prm = spc_prm                      # Spacecraft parameters

        Mission.__init__(self, spc_prm, n_states=7, n_controls=4, n_event_con=13, n_st_path_con=0, n_ct_path_con=1, n_f_par=1, n_nodes=n_nodes)

        # ==================================================
        # self.mission_analysis()
        # ==================================================

        # Computation of free parameters boundaries
        self.set_free_parameters_bnd()

    def set_free_parameters_bnd(self):
        """ Sets the free parameters boundaries """
        
        # True anomaly [rad]
        self.low_bnd.f_par[0] = -np.pi 
        self.upp_bnd.f_par[0] = np.pi

    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Compute the Mayer term of the cost function """

        # Maximization of the final mass
        #return -xf[-1] / self.spacecraft.mass0

        mi = xi[6]
        mf = xf[6]

        #return (mf-0.9*mi)*(mf-0.9*mi)/mi
        return -mf/mi

    def path_constraints(self, states, controls, states_add, controls_add, controls_col, f_par):
        """ Computation of the path constraints  """
        st_path = np.ndarray((self.prm['n_st_path_con'],
                            2*self.prm['n_nodes']-1), dtype=cppad_py.a_double)
        ct_path = np.ndarray((self.prm['n_ct_path_con'],
                            4*self.prm['n_nodes']-3), dtype=cppad_py.a_double)

        ux = np.concatenate((controls[1], controls_add[1], controls_col[1]))
        uy = np.concatenate((controls[2], controls_add[2], controls_col[2]))
        uz = np.concatenate((controls[3], controls_add[3], controls_col[3]))

        T = np.concatenate((controls[0], controls_add[0], controls_col[0]))

        u2 = ux*ux + uy*uy + uz*uz

        # ct_path[0] = u2
        ct_path[0] = u2 - 1
        #ct_path[1] = T

        return st_path, ct_path

    def set_path_con_bnd(self):
        """ Sets the path constraints boundaries """
        # self.low_bnd.ct_path[0] = self.upp_bnd.ct_path[0] = 1
        self.low_bnd.ct_path[0] = self.upp_bnd.ct_path[0] = 0
        # We impose that the magnitude of the thrusts is constantly equal to 0
        #self.low_bnd.ct_path[1] = self.upp_bnd.ct_path[1] = 0

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):

        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial orbit free parameters
        #ta = f_prm
        ta = f_prm

        # States in ECI
        r0_eci, v0_eci = coe2sv_vec(self.departure_orb_prm['a'], self.departure_orb_prm['e'], self.departure_orb_prm['i'], self.departure_orb_prm['W'], self.departure_orb_prm['w'], ta, cst.GM_EARTH)
        
        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0 = eci2syn(np.hstack((r0_eci[0], v0_eci[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

        # Targeted position
        theta = 0.5
        n,m = np.shape(self.arrival_orb.state_vec)

        x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.arrival_orb.state_vec[math.floor(n*theta),:6]        # Target one point on the arrival orbit
        #x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.arrival_orb.state_vec[0,:6]        # Target one point on the arrival orbit

        #m0 = self.initial_guess.states[6,0]
        m0 = self.spc_prm['mass0']

        # x, y, z (init)[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # # m [-]
        # constraints[6] = xi[6]

        constraints[6] = m0 - xi[6]

        # x, y, z (final)[-]
        constraints[7] = x_tgt - xf[0]
        constraints[8] = y_tgt - xf[1]
        constraints[9] = z_tgt - xf[2]

        # vx, vy, vz (final) [-]
        constraints[10] = vx_tgt - xf[3]
        constraints[11] = vy_tgt - xf[4]
        constraints[12] = vz_tgt - xf[5]

        return constraints

    def set_events_con_bnd(self):
        """ Sets the events constraints """

        #xi, yi, zi, vxi, vyi, vzi, mi = self.initial_guess.states[:, 0]

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        # self.low_bnd.event[6] = self.upp_bnd.event[6] = mi
        self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

        # Position (init) [-]
        self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
        self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
        self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

        # Velocity (init) [-]
        self.low_bnd.event[10] = self.upp_bnd.event[10] = 0
        self.low_bnd.event[11] = self.upp_bnd.event[11] = 0
        self.low_bnd.event[12] = self.upp_bnd.event[12] = 0

    def set_initial_guess(self):
        """ Set the initial guess using the set_Earth2Halo_guess fonction 
        """
        n_nodes_1 = 650

        earth2halo_states, earth2halo_controls, earth2halo_time, earth2halo_prm = set_Earth2Halo_guess(self.cr3bp, self.spc_prm, 
                                n_nodes_1, self.departure_orb_prm, self.arrival_orb)

        # with open('pickled_results/earth2halo/earth2halo_pickles_900_nodes', 'rb') as earth2manifolds_pickles:
        #     pickled_results = pickle.load(earth2manifolds_pickles)

        # earth2halo_states = pickled_results['opt_st']
        # earth2halo_controls = pickled_results['opt_ct']
        # earth2halo_total_ct = pickled_results['total_ct']
        # earth2halo_time = pickled_results['opt_tm']
        # earth2halo_prm = pickled_results['opt_pr']

        # Set of the initial guess
        self.initial_guess.states = earth2halo_states
        self.initial_guess.controls = earth2halo_controls
        self.initial_guess.time = earth2halo_time
        self.initial_guess.f_prm[0] = earth2halo_prm[0]    # WARNING : in the pickled results, there are 2 free parameters, the second is useless


    def plot_trajectory(self, states, color, index = None):

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # Extraction of the states
        x = states[0]
        y = states[1]
        z = states[2]

        # Orbit 
        orbit_vec = self.arrival_orb.state_vec

        x_o = orbit_vec[:, 0]
        y_o = orbit_vec[:, 1]
        z_o = orbit_vec[:, 2] 



        theta = 0.5
        n,m = np.shape(self.arrival_orb.state_vec)
    
        x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.arrival_orb.state_vec[math.floor(n*theta),:6]        # Target one point on the arrival orbit

        # Plot the trajectory
        ax.plot(x, y, z, label='Spacecraft trajectory', color = color)
        ax.plot(x_o, y_o, z_o, label='Arrival orbit', color = 'red')

        # # Plot the manifolds
        # if index is None:
        #     for i in range(len(halo.manifolds['stable_interior'])):
        #         ax.plot(halo.manifolds['stable_interior'][i].state_vec[:, 0], halo.manifolds['stable_interior'][i].state_vec[:, 1], 
        #             halo.manifolds['stable_interior'][i].state_vec[:, 2], '-')

        # else:
        #     ax.plot(halo.manifolds['stable_interior'][index].state_vec[:, 0], halo.manifolds['stable_interior'][index].state_vec[:, 1], 
        #             halo.manifolds['stable_interior'][index].state_vec[:, 2], '-')

        # # Plot the target position
        # target_state = self.interpolated_manifold(self.manifolds_dt['target_theta'], self.manifolds_dt['target_tau'])
        # plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')

        plt.plot([x_tgt], [y_tgt], [z_tgt], 'mo')
        plt.legend()
        plt.show()

if __name__ == '__main__':

    # ____________________________________ Optimization / Transcription Parameters ______________________________________________________

    # # Bounds parameters
    # bounds_prm = {'x_fct'  : 10, 'y_fct' : 5, 'z_fct' : 5, 
    #               'vx_fct' : 5, 'vy_fct': 5, 'vz_fct': 5, 
    #               'fuel_prc': 0.0, 'time_bnd': (0, 2)}

    host = 'laptop'

    #n_nodes = 100
    n_nodes = 550

    # Possible linear solvers : ma27, ma57, ma77, ma86, mumps
    linear_solver = 'ma86'

    options = {'explicit_integration': False, 'host': host, 'linear_solver': linear_solver,
                'plot_results': True, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True, 'name': 'earth2halo_pickles'}

    # ____________________________________ Spacecraft Parameters __________________________________________

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'thrust_max': thrust_max,'isp': Isp, 'mass_dry': m_dry, 'thrust_min': thrust_min}

    # ____________________________________ Initial Orbit Parameters __________________________________________

    # # Departure orbit
    # a = 42164  # initial semi-major axis [km]
    # e = 0      # final semi-major axis [km]
    # i = 28.58  # inclination [deg]
    # W = 90    # right Ascension of the Ascending Node [deg]
    # w = 0      # perigee argument [deg]
    # #ta = -180     # initial true anomaly [deg]
    # ta = 0.2047976  # from pickeld results in Earth2Halo_guess


    # Departure orbit
    a = 35000  # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58  # inclination [deg]
    W = 0    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    #ta = -180     # initial true anomaly [deg]
    ta = 0  # from pickeld results in Earth2Halo_guess

    orbital_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180} # We take away the ta, 'ta': ta*np.pi/180}


    # ____________________________________ Arrival orbit Generation _______________________________________________

    # Manifolds 
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    # halo = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo.interpolation()

    # ____________________________________ Class Instantiation _______________________________________________

    # Instantiation of the problem
    problem = Earth2Halo(cr3bp, spacecraft_prm, orbital_prm, halo, n_nodes)

    # Instantiation of the optimization
    optimization = Optimization(problem=problem, **options)

    optimization.run()

    # problem.plot_trajectory(optimization.results['opt_st'], color = 'green')

    # with open('earth2halo_pickles_19:04:2021_15:33:12', 'rb') as earth2manifolds_pickles:
    #     pickled_results = pickle.load(earth2manifolds_pickles)

    #print(np.shape(optimization.results['opt_st']))
    # print(np.shape(optimization.results['int_st']))
    #problem.plot_trajectory(optimization.results['opt_st'])

    # problem.plot_trajectory(optimization.results['int_st'])

    # r_opt = optimization.results['opt_st'][0:6,0]
    # r_int = optimization.results['int_st'][0:6,0]

    # print('Erreur pos index 0 :')

    # error_pos = (r_opt[0] - r_int[0])**2 + (r_opt[1] - r_int[1])**2 + (r_opt[2] - r_int[2])**2
    # print(error_pos)

    # print('Erreur vel index 0 :')

    # error_vel = (r_opt[3] - r_int[3])**2 + (r_opt[4] - r_int[4])**2 + (r_opt[5] - r_int[5])**2
    # print(error_vel)

    #####################"" BONUS LINE TO SEE EARTH2HALO RESULTS WITH THIS PLOT TRAJECTORY METHOD ################################


    # #############" EARTH2HALO "#####################
    # with open('earth2halo_pickles_5000_itr', 'rb') as earth2manifolds_pickles:
    #     pickled_results = pickle.load(earth2manifolds_pickles)

    # #############" EARTH2MANIFOLD" ####################
    # # with open('earth2manifolds_pickles_vel_16:04:2021_10:58:30', 'rb') as earth2manifolds_pickles_vel:
    # #     pickled_results = pickle.load(earth2manifolds_pickles_vel)

    # with open('earth2halo_pickles_21:04:2021_14:22:07', 'rb') as earth2manifolds_pickles_vel:
    #     pickled_results = pickle.load(earth2manifolds_pickles_vel)

    # earth2halo_opt = pickled_results['opt_st']
    # earth2halo_ct = pickled_results['opt_ct']
    # earth2halo_total_ct = pickled_results['total_ct']
    # earth2halo_time = pickled_results['opt_tm']
    # earth2halo_fpar = pickled_results['opt_pr']

    # # # For explicit int v1
    # # # int_states = explicit_integration(earth2halo_opt[:,0], earth2halo_total_ct[:,1197:], earth2halo_time[-1] - earth2halo_time[300], problem.dynamics, earth2halo_fpar)

    # # For explicit int v2
    # int_states = explicit_integration(earth2halo_opt, earth2halo_total_ct, earth2halo_time[-1], problem.dynamics, earth2halo_fpar)

    # problem.plot_trajectory(int_states, 'red')
    # problem.plot_trajectory(earth2halo_opt, 'blue')

    # plt.legend()
    # plt.show()



