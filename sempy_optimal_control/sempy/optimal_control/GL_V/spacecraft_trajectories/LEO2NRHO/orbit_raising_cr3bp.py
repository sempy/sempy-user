from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np 

from sempy.optimal_control.src.cr3bp_dynamics_adapted import Cr3bpDynamics
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.primary import Primary
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.coc.rotmat import rot2irf, eci2syn
from sempy.optimal_control.src.spacecraft import Spacecraft
import sempy.core.init.constants as cst 


class OrbitRaising:
	""" OrbitRaising class, computes a orbit raising trajectory between 
		an orbit around the Earth and an orbit of a given radius in the 
		synodic frame """

	def __init__(self, cr3bp, spacecraft, init_orbit_prm, time):
		""" Initialization of the OrbitRaising class """

		self.init_orbit_prm = init_orbit_prm
		self.spacecraft = spacecraft
		self.time = time

		self.cr3bp = cr3bp
		self.cr3bp_dyna =  Cr3bpDynamics(
            self.cr3bp, Cr3bpDynamics.Eqm.dimensions6)

		self.g0 = 9.80665

		# Computation of initial states in Earth centered initial frame
		self.r_init, self.v_init = coe2sv_vec(init_orbit_prm['a'], init_orbit_prm['e'], init_orbit_prm['i'], init_orbit_prm['W'], init_orbit_prm['w'], [init_orbit_prm['ta']], cst.GM_EARTH)

		# Conversion of states from Earth centered frame to synodic frame
		self.state_syn = eci2syn(state_eci=np.concatenate((self.r_init[0], self.v_init[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

		# Propagation
		self.traj_rot = self.propagate()
		self.traj_irf = np.array([ np.dot( rot2irf(self.time[i]), self.traj_rot[i, :-1].reshape(6,1) ).transpose() for i in range(len(self.time)) ])[:, 0, :]

		# Controls history construction (T, ux, uy, uz)
		T = self.spacecraft.thrust_max * np.ones(len(time))

		v_norm = [np.linalg.norm(v) for v in self.traj_rot[:, 3:6]]
		u = np.array([[vx_, vy_, vz_]/v_norm_ for vx_, vy_, vz_, v_norm_ in zip(self.traj_rot[:, 3], self.traj_rot[:, 4], self.traj_rot[:, 5], v_norm)]).transpose()

		# States and controls matrices construction
		self.states = self.traj_rot.transpose()
		self.controls = np.vstack((T, u))

	def cr3bp_dynamics(self, y, t, T, t_f):
		""" Dynamic equations in the cr3bp for a powered spacecraft """

		r = y[:3]
		v = y[3:6]
		m = y[-1]

		# cr3bp dynamics
		x_dot, y_dot, z_dot, vx_dot, vy_dot, vz_dot = self.cr3bp_dyna.eqm_6_synodic(
            np.concatenate((r, v)), self.cr3bp.mu)

		v_norm = np.linalg.norm(v)

		# Thrust acceleration
		acc_ref = 1000 * self.cr3bp.L / ((self.cr3bp.T / (2*np.pi)) ** 2)
		acc =   T / m / acc_ref

		x_dot = x_dot
		y_dot = y_dot
		z_dot = z_dot

		ux = v[0]/v_norm
		uy = v[1]/v_norm
		uz = v[2]/v_norm

		vx_dot = vx_dot + acc*ux
		vy_dot = vy_dot + acc*uy
		vz_dot = vz_dot + acc*uz

		m_dot = - (self.cr3bp.T / (2*np.pi)) * T / self.spacecraft.isp / self.g0

		return [x_dot, y_dot, z_dot, vx_dot, vy_dot, vz_dot, m_dot]

	def propagate(self):
		""" Propagate the trajectory """
		
		# Initial state
		y0 = np.concatenate((self.state_syn, [self.spacecraft.mass0]))

		# Propagation
		y = odeint(func=self.cr3bp_dynamics, t=self.time, y0=y0, args=(self.spacecraft.thrust_max, self.time[-1]), rtol=3e-14, atol=1e-14)

		return y

	def get_initial_guess(self):
		""" Returns the initial guess """

		return self.states, self.controls, self.time

	def plot(self):
		""" Plots the trajectory """

		fig = plt.figure()
		ax = fig.add_subplot(111, projection='3d')
		plt.plot(self.traj_rot[:, 0], self.traj_rot[:, 1], self.traj_rot[:, 2], color=(0, 200/255, 200/255))
		plt.show()

if __name__ == '__main__':

	# Orbital parameters of the initial orbit
	init_orbit_prm = {'a': 42200, 'e': 0., 'i': 0 * np.pi/180, 'W': 0 * np.pi/180, 'w': 0 * np.pi/180, 'ta': 0 * np.pi/180}

	# Simulation time [-]
	prop_time = 15
	n_nodes = 1e5

	time = np.arange(0, prop_time, prop_time/n_nodes)

	# CR3BP problem
	cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

	# Spacecraft
	spacecraft = Spacecraft(mass0=1000, thrust_max=.5, isp=2000)

	# Instantiation of an OrbitRaising object
	orbit_raising = OrbitRaising(cr3bp, spacecraft, init_orbit_prm, time)

	states, controls, time = orbit_raising.get_initial_guess()

	# Plot 
	orbit_raising.plot()