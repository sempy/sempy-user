import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle
import math

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.core.crtbp import jacobi
from sempy.optimal_control.src.spacecraft import Spacecraft
import sempy.core.init.constants as cst 

from sempy.optimal_control.spacecraft_trajectories.mission import Mission
from sempy.optimal_control.src.transcription import Transcription
from sempy.optimal_control.src.solver import IPOPT
from sempy.optimal_control.src.optimization import Optimization
from sempy.optimal_control.src import utils

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising
from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.cr3bp_propagation import Cr3bpPropagation

from sempy.optimal_control.src.find_targets import find_targets

from sempy.optimal_control.src.manifold_approximation import manifold_approximation
from sempy.optimal_control.src.explicit_integration import explicit_integration

# from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_guess import Earth2NRHOGuess
# from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_guess import Earth2NRHOFixedPositionGuess
# from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_guess import Earth2NRHOFixedVelocityGuess

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_v2 import Earth2Manifold


def plot_trajectory(states, halo, index=None, branch = None, thrust_vec = None, int_ = False):

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Extraction of the states
    x = states[0]
    y = states[1]
    z = states[2]


    if thrust_vec is not None:

        N = len(thrust_vec)
        first1 = True
        first2 = True

        if int_ == False:

            for i in range(N-1):
                if thrust_vec[i]>10**(-2):

                    if first1:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', label='Thrust phases', color = 'red')
                        first1 = False
                    else:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', color = 'red')
                else:

                    if first2:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', label='Coast phases', color = 'C0')
                        first2 = False
                    else:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', color = 'C0')

        else:
        # Case of the explicit integration plot
            M = 200
            j = 0
            for i in range(M-1):

                if thrust_vec[i]>10**(-2):

                    if first1:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', label='Thrust phases', color = 'red')
                        first1 = False
                    else:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', color = 'red')

                else:
                    if first2:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', label='Coast phases', color = 'C0')    
                        first2 = False
                    else:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', color = 'C0')    
                j+=4


    x_o = halo.state_vec[:,0]
    y_o = halo.state_vec[:,1]
    z_o = halo.state_vec[:,2]

    # Plot the trajectory
    if thrust_vec is None:
        ax.plot(x, y, z, label='Spacecraft trajectory')

    # Plot the manifolds

    if branch is not None:
        ax.plot(branch.state_vec[:,0], branch.state_vec[:,1], branch.state_vec[:,2], color = 'C0')

    ax.plot(x_o, y_o, z_o, label='Arrival halo orbit', color = 'orange')

    # if index is None:
    #     for i in range(len(halo.manifolds['stable_interior'])):
    #         ax.plot(halo.manifolds['stable_interior'][i].state_vec[:, 0], halo.manifolds['stable_interior'][i].state_vec[:, 1], 
    #             halo.manifolds['stable_interior'][i].state_vec[:, 2], '-')

    # else:
    #     ax.plot(halo.manifolds['stable_interior'][index].state_vec[:, 0], halo.manifolds['stable_interior'][index].state_vec[:, 1], 
    #             halo.manifolds['stable_interior'][index].state_vec[:, 2], '-')

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    plt.legend()
    plt.show()

def approximated_manifold(halo, theta_arr, tau_arr, theta, tau):
    """ Return the states [x, y, z, vx, vy, vz] on the manifold corresponding to the two parameters
        theta and tau """

    # Inputs of the manifold_approximation() function
    cr3bp = halo.cr3bp
    C_jac = halo.C
    grid = halo.manifolds['stable_interior']

    approx_states = manifold_approximation(cr3bp, C_jac, grid, theta_arr, tau_arr, theta, tau)

    return approx_states


def set_Earth2Halo_guess(cr3bp, spc_prm, n_nodes_1, departure_orb_prm, arrival_orb):

# ###################################### First part : Earth to Manifold ######################################


    # __________Initial guess trajectory___________

    # Time of propagation of the initial trajectory [-]
    propagation_time = 3.0

    # Data needed for the construction of the initial trajectory
    # (orbit raising)
    orbit_raising_dt = {'t_prop': propagation_time}

    # __________Manifold branch computation___________

    # Manifold branch to compute (0 <= theta <= 1)
    theta_min = 0.15
    theta_max = 0.40

    theta_arr = np.linspace(theta_min, theta_max, 50)
        
    # Manifold time of propagation [-]
    tau2cpt = 15

    arrival_orb.single_manifold_computation(stability='stable', way='interior', theta=theta_arr, t_prop=tau2cpt, max_internal_steps=4_000_000)


    # Inverse the tau array
    tau_arr = np.array([-tau for tau in arrival_orb.manifolds['stable_interior'][0].t_vec])

    # Useful portion of the manifold (in percentage)
    # Only this part will be interpolated to save computation time
    useful_prt = 0

    # __________Point of the manifold targeted, parametrized by tau and theta__________

    propagation_time = orbit_raising_dt['t_prop']
    # time = np.arange(0, propagation_time, propagation_time/n_nodes_1)


    # arg_min, idx_manifold, m = find_targets(crtbp = cr3bp, spacecraft = Spacecraft(**spc_prm), init_orbit_prm = departure_orb_prm, time = time,
    #                                          manifolds = arrival_orb.manifolds, stability = 'stable', way = 'interior')

    arg_min = 3365
    idx_manifold = 37

    # print(idx_manifold)
    # print(arg_min)

    target_theta = theta_arr[idx_manifold]
    target_tau = tau_arr[arg_min]

    print(target_theta )
    print(target_tau)


    # ___________Interpolation of the manifolds___________

    beg_prc = useful_prt
    end_prc = 1


    target_state = approximated_manifold(arrival_orb, theta_arr, tau_arr, target_theta, target_tau)

    ######################################## Optimization over pos, vel and theta and tau ########################################

    # Manifolds data
    manifolds_dt = {'halo': arrival_orb, 'target_tau': target_tau, 'target_theta': target_theta, 'useful_part': useful_prt,'theta_arr': theta_arr, 'tau_arr': tau_arr}


    # ____________________________________ Class Instantiation _______________________________________________
    #Possible linear solvers : ma27, ma57, ma77, ma86, mumps
    linear_solver = 'ma86'

    options = {'explicit_integration': False, 'host': 'laptop', 'linear_solver': linear_solver,
                    'plot_results': False, 'plot_physical_err': False, 'n_nodes': n_nodes_1, 'pickle_results': True, 'name': 'earth2mfld_theta_tau_L1'}

    # Instantiation of the problem
    problem = Earth2Manifold(spc_prm, departure_orb_prm, orbit_raising_dt, manifolds_dt, n_nodes_1)

    # # Instantiation of the optimization
    # optimization = Optimization(problem=problem, **options)

    # optimization.run()


    # ###################################### Second part : drift on the manifold ######################################

    with open('pickled_results/opt_vel/vel_l2_MEO', 'rb') as earth2manifolds_pickles_vel:
        pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)

    # Recovery of the states, controls, time and free parameters values from pickled results
    states_ = pickled_results_vel['opt_st']
    controls_ = pickled_results_vel['opt_ct']
    total_ct = pickled_results_vel['total_ct']
    time_ = pickled_results_vel['opt_tm']
    par_ = pickled_results_vel['opt_pr']

    # # Recovery of the states, controls, time and free parameters values
    # states_ = optimization.results['opt_st']
    # controls_ = optimization.results['opt_ct']
    # time_ = optimization.results['opt_tm']
    # par_ = optimization.results['opt_pr']

    print(par_)

    # plot_trajectory(states_, arrival_orb, branch)


    # # Taking last part's final states to start this step
    # init_state = states_[:,-1]

    # prop_time_2 = tau2cpt

    # time = np.arange(0, prop_time_2, prop_time_2/n_nodes_2)  # <---------------- Attempt with same propagation time

    # cr3bp_propagation = Cr3bpPropagation(cr3bp, Spacecraft(**spc_prm), init_state, time)

    # states, controls, time = cr3bp_propagation.get_initial_guess()

    # Method 2 : Just attributing the spacecraft the manifolds' states from its insertion point
    earth2manifold_states = states_

    # adding the mass to the states

    n_nodes_2 = 350

    x_temp = np.zeros((1,n_nodes_2))
    x_states = x_temp + earth2manifold_states[6,-1]

    # ___________Initial guess states__________________________

    # time = np.linspace(0,par_[2], n_nodes_2)
    time = np.linspace(0,target_tau, n_nodes_2)

    copy_arr_orb = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10)
    copy_arr_orb.interpolation()

    branch = arrival_orb.ManifoldBranch(orbit = copy_arr_orb, stability = 'stable', way = 'interior')
    # branch.computation(par_[1], par_[2])
    branch.computation(target_theta, target_tau)

    mlfd_branch = np.flipud(branch.state_vec[:,:])
    N,M = np.shape(mlfd_branch)

    step = int(N/n_nodes_2)

    temp_states = mlfd_branch[0:-1:step,:]

    # Removing the excess of compenents 
    n,m = np.shape(temp_states)

    idx_to_remove = [-(i+1) for i in range(n - n_nodes_2-1)]

    temp_states = np.delete(temp_states, idx_to_remove,0)
    # temp_states = np.vstack((temp_states, branch.state_vec[0,:])) # ---> We add the last branch elt to be sure to arrive on the orbit


    mfld2halo_pos_vel = temp_states[1:,:].transpose()

    # Computing and adding the mass to the states
    mass_vec = earth2manifold_states[6,-1]*np.ones((1,n_nodes_2))
    mfld2halo_states = np.concatenate((mfld2halo_pos_vel, mass_vec))

    earth2halo_states = np.hstack((earth2manifold_states,mfld2halo_states))

    # cr3bp_propagation = Cr3bpPropagation(cr3bp, Spacecraft(**spc_prm), earth2manifold_states[0:7,-1], time)
    # states_prop, controls, time = cr3bp_propagation.get_initial_guess()

    # manifold2halo_states = states_prop
    # earth2halo_states = np.hstack((earth2manifold_states,manifold2halo_states))

    # ___________Initial guess controls__________________________

    # x_temp_time = np.zeros((4,n_nodes_2))
    # earth2halo_controls = np.hstack((controls_,x_temp_time))          # Adding zero component for the thrust (coast phase on the maniold branch)

    v_norm = [np.linalg.norm(v) for v in mfld2halo_states[3:6, :].transpose()]
    u = np.array([[vx_, vy_, vz_]/v_norm_ for vx_, vy_, vz_, v_norm_ in zip(mfld2halo_states[3, :], mfld2halo_states[4, :], mfld2halo_states[5, :], v_norm)]).transpose()
    T = np.zeros(len(time))

    manifold2halo_controls = np.vstack((T,u))

    earth2halo_controls = np.hstack((controls_,manifold2halo_controls))          # Adding zero component for the thrust (coast phase on the maniold branch)

    # ___________Initial guess time__________________________

    # time2add = np.linspace(time_[-1], time_[-1] + par_[2], n_nodes_2+1)
    time2add = np.linspace(time_[-1], time_[-1] + target_tau, n_nodes_2+1)
    earth2halo_time = np.concatenate([time_, time2add[1:]])

    # ___________Initial guess prm__________________________
    
    earth2halo_prm = par_

    ############################################################## For Results ######################################################

    # # plot_trajectory(states_, arrival_orb, branch = branch)
    # # input()

    # int_states = explicit_integration(states_, total_ct, time_[-1], problem.dynamics, par_)

    # # Plotting the results of explicit integration

    # plot_trajectory(int_states, arrival_orb, controls_[0,:], branch = branch, thrust_vec = earth2halo_controls[0,:], int_ = True)
    # input()

    # plot_trajectory(earth2halo_states, arrival_orb, thrust_vec = earth2halo_controls[0,:])
    # utils.classic_display(earth2halo_time, earth2halo_states, earth2halo_controls)

    ############################################################## For Results ######################################################


    return earth2halo_states, earth2halo_controls, earth2halo_time, earth2halo_prm



if __name__ == '__main__':

    # Three body problem
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spc_prm = {'mass0': m0, 'thrust_max': thrust_max,'isp': Isp, 'mass_dry': m_dry, 'thrust_min': thrust_min}

    # # Departure orbit
    # a = 42164  # initial semi-major axis [km]
    # e = 0      # final semi-major axis [km]
    # i = 28.58  # inclination [deg]
    # W = 90    # right Ascension of the Ascending Node [deg]
    # w = 0      # perigee argument [deg]
    # ta = 120     # initial true anomaly [deg]

    # Departure orbit
    a = 35000  # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58  # inclination [deg]
    W = 0    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 120     # initial true anomaly [deg]

    departure_orb_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180, 'ta': ta*np.pi/180}

    # Arrival orbit
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo.interpolation()

    # Implementing n_nodes for the different steps
    n_nodes_1 = 225

    earth2halo_states, earth2halo_controls, earth2halo_time, earth2halo_prm = set_Earth2Halo_guess(cr3bp, spc_prm, n_nodes_1, departure_orb_prm, halo)
    plot_trajectory(earth2halo_states, halo)

    #print(np.shape(earth2halo_states))
