import numpy as np
import math
import cppad_py

import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.spacecraft_trajectories.mission import Mission
from sempy.optimal_control.src.transcription import Transcription
from sempy.optimal_control.src.solver import IPOPT
from sempy.optimal_control.src.optimization import Optimization
import sempy.core.init.constants as cst 


class Halo2NRHOGuess(Mission):

    def __init__(self, spacecraft_prm, halo, target_state, **kwargs):
        """ Initialization of the LLO2LLO class."""

        Mission.__init__(self, spacecraft_prm, **kwargs)

        # Stocking orbital parameters and target states as Variables
        self.halo = halo
        self.target_state = target_state
        self.spacecraft_prm = spacecraft_prm 
        self.n_nodes = kwargs['n_nodes']

    def path_constraints(self, states, controls, states_add, controls_add, controls_col, f_par):
        """ Computation of the path constraints 
        """
        st_path = np.ndarray((self.prm['n_st_path_con'],
                            2*self.prm['n_nodes']-1), dtype=cppad_py.a_double)
        ct_path = np.ndarray((self.prm['n_ct_path_con'],
                            4*self.prm['n_nodes']-3), dtype=cppad_py.a_double)

        ux = np.concatenate((controls[1], controls_add[1], controls_col[1]))
        uy = np.concatenate((controls[2], controls_add[2], controls_col[2]))
        uz = np.concatenate((controls[3], controls_add[3], controls_col[3]))

        u2 = ux*ux + uy*uy + uz*uz

        ct_path[0] = u2 - 1

        return st_path, ct_path

    def set_path_constraints_boundaries(self):
        """ Setting of the path constraints boundaries """
        self.low_bnd.ct_path[0] = self.upp_bnd.ct_path[0] = 0


class Halo2NRHOFixedPositionGuess(Halo2NRHOGuess):

    def __init__(self, spacecraft_prm, starting_point, halo, target_state, **kwargs):
        """ Initialization of the LLO2LLO class."""

        self.starting_point = starting_point
        
        Halo2NRHOGuess.__init__(self, spacecraft_prm, halo, target_state,  \
            n_states=7, n_controls=4, n_event_con=7, n_st_path_con=0, n_ct_path_con=1,  n_f_par=0, **kwargs)


    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Computation of the end point cost (Mayer term) 

            Parameters
            ----------
            ti : float
                Initial time value
            xi : array
                Initial states array
            tf : float
                Final time value
            xf : array
                Final states array
            f_prm : array
                Free parameters array

            Returns
            -------
            float
                Mayer term value

        """

        # Target position in Synodic frame
        x_tgt, y_tgt, z_tgt = self.target_state[:3]


        # Final position
        x_f, y_f, z_f = xf[:3]

        # Initial and final mass
        m0 = xi[6]
        m_f = xf[6]

        return ((x_f-x_tgt)*(x_f-x_tgt) + (y_f-y_tgt)*(y_f-y_tgt) + (z_f-z_tgt)*(z_f-z_tgt)) #+ 0.001*(m0 - m_f)*(m0 - m_f)/(m0)**2

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
        """ Computation of the events constraints 

            Parameters
            ----------
            xi : array
                Array of states at initial time
            ui : array
                Array of controls at initial time
            xf : array
                Array of states at final time
            uf : array
                Array of controls at final time
            ti : float
                Value of initial time
            tf : float
                Value of final time
            f_prm : array
                Free parameters array

            Returns
            -------
            constraints : array
                Array of the event constraints

        """
        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0, m0 = self.initial_guess.states[:,0]

        # x, y, z (init )[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = m0 - xi[6]

        return constraints

    def set_events_constraints_boundaries(self):
        """ Setting of the events constraints boundaries """

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

    def set_initial_guess(self):
        """ Setting of the initial guess for the states, controls, free-parameters
            and time grid """

        # We first try to recognize the starting point on the Halo orbit
        N,M = np.shape(self.halo.state_vec)
        # idx = 0

        # for n in range(N):
        #     if self.starting_point in self.halo.state_vec[n,:6]:
        #         idx = n

        # if idx == 0:
        diff = self.halo.state_vec[:,:6] - self.starting_point
        norm_diff = [np.linalg.norm(vect) for vect in diff]
        idx = np.argmin(norm_diff)

        step = int((N-idx)/self.n_nodes)
        
        # Shifts for the Halo orbit to begin indices at idx
        shift_h = N-idx
        halo_st = self.halo.state_vec.transpose()[:6]
        halo_st = np.roll(halo_st, shift_h, axis=1)
        
        # Initialization of the states, controls and time
        states = np.ndarray(shape=(7, self.n_nodes))
        controls = np.ndarray(shape=(4, self.n_nodes))
        time = np.zeros(self.n_nodes)

        # Removing the excess of compenents 
        n,m = np.shape(halo_st[:,0:-1:step])

        idx_to_remove = [-(i+1) for i in range(m - self.n_nodes)]

        temp_for_states = np.delete(halo_st[:,0:-1:step], idx_to_remove,1)

        # Defining the states for inital guess
        states[:6,:] = temp_for_states
        
        # Adding the mass
        states[6] = self.spacecraft.mass0 * np.ones(self.n_nodes)

        # Set the controls
        controls[0] = np.zeros(self.n_nodes)

        v_norm = np.sqrt( states[3]*states[3] + states[4]*states[4] + states[5]*states[5])
        controls[1] = states[3] / v_norm
        controls[2] = states[4] / v_norm
        controls[3] = states[5] / v_norm

        # Set the time
        time = np.linspace(0, 2, self.n_nodes)

        # Set the initial guess
        self.initial_guess.states = states
        self.initial_guess.controls = controls
        self.initial_guess.time = time


class Halo2NRHOFixedVelocityGuess(Halo2NRHOGuess):

    def __init__(self, spacecraft_prm, halo, target_state, initial_guess_prv, **kwargs):
        """ Initialization of the LLO2LLO class."""

        # Storage of the target states 
        self.halo = halo
        self.target_state = target_state

        # Storage of the initial guess as an attribut
        self.initial_guess_prv = initial_guess_prv

        Halo2NRHOGuess.__init__(self, spacecraft_prm, halo, target_state, \
            n_states=7, n_controls=4, n_event_con=10, n_st_path_con=0, n_ct_path_con=1,  n_f_par=0, **kwargs)

    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Computation of the end point cost (Mayer term) 

            Parameters
            ----------
            ti : float
                Initial time value
            xi : array
                Initial states array
            tf : float
                Final time value
            xf : array
                Final states array
            f_prm : array
                Free parameters array

            Returns
            -------
            float
                Mayer term value

        """
        # Target velocity in Synodic frame
        vx_tgt, vy_tgt, vz_tgt = self.target_state[3:6]

        # Final velocity
        vx_f, vy_f, vz_f = xf[3:-1]

        # Initial and final mass
        m0 = xi[6]
        m_f = xf[6]

        return ((vx_f-vx_tgt)*(vx_f-vx_tgt) + (vy_f-vy_tgt)*(vy_f-vy_tgt) + (vz_f-vz_tgt)*(vz_f-vz_tgt)) #+ 0.001*(m0 - m_f)*(m0 - m_f)/(m0)**2

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
        """ Computation of the events constraints 

            Parameters
            ----------
            xi : array
                Array of states at initial time
            ui : array
                Array of controls at initial time
            xf : array
                Array of states at final time
            uf : array
                Array of controls at final time
            ti : float
                Value of initial time
            tf : float
                Value of final time
            f_prm : array
                Free parameters array

            Returns
            -------
            constraints : array
                Array of the event constraints

        """
        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0, m0 = self.initial_guess_prv['states'][:,0]

        # x, y, z (init )[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = m0 - xi[6]

        # x, y, z (final) [-]
        constraints[7] = self.target_state[0] - xf[0]
        constraints[8] = self.target_state[1] - xf[1]
        constraints[9] = self.target_state[2] - xf[2]

        return constraints

    def set_events_constraints_boundaries(self):
        """ Setting of the events constraints boundaries """

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

        # Position (final) [-]
        self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
        self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
        self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

    def set_initial_guess(self):
        """ Setting of the initial guess for the states, controls, free-parameters
            and time grid """
        
        self.initial_guess.states = self.initial_guess_prv['states']
        self.initial_guess.controls = self.initial_guess_prv['controls']
        self.initial_guess.time = self.initial_guess_prv['time']
