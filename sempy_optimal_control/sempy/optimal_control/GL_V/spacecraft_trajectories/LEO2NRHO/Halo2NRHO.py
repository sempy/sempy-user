#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  01 10:40:21 2021

@author: GARY Yvan

"""
import numpy as np
import math
import cppad_py

import pickle

import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.spacecraft import Spacecraft

from sempy.optimal_control.spacecraft_trajectories.mission import Mission
from sempy.optimal_control.src.optimization import Optimization

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.Halo2NRHO_guess import Halo2NRHOFixedPositionGuess, Halo2NRHOFixedVelocityGuess

import sempy.core.init.constants as cst 


class Halo2NRHO(Mission):

	def __init__(self, spacecraft_prm, starting_point, halo, nrho, n_nodes):
		""" Pattern of an optimal control problem definition """
	

		""" `HALO2NRHO` class implements a low-thrust transfer between a Halo and a NRHO orbits
			using trajectory stacking method to generate the initial guess.
			
			cf. Robert Pritchett, Kathleen Howell, and Daniel Grebow. “Low-Thrust Transfer Design Based 
			on Collocation Techniques: Applications in the Restricted Three-Body Problem”. 

			Parameters
			----------
			spacecraft_prm : dict
				Spacecraft parameters dictionnary
			halo : CrtbpOrbit Object
				Halo departure orbit 
			nrho : CrtbpOrbit Object
				NRHO arrival orbit 

			Attributs
			---------
			halo : Halo orbit object
				Departure Halo orbit object
			nrho : NRHO orbit object
				Arrival NRHO orbit object


		"""
		Mission.__init__(self, spacecraft_prm, n_states=7, n_controls=4, n_event_con=13, n_st_path_con=0, n_ct_path_con=1, n_f_par=0, n_nodes=n_nodes)

		self.halo = halo
		self.nrho = nrho
		self.spacecraft_prm = spacecraft_prm
		self.n_nodes = n_nodes
		self.starting_point = starting_point

	def set_boundaries(self):
		""" Setting of the states, controls, free-parameters, initial and final times
						boundaries """

		# X (syn. fram) [-]
		self.low_bnd.states[0] = -0.5 #- self.cr3bp.mu #- 1.0 # peut-etre a modifier
		self.upp_bnd.states[0] = 1.5 #- self.cr3bp.mu #+ 1.0 # peut-etre a modifier

		# Y (syn. fram) [-]
		self.low_bnd.states[1] = - 0.5
		self.upp_bnd.states[1] = 0.5

		# Z (syn. fram) [-]
		self.low_bnd.states[2] = - 0.5
		self.upp_bnd.states[2] = 0.5

		# Vx (syn. fram) [-]
		self.low_bnd.states[3] = - 1
		self.upp_bnd.states[3] = 1

		# Vy (syn. fram) [-]
		self.low_bnd.states[4] = - 1
		self.upp_bnd.states[4] = 1

		# Vz (syn. fram) [-]
		self.low_bnd.states[5] = - 1
		self.upp_bnd.states[5] = 1

		# Set the mass limits [-]
		self.low_bnd.states[6] = 10**(-4)
		self.upp_bnd.states[6] = self.spacecraft.mass0

		# Set the thrust limits [-]
		self.low_bnd.controls[0] = self.spacecraft.thrust_min 
		self.upp_bnd.controls[0] = self.spacecraft.thrust_max 

		# Set the limits for controls ux, uy, uz
		for i in [1, 2, 3]:
			self.low_bnd.controls[i] = -1
			self.upp_bnd.controls[i] = 1

		# Set the times limits
		t_init  = self.initial_guess.time[0]
		t_final = self.initial_guess.time[-1]

		self.low_bnd.ti = self.upp_bnd.ti = t_init

		self.low_bnd.tf = 0.5 * t_final
		self.upp_bnd.tf = 1.5 * t_final


	def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):
		""" Computation of the events constraints """
		constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

		x0, y0, z0, vx0, vy0, vz0, m0 = self.initial_guess.states[:,0]

		x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = self.initial_guess.states[:6,-1]

		# x, y, z (init)[-]
		constraints[0] = x0 - xi[0]
		constraints[1] = y0 - xi[1]
		constraints[2] = z0 - xi[2]

		# vx, vy, vz (init) [-]
		constraints[3] = vx0 - xi[3]
		constraints[4] = vy0 - xi[4]
		constraints[5] = vz0 - xi[5]

		# m [-]
		constraints[6] = m0 - xi[6]

		# x, y, z (final)[-]
		constraints[7] = x_tgt - xf[0]
		constraints[8] = y_tgt - xf[1]
		constraints[9] = z_tgt - xf[2]

		# vx, vy, vz (final) [-]
		constraints[10] = vx_tgt - xf[3]
		constraints[11] = vy_tgt - xf[4]
		constraints[12] = vz_tgt - xf[5]

		return constraints

	def set_events_constraints_boundaries(self):
		""" Setting of the events constraints boundaries """

		# Position (init) [-]
		self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
		self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
		self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

		# Velocity (init) [-]
		self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
		self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
		self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

		# Mass (init) [-]
		self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

		# Position (init) [-]
		self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
		self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
		self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

		# Velocity (init) [-]
		self.low_bnd.event[10] = self.upp_bnd.event[10] = 0
		self.low_bnd.event[11] = self.upp_bnd.event[11] = 0
		self.low_bnd.event[12] = self.upp_bnd.event[12] = 0


	def path_constraints(self, states, controls, states_add, controls_add, controls_col, f_par):
		""" Computation of the path constraints """
		# paths = np.ndarray((self.prm['n_path_con'],
		# 					self.prm['n_nodes']), dtype=cppad_py.a_double)

		st_path = np.ndarray((self.prm['n_st_path_con'],
							2*self.prm['n_nodes']-1), dtype=cppad_py.a_double)
		ct_path = np.ndarray((self.prm['n_ct_path_con'],
							4*self.prm['n_nodes']-3), dtype=cppad_py.a_double)

		ux = np.concatenate((controls[1], controls_add[1], controls_col[1]))
		uy = np.concatenate((controls[2], controls_add[2], controls_col[2]))
		uz = np.concatenate((controls[3], controls_add[3], controls_col[3]))

		u2 = ux*ux + uy*uy + uz*uz

		ct_path = u2 - 1

		return st_path, ct_path

	def set_path_constraints_boundaries(self):
		""" Setting of the path constraints boundaries """
		self.low_bnd.ct_path[0] = self.upp_bnd.ct_path[0] = 0


	def end_point_cost(self, ti, xi, tf, xf, f_prm):
		""" Compute the Mayer term of the cost function """

		m0 = xi[6]
		m_f = xf[6]

		# Maximization of the final mass
		return (m0 - m_f)*(m0 - m_f)/(m0)**2

	def set_initial_guess(self):
		""" Setting of the initial guess for the states, controls, free-parameters
						and time grid """

		# Manifold branch point to target (specific here):
		# target_state = self.nrho.manifolds['stable_interior'][9].state_vec[-1,:6]
		target_state = self.nrho.state_vec[500,:]
		n_nodes = 150

		# Possible linear solvers : ma27, ma57, ma77, ma86, mumps
		linear_solver = 'ma86'

		options = {'explicit_integration': False, 'host': 'laptop', 'linear_solver': linear_solver,
					'plot_results': False, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True}

		# ____________________________________________ Fixed Position Optimization ____________________________________________

		# First optimisation on position for insertion on an nrho manifold branch

		halo2nrho_pos = Halo2NRHOFixedPositionGuess(self.spacecraft_prm, self.starting_point, self.halo, target_state, n_nodes = self.n_nodes)

		# Instantiation of the optimization
		fixed_pos_optimization = Optimization(problem=halo2nrho_pos, **options)

		# Running the optimization process
		fixed_pos_optimization.run()

		# # Plot the trajectory
		# self.plot_trajectory(fixed_pos_optimization.results['opt_st'])

		# ____________________________________________ Fixed Position & Velocity Optimization ____________________________________________

		# Recovery of the previous optimization results
		intermediate_states = fixed_pos_optimization.results['opt_st']
		intermediate_controls = fixed_pos_optimization.results['opt_ct']
		intermediate_time = fixed_pos_optimization.results['opt_tm']
		intermediate_par = fixed_pos_optimization.results['opt_pr']

		# Optimized initial guess
		initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}

		halo2nrho_vel = Halo2NRHOFixedVelocityGuess(self.spacecraft_prm, self.halo, target_state, initial_guess, n_nodes=self.n_nodes)

		# Instantiation of the optimization
		fixed_vel_optimization = Optimization(problem=halo2nrho_vel, **options)

		# Running the optimization process
		fixed_vel_optimization.run()

		# # Plot the trajectory
		# self.plot_trajectory(fixed_vel_optimization.results['opt_st'])

		# Recovery of the states, controls, time and free parameters values
		states_ = fixed_vel_optimization.results['opt_st']
		controls_ = fixed_vel_optimization.results['opt_ct']
		time_ = fixed_vel_optimization.results['opt_tm']
		par_ = fixed_vel_optimization.results['opt_pr']

		# with open('pickled_results/halo2nrho/halo2nrho_vel_m', 'rb') as earth2manifolds_pickles_vel:
		# 	pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)
        


  #       # Recovery of the states, controls, time and free parameters values from pickled results
		# states_ = pickled_results_vel['opt_st']
		# controls_ = pickled_results_vel['opt_ct']
		# time_ = pickled_results_vel['opt_tm']

		# self.plot_trajectory(states_)

		# Getting an inital guess
		self.initial_guess.states = states_
		self.initial_guess.controls = controls_
		self.initial_guess.time = time_

	def plot_trajectory(self, states):

		fig = plt.figure()
		ax = fig.gca(projection='3d')

		# Extraction of the states
		x = states[0]
		y = states[1]
		z = states[2]

		# Orbit 
		orbit_dep = self.halo
		orbit_arr = self.nrho

		x_dep = orbit_dep.state_vec[:, 0]
		y_dep = orbit_dep.state_vec[:, 1]
		z_dep = orbit_dep.state_vec[:, 2] 

		x_arr = orbit_arr.state_vec[:, 0]
		y_arr = orbit_arr.state_vec[:, 1]
		z_arr = orbit_arr.state_vec[:, 2] 

		# Plot the trajectory
		ax.plot(x, y, z, label='Spacecraft trajectory')
		ax.plot(x_dep, y_dep, z_dep, label='Departure orbit', color = 'red')
		ax.plot(x_arr, y_arr, z_arr, label='Arrival orbit', color = 'red')
		ax.plot([1], [0], [0], 'mo')

		# self.nrho.single_manifold_computation(stability='stable', way='interior', theta=10, t_prop=7, max_internal_steps=4_000_000)

		# # Plot manifolds from nrho orbit
		# for i in range(len(self.nrho.manifolds['stable_interior'])):
		# i = 9
		# ax.plot(self.nrho.manifolds['stable_interior'][i].state_vec[:, 0], self.nrho.manifolds['stable_interior'][i].state_vec[:, 1], 
		#                 self.nrho.manifolds['stable_interior'][i].state_vec[:, 2], '-')


		plt.legend()
		plt.show()

if __name__ == '__main__':

	# Number of nodes
	n_nodes = 150

	linear_solver = 'ma86'

	options = {'explicit_integration': False, 'host': 'laptop', 'linear_solver': linear_solver,
				'plot_results': False, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True, 'name': 'halo2nrho_m'}

	# Spacecraft properties
	m0 = 1000  # initial mass [kg]
	m_dry = 10  # dry mass [kg]
	thrust_max = 2  # maximum thrust [N]
	thrust_min = 0  # minimum thrust [N]
	Isp = 2000  # specific impulse [s]

	spacecraft_prm = {'mass0': m0, 'mass_dry': m_dry, 'thrust_max': thrust_max, 'thrust_min': thrust_min, 'isp': Isp}

	cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

	# Departure orbit
	halo = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac=3.10)
	halo.interpolation()

	# Final orbit
	nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Cjac=3.05)
	nrho.interpolation()

	nrho.single_manifold_computation(stability='stable', way='interior', theta=10, t_prop=7.3, max_internal_steps=4_000_000)

	starting_point = halo.state_vec[50,:6]

	# Instantiation of the problem
	problem = Halo2NRHO(spacecraft_prm, starting_point, halo, nrho, n_nodes=n_nodes)

	# Instantiation of the optimization
	optimization = Optimization(problem=problem, **options)

	# Launch of the optimization
	optimization.run()

	# # Plot results
	# problem.plot_trajectory(optimization.results['opt_st'])