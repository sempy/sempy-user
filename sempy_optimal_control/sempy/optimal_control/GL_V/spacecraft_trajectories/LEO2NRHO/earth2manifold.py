import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.core.crtbp import jacobi
from sempy.optimal_control.src.spacecraft import Spacecraft
from sempy.optimal_control.coc.coe2svvec import coe2sv_vec
from sempy.optimal_control.coc.rotmat import eci2syn
import sempy.core.init.constants as cst 

from sempy.optimal_control.spacecraft_trajectories.mission import Mission
from sempy.optimal_control.src.optimization import Optimization
from sempy.optimal_control.src import utils

# from sempy.core.bspline.bispev import bispev

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising
from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_guess import Earth2NRHOFixedPositionGuess, Earth2NRHOFixedVelocityGuess

from sempy.optimal_control.src.find_targets import find_targets

# For explicit integration
from sempy.optimal_control.src.explicit_integration import explicit_integration

# For manifold interpolation
from sempy.optimal_control.src.manifold_approximation import manifold_approximation






class Earth2Manifold(Mission):

    def __init__(self, spc_prm, orb_prm, orbit_raising_dt, manifolds_dt, n_nodes):
        """ Initialization of the LLO2LLO class."""

        # Storing some parameters
        self.orb_prm = orb_prm                      # Initial orbit parameters
        self.orbit_raising_dt = orbit_raising_dt    # Parameters destinated to the OrbitRaising class
        self.manifolds_dt = manifolds_dt            # Parameters destinated to the cpt_interpolation_coefficients method
        self.spc_prm = spc_prm                      # Spacecraft parameters
        self.n_nodes = n_nodes


        Mission.__init__(self, spc_prm, n_states=7, n_controls=4, n_event_con=13, n_st_path_con=0, n_ct_path_con=1, n_f_par=1, n_nodes=n_nodes)

        # Computation of free parameters boundaries
        self.set_free_parameters_bnd()

    def approximated_manifold(self, theta, tau):
        """ Return the states [x, y, z, vx, vy, vz] on the manifold corresponding to the two parameters
            theta and tau """

        # Inputs of the manifold_approximation() function
        cr3bp = self.manifolds_dt['halo'].cr3bp
        C_jac = self.manifolds_dt['halo'].C
        grid = self.manifolds_dt['halo'].manifolds['stable_interior']
        theta_array = self.manifolds_dt['theta_arr']
        tau_array = self.manifolds_dt['tau_arr']

        approx_states = manifold_approximation(cr3bp, C_jac, grid, theta_array, tau_array, theta, tau)

        return approx_states

    def set_free_parameters_bnd(self):
        """ Sets the free parameters boundaries """
        
        # True anomaly [rad]
        self.low_bnd.f_par[0] = -np.pi 
        self.upp_bnd.f_par[0] = np.pi

        # # Inclination
        # self.low_bnd.f_par[1] = 0
        # self.upp_bnd.f_par[1] = np.pi/2


        # # Semi-major axis
        # self.low_bnd.f_par[2] = 2000 
        # self.upp_bnd.f_par[2] = 35786



        # Theta [-]
        # self.low_bnd.f_par[1] = min([man_.theta for man_ in self.manifolds_dt['halo'].manifolds['stable_interior']])
        # self.upp_bnd.f_par[1] = max([man_.theta for man_ in self.manifolds_dt['halo'].manifolds['stable_interior']])

        # self.low_bnd.f_par[1] = 0.15
        # self.upp_bnd.f_par[1] = 0.40

        # beg_ind = int(self.manifolds_dt['useful_part']*len(self.manifolds_dt['halo'].manifolds['stable_interior'][0].t_vec))

        # # Tau [-]
        # # self.low_bnd.f_par[2] = min(self.manifolds_dt['tau_arr'][beg_ind:])
        # # self.upp_bnd.f_par[2] = max(self.manifolds_dt['tau_arr'][beg_ind:])
        # self.low_bnd.f_par[2] = 6
        # self.upp_bnd.f_par[2] = 15


    def end_point_cost(self, ti, xi, tf, xf, f_prm):
        """ Compute the Mayer term of the cost function """

        m0 = xi[6]
        m_f = xf[6]

        # Maximization of the final mass
        # return (m0 - m_f)*(m0 - m_f)/(m0)**2
        return (m0-m_f)**2/(m0**2)


    def path_constraints(self, states, controls, states_add, controls_add, controls_col, f_par):
        """ Computation of the path constraints  """
        st_path = np.ndarray((self.prm['n_st_path_con'],
                            2*self.prm['n_nodes']-1), dtype=cppad_py.a_double)
        ct_path = np.ndarray((self.prm['n_ct_path_con'],
                            4*self.prm['n_nodes']-3), dtype=cppad_py.a_double)

        ux = np.concatenate((controls[1], controls_add[1], controls_col[1]))
        uy = np.concatenate((controls[2], controls_add[2], controls_col[2]))
        uz = np.concatenate((controls[3], controls_add[3], controls_col[3]))

        u2 = ux*ux + uy*uy + uz*uz

        ct_path = u2 - 1

        return st_path, ct_path

    def set_path_con_bnd(self):
        """ Sets the path constraints boundaries """

        self.low_bnd.ct_path[0] = self.upp_bnd.ct_path[0] = 0

    def event_constraints(self, xi, ui, xf, uf, ti, tf, f_prm):

        constraints = np.ndarray((self.prm['n_event_con'], 1),
                                 dtype=cppad_py.a_double)

        # Initial orbit free parameters
        #ta, theta, tau = f_prm
        ta = f_prm[0]
        # i = f_prm[1]

        # target_theta = f_prm[1]
        # target_tau = f_prm[2]

        # States in ECI
        r0_eci, v0_eci = coe2sv_vec(self.orb_prm['a'], self.orb_prm['e'], self.orb_prm['i'], self.orb_prm['W'], self.orb_prm['w'], [ta], cst.GM_EARTH)
        
        # Conversion in Synodic frame
        x0, y0, z0, vx0, vy0, vz0 = eci2syn(np.hstack((r0_eci[0], v0_eci[0])), time=0., mu_cr3bp=self.cr3bp.mu, lc_cr3bp=self.cr3bp.L, tc_cr3bp=self.cr3bp.T/(2*np.pi))

        # Targeted position, function of theta and tau parameters
        state_target = self.approximated_manifold(target_theta, target_tau)

        x_tgt, y_tgt, z_tgt, vx_tgt, vy_tgt, vz_tgt = state_target[:]

        m0 = self.spc_prm['mass0']

        # x, y, z (init)[-]
        constraints[0] = x0 - xi[0]
        constraints[1] = y0 - xi[1]
        constraints[2] = z0 - xi[2]

        # vx, vy, vz (init) [-]
        constraints[3] = vx0 - xi[3]
        constraints[4] = vy0 - xi[4]
        constraints[5] = vz0 - xi[5]

        # m [-]
        constraints[6] = m0 - xi[6]

        # x, y, z (final)[-]
        constraints[7] = x_tgt - xf[0]
        constraints[8] = y_tgt - xf[1]
        constraints[9] = z_tgt - xf[2]

        # vx, vy, vz (final) [-]
        constraints[10] = vx_tgt - xf[3]
        constraints[11] = vy_tgt - xf[4]
        constraints[12] = vz_tgt - xf[5]

        return constraints

    def set_events_con_bnd(self):
        """ Sets the events constraints """

        xi, yi, zi, vxi, vyi, vzi, mi = self.initial_guess.states[:, 0]

        # Position (init) [-]
        self.low_bnd.event[0] = self.upp_bnd.event[0] = 0
        self.low_bnd.event[1] = self.upp_bnd.event[1] = 0
        self.low_bnd.event[2] = self.upp_bnd.event[2] = 0

        # Velocity (init) [-]
        self.low_bnd.event[3] = self.upp_bnd.event[3] = 0
        self.low_bnd.event[4] = self.upp_bnd.event[4] = 0
        self.low_bnd.event[5] = self.upp_bnd.event[5] = 0

        # Mass (init) [-]
        self.low_bnd.event[6] = self.upp_bnd.event[6] = 0

        # Position (init) [-]
        self.low_bnd.event[7] = self.upp_bnd.event[7] = 0
        self.low_bnd.event[8] = self.upp_bnd.event[8] = 0
        self.low_bnd.event[9] = self.upp_bnd.event[9] = 0

        # Velocity (init) [-]
        self.low_bnd.event[10] = self.upp_bnd.event[10] = 0
        self.low_bnd.event[11] = self.upp_bnd.event[11] = 0
        self.low_bnd.event[12] = self.upp_bnd.event[12] = 0

    def set_initial_guess(self):
        """ Set the initial guess using the earth2manifold_guess classes
            `Earth2NRHOFixedPositionGuess` and `Earth2NRHOFixedVelocityGuess` """

        # ________________________________ Fixed Position Initial Guess Generation _______________________________________________

        target_state = self.approximated_manifold(self.manifolds_dt['target_theta'], self.manifolds_dt['target_tau'])

        halo = self.manifolds_dt['halo']

        n_nodes = self.n_nodes

        # # Possible linear solvers : ma27, ma57, ma77, ma86, mumps
        # linear_solver = 'ma86'

        # options = {'explicit_integration': False, 'host': 'laptop', 'linear_solver': linear_solver,
        #             'plot_results': False, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True, 'name':'pickled_pos_to_l2_150nodes'}

        # earth2nrho_fixed_pos = Earth2NRHOFixedPositionGuess(self.spc_prm, target_state, self.orb_prm, self.orbit_raising_dt['t_prop'], n_nodes=self.prm['n_nodes'])

        # # Instantiation of the optimization
        # fixed_pos_optimization = Optimization(problem=earth2nrho_fixed_pos, **options)

        # # Running the optimization process
        # # self.plot_trajectory(earth2nrho_fixed_pos.initial_guess.states, index = np.searchsorted(theta_arr, target_theta, side='left'), target = target_state)
        # fixed_pos_optimization.run()

        # # with open('pickled_results/opt_pos/pos_to_l1_250nodes', 'rb') as earth2manifolds_pickles_vel:
        # #     pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)

        # # # idx_manifold = 5 # <------ added manually for a test

        # # # Recovery of the states, controls, time and free parameters values from pickled results
        # # intermediate_states = pickled_results_vel['opt_st']
        # # intermediate_controls = pickled_results_vel['opt_ct']
        # # intermediate_time = pickled_results_vel['opt_tm']
        # # intermediate_par = pickled_results_vel['opt_pr']

        # # Recovery of the previous optimization results
        # intermediate_states = fixed_pos_optimization.results['opt_st']
        # intermediate_controls = fixed_pos_optimization.results['opt_ct']
        # intermediate_time = fixed_pos_optimization.results['opt_tm']
        # intermediate_par = fixed_pos_optimization.results['opt_pr']

        # # Plot the trajectory
        # # self.plot_trajectory(intermediate_states)

        # # Updated orbital parameters for initial orbit
        # self.orb_prm['ta'] = intermediate_par[0]

        # # Optimized initial guess
        # initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}

        with open('pickled_results/opt_vel/vel_l2_MEO_300', 'rb') as pickeld_pos:
            pickled_results_pos = pickle.load(pickeld_pos)


        # Recovery of the states, controls, time and free parameters values from pickled results
        intermediate_states = pickled_results_pos['opt_st']
        intermediate_controls = pickled_results_pos['opt_ct']
        intermediate_time = pickled_results_pos['opt_tm']
        intermediate_par = pickled_results_pos['opt_pr']

        # Updated orbital parameters for initial orbit
        self.orb_prm['ta'] = intermediate_par[0]

        # Optimized initial guess
        initial_guess = {'states': intermediate_states, 'controls': intermediate_controls, 'time': intermediate_time}


        # # ________________________________ Fixed Position & Velocity Initial Guess Generation _______________________________________________

        # Possible linear solvers : ma27, ma57, ma77, ma86, mumps
        linear_solver = 'ma86'

        options = {'explicit_integration': False, 'host': 'laptop', 'linear_solver': linear_solver,
                    'plot_results': False, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True, 'name': 'pickled_vel_to_l2_150nodes' }

        earth2nrho_fixed_vel = Earth2NRHOFixedVelocityGuess(self.spc_prm, target_state, self.orb_prm, initial_guess, n_nodes=self.prm['n_nodes'])

        # Instantiation of the optimization
        fixed_vel_optimization = Optimization(problem=earth2nrho_fixed_vel, **options)

        # Running the optimization process
        fixed_vel_optimization.run()

        # # Plot the trajectory
        # # self.plot_trajectory(fixed_vel_optimization.results['opt_st'])#, idx_manifold)

        # Recovery of the states, controls, time and free parameters values
        states_ = fixed_vel_optimization.results['opt_st']
        controls_ = fixed_vel_optimization.results['opt_ct']
        time_ = fixed_vel_optimization.results['opt_tm']
        par_ = fixed_vel_optimization.results['opt_pr']

        # with open('pickled_results/opt_vel/vel_l2_MEO_300', 'rb') as earth2manifolds_pickles_vel:
        #     pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)


        # # Recovery of the states, controls, time and free parameters values from pickled results
        # states_ = pickled_results_vel['opt_st']
        # controls_ = pickled_results_vel['opt_ct']
        # time_ = pickled_results_vel['opt_tm']
        # par_ = pickled_results_vel['opt_pr']

        # theta_idx = np.searchsorted(theta_arr, par_[1], side= 'left')
        # tau_idx = np.searchsorted(tau_arr, par_[2], side='left')

        # utils.classic_display(time_, states_, controls_)
        # self.plot_trajectory(states_, theta_idx)
        # input()

        # Set of the initial guess
        self.initial_guess.states = states_
        self.initial_guess.controls = controls_
        self.initial_guess.time = time_

        self.initial_guess.f_prm[0] = par_
        # self.initial_guess.f_prm[1] = self.manifolds_dt['target_theta']
        # self.initial_guess.f_prm[2] = self.manifolds_dt['target_tau']

        # self.initial_guess.f_prm[0] = par_[0]
        # self.initial_guess.f_prm[1] = par_[1]
        # self.initial_guess.f_prm[2] = par_[2]

        input()

    def plot_trajectory(self, states, index = None, target = None):

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # Extraction of the states
        x = states[0]
        y = states[1]
        z = states[2]

        # Plot the trajectory
        ax.plot(x, y, z, label='Spacecraft trajectory')

        # Plot the manifolds

        if index is None:
            for i in range(len(self.manifolds_dt['halo'].manifolds['stable_interior'])):
                ax.plot(self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 0], self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 1], 
                    self.manifolds_dt['halo'].manifolds['stable_interior'][i].state_vec[:, 2], '-')
        else:
            ax.plot(self.manifolds_dt['halo'].manifolds['stable_interior'][index].state_vec[:, 0], self.manifolds_dt['halo'].manifolds['stable_interior'][index].state_vec[:, 1], 
                self.manifolds_dt['halo'].manifolds['stable_interior'][index].state_vec[:, 2], '-')

        if target is not None:
            x_tgt, y_tgt, z_tgt = target[0:3]
            ax.plot(x_tgt, y_tgt, z_tgt, 'mo')

        # # Plot the target position
        # target_state = self.interpolated_manifold(self.manifolds_dt['target_theta'], self.manifolds_dt['target_tau'])
        # plt.plot([target_state[0]], [target_state[1]], [target_state[2]], 'mo')

        plt.legend()
        plt.show()

if __name__ == '__main__':

    # ____________________________________ Optimization / Transcription Parameters ______________________________________________________

    n_nodes = 300
    # Possible linear solvers : ma27, ma57, ma77, ma86, mumps
    linear_solver = 'ma86'

    options = {'explicit_integration': False, 'linear_solver': linear_solver,
                'plot_results': True, 'plot_physical_err': False, 'n_nodes': n_nodes, 'pickle_results': True, 'name': 'earth2mfld_to_l2_150nodes'}

    # ____________________________________ Spacecraft Parameters __________________________________________

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spacecraft_prm = {'mass0': m0, 'thrust_max': thrust_max,'isp': Isp, 'mass_dry': m_dry, 'thrust_min': thrust_min}

    # ____________________________________ Initial Orbit Parameters __________________________________________

    # # Departure orbit - GEO 
    # a = 42164  # initial semi-major axis [km]
    # e = 0      # final semi-major axis [km]
    # # i = 28.58  # inclination [deg]
    # i = 0
    # W = 90    # right Ascension of the Ascending Node [deg]
    # w = 0      # perigee argument [deg]
    # ta = 0     # initial true anomaly [deg]

    # Departure orbit - MEO
    a = 35000   # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58  # inclination [deg]
    W = 0    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 80     # initial true anomaly [deg]
  

    orbital_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180, 'ta': ta*np.pi/180}

    # ____________________________________ Initial Guess Trajectory __________________________________________

    # Time of propagation of the initial trajectory [-]
    # propagation_time = 3.6
    propagation_time = 2.3

    # Data needed for the construction of the initial trajectory
    # (orbit raising)
    orbit_raising_dt = {'t_prop': propagation_time}

    # ____________________________________ Orbit Generation _______________________________________________
 
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo.interpolation()

    # ____________________________________ Manifolds Generation _______________________________________________

    # Manifold branch to compute (0 <= theta <= 1)
    # theta_min = 0.0
    # theta_max = 1.0

    # theta_arr = np.linspace(theta_min, theta_max, 100)

    theta_min = 0.15
    theta_max = 0.40

    # theta_arr = np.linspace(theta_min, theta_max, 50)
    theta_arr = np.linspace(theta_min, theta_max, 50)
    
    
    # Manifold time of propagation [-]
    tau2cpt = 15

    ########## Increase max_internal_steps if ODE Warning

    halo.single_manifold_computation(stability='stable', way='interior', theta=theta_arr, t_prop=tau2cpt, max_internal_steps=4_000_000)

    # Inverse the tau array
    tau_arr = np.array([-tau for tau in halo.manifolds['stable_interior'][0].t_vec])

    # Useful portion of the manifold (in percentage)
    # Only this part will be interpolated to save computation time
    useful_prt = 0


    # arg_min = 3881
    # idx_manifold = 20

    # arg_min = 2500
    # idx_manifold = 12

    # arg_min = 3894
    # idx_manifold = 3

    arg_min = 3365
    idx_manifold = 37

    # time = np.arange(0, propagation_time, propagation_time/n_nodes)

    # arg_min, idx_manifold, m = find_targets(crtbp = cr3bp, spacecraft = Spacecraft(**spacecraft_prm), init_orbit_prm = orbital_prm, time = time,
    #                                       manifolds = halo.manifolds, stability = 'stable', way = 'interior')

    target_theta = theta_arr[idx_manifold]
    target_tau = tau_arr[arg_min]

    # target_theta = theta_arr[30]
    # target_tau = np.float64(11.9)

    # print(arg_min)
    # print(idx_manifold)
    # print(target_theta)
    # print(target_tau)
    # input()

    # Manifolds data
    manifolds_dt = {'halo': halo, 'target_tau': target_tau, 'target_theta': target_theta, 'useful_part': useful_prt,'theta_arr': theta_arr, 'tau_arr': tau_arr}


    # ____________________________________ Class Instantiation _______________________________________________

    # Instantiation of the problem
    problem = Earth2Manifold(spacecraft_prm, orbital_prm, orbit_raising_dt, manifolds_dt, n_nodes)

    # Instantiation of the optimization
    optimization = Optimization(problem=problem, **options)

    optimization.run()

    par_ = optimization.results['opt_pr']
    # print(par_)

    theta_idx = np.searchsorted(theta_arr, par_[1], side= 'left')
    tau_idx = np.searchsorted(tau_arr, par_[2], side='left')

    problem.plot_trajectory(optimization.results['opt_st'], index = theta_idx)
