"""
Long-term propagation of EML-2 southern Halo manifold tubes.

@author: Alberto FOSSA'
"""

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo

# %% input parameters
az_dim = 50e3  # Halo vertical extension [km]
stb = 'stable'  # manifold stability (stable/unstable)
way = 'interior'  # manifold way (interior/exterior)
nb_theta = 200  # number of branches
nb_pts = None  # number of time steps per branch. If None proportional to Halo ones
t_prop = 10.0  # propagation duration
d_min_moon_radii = 1.0  # minimum allowed distance from Moon center [Moon radii]
d_max_l2_scale = 1.05  # maximum distance from EM barycenter as fraction of L2 x-coordinate

# %% environment and orbit
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=az_dim)
halo.interpolation()

d_min = cr3bp.m2.Req * d_min_moon_radii / cr3bp.L  # minimum allowed distance from Moon center [-]
d_max = cr3bp.l2.position[0] * d_max_l2_scale  # maximum distance from EM barycenter [-]
pos_moon = np.asarray(cr3bp.m2_pos)  # position of the Moon in synodic frame [-]


# %% close passage event
def close_passage(_, state):
    """Event function to detect close passages of manifold branches w.r.t. the Moon surface. """
    return np.linalg.norm(state[0:3] - pos_moon) - d_min


close_passage.terminal = True  # stop propagation at event detection
close_passage.direction = 0  # all directions are considered

# %% manifold propagation
name = '_'.join((stb, way))
halo.manifolds[name] = []
halo.single_manifold_computation(stb, way, nb_theta, t_prop, time_steps=nb_pts,
                                 max_internal_steps=8_000_000, events=close_passage)

# %% branch subdivision
brc_impact = []  # branches that cross the minimum allowed distance from Moon center
brc_no_impact = []  # branches that do not cross the minimum allowed distance from Moon center
brc_remain = []  # branches that remain in the interior realm as expected
brc_leave = []  # branches that leave the interior realm

for b in halo.manifolds[name]:
    if b.t_event is None or b.t_event[0].size == 0:
        brc_no_impact.append(b)
    else:
        brc_impact.append(b)

for b in brc_no_impact:
    d_vec = np.linalg.norm(b.state_vec[:, 0:3], axis=1)
    if np.max(d_vec > d_max):
        brc_leave.append(b)
    else:
        brc_remain.append(b)

assert len(brc_leave) + len(brc_remain) + len(brc_impact) == nb_theta

# %% plots
r_moon = cr3bp.m2.Req / cr3bp.L
ang = np.linspace(0.0, 2.0 * np.pi, 1000)
x_moon, y_moon = cr3bp.m2_pos[0] + r_moon * np.cos(ang), r_moon * np.sin(ang)
x_bary, y_bary = d_max * np.cos(ang), d_max * np.sin(ang)

fig, ax = plt.subplots(constrained_layout=True)
# for i, b in enumerate(brc_remain):
#     ax.plot(b.state_vec[:, 0], b.state_vec[:, 1], color='forestgreen',
#             label='interior realm' if i == 0 else "")
for i, b in enumerate(brc_leave):
    ax.plot(b.state_vec[:, 0], b.state_vec[:, 1], color='gold',
            label='exterior realm' if i == 0 else "")
for i, b in enumerate(brc_impact):
    ax.plot(b.state_vec[:, 0], b.state_vec[:, 1], color='coral',
            label='Moon impact' if i == 0 else "")
ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], color='purple', label='Halo')
ax.plot(x_moon, y_moon, color='k', label='Moon')
ax.plot(x_bary, y_bary, color='grey', label='threshold')
ax.set_xlabel(r'$x\ [-]$')
ax.set_ylabel(r'$y\ [-]$')
ax.grid(True)
ax.set_aspect(1)
ax.legend(loc=0)
ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')

scaler = 1.0 if way == 'exterior' else -1.0

fig, ax = plt.subplots(constrained_layout=True)
fig.suptitle("X-component of the initial perturbation")
# for i, b in enumerate(brc_remain):
#     ax.scatter(b.theta, b.d_man * scaler * b.eigvec[0], color='forestgreen', marker='.',
#                label='interior realm' if i == 0 else "")
for i, b in enumerate(brc_leave):
    ax.scatter(b.theta, b.d_man * scaler * b.eigvec[0], color='gold', marker='.',
               label='exterior realm' if i == 0 else "")
for i, b in enumerate(brc_impact):
    ax.scatter(b.theta, b.d_man * scaler * b.eigvec[0], color='coral', marker='.',
               label='Moon impact' if i == 0 else "")
ax.set_xlabel(r'$\theta\ [-]$')
ax.set_ylabel(r'$\delta x_0\ [-]$')
ax.grid(True)
ax.legend(loc=0)
ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
plt.show()