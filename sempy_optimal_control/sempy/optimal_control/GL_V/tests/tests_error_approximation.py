import matplotlib.pyplot as plt
import numpy as np
import math

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo

from sempy.core.orbits.crtbporbit import CrtbpOrbit
from sempy.optimal_control.src.manifold_approximation import manifold_approximation

##### Excluding branches that jeopardize the interplation

def sort_branches(cr3bp, halo, theta_arr, tau2cpt):

    d_min_moon_radii = 1.0  # minimum allowed distance from Moon center [Moon radii]
    d_max_l2_scale = 1.05  # maximum distance from EM barycenter as fraction of L2 x-coordinate
    d_min = cr3bp.m2.Req * d_min_moon_radii / cr3bp.L  # minimum allowed distance from Moon center [-]
    d_max = cr3bp.l2.position[0] * d_max_l2_scale  # maximum distance from EM barycenter [-]
    pos_moon = np.asarray(cr3bp.m2_pos)  # position of the Moon in synodic frame [-]
    # %% close passage event
    def close_passage(_, state):
        """Event function to detect close passages of manifold branches w.r.t. the Moon surface. """
        return np.linalg.norm(state[0:3] - pos_moon) - d_min


    close_passage.terminal = True  # stop propagation at event detection
    close_passage.direction = 0  # all directions are considered

    # %% manifold propagation
    name = 'stable_interior'

    halo.manifolds[name] = []
    halo.single_manifold_computation(stability='stable', way='interior', theta=theta_arr_1, t_prop=tau2cpt,
                                     max_internal_steps=8_000_000, events=close_passage)

    grid_for_approx = np.copy(halo.manifolds[name])

    # %% branch subdivision
    idx_1 = []

    for i in range(len(halo.manifolds[name])):
        b = halo.manifolds[name][i]
        if b.t_event is None or b.t_event[0].size == 0:
            # We keep these
            pass
        else:
            idx_1.append(i)


    grid_for_approx = np.delete(grid_for_approx,idx_1)
    theta_arr = np.delete(theta_arr,idx_1)

    temp_grid = np.copy(grid_for_approx)
    idx_2 = []

    for i in range(len(temp_grid)):
        b = temp_grid[i]
        d_vec = np.linalg.norm(b.state_vec[:, 0:3], axis=1)
        if np.max(d_vec > d_max):
            idx_2.append(i)
        else:
            # We keep these
            pass

    grid_for_approx = np.delete(grid_for_approx,idx_2)
    theta_arr = np.delete(theta_arr,idx_2)


    return grid_for_approx, theta_arr

##### Compute the error

def compute_error(halo_1, halo_2, theta_arr_1, tau_arr):

    ###### Parameters ######
    #
    # halo : orbit from which the first sample of manifold branch are generated
    #
    # theta_arr_1 : array used to generated manifolds from halo
    #
    # tau_arr : array used to compute manifolds from halo
    #
    ###### Returns #####
    #
    # errors : ndarray of size (N-3) x (K-3) containing the error
    #
    # theta_arr_approx : array containing the values of theta for which the manifold was approximated
    #
    # tau_arr_approx : array containing the values of tau for which the manifold was approximated
    #

    N = len(theta_arr_1)
    K = len(tau_arr)

    errors = np.ndarray((N-3,K-3))

    approximated_manifolds = np.ndarray((N-3,K-3,6))

    theta_arr_approx = []
    tau_arr_approx = []

    print('---------------------------------')
    print(f'Number of iteration to go : {N-3}')
    print('---------------------------------')
    print('\n')

    for n in range(1,N-2):
        print(n)

        theta =  (theta_arr_1[n] + theta_arr_1[n+1])/2
        theta_arr_approx.append(theta)

        # branch = halo_2.ManifoldBranch(orbit = halo_1, stability = 'stable', way = 'interior')
        branch = halo_2.ManifoldBranch(orbit = halo_1, stability = 'stable', way = 'interior')
        branch.computation(theta, tau_arr[-1])

        for k in range(1,K-2):

            tau = tau_arr[k]

            if n == 1:
                tau_arr_approx.append(tau)

            x_s = branch.state_vec[k,:]
            # x_s_app, x_ipl = manifold_approximation(halo_1.cr3bp, halo_1.C, halo_1.manifolds['stable_interior'], theta_arr_1, tau_arr, theta, tau)
            x_s_app = manifold_approximation(halo_1.cr3bp, halo_1.C, halo_1.manifolds['stable_interior'], theta_arr_1, tau_arr, theta, tau)

            approximated_manifolds[n-1,k-1,:] = x_s_app
            errors[n-1,k-1] = np.linalg.norm(x_s - x_s_app)/np.linalg.norm(x_s)
            

            # if np.linalg.norm(x_s - x_s_app)/np.linalg.norm(x_s)>0.1:
            #     errors[n-1,k-1] = np.linalg.norm(x_s - x_ipl)/np.linalg.norm(x_s)
            # else:
            #     errors[n-1,k-1] = np.linalg.norm(x_s - x_s_app)/np.linalg.norm(x_s)


    return errors, theta_arr_approx, tau_arr_approx, approximated_manifolds


# Defining a function used to plot the error as a surface


def error_surface(theta_idx, tau_idx, errors, log = True):

    if log == True :
        return np.log10(errors[theta_idx, tau_idx])
    else:
        return errors[theta_idx, tau_idx]

# Function to plot the error as a surface

def plot_error_surface(theta_arr, tau_arr, errors, dim = '2D'):

    fig = plt.figure()

    x_data = theta_arr
    y_data = tau_arr
    X,Y = np.meshgrid(x_data, y_data)

    x_z_data = [i for i in range(len(theta_arr_approx))]
    y_z_data = [j for j in range(len(tau_arr_approx))]

    X_z,Y_z = np.meshgrid(x_z_data, y_z_data)

    Z = error_surface(X_z,Y_z,errors)

    if dim == '3D':
        ax = fig.gca(projection='3d')
        ax.plot_surface(X,Y,Z, cmap = 'plasma')
        ax.set_xlabel('theta')
        ax.set_ylabel('tau')
        ax.set_zlabel('error')
    else : 
        im = plt.imshow(Z, cmap=plt.cm.jet, extent=(theta_arr[0], theta_arr[-1], tau_arr[0], tau_arr[-1]), interpolation='bilinear', aspect = 'auto')
        plt.colorbar(im);
        plt.title('$Relative  error$')
        plt.xlabel('theta')
        plt.ylabel('tau')
        plt.show()

    plt.show()


# Function to plot the approximated manifolds

def plot_approx_manifolds(approximated_manifolds):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Plot the manifolds
    for i in range(len(approximted_manifolds)):
        ax.plot(approximated_manifolds[i,0,0], approximated_manifolds[i,0,1], approximated_manifolds[i,0,2], 'mo', color = 'red')
        ax.plot(approximated_manifolds[i,:,0], approximated_manifolds[i,:,1], approximated_manifolds[i,:,2], '-', color = 'red')

    plt.show()

# Function to plot the manifolds generated from the orbits

def plot_generated_manifolds(grid):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Plot Earth
    plt.plot([cr3bp.mu],[0],[0],'mo')

    # Plot the manifold
    for i in range(len(grid)):
        ax.plot(grid[i].state_vec[:,0], grid[i].state_vec[:,1], \
            grid[i].state_vec[:,2], '-',)

    # Orbit 
    orbit_vec = halo_1.state_vec

    x_o = orbit_vec[:, 0]
    y_o = orbit_vec[:, 1]
    z_o = orbit_vec[:, 2] 

    ax.plot(x_o, y_o, z_o, label='Arrival orbit', color = 'orange')

    plt.show()


def plot_together(halo_1, halo_2, approximted_manifolds):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

        # Plot the approximated manifolds
    for i in range(len(approximted_manifolds)):
        ax.plot(approximted_manifolds[i,0,0], approximted_manifolds[i,0,1], approximted_manifolds[i,0,2], 'mo', color = 'red')
        ax.plot(approximted_manifolds[i,:,0], approximted_manifolds[i,:,1], approximted_manifolds[i,:,2], '-', color = 'red')

    # Plot the generated manifold

    for i in range(len(halo_1.manifolds['stable_interior'])-1):
            # Plotting the starting point
        ax.plot(halo_2.manifolds['stable_interior'][i+3].state_vec[0,0], halo_2.manifolds['stable_interior'][i+3].state_vec[0,1], \
             halo_2.manifolds['stable_interior'][i+3].state_vec[0,2], 'mo', color = 'green')

        ax.plot(halo_2.manifolds['stable_interior'][i+3].state_vec[:,0], halo_2.manifolds['stable_interior'][i+3].state_vec[:,1], \
             halo_2.manifolds['stable_interior'][i+3].state_vec[:,2], '-', color = 'green')

    plt.show()



if __name__ == '__main__':

    # # Three body problem
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

    # Arrival orbit
    C_jac = 3.10

    halo_1 = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac = C_jac) 
    halo_2 = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac = C_jac) 

    halo_1.interpolation()
    halo_2.interpolation()

    # Theta and tau for manifold generation

    tau2cpt = 15

    theta_min = 0.0
    theta_max = 1.0


    N = 100
    theta_arr_1 = np.linspace(theta_min, theta_max, N)

    # # Manifold generation

    # grid_for_approx, theta_arr = sort_branches(cr3bp,halo_1,theta_arr_1, tau2cpt)


    halo_1.single_manifold_computation(stability='stable', way='interior', theta=theta_arr_1, t_prop=tau2cpt, max_internal_steps=4_000_000)

    # plot_generated_manifolds(grid_for_approx)
    # input()

    # halo_1.single_manifold_computation(stability='stable', way='exterior', theta=10, t_prop=tau2cpt, max_internal_steps=2_000_000)
    # halo_2.single_manifold_computation(stability='stable', way='interior', theta=theta_arr_2, t_prop=tau2cpt, max_internal_steps=2_000_000)

    tau_arr = np.array([-tau for tau in halo_1.manifolds['stable_interior'][0].t_vec])

    # Computation of the error

    # We define tau_arr_usefull that we use for the optimization
    idx_for_tau = np.searchsorted(tau_arr, 6, side='left')
    tau_arr_usefull = tau_arr[idx_for_tau:]

    errors, theta_arr_approx, tau_arr_approx, approximted_manifolds = compute_error(halo_1, halo_2, theta_arr_1, tau_arr_usefull)
    print('Minimal error : ', np.min(errors))
    print('Mean error : ', np.mean(errors))
    input()

    # Plotting the approximated and the generated manifolds

    # plot_together(halo_1, halo_2, approximted_manifolds)
    # plot_approx_manifolds(approximted_manifolds)
    # plot_generated_manifolds(halo_1)

    # Plotting the error

    plot_error_surface(theta_arr_approx, tau_arr_approx, errors)
    plot_error_surface(theta_arr_approx, tau_arr_approx, errors, dim = '3D')

