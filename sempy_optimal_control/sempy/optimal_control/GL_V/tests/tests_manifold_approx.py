import matplotlib.pyplot as plt
import numpy as np
import pickle
import math

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.optimal_control.src.manifold_approximation import manifold_approximation




if __name__ == '__main__':
    # # Three body problem
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spc_prm = {'mass0': m0, 'thrust_max': thrust_max,'isp': Isp, 'mass_dry': m_dry, 'thrust_min': thrust_min}

    # Arrival orbit
    C_jac = 3.10
    halo = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac=C_jac) # <---- Vary this parameter
    halo2 = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Cjac=C_jac)

    halo.interpolation()
    halo2.interpolation()
    print('halo orbit created')

    # Building the theta array of interest
    # theta_min = 0.1
    # theta_max = 0.9

    # theta_arr = np.arange(theta_min, theta_max, theta_step)
    # theta_arr = np.linspace(theta_min, theta_max, 100)
    
    # Manifold time of propagation [-]
    # tau2cpt = 9

    # halo.single_manifold_computation(stability='stable', way='interior', theta=theta_arr, t_prop=tau2cpt, max_internal_steps=2_000_000)
    # print('manifold generated')

    # # Building the tau array
    # tau_arr = np.array([-tau for tau in halo.manifolds['stable_interior'][0].t_vec])

    # # Useful portion of the manifold (in percentage)
    # # Only this part will be interpolated to save computation time
    # useful_prt = 0

    # # # Getting back states from manifold
    # # n_nodes_m = 300

    # # step = math.floor(arg_min/n_nodes_m)
    # # arg_n_nodes = np.arange(0, arg_min, step)
    # # manifold_states_temp = halo.manifolds['stable_interior'][idx_manifold].state_vec[arg_n_nodes,:6]
    # # manifold_states = np.flipud((manifold_states_temp))

    # theta1 = theta_arr[30]
    # tau1 = tau_arr[2000] 

    # theta2 = theta_arr[31]
    # tau2 = tau_arr[2001]

    # theta = (theta1 + theta2)/2
    # tau = (tau1 + tau2)/2

    # x_s_d = halo.manifolds['stable_interior'][30].state_vec[2000]
    # x_s_u = halo.manifolds['stable_interior'][31].state_vec[2001]

    # print('approximating manifold...')
    # x_s_app = manifold_approximation(cr3bp, C_jac, halo.manifolds['stable_interior'], theta_arr, tau_arr, theta, tau)
    # print('done')

    # print('Value for xs from numerical integration :')
    # print(x_s_d)
    # print(x_s_u)
    # print('\n')
    # print('Value for xs from manifold approximation :')
    # print(x_s_app)

    def u(s):

        if np.linalg.norm(s) > 0 and np.linalg.norm(s) < 1:
            result = 1.5*np.linalg.norm(s)**3 - 2.5*np.linalg.norm(s)**2 + 1
        elif np.linalg.norm(s) > 1 and np.linalg.norm(s) < 2:
            result = -0.5*np.linalg.norm(s)**3 + 2.5*np.linalg.norm(s)**2 - 4*np.linalg.norm(s) + 2
        elif s==0:
            result = 1
        else:
            result = 0

        return result

    def step(s):

        return 1/(1+100*s**(16))

    def u_ad(s):
        s_n = np.linalg.norm(s)

        a = step(s_n-0.5)
        b = step(s_n - 1.5)

        return a*(1.5*s_n**3 - 2.5*s_n**2 + 1) + b*(-0.5*s_n**3 + 2.5*s_n**2 - 4*s_n + 2)


    X = np.linspace(-3,3,401)
    U = []
    U_AD = []


    for x in X:
        U.append(u(x))
        U_AD.append(u_ad(x))
        

    plt.plot(X,U, color='green')
    plt.plot(X,U_AD, color = 'red')
    plt.show()






    





















