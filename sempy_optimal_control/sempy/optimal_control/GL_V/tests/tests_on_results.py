import matplotlib.pyplot as plt
import numpy as np
import cppad_py
import pickle
import math

# import scipy.stats
from scipy.integrate import simps
# from scipy.interpolate import lagrange
# from scipy.interpolate import KroghInterpolator
# from numpy.polynomial.polynomial import Polynomial

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.halo import Halo

from sempy.core.crtbp import jacobi
from sempy.optimal_control.src.spacecraft import Spacecraft

from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.earth2manifold_v2 import Earth2Manifold
from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.cr3bp_propagation import Cr3bpPropagation
from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.Earth2Halo_guess import set_Earth2Halo_guess

from sempy.optimal_control.src.manifold_approximation import manifold_approximation
from sempy.optimal_control.src import utils

# For explicit integration
from sempy.optimal_control.src.explicit_integration import explicit_integration


if __name__ == '__main__':
    # # Three body problem
    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

    # Spacecraft properties
    m0 = 1000  # initial mass [kg]
    m_dry = 10  # dry mass [kg]
    thrust_max = 2  # maximum thrust [N]
    thrust_min = 0  # minimum thrust [N]
    Isp = 2000  # specific impulse [s]

    spc_prm = {'mass0': m0, 'thrust_max': thrust_max,'isp': Isp, 'mass_dry': m_dry, 'thrust_min': thrust_min}

    # Departure orbit

    a = 42164  # initial semi-major axis [km]
    e = 0      # final semi-major axis [km]
    i = 28.58  # inclination [deg]
    W = 90    # right Ascension of the Ascending Node [deg]
    w = 0      # perigee argument [deg]
    ta = 120     # initial true anomaly [deg]
  

    orb_prm = {'a': a, 'e': e, 'i': i*np.pi/180, 'W': W*np.pi/180, 'w': w*np.pi/180, 'ta': ta*np.pi/180}

    # Arrival orbit
    halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo.interpolation()
    print('Departure halo orbit created')

    # nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Cjac=3.05) # <---- Vary this parameter
    # nrho.interpolation()

    halo_arr = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Cjac=3.10) # <---- Vary this parameter
    halo_arr.interpolation()

    print('Arrival halo orbit created')

    # Building the theta array of interest
    theta_min = 0.15
    theta_max = 0.40

    # theta_arr = np.arange(theta_min, theta_max, theta_step)
    N=50
    theta_arr = np.linspace(theta_min, theta_max, N)
    
    # Manifold time of propagation [-]
    tau2cpt = 14.5

    halo.single_manifold_computation(stability='stable', way='interior', theta=theta_arr, t_prop=tau2cpt, max_internal_steps=8_000_000)
    print('manifold generated')

    # Building the tau array
    tau_arr = np.array([-tau for tau in halo.manifolds['stable_interior'][0].t_vec])

    ################################# PICKLED RESULTS ###############################

    # with open('../spacecraft_trajectories/LEO2NRHO/pickled_results/earth2halo/earth2halo_pickles_750_nodes_1', 'rb') as earth2manifolds_pickles_vel:
    #     pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)
    with open('../spacecraft_trajectories/LEO2NRHO/pickled_results/opt_vel/vel_l2_MEO_300', 'rb') as earth2manifolds_pickles_vel:
        pickled_results_vel = pickle.load(earth2manifolds_pickles_vel)


    # Recovery of the states, controls, time and free parameters values from pickled results
    states_ = pickled_results_vel['opt_st']
    controls_ = pickled_results_vel['opt_ct']
    time_ = pickled_results_vel['opt_tm']
    par_ = pickled_results_vel['opt_pr']
    total_ct = pickled_results_vel['total_ct']


    # theta_idx = np.searchsorted(theta_arr, par_[1], side= 'left') - 1
    # tau_idx = np.searchsorted(tau_arr, par_[2], side='left') - 1

    # print(par_)
    # input()

    # Getting the initial guess for Earth2halo transfers
    # arg_min = 2803
    # idx_manifold = 11

    useful_prt = 0

    # target_theta = theta_arr[idx_manifold]
    # target_tau = tau_arr[arg_min]

    target_theta = theta_arr[30]
    target_tau = np.float64(9.4)
    # Manifolds data
    manifolds_dt = {'halo': halo, 'target_tau': target_tau, 'target_theta': target_theta, 'useful_part': useful_prt,'theta_arr': theta_arr, 'tau_arr': tau_arr}

    n_nodes_1 = 300

    ######## To plot trajectory ########

    def plot_trajectory(states, orbit_vec, index1 = None, index2 = None):

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # Extraction of the states
        x = states[0]
        y = states[1]
        z = states[2]

        x_o = orbit_vec[:, 0]
        y_o = orbit_vec[:, 1]
        z_o = orbit_vec[:, 2] 

        x_moon = 1
        y_moon = 0
        z_moon = 0 

        # Plot the trajectory
        ax.plot(x, y, z, label='Spacecraft trajectory', color = 'orange')
        ax.plot(x_o, y_o, z_o, label='Arrival orbit', color = 'red')
        ax.plot(x_moon, y_moon, z_moon, 'mo')

        ax.plot(halo.manifolds['stable_interior'][index1].state_vec[:, 0], halo.manifolds['stable_interior'][index1].state_vec[:, 1], \
                halo.manifolds['stable_interior'][index1].state_vec[:, 2], '-')

        # ax.plot(halo.manifolds['stable_interior'][index2].state_vec[:, 0], halo.manifolds['stable_interior'][index2].state_vec[:, 1], \
        #        halo.manifolds['stable_interior'][index2].state_vec[:, 2], '-')

        plt.show()


    def plot_trajectory_thrust(states, orbit_vec, thrust_vec, nrho = None, int_ = False):

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        # Extraction of the states
        x = states[0]
        y = states[1]
        z = states[2]

        x_o = orbit_vec[:, 0]
        y_o = orbit_vec[:, 1]
        z_o = orbit_vec[:, 2] 

        x_moon = 1
        y_moon = 0
        z_moon = 0 

        N = len(thrust_vec)

        # # Plot the arrival orbit and the moon
        # ax.plot(x_o, y_o, z_o, label='Departure EML-1 Halo orbit', color = 'magenta')
        # ax.plot(x_o, y_o, z_o, label='Intermediate Halo orbit', color = 'orange')

        if nrho is not None:
            x_ = nrho.state_vec[:,0]
            y_ = nrho.state_vec[:,1]
            z_ = nrho.state_vec[:,2]
            ax.plot(x_, y_, z_, label = 'Arrival EML-2 Halo orbit', color = 'gold')

        # Plot the trajectory

        first1 = True
        first2 = True

        if int_ == False:
            for i in range(N-1):
                if thrust_vec[i]>10**(-2):
                    if first1:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', label='Thrust phases', color = 'red')
                        first1 = False
                    else:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', color = 'red')
                else:
                    if first2:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', label='Coast phases', color = 'C0')
                        first2 = False
                    else:
                        ax.plot(x[i:i+2], y[i:i+2], z[i:i+2], '-', color = 'C0')

        else: # Case of the explicit integration plot
            j = 0
            for i in range(N-1):
                if thrust_vec[i]>10**(-2):
                    if first1:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', label='Thrust phases', color = 'red')
                        first1 = False
                    else: 
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', color = 'red')
                else:
                    if first2:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', label='Coast phases', color = 'C0')
                        first2 = False
                    else:
                        ax.plot(x[j:j+5], y[j:j+5], z[j:j+5], '-', color = 'C0')            
                j+=4

        ax.plot(x_moon, y_moon, z_moon, 'mo', label = 'Moon')

        ax.set_xlabel('x axis')
        ax.set_ylabel('y axis')
        ax.set_zlabel('z axis')

        # ax.legend(fontsize = 6)
        plt.rc('legend', fontsize = 6)

        plt.legend()
        plt.show()

    # earth2halo_states, earth2halo_controls, earth2halo_time, earth2halo_prm = set_Earth2Halo_guess(cr3bp, spc_prm, n_nodes_1, orb_prm, halo)
    # plot_trajectory(earth2halo_states, halo.state_vec)

    plot_trajectory(states_, halo.state_vec, index1 = 37)
    # plot_trajectory_thrust(states_, halo.state_vec, thrust_vec = controls_[0,:], nrho = halo_arr)

    # time = np.linspace(0,par_[2], 600)

    # cr3bp_propagation = Cr3bpPropagation(cr3bp, Spacecraft(**spc_prm), states_[0:7,-1], time)
    # states_prop, controls, time = cr3bp_propagation.get_initial_guess()


    # total_states = np.hstack((states_[0:6,:], states_prop[:6,1:]))

    # plot_trajectory(total_states, theta_idx,theta_idx+1)

    ######## To plot the thrust profil #########

    utils.classic_display(time_, states_, controls_)


    ######## To calculate the delta v ########

    def compute_delta_v(thrust_vec, mass_vec, time_arr):

    	integrand = thrust_vec/mass_vec
    	delta_v = simps(integrand, time_arr)

    	return delta_v

    thrust_vec = controls_[0,:]
    mass_vec = states_[6,:]
    time_arr = time_*cr3bp.T/(2*np.pi)

    delta_v = compute_delta_v(thrust_vec, mass_vec, time_arr)
    delta_v_k = delta_v/1000

    print('\n')
    print(f'delta_v for this mission is :')
    print(f' {delta_v} [m/s]')
    print(f' {delta_v_k} [km/s]')
    print('\n')

    ######## To do explicit integration ########
    orbit_raising_dt = {'t_prop': time_[-1]}
    useful_prt = 0
    # manifolds_dt = {'halo': halo, 'target_tau': par_[2], 'target_theta': par_[1], 'useful_part': useful_prt,'theta_arr': theta_arr, 'tau_arr': tau_arr}

    # Defining a problem only to extract the dynamics

    n_nodes = len(time_)

    problem = Earth2Manifold(spc_prm, orb_prm, orbit_raising_dt, manifolds_dt, n_nodes)

    # Proceeding to explicit integration
    int_states = explicit_integration(states_, total_ct, time_[-1], problem.dynamics, par_)

    # Plotting the results of explicit integration

    plot_trajectory_thrust(int_states, halo.state_vec, controls_[0,:], int_ = True, nrho = halo_arr)
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')

    # # Extraction of the int_states
    # x_int = int_states[0]
    # y_int = int_states[1]
    # z_int = int_states[2]

    # # Extraction of states for comparison
    # x = states_[0]
    # y = states_[1]
    # z = states_[2]

    # # Plot the trajectory
    # ax.plot(x, y, z, label='spacecraft_trajectorie', color = 'green')
    # ax.plot(x_int, y_int, z_int, label='Explicit integration', color = 'red')

    # plt.show()


    r_opt = states_[0:6,-1]
    r_int = int_states[0:6,-1]

    print('Erreur pos:')

    error_pos = (r_opt[0] - r_int[0])**2 + (r_opt[1] - r_int[1])**2 + (r_opt[2] - r_int[2])**2
    print(error_pos)

    print('\n')

    print('Erreur vel:')

    error_vel = (r_opt[3] - r_int[3])**2 + (r_opt[4] - r_int[4])**2 + (r_opt[5] - r_int[5])**2
    print(error_vel)