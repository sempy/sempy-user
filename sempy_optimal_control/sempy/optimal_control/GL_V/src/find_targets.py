import numpy as np
import math


from sempy.optimal_control.spacecraft_trajectories.LEO2NRHO.orbit_raising_cr3bp import OrbitRaising

# Precalculated values
mean_pos_min = 0.6986296987165758
mean_vel_min = 0.4649244372230631

def norm_pos_vel_1(vect):
	return np.linalg.norm(vect[:3])/mean_pos_min + np.linalg.norm(vect[3:6])/mean_vel_min

def norm_pos_vel_2(vect):
	return np.sqrt((vect[0]**2 + vect[1]**2 + vect[2]**2)/mean_pos_min**2 + (vect[3]**2 + vect[4]**2 + vect[5]**2)/mean_vel_min**2) 

def norm_pos_vel_3(vect, vect_tgt):
	return np.linalg.norm(vect[0:3])/np.linalg.norm(vect_tgt[0:3]) + np.linalg.norm(vect[3:])/np.linalg.norm(vect_tgt[3:])

	

def find_targets(crtbp, spacecraft, init_orbit_prm, time, manifolds, stability, way):

	orbit_raising = OrbitRaising(crtbp, spacecraft, init_orbit_prm, time)
	states_vec_sc, controls, time = orbit_raising.get_initial_guess()
	states_sc = np.transpose(states_vec_sc)

	# Portion utile de la trajectoire propagée

	N = len(states_sc) # nombre de lignes
	N_util = math.floor(0.5*N)
	print(N_util)
	print('\n')

	states_vec_sc_util = states_sc[0:N_util,:6]

	# nombre de branches
	n = np.size(manifolds[stability+'_'+way])

	idx_manifold = 0
	m = -1
	temp = 0

	for vect_sc in states_vec_sc_util:
		temp+=1
		print(temp)

		for i in range(n):

			state_vec_manifolds = manifolds[stability+'_'+way][i].state_vec[:,0:6]
		
			diff = state_vec_manifolds - vect_sc
			#norm_diff = [norm_pos_vel_2(vect) for vect in diff]
			norm_diff = [norm_pos_vel_3(vect, vect_sc) for vect in diff]
			
			#norm_diff = [norm_pos_vel_2(vect) for vect in diff]


			min_i = np.min(norm_diff)

			if m < 0:
				m = min_i
				idx_manifold = i
				arg_min = np.argmin(norm_diff)

			elif min_i < m:
				m = min_i
				idx_manifold = i
				arg_min = np.argmin(norm_diff)




	# min_dist = np.min(dist_to_manifolds)
	# min_idx = np.argmin(dist_to_manifolds)

	# print(np.mean(min_pos))
	# print(np.mean(min_vel))

	return arg_min, idx_manifold, m #, min_dist, dist_to_manifolds
