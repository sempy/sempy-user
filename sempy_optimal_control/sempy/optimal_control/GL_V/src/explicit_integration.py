import numpy as np
from scipy.interpolate import KroghInterpolator
from scipy.integrate import solve_ivp

def explicit_integration(states, total_ct, propagation_time, dynamics, f_par):

	# Original number of nodes
	temp, n_nodes = np.shape(states)

	# Getting the number of nodes : m (with additionnal nodes)
	n, m = np.shape(total_ct)

	# Time array
	time = np.linspace(0, propagation_time, m)

	# Getting the "correct" or "starting" number of nodes
	N_float = (m+3)/4
	N = int(N_float)

	# Initialization of the integrated states (len = 7*m)
	int_states = np.zeros((7,m))

	# Initialization of the index to fill int_states
	j=0

	for k in range(N-1):

		current_states = states[:,k]

		# Pairs for interpolation function
		if k == N-2:
			# Managing the last term
			u_array = np.hstack((total_ct[:,k:k+1], total_ct[:,k+1:k+2], total_ct[:,k+2:k+3], total_ct[:,k+3:k+4], total_ct[:,k+4:k+5]))
			t_array = np.array([time[k], time[k+1], time[k+2], time[k+3], time[k+5]])	
		else:
			u_array = np.hstack((total_ct[:,k:k+1], total_ct[:,k+1:k+2], total_ct[:,k+2:k+3], total_ct[:,k+3:k+4]))
			t_array = np.array([time[k], time[k+1], time[k+2], time[k+3]])		


		# Interpolated control
		u_0_k = KroghInterpolator(t_array, u_array[0,:])
		u_x_k = KroghInterpolator(t_array, u_array[1,:])
		u_y_k = KroghInterpolator(t_array, u_array[2,:])
		u_z_k = KroghInterpolator(t_array, u_array[3,:])


		def dynamic_redef(t, y, u_0_k, u_x_k, u_y_k, u_z_k, f_par):

			u_0_t = u_0_k(t)
			u_x_t = u_x_k(t)
			u_y_t = u_y_k(t)
			u_z_t = u_z_k(t)

			u_t = np.array([u_0_t, u_x_t, u_y_t, u_z_t])

			return dynamics(y, u_t, f_par, expl_int=True)

		# Explicit integration using scipy.solve_ivp module
		explicit_int = solve_ivp(fun=dynamic_redef, t_span=(t_array[0], t_array[-1]), y0 = current_states, t_eval=t_array,
	                             args=(u_0_k, u_x_k, u_y_k, u_z_k, f_par), method='DOP853', rtol=3e-14, atol=1e-14)


		new_states = explicit_int.y

		if k == N-2:
			int_states[:,j] = new_states[:,0]
			int_states[:,j+1] = new_states[:,1] 
			int_states[:,j+2] = new_states[:,2] 
			int_states[:,j+3] = new_states[:,3]
			int_states[:,j+4] = new_states[:,4]
		else:
			int_states[:,j] = new_states[:,0]
			int_states[:,j+1] = new_states[:,1] 
			int_states[:,j+2] = new_states[:,2] 
			int_states[:,j+3] = new_states[:,3] 

		j += 4 

	# int_states[:,j] = states[:,-1]


	return int_states