#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 09:25:59 2019

@author: Alberto FOSSA'
"""


class Spacecraft:
    """Spacecraft class defines the Variables for an electric propelled spacecraft.

    Parameters
    ----------
    mass0 : float
        Initial spacecraft mass [kg]
    thrust_max : float
        Maximum thrust magnitude [N]
    isp : float
        Specific impulse [s]
    mass_dry : float, optional
        Dry mass [kg]. Default is ``m0/100``
    thrust_min : float, optional
        Minimum thrust magnitude [N]. Default is 0.0

    Variables
    ----------
    mass0 : float
        Initial spacecraft mass [kg]
    thrust_max : float
        Maximum thrust magnitude [N]
    isp : float
        Specific impulse [s]
    mass_dry : float
        Dry mass [kg]
    thrust_min : float
        Minimum thrust magnitude [N]

    """

    def __init__(self, mass0, thrust_max, isp, mass_dry=None, thrust_min=0.0):
        """Initializes Spacecraft class. """

        self.mass0 = float(mass0)
        self.thrust_max = float(thrust_max)
        self.isp = float(isp)

        if mass_dry is None:
            self.mass_dry = self.mass0 / 100
        else:
            self.mass_dry = mass_dry

        self.thrust_min = thrust_min

    def __str__(self):
        """Prints the Spacecraft object Variables. """

        lines = ['\n{:^40s}'.format('Spacecraft characteristics:'),
                 '\n{:<25s}{:>12.3f}{:>3s}'.format('Initial mass:', self.mass0, 'kg'),
                 '{:<25s}{:>12.3f}{:>3s}'.format('Dry mass:', self.mass_dry, 'kg'),
                 '{:<25s}{:>12.3f}{:>2s}'.format('Max thrust:', self.thrust_max, 'N'),
                 '{:<25s}{:>12.3f}{:>2s}'.format('Min thrust:', self.thrust_min, 'N'),
                 '{:<25s}{:>12.3f}{:>2s}'.format('Specific impulse:', self.isp, 's')]

        printed = '\n'.join(lines)

        return printed


if __name__ == '__main__':

    SC = Spacecraft(1000, 1, 2000)
    print(SC)
