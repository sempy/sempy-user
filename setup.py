# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 2020

@author: Emmanuel BLAZQUEZ
"""

from setuptools.command.install import install
from setuptools.command.develop import develop
from setuptools.command.egg_info import egg_info

import os
from os.path import (
    basename,
    splitext
)
from importlib.machinery import SourceFileLoader
from setuptools import setup, find_packages
from glob import glob
import io

import atexit
import runpy

############## In order to compile Cr3bp dynamics ##############
def _post_install():
    print('Compiling CR3BP dynamics with numba ...')
    """Post-installation script."""
    runpy.run_path('sempy_core/sempy/core/dynamics/srcs/cr3bp_dynamics.py', run_name='__main__')

class NewDevelop(develop):
    def __init__(self, *args, **kwargs):
          super(NewDevelop, self).__init__(*args, **kwargs)
          atexit.register(_post_install)

class NewEggInfo(egg_info):
    def __init__(self, *args, **kwargs):
          super(NewEggInfo, self).__init__(*args, **kwargs)
          atexit.register(_post_install)

class NewInstall(install):
    def __init__(self, *args, **kwargs):
        super(NewInstall, self).__init__(*args, **kwargs)
        atexit.register(_post_install)

################################################################

class Subpackage:
    def __init__(self, name, is_extra=False):
        self.name = name
        self.is_extra = is_extra

    @property
    def version(self):
        version_file = 'sempy_{}/sempy/{}/version.py'.format(self.name, self.name)
        return SourceFileLoader('sempy.{}.version'.format(self.name),
                           os.path.join(
                              os.path.dirname(__file__),
                              version_file
                          )).load_module().__version__

    @property
    def requirement_name(self):
        return 'sempy_{}=={}'.format(self.name, self.version)

subpackages = [
    Subpackage('core'),
    Subpackage('optimal_control', is_extra=True),
    Subpackage('visualisation', is_extra=True),
]

here = os.path.abspath(os.path.dirname(__file__))

# get the dependencies and installs
with io.open(os.path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    # Remove flags like "--no-binary=rasterio"
    install_requires = [line.split(' ')[0] for line in f.read().split('\n')]

with open(os.path.join(here, 'README.md')) as readme_file:
    readme = readme_file.read()

# These subpackages will be installed by default
default_subpackages = [
    p.requirement_name for p in subpackages if not p.is_extra
]

# List subpackages as extras
extras_require = {
    'all': []
}

for p in subpackages:
    if p.is_extra:
        extras_require[p.name] = [p.requirement_name]
        extras_require['all'].append(p.requirement_name)

version = max([p.version for p in subpackages])

setup(
    name='SEMpy',
    version=version,
    description=("A Research Tool for Mission Analysis in the Sun-Earth-Moon System."),
    long_description=readme,
    long_description_content_type="text/markdown",
    author='Emmanuel BLAZQUEZ, Thibault GATEAU',
    author_email='emmanuel.blazquez@isae-supaero.fr, thibault.gateau@isae-supaero.fr',
    url='https://gitlab.isae-supaero.fr/sempy/',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU GPL V3 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    cmdclass={'install': NewInstall,
              'develop': NewDevelop,
              'egg_info': NewEggInfo},
    include_package_data=True,
    packages=find_packages(),
    install_requires= install_requires,
    extras_require=extras_require,
)

# # -*- coding: utf-8 -*-
# """
# Created on Wed Aug 26 2020

# @author: Emmanuel BLAZQUEZ
# """

# from setuptools import setup, find_packages
# from setuptools.command.install import install
# from setuptools.command.develop import develop
# from setuptools.command.egg_info import egg_info



# import atexit
# import runpy


# # def _post_install():
# #     print('Compiling CR3BP dynamics with numba ...')
# #     """Post-installation script."""
# #     runpy.run_path('sempy/core/dynamics/srcs/cr3bp_dynamics.py', run_name='__main__')

# # class NewDevelop(develop):
# #     def __init__(self, *args, **kwargs):
# #           super(NewDevelop, self).__init__(*args, **kwargs)
# #           atexit.register(_post_install)

# # class NewEggInfo(egg_info):
# #     def __init__(self, *args, **kwargs):
# #           super(NewEggInfo, self).__init__(*args, **kwargs)
# #           atexit.register(_post_install)

# # class NewInstall(install):
# #     def __init__(self, *args, **kwargs):
# #         super(NewInstall, self).__init__(*args, **kwargs)
# #         atexit.register(_post_install)


# if __name__ == "__main__":

#   setup(

#       name='SEMpy',
#       version='1.0',
#       description='SEMpy: A Research Tool for Mission Analysis in the Sun-Earth-Moon System',
#       author='Emmanuel BLAZQUEZ, Thibault GATEAU',
#       author_email='emmanuel.blazquez@isae-supaero.fr, thibault.gateau@isae-supaero.fr',
#       url='https://gitlab.isae-supaero.fr/sempy-dev/sempy',
#       classifiers=[
#           "Programming Language :: Python :: 3",
#           "License :: GNU GPL V3 License",
#           "Operating System :: OS Independent",
#       ],

#       # python_requires='>=3.8',

#       packages=find_packages(),

#       zip_safe=False,

#       include_package_data=True,

#       install_requires = ['numpy==1.20.2',
#                             'spiceypy==4.0.0',
#                             'scipy==1.6.2',
#                             'matplotlib==3.4.1',
#                             'cffi==1.14.4',
#                             'multiprocess==0.70.11.1',
#                             'numba==0.53.1',
#                             'orekit==10.3',
#                             'psutil==5.8.0',
#                             'pykep==2.6',
#                             'tqdm==4.59.0',
#                             'pygmo==2.16.1',
#                             'python==3.8.5',
#                        ],

#       # cmdclass={'install': NewInstall,
#       #           'develop': NewDevelop,
#       #           'egg_info': NewEggInfo},
      
#       # scripts = ['scripts/genesis.py'],

#   )

