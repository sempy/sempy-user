numpy==1.19.2
spiceypy==4.0.0
scipy==1.5.2
matplotlib==3.4.1
cffi==1.14.4
multiprocess==0.70.11.1
numba==0.53.1
orekit==10.3
psutil==5.8.0
pykep==2.6
tqdm==4.59.0
pygmo==2.16.1
python==3.8.5