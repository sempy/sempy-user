import os
from importlib.machinery import SourceFileLoader
from setuptools import setup, find_namespace_packages
from glob import glob
import io

name = 'sempy_core'
description = ("Core subpackage of sempy"
              )

__version__ = SourceFileLoader(
    'sempy.core.version',
    os.path.join(os.path.dirname(__file__),
                 'sempy/core/version.py')).load_module().__version__

from os.path import (basename, splitext)

here = os.path.abspath(os.path.dirname(__file__))

# get the dependencies and installs
with io.open(os.path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    # Remove flags like "--no-binary=rasterio"
    install_requires = [line.split(' ')[0] for line in f.read().split('\n')]

with open(os.path.join(here, 'README.md')) as readme_file:
    readme = readme_file.read()

setup(name=name,
      description=description,
      version=__version__,
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Emmanuel BLAZQUEZ, Thibault GATEAU',
      author_email='emmanuel.blazquez@isae-supaero.fr, thibault.gateau@isae-supaero.fr',
      url='https://gitlab.isae-supaero.fr/sempy/',
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: GNU GPL V3 License",
          "Operating System :: OS Independent",
      ],
      include_package_data=True,
      packages=find_namespace_packages(),
      install_requires=install_requires,
)


# # -*- coding: utf-8 -*-
# """
# Created on Wed Aug 26 2020

# @author: Emmanuel BLAZQUEZ
# """
# import os
# from importlib.machinery import SourceFileLoader
# from glob import glob
# import io
# from setuptools import setup, find_namespace_packages
# from setuptools.command.install import install
# from setuptools.command.develop import develop
# from setuptools.command.egg_info import egg_info



# import atexit
# import runpy


# def _post_install():
#     print('Compiling CR3BP dynamics with numba ...')
#     """Post-installation script."""
#     runpy.run_path('dynamics/srcs/cr3bp_dynamics.py', run_name='__main__')

# class NewDevelop(develop):
#     def __init__(self, *args, **kwargs):
#           super(NewDevelop, self).__init__(*args, **kwargs)
#           atexit.register(_post_install)

# class NewEggInfo(egg_info):
#     def __init__(self, *args, **kwargs):
#           super(NewEggInfo, self).__init__(*args, **kwargs)
#           atexit.register(_post_install)

# class NewInstall(install):
#     def __init__(self, *args, **kwargs):
#         super(NewInstall, self).__init__(*args, **kwargs)
#         atexit.register(_post_install)


# name = 'sempy_core'
# description = ("Core module of SEMpy"
#               )

# __version__ = SourceFileLoader(
#     'version',
#     os.path.join(os.path.dirname(__file__),
#                  'version.py')).load_module().__version__

# from os.path import (basename, splitext)

# here = os.path.abspath(os.path.dirname(__file__))

# # get the dependencies and installs
# with io.open(os.path.join(here, 'requirements.txt'), encoding='utf-8') as f:
#     # Remove flags like "--no-binary=rasterio"
#     install_requires = [line.split(' ')[0] for line in f.read().split('\n')]

# with open(os.path.join(here, 'README.md')) as readme_file:
#     readme = readme_file.read()

# setup(name=name,
#       description=description,
#       version=__version__,
#       long_description=readme,
#       long_description_content_type="text/markdown",
#       author='Emmanuel BLAZQUEZ, Thibault GATEAU',
#       author_email='emmanuel.blazquez@isae-supaero.fr, thibault.gateau@isae-supaero.fr',
#       url='https://gitlab.isae-supaero.fr/yv.gary/sempy-v1.git',
#       classifiers=[
#           "Programming Language :: Python :: 3",
#           "License :: GNU GPL V3 License",
#           "Operating System :: OS Independent",
#       ],
#       packages=find_namespace_packages(),
#       install_requires=install_requires,
#       cmdclass={'install': NewInstall,
#                 'develop': NewDevelop,
#                 'egg_info': NewEggInfo},
# )
