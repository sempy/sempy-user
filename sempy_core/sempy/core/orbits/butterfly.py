# pylint: disable=too-many-branches
"""
Created on 19/05/2020

@author: Edgar PEREZ, Alberto FOSSA', Emmanuel Blazquez
"""

from sempy.core.orbits.crtbporbit import CrtbpOrbit

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.plotting.util import set_axes_equal

import sempy.core.crtbp.jacobi as comp
import sempy.core.init.defaults as dft
from sempy.core.init.constants import A_MAT, H1_MAT, H2_MAT, DAYS2SEC
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.diffcorr.diff_corr_3d import DiffCorr3D
from sempy.core.utils.json_encoder import save_json, load_json
from copy import deepcopy
from matplotlib.pyplot import cm
from tqdm import tqdm
from matplotlib.colors import Normalize
from matplotlib.cm import ScalarMappable
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
diff_corr = DiffCorr3D(cr3bp.mu, max_internal_steps=4_000_000)

class Butterfly(CrtbpOrbit):
    """ Butterfly class will initialize and compute or initialize and interpolate a Butterfly orbit.
    This class inherits from CrtbpOrbit.

    This class will initialize and compute a periodic Butterfly orbit in a given CRTBP system,
    previously created with Cr3bp class, about the libration point selected
    and with the orbit desired period (T) or desired Jacobi constant (Cjac).
    As well, the family of the Butterfly orbit can be selected as southern.

    When the orbit is declared in can not be defined. With the function 'computation' the orbit is
    computed taking as input parameters the guess of the initial state and the guess of the period.
    Then the differential correction is applied

    Another option to get a Halo orbit is provided by Butterfly class, interpolation.
    Initialization is done in the same way as before, then interpolation method can be used.
    Interpolation method, will fetch stateV_aba, from an abacus according to T or Cjac passed
    in the initialization, with this data the state0 is estimated using np.polyfit and
    np.polyval, then crtbp method is applied to this initial state in order to get the
    interpolated Butterfly orbit.

    Instances of this class has access to the Orbit and CrtbpOrbit instance attributes.

    Parameters
    ----------
    cr3bp : Cr3bp
        An object created with the Cr3bp class, e.g. cr3bp.
    libp : Cr3bp.Libp
        A libration point of the object cr3bp, e.g. cr3bp.l2.
    family : Family
        The orbit family which is desired to be created, e.g. Halo.Family.southern.
    state0 : ndarray
        Orbit's initial state [-].

    Other Parameters
    ----------------

    Cjac : float
        Jacobi constant [-].
    T : floatnor,
        Orbit's period [-].

    Attributes
    ----------
    cr3bp : Cr3bp
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    li : Cr3bp.Libp
        The libration point which was selected for the orbit design. Attributes of this libration
        point can be accessed (check the lagrange points attributes in Cr3bp class).
    family : Family
        The orbit family selected.
    family.name: str
        The kind of family that was selected, string with the family name.
    C : float
        Jacobi constant value [-].
    E : float
        Orbit energy value [-].
    T12 : float
        Orbit's half period [-].
    T : float
        Orbit's period in normalized units [-].
    m1_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m1.
    m2_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m2.
    monodromy : ndarray
        Monodromy matrix.
    eigvals : ndarray
        Eigenvalues of the monodromy matrix.
    eigvecs : ndarray
        Eigenvectors corresponding to `eigvals`.
    stable_dir : ndarray or None
        Eigenvector along the stable manifold direction at the orbit's initial state.
    unstable_dir : ndarray or None
        Eigenvector along the unstable manifold direction at the orbit's initial state.
    center_dir : ndarray
        Eigenvectors along the center manifold directions at the orbit's initial state.
    stability_idx : ndarray
        Orbit's stability indexes.
    continuation : dict
        Dictionary with free-variables vector and null vector of the Jacobian for continuation
        procedure.
    manifolds : dict
        Dictionary storing several instances of the inner class `ManifoldBranch` that describe
        the manifold tubes associated to the orbit. Valid keys are 'stable_interior',
        'stable_exterior', 'unstable_interior', 'unstable_exterior' to identify all combinations of
        manifold stability and directions. Each key is mapped to a list of `ManifoldBranch` objects
        corresponding to different branches belonging to the same tube computed for different
        positions along the orbit.
    kwargs : str
        Will store the keyword used to initialize the orbit (Azdim or Axdim or Cjac).

    """

    def __init__(self, cr3bp, libp, family, state0 = None,T=None, **kwargs):
        """Inits Halo class."""
        if family not in (CrtbpOrbit.Family.northern, CrtbpOrbit.Family.southern):
            raise Exception('Family must be one between northern and southern')
        if family != CrtbpOrbit.Family.southern:
            raise Exception('TO DO: The northen family has not been computed yet')
        self.state0 = state0


        CrtbpOrbit.__init__(self, cr3bp, libp, family, center=None, state0=state0, **kwargs)
        self.T = T

    def postprocess(self, time_steps=dft.time_steps*2):
        """This method will update C, E, state_vec and distances from
        primaries.

        Parameters
        ----------
        time_steps : int, optional
            Number of discrete orbit's states and times included in `self.state_vec` and
            `self.t_vec` respectively. The first and last points always coincide with the orbit's
            initial state (either the periselene or the aposelene).
            If an even number, `time_steps` will be increased by one to better estimate the
            orbit's states at both apsides and its extension.
            Default is given by `src.init.defaults.time_steps`.

        """

        self.C = comp.jacobi(self.state0, self.cr3bp.mu)  # Jacobi constant [-]
        self.E = -0.5 * self.C  # orbit's energy [-]

        # propagator with odd number of time steps to include both apsis in the orbit state vector
        time_steps = time_steps if time_steps % 2 == 1 else time_steps + 1  # odd number of steps
        prop = Cr3bpSynodicPropagator(self.cr3bp.mu, with_stm=True, time_steps=time_steps)

        # orbit state vector for one complete revolution
        self.t_vec, self.state_vec, _, _ = prop.propagate([0.0, self.T], self.state0)

        # maximum and minimum distances from primaries, apoapsis and periapsis positions
        self.set_apsis(self.m1_apsis, self.cr3bp.m1_pos, self.cr3bp.m1.Rm)
        self.set_apsis(self.m2_apsis, self.cr3bp.m2_pos, self.cr3bp.m2.Rm)

        # monodromy matrix and orbit stability properties
        stm12 = self.state_vec[prop.time_steps // 2, 6:].reshape((6, 6))  # STM after half period
        self.monodromy = np.linalg.multi_dot([A_MAT, H1_MAT, stm12.T, H2_MAT, A_MAT, stm12])
        self.stability_properties()


    def computation(self, state0, T,time_steps=dft.time_steps, **kwargs):

        self.state0 = state0
        self.T = T
        self.T12 = T/2.0
        if 'Estim' in kwargs:
            state0b, T12b, T, g_vec, null_vec, flag = \
                diff_corr.diff_corr_3d_purdue(self.state0, self.T12, 1)
            self.state0 = state0b
            self.T = T
            self.T12 = T12b

        else:
            state0b, T12b, T, g_vec, null_vec, flag = \
                diff_corr.diff_corr_3d_purdue(self.state0, self.T12, 0)
            self.state0 = state0b
            self.T = T
            self.T12 = T12b

        self.postprocess()
        self.continuation['g_vec'] = g_vec
        self.continuation['null_vec'] = null_vec

        return flag

    def save_abacus(self, state0_aba, period_aba, cjac_aba):
        """Save the abacus for the orbit's family on a JSON file. """
        save_json(self.get_abacus_name(), State0=state0_aba,
                   T=period_aba, Cjac=cjac_aba,)

    def load_abacus(self):
        """Load the abacus for the orbit's family from a JSON file. """
        abacus_dict = load_json(self.get_abacus_name())
        return [np.asarray(abacus_dict[k]) for k in ('State0', 'T', 'Cjac')]

    def interpolation(self, **kwargs):
        root = 0
        time_steps = dft.time_steps*2
        state0_aba, per_aba, cjac_aba = self.load_abacus()

        self.kwargs = kwargs

        if 'T' in kwargs:
            self.state0, self.T= self.interp_eval(per_aba, self.kwargs['T'], (state0_aba,per_aba), root)


            #fix_dim = Halo.DiffCorrFixDim.period if fix_dim is None else fix_dim

        elif 'Cjac' in self.kwargs:
            self.state0, self.T = self.interp_eval(cjac_aba, self.kwargs['Cjac'],
                                                            (state0_aba, per_aba), root)
        else:
            raise Exception(' T must be provided')

        self.computation(self.state0, self.T, time_steps=time_steps)

    def fam_continuation(self,delta_sb,n_orbits,n_dysplay =10, save_abacus = False):

        progress_bar = tqdm(total=1.0, desc='EM L2 southern Butterfly')
        k = 0

        orbits = [deepcopy(self)]
        butterfly = deepcopy(self) #Help variable
        while True:


            for j in range(3):  # perturbation of the initial state, to continuate the family
                butterfly.state0[j * 2] += delta_sb * butterfly.continuation['null_vec'][j, 0]
            butterfly.T12 += delta_sb * butterfly.continuation['null_vec'][3, 0]

            butterfly.state0, butterfly.T12, butterfly.T, butterfly.continuation, flag = \
                diff_corr.diff_corr_3d_cont(butterfly.state0, butterfly.T12, delta_sb, butterfly.continuation)

            butterfly.postprocess()

            if flag:  # reduce the step if the differential correction does not converge
                butterfly = orbits[-1]
                delta_sb *= 0.5
                k -= 1
            else:  # Add element to the abacus
                orbits.append(deepcopy(butterfly))

            k += 1
            #print(k)
            progress_bar.update((1) / (n_orbits))

            if k == n_orbits or delta_sb < 1e-8:
                break




        state0_vec = [o.state0 for o in orbits]  # initial state
        cjac_vec = [o.C for o in orbits]  # Jacobi constant
        period_vec = [o.T for o in orbits]  # orbit period
        if save_abacus:
            orbit = Butterfly(cr3bp, cr3bp.l2, butterfly.family, state0_vec[0], period_vec[0])
            orbit.save_abacus(state0_aba=state0_vec, period_aba=period_vec, cjac_aba=cjac_vec,)

        # plot the family
        norm = Normalize(clip=True)
        norm.autoscale(cjac_vec)
        smap = ScalarMappable(norm, 'rainbow')
        carr = smap.to_rgba(cjac_vec)

        desc = butterfly.get_abacus_name(separator=' ')
        fig = plt.figure()
        fig.suptitle(desc + ' family')
        axs = plt.axes(projection='3d')
        #color = cm.rainbow(np.linspace(0, 1, len(orbits)))
        for i in np.arange(0, len(orbits), n_dysplay):
            axs.plot3D(orbits[i].state_vec[:, 0], orbits[i].state_vec[:, 1], orbits[i].state_vec[:, 2], c=carr[i])

        cbar = plt.colorbar(smap)
        cbar.set_label('Jacobi constant')
        set_axes_equal(axs)

        # Plot The Family with respect to the periods

        norm = Normalize(clip=True)
        norm.autoscale((np.array(period_vec)*cr3bp.T / 2.0 / np.pi)/DAYS2SEC)
        smap = ScalarMappable(norm, 'rainbow')
        carr = smap.to_rgba((np.array(period_vec)*cr3bp.T / 2.0 / np.pi)/DAYS2SEC)

        desc = butterfly.get_abacus_name(separator=' ')
        fig1 = plt.figure()
        fig1.suptitle(desc + ' family')
        axs1 = plt.axes(projection='3d')
        #color = cm.rainbow(np.linspace(0, 1, len(orbits)))
        for i in np.arange(0, len(orbits), n_dysplay):
            axs1.plot3D(orbits[i].state_vec[:, 0], orbits[i].state_vec[:, 1], orbits[i].state_vec[:, 2], c=carr[i])

        cbar = plt.colorbar(smap)
        cbar.set_label('Orbital Period [day]')
        set_axes_equal(axs1)
        return orbits

