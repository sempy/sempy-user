"""
This folder contains all the abacuses to initialize Cr3bp orbits via interpolation.
"""

import os

DIRNAME = os.path.dirname(__file__) + '/'
