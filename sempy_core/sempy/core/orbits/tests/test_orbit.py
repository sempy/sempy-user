# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 09:44:34 2019

@author: Edgar PEREZ, Alberto FOSSA'
"""

import unittest

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.orbits.plyap import Plyap

class TestOrbit(unittest.TestCase):

    def test_halo_orbit_init(self):

        # The following data was obtained with SEMAT
        m = 1
        dm = -1
        Azdim = 12000
        Az = 0.031217481789802
        Azdim_estimate = 12000
        Az_estimate = 0.031217481789802

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)

        self.assertEqual(orbit.m_richardson, m)
        self.assertEqual(orbit.dm_richardson, dm)
        self.assertEqual(orbit.Azdim, Azdim) 
        self.assertAlmostEqual(orbit.Az, Az) 
        self.assertEqual(orbit.Azdim_estimate, Azdim_estimate)
        self.assertAlmostEqual(orbit.Az_estimate, Az_estimate) 

    def test_orbit_warns(self):
        """ Test that exceptions are raised."""
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        with self.assertRaisesRegex(Exception, 'Family must be one between northern and southern'):
            Halo(cr3bp, cr3bp.l2, Halo.Family.planar, Azdim=12000)
        with self.assertRaisesRegex(Exception, 'Family must be planar'):
            Plyap(cr3bp, cr3bp.l2, Plyap.Family.southern, Axdim=12000)
        with self.assertRaises(Exception):
            Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=12000, Cjac=3.12)
        with self.assertRaises(Exception):
            Plyap(cr3bp, cr3bp.l2, Plyap.Family.planar, Axdim=12000, Cjac=3.12)


if __name__ == '__main__':
    unittest.main()
