# pylint: disable=invalid-name

"""
Test script to validate the DRO abacus generation method with the SEMat validated DRO abacus

@author: Paolo GUARDABASSO

Created: 24/02/2021
"""

# %% Imports
import os.path
from copy import deepcopy
import numpy as np
from scipy.io import loadmat

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO

dirname = os.path.dirname(__file__)

# %% SEMat abacus extraction
filename = os.path.join(dirname, 'data', 'test_dro_abacus_generation.mat')
dro_mat = loadmat(filename, squeeze_me=True, struct_as_record=False)['dro_init']

# %% Initialisation and environment definition
N_orbits = 450

# define environment (EM system)
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# Choice of central body of DROs
center = cr3bp.m2

# %% Orbit initialisation and computation
state0_guess_1 = np.array([1-cr3bp.mu - 0.01, 0, 0, 0, 0.1, 0])
state0_guess_2 = np.array([1-cr3bp.mu - 0.012, 0, 0, 0, 0.1, 0])

dro_1 = DRO(cr3bp, center, state0=state0_guess_1)
dro_2 = DRO(cr3bp, center, state0=state0_guess_2)

dro_1.computation()
dro_2.computation()

# %% Family continuation
# init continuation process
orbits = []  # list to be filled with all computed orbits

for i in range(N_orbits):
    # perturbation of the initial state
    state0_guess = dro_2.state0 + (dro_2.state0 - dro_1.state0)

    dro_1 = deepcopy(dro_2)
    dro_2 = DRO(cr3bp, center, state0=state0_guess)
    dro_2.computation()

    orbits.append(deepcopy(dro_2))

# %% extract data to be tested
state0_vec = [o.state0 for o in orbits]  # initial state
x0_vec = [o.state0[0] for o in orbits]
cjac_vec = [o.C for o in orbits]  # Jacobi constant

# %% Test that abacuses are the same
np.testing.assert_allclose(x0_vec, dro_mat.X0/cr3bp.L, rtol=0., atol=1e-8)
np.testing.assert_allclose(cjac_vec, dro_mat.C, rtol=0., atol=1e-6)
np.testing.assert_allclose(state0_vec, dro_mat.initialConditions, rtol=0., atol=1e-5)

print("Test succesfull!")
