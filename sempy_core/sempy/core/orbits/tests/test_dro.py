"""
Validation of DRO initialization, computation and postprocessing routines.

@author: Paolo GUARDABASSO, Created on 22/2/2021
"""

import unittest
import os.path
import numpy as np
from scipy.io import loadmat

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO


dirname = os.path.dirname(__file__)


class TestDRO(unittest.TestCase):
    """Class for DRO tests"""
    def test_dro_computation(self):
        """Validation of initialization, refinement and postprocessing for a Moon DRO computed in
        SEMAT. """

        # results obtained in SEMAT for a Moon DRO orbit with a fixed initial state
        filename = os.path.join(dirname, 'data', 'test_dro_validation_comp.mat')
        dro_mat = loadmat(filename,
                          squeeze_me=True, struct_as_record=False)['dro_comp']

        # initialize DRO
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        dro = DRO(cr3bp, cr3bp.m2,
                  state0=np.array([0.715849418376566, 0, 0, 0, 0.662220988182704, 0]))
        dro.computation(fix_dim=None)

        # init parameters
        self.assertEqual(dro.Az, None)
        self.assertEqual(dro.Azdim, None)
        self.assertEqual(dro.Az_estimate, None)
        self.assertEqual(dro.Azdim_estimate, None)

        # orbit's parameters
        self.assertAlmostEqual(dro.Ax, dro_mat.Ax+cr3bp.m2_pos[0], places=6)
        self.assertAlmostEqual(dro.C, dro_mat.C, places=6)
        self.assertAlmostEqual(dro.E, dro_mat.E, places=6)
        self.assertAlmostEqual(dro.T, dro_mat.T, places=6)
        self.assertAlmostEqual(dro.T12, dro_mat.T12, places=6)

        # Apoapsis radius and altitude
        self.assertAlmostEqual(dro.m2_apsis['apoapsis_radius'], dro_mat.apogee.radius, places=6)
        self.assertAlmostEqual(dro.m2_apsis['apoapsis_altitude'], dro_mat.apogee.altitude, places=6)

        # Periapsis radius and altitude
        self.assertAlmostEqual(dro.m2_apsis['periapsis_radius'], dro_mat.perigee.radius, places=6)
        self.assertAlmostEqual(dro.m2_apsis['periapsis_altitude'], dro_mat.perigee.altitude,
                               places=6)

        # Apoapsis and periapsis positions
        for i in (0, 2):
            self.assertAlmostEqual(dro.m2_apsis['apoapsis_position'][i],
                                   dro_mat.apogee.position[i], places=6)
        self.assertAlmostEqual(abs(dro.m2_apsis['apoapsis_position'][1]),
                               abs(dro_mat.apogee.position[1]), places=6)

        # Other SEMat definitions
        self.assertAlmostEqual(dro.m2_apsis['periapsis_radius'], dro_mat.minDistToM2, places=6)
        self.assertAlmostEqual(dro.m2_apsis['apoapsis_radius'], dro_mat.maxDistToM2, places=6)
        self.assertAlmostEqual(dro.m1_apsis['periapsis_radius'], dro_mat.minDistToM1, places=6)
        self.assertAlmostEqual(dro.m1_apsis['apoapsis_radius'], dro_mat.maxDistToM1, places=6)

        # initial state and state vector
        np.testing.assert_allclose(dro.t_vec, dro_mat.tv, rtol=0., atol=1e-8)
        np.testing.assert_allclose(dro.state0, dro_mat.y0[:6], rtol=0., atol=1e-8)
        np.testing.assert_allclose(dro.state_vec, dro_mat.yv, rtol=0., atol=1e-5)
        np.testing.assert_allclose(dro.state_vec[-1, 6:].reshape(6, 6), dro_mat.monodromy,
                                   rtol=0., atol=1e-5)

    def test_dro_interpolation(self):
        """Validation of interpolation for DROs. """

        # initialize DRO with orbital period T
        per = 2.000000
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        dro_per = DRO(cr3bp, cr3bp.m2, T=per)
        dro_per.interpolation()
        self.assertAlmostEqual(dro_per.T, per, places=6)

        # initialize DRO with Jacobi constant cjac
        cjac = 3.000000
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        dro_cjac = DRO(cr3bp, cr3bp.m2, Cjac=cjac)
        dro_cjac.interpolation()
        self.assertAlmostEqual(dro_cjac.C, cjac, places=6)

        # initialize DRO with Ax parameter
        ax_dim = 500000
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        dro_axdim = DRO(cr3bp, cr3bp.m2, Axdim=ax_dim)
        dro_axdim.interpolation()
        self.assertAlmostEqual(dro_axdim.Axdim, ax_dim, places=6)


if __name__ == '__main__':
    unittest.main()
