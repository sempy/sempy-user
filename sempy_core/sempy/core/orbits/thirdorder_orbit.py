# pylint: disable=invalid-name, too-many-locals

"""
Created on 19/05/2020

@author: Emmanuel Blazquez, Alberto FOSSA', Edgar Perez
"""

import numpy as np
from sempy.core.orbits.crtbporbit import CrtbpOrbit

from sempy.core.crtbp.richardson_coefficients import RichardsonCoefficients


class ThirdOrderOrbit(CrtbpOrbit):
    """ ThirdOrderOrbit class will provide a first guess for Halo, Vlyap (Vertical Lyapunov) and
    Plyap (Planar Lyapunov) orbits. This class inherits from CrtbpOrbit.

    A third-order approximation was obtained by Richardson in his work of 1980,
    Analytic construction of periodic orbits about the collinear points, which will provide us an
    initial state for orbits mentioned earlier, this initial state is composed of position and
    velocities, that later will be used by our differential correctors and propagator to compute
    orbits within the Halo, Vlyap and Plyap classes.
    For more details, See Koon et al. 2011, chapter 6.

    Instances of this class has access to the parent class, CrtbpOrbit, instance attributes.

    Parameters
    ----------
    orbit : CrtbpOrbit
        An object created with the classes Halo, Vlyap, Plyap.
    t : float
        time, [s]. This time is used to get tau1 parameter and is not a required argument,
        just use its default value t=0.

    Attributes
    ----------
    orbit : CrtbpOrbit
        Attributes of this object can be accessed (check attributes in CrtbpOrbit, Halo,
        Vlyap and Plyap classes).
    t : float
        This time is related to tau1 Richardson coefficient.
    rc : object
        Will store all the Richardson coefficients,
        for more details check the RichardsonCoefficients class within the coefficients module.
    Az_li : float
        Z-amplitude in Li-frame.
    Ax_li : float
        X-amplitude in Li-frame.
    T_li : float
        Orbital period estimate in Li-frame.
    nu : float
        Third-order approximation parameter.
    state0_li_frame : ndarray
        State estimate at t in the Li-frame.
    state0 : ndarray or None
        Initial state, positions and velocities, in synodic frame.
        This is our first guess for Halo, Vlyap and Plyap orbits.

    """

    def __init__(self, orbit, t=0):
        """Inits ThirdOrderOrbit class."""
        self.orbit = orbit
        self.t = t
        self.rc = RichardsonCoefficients(self.orbit.cr3bp, self.orbit.li)

        # set phase parameter equal to PI for L1 orbits to have a state0 at periapsis
        self.phi = np.pi if self.orbit.li.number == 1 else 0.0

        if self.orbit.Az_estimate is None:
            self.orbit.Az_estimate = 0

        self.Az_li, self.Ax_li = ThirdOrderOrbit.amplitude_li_frame(self.orbit, self.rc)

        self.T_li, self.nu = ThirdOrderOrbit.params_li_frame(self.rc, self.Az_li, self.Ax_li)

        self.state0_li_frame = self.richardson_state0_li_frame(self.orbit, self.Ax_li, self.Az_li,
                                                               self.nu, self.rc, self.phi)

        if 'Azdim' in orbit.kwargs or 'Axdim' in orbit.kwargs:
            CrtbpOrbit.__init__(self, orbit.cr3bp, orbit.li, orbit.family, **orbit.kwargs)
        else:
            raise Exception('Must provide extension parameter, Axdim or Azdim'
                            '(depending on the orbit), in the orbit initialization')

        self.state0 = self.richardson_state0_synodic_frame(self.orbit, self.orbit.li,
                                                           self.state0_li_frame)

    @staticmethod
    def amplitude_li_frame(orbit, rc):
        """X-amplitude and Z-amplitude in Li-frame."""

        if orbit.Az_estimate is None:
            orbit.Az_estimate = 0

        # for NRHO maybe the Third Order Orbit approximation is not enough, so be aware of using
        # this as first guess for NRHO
        if orbit.kind == 'Halo' or orbit.kind == 'NRHO':
            # in orbit.li this li corresponds to the libration point selected to initialize
            # the orbit, orbit.li is not related to the li-frame subscript
            # ( on the other hand, e.g. Ax_li is the amplitude in li-frame)
            Az_li = orbit.Az_estimate / orbit.li.gamma_i
            Ax_li = (-(rc.l2 * Az_li ** 2 + rc.Delta) / rc.l1) ** (1 / 2)
        elif orbit.kind == 'Vlyap':
            Az_li = orbit.Az_estimate / orbit.li.gamma_i
            Ax_li = 0
        elif orbit.kind == 'Plyap':
            Az_li = 0
            Ax_li = orbit.Ax_estimate / orbit.li.gamma_i
        else:
            raise Exception('A third-order approximation is only suitable for: '
                            'Halo, Vlyap and Plyap orbits')
        return Az_li, Ax_li

    @staticmethod
    def params_li_frame(rc, Az_li, Ax_li):
        """Parameters in Li-frame."""

        nu = 1 + rc.s1 * Ax_li ** 2 + rc.s2 * Az_li ** 2  # nu parameter
        T_li = 2 * np.pi / (rc.omega_p * nu)  # Period estimate

        return T_li, nu

    @staticmethod
    def richardson_state0_li_frame(orbit, Ax_li, Az_li, nu, rc, phi, t=0):
        """Initial conditions at time t in li frame."""

        tau1 = rc.omega_p * nu * t + phi  # third-order approximation parameter
        dtau1dt = rc.omega_p * nu  # third-order approximation parameter
        state0_li = np.empty(6)  # initial state in li frame

        state0_li[0] = rc.a21 * Ax_li ** 2 + rc.a22 * Az_li ** 2 - Ax_li * np.cos(tau1) + \
            (rc.a23 * Ax_li ** 2 - rc.a24 * Az_li ** 2) * np.cos(2 * tau1) + \
            (rc.a31 * Ax_li ** 3 - rc.a32 * Ax_li * Az_li ** 2) * np.cos(3 * tau1)

        state0_li[1] = rc.kappa * Ax_li * np.sin(tau1) + \
            (rc.b21 * Ax_li ** 2 - rc.b22 * Az_li ** 2) * np.sin(2 * tau1) + \
            (rc.b31 * Ax_li ** 3 - rc.b32 * Ax_li * Az_li ** 2) * np.sin(3 * tau1)

        state0_li[2] = orbit.dm_richardson * Az_li * np.cos(tau1) + \
            orbit.dm_richardson * rc.d21 * Ax_li * Az_li * (np.cos(2 * tau1) - 3) + \
            orbit.dm_richardson * (rc.d32 * Az_li * Ax_li ** 2 - rc.d31 * Az_li ** 3) * \
            np.cos(3 * tau1)

        state0_li[3] = \
            dtau1dt * (Ax_li * np.sin(tau1) -
                       2 * (rc.a23 * Ax_li ** 2 - rc.a24 * Az_li ** 2) * np.sin(2 * tau1) -
                       3 * (rc.a31 * Ax_li ** 3 - rc.a32 * Ax_li * Az_li ** 2) * np.sin(3 * tau1))

        state0_li[4] = \
            dtau1dt * (rc.kappa * Ax_li * np.cos(tau1) +
                       2 * (rc.b21 * Ax_li ** 2 - rc.b22 * Az_li ** 2) * np.cos(2 * tau1) +
                       3 * (rc.b31 * Ax_li ** 3 - rc.b32 * Ax_li * Az_li ** 2) * np.cos(3 * tau1))

        state0_li[5] = \
            dtau1dt * (-orbit.dm_richardson * Az_li * np.sin(tau1) -
                       2 * orbit.dm_richardson * rc.d21 * Ax_li * Az_li * np.sin(2 * tau1) -
                       3 * orbit.dm_richardson * (rc.d32 * Az_li * Ax_li ** 2 -
                                                  rc.d31 * Az_li ** 3) * np.sin(3 * tau1))

        # reset spurious non-zero components due to approximations in trigonometric functions
        state0_li[np.fabs(state0_li) < np.finfo(np.float64).eps] = 0.0

        return state0_li

    def richardson_state0_synodic_frame(self, orbit, libp, state0_li):
        """Initial state in synodic frame from corresponding state in li frame."""

        state0 = np.empty(6)  # initial state in synodic frame

        # special case of the x component
        if libp == self.orbit.cr3bp.l1:
            state0[0] = state0_li[0] * orbit.li.gamma_i - orbit.cr3bp.mu + 1 - orbit.li.gamma_i
        elif libp == self.orbit.cr3bp.l2:
            state0[0] = state0_li[0] * orbit.li.gamma_i - orbit.cr3bp.mu + 1 + orbit.li.gamma_i
        elif libp == self.orbit.cr3bp.l3:
            state0[0] = state0_li[0] * orbit.li.gamma_i - orbit.cr3bp.mu - orbit.li.gamma_i
        else:
            state0[0] = state0_li[0] * orbit.li.gamma_i - orbit.cr3bp.mu + 1 + orbit.li.gamma_i

        # all other components
        state0[1:] = orbit.li.gamma_i * np.asarray(state0_li[1:])

        return state0
