"""
Created on 19/05/2020

@author: Emmanuel Blazquez, Alberto FOSSA'
"""

import sempy.core.init.defaults as dft

from sempy.core.init.primary import Primary
from sempy.core.orbits.halo import Halo


class NRHO(Halo):
    """Near Rectilinear Halo Orbit. """

    def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
        """Inits NRHO class. """
        if cr3bp.m1 != Primary.EARTH or cr3bp.m2 != Primary.MOON:
            raise Exception('Only EARTH-MOON CR3BP system is allowed')
        Halo.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)

    def set_init_parameter(self, kwargs):
        """Initialize the orbit via periselene radius or altitude, vertical extension,
        Jacobi constant or orbit's period.

        Other Parameters
        -----------------
        Altpdim : float
            Periselene altitude [km].
        Rpdim : float
            Periselene radius [km].
        Azdim : float
            Vertical extension [km].
        Cjac : float
            Jacobi constant [-].
        T : float
            Orbit's period [-].

        """
        if 'Rpdim' in kwargs:  # initialization via periapsis radius
            self.m2_apsis['periapsis_radius_dim'] = float(kwargs['Rpdim'])
            self.m2_apsis['periapsis_radius'] = \
                self.m2_apsis['periapsis_radius_dim'] / self.cr3bp.L
            self.m2_apsis['periapsis_altitude_dim'] = \
                self.m2_apsis['periapsis_radius_dim'] - self.cr3bp.m2.Rm
            self.m2_apsis['periapsis_altitude'] = \
                self.m2_apsis['periapsis_altitude_dim'] / self.cr3bp.L
        elif 'Altpdim' in kwargs:  # initialization via periapsis altitude
            self.m2_apsis['periapsis_altitude_dim'] = float(kwargs['Altpdim'])
            self.m2_apsis['periapsis_altitude'] = \
                self.m2_apsis['periapsis_altitude_dim'] / self.cr3bp.L
            self.m2_apsis['periapsis_radius_dim'] = \
                self.m2_apsis['periapsis_altitude_dim'] + self.cr3bp.m2.Rm
            self.m2_apsis['periapsis_radius'] = \
                self.m2_apsis['periapsis_radius_dim'] / self.cr3bp.L
        else:  # initialization via vertical extension, Jacobi constant or orbit's period
            Halo.set_init_parameter(self, kwargs)

    def interpolation(self, root=0, fix_dim=None, time_steps=dft.time_steps):
        """Generates a Near Rectilinear Halo Orbit via interpolation of abacus data.

        Firstly, the correct abacus is loaded and an approximate initial state is interpolated
        from the available abacus data.
        Secondly, a differential correction procedure is applied to correct the approximate
        state and all orbit parameters are updated via the `postprocess` class method.

        Parameters
        ----------
        root : int, optional
            Index of the interpolating spline's root selected for the estimation of the orbit's
            parameters. Lower values correspond to orbits closer to their respective libration
            point. Default is zero for which the closest orbit to the corresponding libration
            point is obtained.
        fix_dim : Halo.DiffCorrFixDim.fix_dim, optional
            Specifies the differential correction procedure, e.g. Halo.DiffCorrFixDim.x0.
        time_steps : int, optional
            Number of discrete orbit's states and times included in `self.state_vec` and
            `self.t_vec` respectively. The first and last points always coincide with the orbit's
            initial state (either the periselene or the aposelene).
            If an even number, `time_steps` will be increased by one to better estimate the
            orbit's states at both apsides and its extension.
            Default is given by `src.init.defaults.time_steps`.

        """
        if 'Altpdim' in self.kwargs:
            self.kwargs['Rpdim'] = self.kwargs['Altpdim'] + self.cr3bp.m2.Req
        if 'Rpdim' in self.kwargs:
            state0_aba, az_aba, rp_aba, _, _ = self.load_abacus()
            self.state0, self.Az = self.interp_eval(rp_aba, self.kwargs['Rpdim']/self.cr3bp.L,
                                                    (state0_aba, az_aba), root)
            self.computation(fix_dim=fix_dim, time_steps=time_steps)
        elif any([k in self.kwargs for k in ('Azdim', 'Cjac', 'T')]):
            Halo.interpolation(self, root=root, fix_dim=fix_dim, time_steps=time_steps)
        else:
            raise Exception('Either Azdim, Altpdim, Rpdim, Cjac or T must be provided')
