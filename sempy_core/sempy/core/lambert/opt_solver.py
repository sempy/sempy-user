"""
Wrappers to PyGMO algorithms to solve optimization problems associated with direct impulsive
transfers in the Circular Restricted Three-Body Problem.

@author: Alberto FOSSA'
"""

import pygmo.core as pg


class NLoptSolver:
    """Wrapper to NLopt local derivative-free and gradient-based optimizers. """

    solvers_list = ('cobyla', 'bobyqa', 'newuoa', 'newuoa_bound', 'praxis', 'neldermead', 'sbplx',
                    'mma', 'ccsaq', 'slsqp', 'lbfgs', 'tnewton_precond_restart', 'tnewton_precond',
                    'tnewton_restart', 'tnewton', 'var2', 'var1')
    properties_list = ('stopval', 'ftol_rel', 'ftol_abs', 'xtol_rel', 'xtol_abs',
                       'maxeval', 'maxtime')

    def __init__(self, solver='lbfgs', verbose=True, **kwargs):
        """Inits GradientBasedSolver. """
        if solver not in self.solvers_list:
            raise Exception(f"solver must be one of {', '.join(self.solvers_list)}")
        self.algo = pg.algorithm(pg.nlopt(solver))
        self.algo.set_verbosity(1) if verbose else self.algo.set_verbosity(0)
        if any([k in self.properties_list for k in kwargs]):
            self.set_properties(**kwargs)

    def set_properties(self, **kwargs):
        """Set solver properties. """
        if 'stopval' in kwargs:
            self.algo.extract(pg.nlopt).stopval = kwargs['stopval']
        if 'ftol_rel' in kwargs:
            self.algo.extract(pg.nlopt).ftol_rel = kwargs['ftol_rel']
        if 'ftol_abs' in kwargs:
            self.algo.extract(pg.nlopt).ftol_abs = kwargs['ftol_abs']
        if 'xtol_rel' in kwargs:
            self.algo.extract(pg.nlopt).xtol_rel = kwargs['xtol_rel']
        if 'xtol_abs' in kwargs:
            self.algo.extract(pg.nlopt).xtol_abs = kwargs['xtol_abs']
        if 'maxeval' in kwargs:
            self.algo.extract(pg.nlopt).maxeval = kwargs['maxeval']
        if 'maxtime' in kwargs:
            self.algo.extract(pg.nlopt).maxtime = kwargs['maxtime']

    def solve(self, pbm, x_vec0):
        """Solve a given optimization problem. """
        pop = pg.population(pg.problem(pbm))  # PyGMO problem and population objects
        pop.push_back(x_vec0)  # initial guess
        pop = self.algo.evolve(pop)  # solve optimization problem
        return pop.champion_x, pbm.solve(pop.champion_x), pop
