"""
Application of Primer Vector Theory to evaluate the optimality of an impulsive transfer
trajectory in an arbitrary force model.

@author: Alberto FOSSA'
"""

from warnings import simplefilter
import numpy as np

import sempy.core.init.defaults as dft
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.ephemeris import Ephemeris
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.propagation.ephemeris_propagator import EphemerisPropagator, EphemerisSHPropagator

simplefilter('ignore', RuntimeWarning)


class PrimerVector:
    """Primer Vector Theory for the evaluation of the optimality of an impulsive transfer arc. """

    def __init__(self, model, ref='J2000', obs=None, t_c=1.0, l_c=1.0, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits PrimerVector class. """
        if isinstance(model, Cr3bp):
            self.prop = Cr3bpSynodicPropagator(model.mu, with_stm=True, rtol=rtol, atol=atol,
                                               time_steps=time_steps,
                                               max_internal_steps=max_internal_steps)
        elif isinstance(model, Ephemeris):
            if model.sh_models is None:
                self.prop = EphemerisPropagator(model, ref=ref, obs=obs, t_c=t_c, l_c=l_c,
                                                with_stm=True, rtol=rtol, atol=atol,
                                                time_steps=time_steps,
                                                max_internal_steps=max_internal_steps)
            else:
                self.prop = EphemerisSHPropagator(model, ref=ref, obs=obs, t_c=t_c, l_c=l_c,
                                                  with_stm=True, rtol=rtol, atol=atol,
                                                  time_steps=time_steps,
                                                  max_internal_steps=max_internal_steps)
        else:
            raise Exception('model must be a Cr3bp or Ephemeris object')

    @staticmethod
    def boundary_conditions(dv1, dv2):
        """Computes the Primer Vector's Boundary Conditions. """
        return dv1 / np.linalg.norm(dv1), dv2 / np.linalg.norm(dv2)

    def history(self, t_span, state1, dv1, dv2):
        """Computes the components of the Primer Vector and its derivative over time. """
        t_vec, state_vec, _, _ = self.prop.propagate(t_span, state1)
        stm_vec = state_vec[:, 6:42].reshape((t_vec.size, 6, 6))
        pv1, pv2 = self.boundary_conditions(dv1, dv2)
        pv1_dot = np.linalg.solve(stm_vec[-1, 0:3, 3:6], (pv2 - stm_vec[-1, 0:3, 0:3].dot(pv1)))
        return t_vec, stm_vec.dot(np.concatenate((pv1, pv1_dot)))

    def magnitude(self, t_span, state1, dv1, dv2):
        """Computes the Primer Vector magnitude and its approximate derivative over time. """
        t_vec, pv_vec = self.history(t_span, state1, dv1, dv2)
        pv_mag = np.linalg.norm(pv_vec[:, 0:3], axis=1)
        return t_vec, pv_mag, np.gradient(pv_mag, t_vec, edge_order=2)
