"""
Utilities functions for transfer in the CR3BP force model.

@author: Alberto FOSSA'
"""

from warnings import simplefilter
import numpy as np

simplefilter('ignore', RuntimeWarning)


def floor_cost(cjac1, cjac2, r2_min, mu_cr3bp):
    """Computes the minimum theoretical cost for a direct orbit transfer in the CR3BP.

    The function implements a simple analytical approximation of the minimum theoretical dV
    required to transfer a spacecraft from an initial orbit characterized by a Jacobi constant
    equal to `cjac1` to a target orbit characterized by a JC equal to `cjac2`.
    The transfer is assumed to be performed in the vicinity of the second primary `m2` with the
    closest approach characterized by a spacecraft to `m2` center distance equal to `r2_min`.

    Parameters
    ----------
    cjac1 : float or ndarray
        Jacobi constant value or values for the departure orbit(s) [-].
    cjac2 : float or ndarray
        Jacobi constant value or values for the arrival orbit(s) [-].
    r2_min : float
        Minimum allowed spacecraft to `m2` center distance [-].
    mu_cr3bp : float
        CR3BP mass parameter.

    Returns
    -------
    dv_min : float or ndarray
        Minimum theoretical dV(s) for a direct transfer between specified orbits [-].

    Warnings
    --------
    The function can perform simultaneous computation of the minimum theoretical dV for several
    departure and arrival orbits defined in the same CR3BP system. To produce correct results, one
    of the following conditions on the two input parameters `cjac1` and `cjac2` must be satisfied:

    * | `cjac1` and `cjac2` are both of type float, for which single departure and arrival orbits
      | are assumed.
    * | `cjac1` and `cjac2` are both ndarrays with the same number of elements, for which transfers
      | from orbits characterized by `cjac1[i]` to orbits characterized by `cjac2[i]` are assumed.
    * | `cjac1` is of type float and `cjac2` is an ndarray, for which transfers from a single
      | departure orbit to several arrival orbits are assumed.
    * | `cjac1` is an ndarray and `cjac2` is of type float, for which transfers from several
      | departure orbits to a single arrival orbit are assumed.

    """

    const = (1.0 - mu_cr3bp - r2_min) ** 2 + \
        2.0 * ((1.0 - mu_cr3bp) / (1.0 - r2_min) + mu_cr3bp / r2_min)
    return np.fabs((const - cjac1) ** 0.5 - (const - cjac2) ** 0.5)
