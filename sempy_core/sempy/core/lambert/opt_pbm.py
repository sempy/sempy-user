# pylint: disable=not-callable
"""
Optimization problems to determine the most fuel-efficient direct impulsive transfers in the
Circular Restricted Three-Body Problem.

@author: Alberto FOSSA'
"""

import numpy as np

import sempy.core.init.defaults as dft
from sempy.core.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm
from sempy.core.lambert.primer_vector import PrimerVector


class Cr3bpOptLambertPbm:
    """Optimal solution to the Lambert Problem in the CR3BP model.

    Parameters
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple
        Lower and upper free variables bounds.

    Attributes
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple
        Lower and upper free variables bounds.
    sol : tuple
        Current solution to the Lambert Problem.
    solve : function
        Cr3bpLambertPbm `solve` method as function of the free variables only.

    """

    def __init__(self, pbm, bounds):
        """Inits Cr3bpOptLambertPbm class. """
        if not isinstance(pbm, Cr3bpLambertPbm):
            raise Exception('pbm must be a Cr3bpLambertPbm instance')
        self.pbm = pbm
        self.bounds = bounds
        self.sol, self.solve = None, None

    def fitness(self, x_vec):
        """Objective function. """
        self.sol = self.solve(x_vec)
        return self.sol[5][2:3]

    def gradient(self, x_vec):
        """Gradient of the objective function. """
        if x_vec[0] != self.sol[0]:
            self.fitness(x_vec)
        pv1, pv2 = PrimerVector.boundary_conditions(self.sol[3], self.sol[4])
        _, state_vec, _, _ = self.pbm.diff_corr.prop.propagate([0.0, x_vec[0]], self.sol[2][0])
        return pv1, pv2, state_vec[-1, 0:6], state_vec[-1, 6:42].reshape((6, 6))

    def get_bounds(self):
        """Free variables lower and upper bounds. """
        return self.bounds

    def estimate_tof(self, theta1, theta2, nb_revs=0):
        """Estimation of the required time of flight. """
        return self.pbm.get_tof(theta1, theta2, nb_revs)


class Cr3bpOptTOFLambertPbm(Cr3bpOptLambertPbm):
    """Optimal time of flight for an impulsive transfer.

    Parameters
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple, optional
        Lower and upper free variables bounds.
        Default is `((1e-6,), (np.inf,))`.
    nb_revs : int, optional
        Number of complete revolutions about the main attractor. Default is zero.
    nb_pts : int or tuple, optional
        Number of patch points to be considered for the differential correction procedure.
        Default is `2` which corresponds to a single shooting technique where only the
        departure and arrival positions are taken into account.
    guess : str, optional
        Method used to compute an approximate solution to be set as initial guess for the
        differential correction procedure. Default is `r2bp` for R2BP approximation.
    clockwise : bool or None, optional
        Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
        approximation set as initial guess. Default is None for which the appropriate
        direction is determined based on the initial angular momentum vector.
    procs : int, optional
        Number of spawn worker processes when propagating subsequent trajectory segments
        in parallel or `None` for serial propagation.
        Default is the number of physical cores on Linux or None on Windows.
    verbose : bool, optional
        Whether printing the error norm at each iteration (True) or not (False).
        Default is `True`.

    Other Parameters
    ----------------
    state1 : ndarray
        Departure state before the first impulsive manoeuvre.
    state2 : ndarray
        Arrival state after the second impulsive manoeuvre.
    theta1 : float
        Departure position on the first orbit specified as a fraction of its period in the
        closed interval ``[0, 1]``.
    theta2 : float
        Arrival position on the target orbit specified as a fraction of its period in the
        closed interval ``[0, 1]``.

    Attributes
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple
        Lower and upper free variables bounds.
    solve : function
        Cr3bpLambertPbm `solve` method as function of the free variables only.

    """

    def __init__(self, pbm, bounds=((1e-6,), (np.inf,)), nb_revs=0, nb_pts=2, guess='r2bp',
                 clockwise=None, procs=dft.procs, verbose=False, **kwargs):
        """Inits Cr3bpLambertPbmOptTOF class. """
        Cr3bpOptLambertPbm.__init__(self, pbm, bounds)
        if isinstance(nb_pts, (int, np.int64)):
            self.solve = lambda x: self.pbm.solve(x[0], False, nb_revs, nb_pts, guess, clockwise,
                                                  procs, verbose, **kwargs)
        elif isinstance(nb_pts, (tuple, list)):
            self.solve = lambda x: self.pbm.solve_min_dv(x[0], False, nb_revs, nb_pts, guess,
                                                         clockwise, procs, verbose, **kwargs)
        else:
            raise Exception('nb_pts must be either an int or a two-elements tuple')

    def gradient(self, x_vec):
        """Gradient of the objective function. """
        pv1, pv2, state2, stm2 = Cr3bpOptLambertPbm.gradient(self, x_vec)
        return [(pv2.dot(stm2[3:6, 3:6]) - pv1).
                dot(np.linalg.solve(stm2[0:3, 3:6], state2[3:6])) -
                pv2.dot(self.pbm.diff_corr.odes(x_vec[0], state2[0:6])[3:6])]


class Cr3bpOptPosTOFLambertPbm(Cr3bpOptLambertPbm):
    """Optimal endpoints positions and time of flight for an impulsive transfer.

    Parameters
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple, optional
        Lower and upper free variables bounds.
        Default is `((1e-6, 0.0, 0.0), (np.inf, 1.0, 1.0))`.
    nb_revs : int, optional
        Number of complete revolutions about the main attractor. Default is zero.
    nb_pts : int or tuple, optional
        Number of patch points to be considered for the differential correction procedure.
        Default is `2` which corresponds to a single shooting technique where only the
        departure and arrival positions are taken into account.
    guess : str, optional
        Method used to compute an approximate solution to be set as initial guess for the
        differential correction procedure. Default is `r2bp` for R2BP approximation.
    clockwise : bool or None, optional
        Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
        approximation set as initial guess. Default is None for which the appropriate
        direction is determined based on the initial angular momentum vector.
    procs : int, optional
        Number of spawn worker processes when propagating subsequent trajectory segments
        in parallel or `None` for serial propagation.
        Default is the number of physical cores on Linux or None on Windows.
    verbose : bool, optional
        Whether printing the error norm at each iteration (True) or not (False).
        Default is `True`.

    Attributes
    ----------
    pbm : Cr3bpLambertPbm
        Cr3bpLambertPbm object.
    bounds : tuple
        Lower and upper free variables bounds.
    solve : function
        Cr3bpLambertPbm `solve` method as function of the free variables only.

    """

    def __init__(self, pbm, bounds=((1e-6, 0.0, 0.0), (np.inf, 1.0, 1.0)), nb_revs=0, nb_pts=2,
                 guess='r2bp', clockwise=None, procs=dft.procs, verbose=False):
        """Inits Cr3bpLambertPbmOptPosTOF class. """
        Cr3bpOptLambertPbm.__init__(self, pbm, bounds)
        if isinstance(nb_pts, (int, np.int64)):
            self.solve = lambda x: self.pbm.solve(x[0], False, nb_revs, nb_pts, guess, clockwise,
                                                  procs, verbose, theta1=x[1], theta2=x[2])
        elif isinstance(nb_pts, (tuple, list)):
            self.solve = \
                lambda x: self.pbm.solve_min_dv(x[0], False, nb_revs, nb_pts, guess, clockwise,
                                                procs, verbose, theta1=x[1], theta2=x[2])
        else:
            raise Exception('nb_pts must be either an int or a two-elements tuple')

    def gradient(self, x_vec):
        """Gradient of the objective function. """
        pv1, pv2, state2, stm2 = Cr3bpOptLambertPbm.gradient(self, x_vec)
        fac = pv2.dot(stm2[3:6, 3:6]) - pv1
        return [fac.dot(np.linalg.solve(stm2[0:3, 3:6], state2[3:6])) -
                pv2.dot(self.pbm.diff_corr.odes(x_vec[0], state2[0:6])[3:6]),
                (fac.dot(np.linalg.solve(stm2[0:3, 3:6], stm2[0:3, 0:3].dot(self.sol[6][3:6]))) -
                 pv2.dot(stm2[3:6, 0:3].dot(self.sol[6][3:6])) -
                 pv1.dot(self.pbm.diff_corr.odes(0.0, self.sol[6])[3:6])) * self.pbm.orbit1.T,
                (- fac.dot(np.linalg.solve(stm2[0:3, 3:6], self.sol[7][3:6])) +
                 pv2.dot(self.pbm.diff_corr.odes(x_vec[0], self.sol[7])[3:6])) * self.pbm.orbit2.T]
