# pylint: disable=too-few-public-methods
"""
Propagator classes to integrate the equations of motion in the Circular Restricted Three-Body
Problem framework.

@author: Alberto FOSSA'
"""

import numpy as np

import sempy.core.init.defaults as dft
from sempy.core.init.constants import STM0
from sempy.core.propagation.propagator import Propagator

try:
    from sempy.core.dynamics.libs.cr3bp_dynamics import eqm_6_synodic, eqm_42_synodic, \
        eqm_6_synodic_arclength, eqm_42_synodic_arclength
except ImportError:
    try:
        from sempy.core.dynamics.cr3bp_dynamics_jit import eqm_6_synodic, eqm_42_synodic, \
            eqm_6_synodic_arclength, eqm_42_synodic_arclength
    except ImportError:
        print('Numba module not found!')
        from sempy.core.dynamics.cr3bp_dynamics import eqm_6_synodic, eqm_42_synodic, \
            eqm_6_synodic_arclength, eqm_42_synodic_arclength


class Cr3bpSynodicPropagator(Propagator):
    """Propagates the equations of motion of the Circular Restricted Three-Body Problem
    in a synodic reference frame.

    `Cr3bpSynodicPropagator` implements methods to easily interface with the Scipy `solve_ivp()`
    and `odeint()` integration routines and it allows to propagate both 6-dimensional and
    42-dimensional equations of motions for the Circular Restricted Three-Body Problem written as
    a set of first-order Ordinary Differential Equations expressed in the synodic frame.
    If 6-dimensional equations are selected, only the state composed by the 3 components of the
    position and the 3 components of the velocity vector is propagated over the given time span.
    If 42-dimensional equations are chosen, the state concatenated with the 36 components of the
    State Transition Matrix (STM) are propagated instead.

    Parameters
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    with_stm : bool, optional
        ``False`` to propagate the 6-dimensional state only, ``True`` to propagate the
        6-dimensional state concatenated with the 36-dimensional STM. Default is ``False``.
    method : str, optional
        Integration method to use. Default defined in `src.init.defaults`.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    with_stm : bool
        ``False`` to propagate the 6-dimensional state only, ``True`` to propagate the
        6-dimensional state concatenated with the 36-dimensional STM.
    stm0 : ndarray or None
        Flattened STM0.
    odes : function
        Equations of motions to be integrated, 6-dim or 42-dim (state and STM).
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float, optional
        Absolute tolerance.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    """

    def __init__(self, mu_cr3bp, with_stm=False, method=dft.method, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits Cr3bpSynodicPropagator. """

        Propagator.__init__(self, method=method, rtol=rtol, atol=atol, time_steps=time_steps,
                            max_internal_steps=max_internal_steps)

        if with_stm:  # propagate 6-dim state and 36-dim STM
            self.odes = lambda t, state: eqm_42_synodic(t, state, mu_cr3bp)
            self.stm0 = STM0.flatten()
        else:  # propagate only 6-dim state
            self.odes = lambda t, state: eqm_6_synodic(t, state, mu_cr3bp)
            self.stm0 = None
        self.with_stm = with_stm

    def propagate(self, t_span, state0, events=None):
        """Integrate the chosen set of ODEs.

        Parameters
        ----------
        t_span : iterable
            Time interval over which the integration is performed.
            If `Cr3bpSynodicPropagator.time_steps` is ``None`` and `t_span` is a two elements
            iterable, i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `Cr3bpSynodicPropagator.time_steps` is a positive integer and `t_span` is a two
            elements iterable, i.e. ``[t0, tf]``, the solution will be returned in a uniformly
            spaced time grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.
            If 6-dimensional equations are selected, `state0` must be a 6-dimensional array of
            initial conditions.
            If 42-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) before propagation.
            If 42-dimensional equations are selected and `state0` is 42-dimensional, an
            initial state already concatenated with the initial STM is assumed and set as initial
            condition for the propagation algorithm.
        events : function or iterable
            Event or events to track.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
        t_event : iterable or None
            List of times corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.
        state_event : iterable or None
            List of states corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.

        """

        if self.with_stm and len(state0) == 6:  # 42-dim eqn selected, no STM provided
            state0 = np.concatenate((state0, self.stm0))

        return Propagator.propagate(self, t_span, state0, events)


class Cr3bpArcLengthPropagator(Propagator):
    """Propagates the equations of motion of the Circular Restricted Three-Body Problem
    in a synodic reference frame.

    `Cr3bpArcLengthPropagator` implements methods to easily interface with the Scipy `solve_ivp()`
    and `odeint()` integration routines and it allows to propagate both 6-dimensional (state only)
    and 42-dimensional (state and STM) equations of motions for the Circular Restricted Three-Body
    Problem written as a set of first-order Ordinary Differential Equations expressed in the
    synodic frame. The orbit arclength is also integrated together with the selected EOMs leading
    to a 7-dimensional or a 43-dimensional augmented state depending on the previous choice.

    Parameters
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    with_stm : bool, optional
        ``False`` to propagate the 6-dimensional state concatenated with the orbit arclength,
        ``True`` to propagate the 6-dimensional state concatenated with the 36-dimensional STM
        and the orbit arclength. Default is ``False``.
    method : str, optional
        Integration method to use. Default defined in `src.init.defaults`.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    with_stm : bool
        ``False`` to propagate the 6-dimensional state concatenated with the orbit arclength,
        ``True`` to propagate the 6-dimensional state concatenated with the 36-dimensional STM
        and the orbit arclength.
    stm0 : ndarray or None
        Flattened STM0.
    odes : function
        Equations of motions to be integrated, 7-dim (state and arclength) or 43-dim
        (state, STM and arclength).
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float, optional
        Absolute tolerance.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    """

    def __init__(self, mu_cr3bp, with_stm=False, method=dft.method, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits Cr3bpArcLengthPropagator. """

        Propagator.__init__(self, method=method, rtol=rtol, atol=atol, time_steps=time_steps,
                            max_internal_steps=max_internal_steps)

        if with_stm:
            self.odes = lambda t, state: eqm_42_synodic_arclength(t, state, mu_cr3bp)
            self.stm0 = STM0.flatten()
        else:
            self.odes = lambda t, state: eqm_6_synodic_arclength(t, state, mu_cr3bp)
            self.stm0 = None
        self.with_stm = with_stm

    def propagate(self, t_span, state0, events=None):
        """Integrates the chosen set of ODEs.

        Parameters
        ----------
        t_span : iterable
            Time interval over which the integration is performed.
            If `Cr3bpArcLengthPropagator.time_steps` is ``None`` and `t_span` is a two elements
            iterable, i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `Cr3bpArcLengthPropagator.time_steps` is a positive integer and `t_span` is a two
            elements iterable, i.e. ``[t0, tf]``, the solution will be returned in a uniformly
            spaced time grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.
            If 7-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with an initial orbit arclength equal to zero before propagation.
            If 7-dimensional equations are selected and `state0` is 7-dimensional, an initial
            orbit arclength equal to `state0[-1]` is assumed.
            If 43-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) and an initial orbit
            arclength equal to zero before propagation.
            If 43-dimensional equations are selected and `state0` is 7-dimensional, an initial
            orbit arclength equal to `state0[-1]' is assumed. The provided state is then split and
            concatenated with the default initial STM before propagation.
            If 43-dimensional equations are selected and `state0` is 42-dimensional, an
            initial state already concatenated with the initial STM is assumed and concatenated
            with an initial orbit arclength equal to zero before propagation.
            If 43-dimensional equations are selected and `state0` is 43-dimensional, an initial
            state already concatenated with the initial STM and orbit arclength is assumed.
        events : function or iterable
            Event or events to track.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
        t_event : iterable or None
            List of times corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.
        state_event : iterable or None
            List of states corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.

        """

        if self.with_stm:  # propagate 6-dim state, 36-dim STM and arclength
            if len(state0) == 6:  # 6-dim initial state provided
                state0 = np.concatenate((state0, self.stm0, [0.0]))
            elif len(state0) == 7:  # 6-dim initial state and initial arclength provided
                state0 = np.concatenate((state0[0:6], self.stm0, state0[-1]))
            elif len(state0) == 42:  # 6-dim initial state and 36-dim initial STM provided
                state0 = np.concatenate((state0, [0.0]))
        elif len(state0) == 6:  # propagate 6-dim state and arclength, 6-dim initial state provided
            state0 = np.concatenate((state0, [0.0]))

        return Propagator.propagate(self, t_span, state0, events)
