"""
Test Ephemeris and EphemerisSH Propagators.

@author: Alberto FOSSA'
"""

import unittest
import os.path
import numpy as np
import spiceypy as sp
from scipy.io import loadmat

from sempy.core.init.load_kernels import load_kernels, KERNELS_ROOT
from sempy.core.init.constants import MOON_SMA, MOON_OMEGA_MEAN
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.ephemeris import Ephemeris
from sempy.core.propagation.ephemeris_propagator import EphemerisPropagator, EphemerisSHPropagator
from sempy.core.dynamics.tests.test_ephemeris_sh_dynamics import dirname as dyna_dirname

DIRNAME = os.path.dirname(__file__)
load_kernels()
sp.furnsh(os.path.join(KERNELS_ROOT, 'pck/moon_pa_de421_1900-2050.bpc'))
sp.furnsh(os.path.join(KERNELS_ROOT, 'fk/moon_080317.tf'))


class TestEphemerisPropagator(unittest.TestCase):

    def test_ephemeris_propagator(self):
        data_mat = loadmat(os.path.join(DIRNAME, 'data', 'test_ephemeris_propagator.mat'),
                           squeeze_me=True, struct_as_record=False)['data']
        eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
        t_c = 1.0 / MOON_OMEGA_MEAN
        max_steps = 4_000_000

        # 6-dim equations, non-dimensional
        prop6_adim = EphemerisPropagator(eph, t_c=t_c, l_c=MOON_SMA, time_steps=None,
                                         max_internal_steps=max_steps)
        tf6_adim, yf6_adim, _, _ = prop6_adim.propagate(data_mat.t_span[0, :], data_mat.y0[0, :])

        # 42-dim equations, non-dimensional
        prop42_adim = EphemerisPropagator(eph, t_c=t_c, l_c=MOON_SMA, with_stm=True,
                                          time_steps=None, max_internal_steps=max_steps)
        tf42_adim, yf42_adim, _, _ = prop42_adim.propagate(data_mat.t_span[0, :], data_mat.y0[0, :])

        # 48-dim equations, non-dimensional
        prop48_adim = EphemerisPropagator(eph, t_c=t_c, l_c=MOON_SMA, with_epoch_partials=True,
                                          time_steps=None, max_internal_steps=max_steps)
        tf48_adim, yf48_adim, _, _ = prop48_adim.propagate(data_mat.t_span[0, :], data_mat.y0[0, :])

        # 6-dim equations, dimensional [km, km/s]
        prop6_dim = EphemerisPropagator(eph, time_steps=None, max_internal_steps=max_steps)
        tf6_dim, yf6_dim, _, _ = prop6_dim.propagate(data_mat.t_span[1, :], data_mat.y0[1, :])

        # 42-dim equations, dimensional [km, km/s]
        prop42_dim = EphemerisPropagator(eph, with_stm=True, time_steps=None,
                                         max_internal_steps=max_steps, rtol=3e-14, atol=1e-13)
        tf42_dim, yf42_dim, _, _ = prop42_dim.propagate(data_mat.t_span[1, :], data_mat.y0[1, :])

        # 48-dim equations, dimensional [km, km/s]
        prop48_dim = EphemerisPropagator(eph, with_epoch_partials=True, time_steps=None,
                                         max_internal_steps=max_steps, rtol=3e-14, atol=1e-13)
        tf48_dim, yf48_dim, _, _ = prop48_dim.propagate(data_mat.t_span[1, :], data_mat.y0[1, :])

        np.testing.assert_allclose(yf6_adim[-1, :], data_mat.yf6[0, :], rtol=0.0, atol=1e-11)
        np.testing.assert_allclose(yf42_adim[-1, :], data_mat.yf42[0, :], rtol=0.0, atol=1e-11)
        np.testing.assert_allclose(yf48_adim[-1, :], data_mat.yf48[0, :], rtol=0.0, atol=1e-11)
        np.testing.assert_allclose(yf6_dim[-1, :], data_mat.yf6[1, :], rtol=0.0, atol=1e-6)
        np.testing.assert_allclose(yf42_dim[-1, :], data_mat.yf42[1, :], rtol=0.0, atol=2e-6)
        np.testing.assert_allclose(yf48_dim[-1, :], data_mat.yf48[1, :], rtol=0.0, atol=2e-6)

        # test with GMAT data, integration is performed in dimensional units of km and s
        y0_gmat = np.array([-203261.3784021969, -338246.0409067131, -129905.6591475186,
                            1.025839533673108, -0.5513935055210328, -0.4809128853776429])
        yf_gmat = np.array([75876.59104060699, -387548.667964136, -205966.1069506892,
                            1.055937995015543, 0.139545498853069, -0.09606399564054512])
        epoch0_utc = '2020 JUN 04 10:12:38.390 UTC'
        et0 = sp.str2et(epoch0_utc)

        tv, yv, _, _ = prop6_dim.propagate([et0, et0 + 259200.0], y0_gmat)
        np.testing.assert_allclose(yv[-1, :], yf_gmat, rtol=0.0, atol=1e-4)

    def test_ephemeris_sh_propagator(self):
        data = loadmat(os.path.join(dyna_dirname, 'data', 'test_sh_dyna_prop.mat'),
                       squeeze_me=True, struct_as_record=False)['out']
        state0 = np.ascontiguousarray(data.y0[:, 0:6])
        t_span = np.ascontiguousarray(data.t_span)

        eph = Ephemeris(Primary.MOON, moon=('GRGM0050', 50, 50))
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        l_c = cr3bp.L
        t_c = cr3bp.T / 2.0 / np.pi

        prop6 = EphemerisSHPropagator(eph, t_c=t_c, l_c=l_c, with_stm=False, time_steps=None,
                                      max_internal_steps=4_000_000)
        prop42 = EphemerisSHPropagator(eph, t_c=t_c, l_c=l_c, with_stm=True, time_steps=None,
                                       max_internal_steps=4_000_000)

        state6_end = np.zeros(state0.shape)
        state42_end = np.zeros((data.t0.size, 42))

        for i in range(data.t0.size):
            _, sv6, _, _ = prop6.propagate(t_span[i], state0[i])
            state6_end[i, :] = sv6[-1, :]
            _, sv42, _, _ = prop42.propagate(t_span[i], state0[i])
            state42_end[i, :] = sv42[-1, :]

        np.testing.assert_allclose(state6_end, data.yf[:, 0:6], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(state42_end, data.yf, rtol=0.0, atol=1e-8)


if __name__ == '__main__':
    unittest.main()
