# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 17:22:02 2019

@author: Edgar PEREZ, Alberto FOSSA'
"""

import unittest
import os.path
from scipy.io import loadmat

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator

DIRNAME = os.path.dirname(__file__)


class TestCr3bpPropagator(unittest.TestCase):

    def test_cr3bp_propagator(self):

        # data obtained in SEMAT for an L1 Halo orbit with Az=15000km
        filename = os.path.join(DIRNAME, 'data', 'cr3bp_propagator.mat')
        d = loadmat(filename)
        state0 = d['y0'].flatten()
        state_end = d['yf'].flatten()
        tf = d['T'][0][0]

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # propagation of state only (6-dim)
        prop6 = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=None)

        # odeint
        t_vec, state_vec, t_event, state_event = prop6.propagate([0.0, tf], state0[0:6])
        for i in range(5):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)

        # solve_ivp
        def event(t, state):
            """Fake event to trigger `solve_ivp`. """
            return 1

        event.terminal = False

        t_vec, state_vec, t_event, state_event = prop6.propagate([0.0, tf], state0[0:6],
                                                                 events=event)
        for i in range(5):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)

        # propagation of state and STM (42-dim)
        prop42 = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=None, with_stm=True)

        # odeint
        t_vec, state_vec, t_event, state_event = prop42.propagate([0.0, tf], state0)
        for i in range(41):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)

        # solve_ivp
        t_vec, state_vec, t_event, state_event = prop42.propagate([0.0, tf], state0,
                                                                  events=event)
        for i in range(41):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)


if __name__ == '__main__':
    unittest.main()
