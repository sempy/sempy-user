"""
Test the event detection feature of the Propagator class.

@author: Alberto FOSSA'
"""

import unittest
import numpy as np

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.diffcorr.ode_event import generate_plane_event


class TestEventDetection(unittest.TestCase):

    def test_terminal_events_fwd(self):
        """Test terminal event detection for forward propagation. """

        cr3bp, halo = self.get_env_orbit()
        prop = Cr3bpSynodicPropagator(cr3bp.mu)

        xy_crossing = generate_plane_event('xy', 1, True)
        xz_crossing = generate_plane_event('xz', 0, False)
        yz_crossing = generate_plane_event('yz', 0, False)

        events = [xy_crossing, xz_crossing, yz_crossing]
        t_vec, state_vec, t_event, state_event = \
            prop.propagate([0.0, 2 * halo.T], halo.state0, events=events)

        self.assertEqual(t_event[0].size, 1)
        self.assertEqual(t_event[1].size, 2)
        self.assertEqual(t_event[2].size, 0)

        np.testing.assert_equal(t_vec[-1], t_event[0][-1])
        np.testing.assert_equal(state_vec[-1], state_event[0][-1])

    def test_terminal_events_bwd(self):
        """Test terminal event detection for backward propagation. """

        cr3bp, halo = self.get_env_orbit()
        prop = Cr3bpSynodicPropagator(cr3bp.mu)

        xy_crossing = generate_plane_event('xy', 0, False)
        xz_crossing = generate_plane_event('xz', -1, True)
        yz_crossing = generate_plane_event('yz', 0, False)

        events = [yz_crossing, xy_crossing, xz_crossing]
        t_vec, state_vec, t_event, state_event = \
            prop.propagate([0.0, -halo.T], halo.state0, events=events)

        self.assertEqual(t_event[0].size, 0)
        self.assertEqual(t_event[1].size, 1)
        self.assertEqual(t_event[2].size, 1)

        np.testing.assert_equal(t_vec[-1], t_event[2][-1])
        np.testing.assert_equal(state_vec[-1], state_event[2][-1])

    @staticmethod
    def get_env_orbit():
        """Get the CR3BP system and the orbit used to perform all tests. """
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        halo = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Azdim=30e3)
        halo.interpolation()
        return cr3bp, halo


if __name__ == '__main__':
    unittest.main()
