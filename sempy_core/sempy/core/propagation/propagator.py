# pylint: disable=too-few-public-methods
"""
Propagator class and sub-classes to integrate the equations of motion in different force models
and reference frames.

@author: Alberto FOSSA'
"""

import numpy as np
import scipy.integrate as sci

import sempy.core.init.defaults as dft


class Propagator:
    """Parent class to provide a common interface for all specialized child classes.

    It allows to preset different propagator settings such as integration method, relative and
    absolute tolerances.

    `Propagator` class takes advantage of the Scipy `solve_ivp()` and `odeint()` method to
    numerically propagate a given set of Ordinary Differential Equations (ODEs) from user-selected
    initial conditions and time span. Within each sub-class, an automatic selection between the two
    methods is performed based on the user defined input parameters. If no events have to be
    tracked, the more performant `odeint()` method is employed to guarantee a fast integration of
    the initial condition over the whole time span. If a specific event is sought, `solve_ivp()`
    is used instead since it provides root finding algorithms to accurately determine the time
    instant and corresponding state at which the event occurs.

    Parameters
    ----------
    method : str, optional
        Integration method to use. Default defined in `src.init.defaults`.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float
        Absolute tolerance.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    """

    def __init__(self, method=dft.method, rtol=dft.rtol, atol=dft.atol, time_steps=dft.time_steps,
                 max_internal_steps=dft.max_internal_steps):
        """Inits Propagator class. """

        self.method = method
        self.rtol = rtol
        self.atol = atol
        self.time_steps = time_steps
        self.max_internal_steps = max_internal_steps
        self.odes = None

    def propagate(self, t_span, state0, events=None):
        """Integrate the chosen set of ODEs.

        Parameters
        ----------
        t_span : iterable
            Time interval over which the integration is performed.
            If `Propagator.time_steps` is ``None`` and `t_span` is a two elements iterable,
            i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `Propagator.time_steps` is a positive integer and `t_span` is a two elements
            iterable, i.e. ``[t0, tf]``, the solution will be returned in a uniformly spaced time
            grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.
        events : function or iterable
            Event or events to track.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
        t_event : iterable or None
            List of times corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.
        state_event : iterable or None
            List of states corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.

        """

        if events is None:  # no events to track, use odeint
            # solution only at the time interval boundaries or on user-provided time grid
            if self.time_steps is None or len(t_span) > 2:
                state_vec = sci.odeint(self.odes, state0, t_span, rtol=self.rtol, atol=self.atol,
                                       tfirst=True, mxstep=self.max_internal_steps, mxhnil=1)
            else:  # solution on uniformly spaced time grid with predefined number of points
                t_span = np.linspace(t_span[0], t_span[-1], self.time_steps)
                state_vec = sci.odeint(self.odes, state0, t_span, rtol=self.rtol,
                                       atol=self.atol, tfirst=True, mxhnil=1,
                                       mxstep=self.max_internal_steps // self.time_steps)
            return np.asarray(t_span), state_vec, None, None

        # events to be tracked, use solve_ivp
        if len(t_span) > 2:  # solution on user-provided time grid
            sol = sci.solve_ivp(self.odes, [t_span[0], t_span[-1]], state0, method=self.method,
                                t_eval=t_span, events=events, rtol=self.rtol, atol=self.atol)
        elif self.time_steps is None:  # solution only at time interval boundaries
            sol = sci.solve_ivp(self.odes, t_span, state0, method=self.method,
                                t_eval=t_span, events=events, rtol=self.rtol, atol=self.atol)
        else:  # solution on uniformly spaced time grid with predefined number of points
            sol = sci.solve_ivp(self.odes, t_span, state0, method=self.method,
                                t_eval=np.linspace(t_span[0], t_span[-1], self.time_steps),
                                events=events, rtol=self.rtol, atol=self.atol)
        t_vec, state_vec, t_event, state_event = sol.t, sol.y.T, sol.t_events, sol.y_events

        # look for terminal events outside the time vector
        t_evt_all = np.concatenate(t_event)  # time vector for all event occurrences
        if t_span[0] < t_span[-1] and t_evt_all.max(initial=t_span[0]) > t_vec[-1]:
            idx_cum = t_evt_all.argmax()  # index of the stopping event for forward propagation
        elif t_span[0] > t_span[-1] and t_evt_all.min(initial=t_span[0]) < t_vec[-1]:
            idx_cum = t_evt_all.argmin()  # index of the stopping event for backward propagation
        else:  # no terminal events outside the time vector
            return t_vec, state_vec, t_event, state_event

        # append the stopping event to both time and state vectors
        idx_evt = np.nonzero(np.asarray([i.size for i in t_event]).cumsum() == (idx_cum + 1))[0][0]
        t_vec = np.concatenate((t_vec, t_event[idx_evt][-1:]))
        state_vec = np.concatenate((state_vec, state_event[idx_evt][-1:]))
        return t_vec, state_vec, t_event, state_event
