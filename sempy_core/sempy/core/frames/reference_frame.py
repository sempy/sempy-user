# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 16:23:09 2019

@author: Edgar PEREZ
"""

class ReferenceFrame:
    pass

class Rotating(ReferenceFrame):
    pass

class Inertial(ReferenceFrame):
    pass

class Synodic(Rotating):
    pass