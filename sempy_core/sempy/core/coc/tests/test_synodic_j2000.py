"""
Unit-tests for change of coordinates in `synodic_j2000.py`.

@author: Alberto FOSSA'
"""

import unittest
import numpy as np
from os import path
from scipy.io import loadmat
from spiceypy import str2et

from sempy.core.init.load_kernels import load_kernels
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from sempy.core.coc.translation_j2000 import translation_j2000

load_kernels()

class TestSynodicJ2000(unittest.TestCase):

    def test_synodic_j2000(self):
        dirname = path.dirname(__file__)
        data_mat = loadmat(path.join(dirname, 'data', 'test_syn_j2000.mat'), squeeze_me=True,
                           struct_as_record=False)

        # data obtained in SEMAT for a L2 northern NRHO with periselene radius of 6000 km
        data_syn = data_mat['syn']
        data_j2000 = data_mat['j2000']

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        l_c = cr3bp.L
        t_c = cr3bp.T/2/np.pi

        y_j2000_adim = data_j2000.y1.T / l_c
        y_j2000_adim[:, 3:6] *= t_c

        et0 = str2et('2020 JUN 01 12:00:00.000')

        t_j2000_dim, state_j2000_dim = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m1)
        t_j2000_dim_bary, state_j2000_dim_bary = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m1, bary_from_spice=True)
        t_j2000_adim, state_j2000_adim = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m1, adim=True)
        t_j2000_adim_bary, state_j2000_adim_bary = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m1,
                             bary_from_spice=True, adim=True)

        np.testing.assert_allclose(t_j2000_dim, data_j2000.t1, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_j2000_dim_bary, data_j2000.t1, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_j2000_adim, data_j2000.t1 / t_c, rtol=0.0, atol=1e-12)
        np.testing.assert_allclose(t_j2000_adim_bary, data_j2000.t1 / t_c, rtol=0.0, atol=1e-12)

        np.testing.assert_allclose(state_j2000_dim, data_j2000.y1.T, rtol=0.0, atol=1e-3)
        np.testing.assert_allclose(state_j2000_dim_bary, data_j2000.y1.T, rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(state_j2000_adim, y_j2000_adim, rtol=0.0, atol=1e-3)
        np.testing.assert_allclose(state_j2000_adim_bary, y_j2000_adim, rtol=0.0, atol=1e-14)

        t_syn_from_dim, state_syn_from_dim = \
            j2000_to_synodic(data_j2000.t1, data_j2000.y1.T, et0, cr3bp, cr3bp.m1)
        t_syn_from_dim_bary, state_syn_from_dim_bary = \
            j2000_to_synodic(data_j2000.t1, data_j2000.y1.T, et0, cr3bp, cr3bp.m1,
                             bary_from_spice=True)
        t_syn_from_adim, state_syn_from_adim = \
            j2000_to_synodic(data_j2000.t1 / t_c, y_j2000_adim, et0, cr3bp, cr3bp.m1, adim=True)
        t_syn_from_adim_bary, state_syn_from_adim_bary = \
            j2000_to_synodic(data_j2000.t1 / t_c, y_j2000_adim, et0, cr3bp, cr3bp.m1,
                             bary_from_spice=True, adim=True)

        np.testing.assert_allclose(t_syn_from_dim, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_dim_bary, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_adim, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_adim_bary, data_syn.t2, rtol=0.0, atol=0.0)

        np.testing.assert_allclose(state_syn_from_dim, data_syn.y2.T, rtol=0.0, atol=1e-3)
        np.testing.assert_allclose(state_syn_from_dim_bary, data_syn.y2.T, rtol=0.0, atol=1e-14)
        np.testing.assert_allclose(state_syn_from_adim, data_syn.y2.T, rtol=0.0, atol=1e-3)
        np.testing.assert_allclose(state_syn_from_adim_bary, data_syn.y2.T, rtol=0.0, atol=1e-14)

        # translation
        t_lci_dim, state_lci_dim = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m2)
        t_lci_dim_bary, state_lci_dim_bary = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m2, bary_from_spice=True)
        t_lci_adim, state_lci_adim = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m2, adim=True)
        t_lci_adim_bary, state_lci_adim_bary = \
            synodic_to_j2000(data_syn.t1, data_syn.y1.T, et0, cr3bp, cr3bp.m2,
                             bary_from_spice=True, adim=True)
        t_lci_dim2, state_lci_dim2 = \
            translation_j2000(t_j2000_dim, state_j2000_dim, Primary.EARTH, Primary.MOON)
        t_lci_dim_bary2, state_lci_dim_bary2 = \
            translation_j2000(t_j2000_dim_bary, state_j2000_dim_bary, Primary.EARTH, Primary.MOON)
        t_lci_adim2, state_lci_adim2 = \
            translation_j2000(t_j2000_adim, state_j2000_adim, Primary.EARTH, Primary.MOON,
                              t_c=t_c, l_c=l_c)
        t_lci_adim_bary2, state_lci_adim_bary2 = \
            translation_j2000(t_j2000_adim_bary, state_j2000_adim_bary, Primary.EARTH, Primary.MOON,
                              t_c=t_c, l_c=l_c)

        np.testing.assert_allclose(t_lci_dim, t_lci_dim2, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(t_lci_dim_bary, t_lci_dim_bary2, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(t_lci_adim, t_lci_adim2, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(t_lci_adim_bary, t_lci_adim_bary2, rtol=0.0, atol=1e-16)

        np.testing.assert_allclose(state_lci_dim, state_lci_dim2, rtol=0.0, atol=1e-1)
        np.testing.assert_allclose(state_lci_dim_bary, state_lci_dim_bary2, rtol=0.0, atol=1e-15)
        np.testing.assert_allclose(state_lci_adim, state_lci_adim2, rtol=0.0, atol=1e-1)
        np.testing.assert_allclose(state_lci_adim_bary, state_lci_adim_bary2, rtol=0.0, atol=1e-15)

        _, y_lci_dim = translation_j2000(data_j2000.t1, data_j2000.y1.T,
                                         Primary.EARTH, Primary.MOON)
        _, y_lci_adim = translation_j2000(data_j2000.t1 / t_c, y_j2000_adim,
                                          Primary.EARTH, Primary.MOON, t_c=t_c, l_c=l_c)

        t_syn_from_lci_dim, state_syn_from_lci_dim = \
            j2000_to_synodic(data_j2000.t1, y_lci_dim, et0, cr3bp, cr3bp.m2)
        t_syn_from_lci_dim_bary, state_syn_from_lci_dim_bary = \
            j2000_to_synodic(data_j2000.t1, y_lci_dim, et0, cr3bp, cr3bp.m2,
                             bary_from_spice=True)
        t_syn_from_lci_adim, state_syn_from_lci_adim = \
            j2000_to_synodic(data_j2000.t1 / t_c, y_lci_adim, et0, cr3bp, cr3bp.m2, adim=True)
        t_syn_from_lci_adim_bary, state_syn_from_lci_adim_bary = \
            j2000_to_synodic(data_j2000.t1 / t_c, y_lci_adim, et0, cr3bp, cr3bp.m2,
                             bary_from_spice=True, adim=True)

        np.testing.assert_allclose(t_syn_from_lci_dim, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_lci_dim_bary, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_lci_adim, data_syn.t2, rtol=0.0, atol=0.0)
        np.testing.assert_allclose(t_syn_from_lci_adim_bary, data_syn.t2, rtol=0.0, atol=0.0)

        np.testing.assert_allclose(state_syn_from_lci_dim, data_syn.y2.T, rtol=0.0, atol=1e-1)
        np.testing.assert_allclose(state_syn_from_lci_dim_bary, data_syn.y2.T,
                                   rtol=0.0, atol=2e-15)
        np.testing.assert_allclose(state_syn_from_lci_adim, data_syn.y2.T, rtol=0.0, atol=1e-1)
        np.testing.assert_allclose(state_syn_from_lci_adim_bary, data_syn.y2.T,
                                   rtol=0.0, atol=2e-15)


if __name__ == '__main__':
    unittest.main()
