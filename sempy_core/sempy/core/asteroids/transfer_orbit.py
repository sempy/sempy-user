# -*- coding: utf-8 -*-
"""
@author: Irina Kovalenko
"""

import datetime
import numpy
import pykep as pk
from numpy.linalg import norm

import sempy.core.init.constants as cst
from sempy.core.asteroids.ephemerides import get_state_vector


class TransferOrbit:
    '''TransferOrbit provides optimal trajecroty between asteroids and planets.
    The class computes hyperbolic excess velocity vectors at departure and
    arrival for a given departure date and time of flight between two bodies
    (planets, asteroids). The solution is the Lambert's problem solution,
    optimised among all solutions for maximum number of revolutions < 2,
    for both clockwise and counter-clock wise arcs.

    Parameters
    ----------
    object_1 : str
        planet, or natural satellite name, case-sensitive asteroid name
        (e.g. '2017 AP4'), or number, designation, MPC packed designation, or
        (SPK ID-2000000) number
    object_2 : str
        planet, or natural satellite name, case-sensitive asteroid name
        (e.g. '2017 AP4'), or number, designation, MPC packed designation, or
        (SPK ID-2000000) number
    departure : str
        Departure date, e.g. '2020-01-01'
    tof : float
        Time of Flight [days]
    idtype_1 : str
        Type of object_1, allowed values are 'asteroid' or 'planet'. Default is
        'planet'.
    idtype_2 : str
        Type of object_2, allowed values are 'asteroid' or 'planet'. Default is
        'asteroid'.



    Attributes
    ----------
    object_1 : str
        asteroid or planet name, number, designation, MPC packed designation,
        or SPK ID number
    object_2 : str
        asteroid or planet name, number, designation, MPC packed designation,
        or SPK ID number
    departure : str
        Departure date.
    tof : float
        Mission duration [days]
    arrival : str
        Arrival date.
    v_inf0 : ~numpy.array
        delta-V at departure (hyperbolic excess velocity, which is the vector
        difference between Earth's velocity and the heliocentric orbit's
        velocity).
    v_inf1 : ~numpy.array
        delta-V at arrival (required for rendezvous with the asteroid,
        calculated from the vector difference between the spacecraft and
        asteroid's velocity vectors).
    nb_orbits : int
        Number of compleet orbits.
    sc_state_vec_dep : ~numpy.array
        State vector of the spacecraft at the departure : [x, y, z, vx, vy, vz]
        in Ecliptic J2000 w.r.t Sun.
    sc_state_vec_arr : ~numpy.array
        State vector of the spacecraft at the arrival : [x, y, z, vx, vy, vz]
        in Ecliptic J2000 w.r.t Sun.



    '''

    def __init__(self, object_1, object_2, departure, tof):
        """Inits Oneway_mission class."""
        self.object_1 = object_1
        self.object_2 = object_2
        self.departure = datetime.date.fromisoformat(departure)
        self.tof = tof
        self.arrival = self.departure + datetime.timedelta(days=self.tof)
        self.sc_state_vec_arr = list()
        self.sc_state_vec_dep = list()
        self.v_inf0 = list()
        self.v_inf1 = list()
        self.nb_orbits = 2

    def get_solution(self):
        """Function returns optimal solution of the Lambert's problem,
        calculated for all solutions for maximum number of revolutions < 2,
        for both clockwise and counter-clock wise arcs."""

        state_vec0 = get_state_vector(self.object_1, str(self.departure)) 
        # departure state vector
        r_0 = numpy.array(state_vec0[0:3])    # departure object position
        v_0 = numpy.array(state_vec0[3:6])    # departure object velocity

        arrival = self.arrival
        state_vec1 = get_state_vector(self.object_2, str(arrival))  
        # arrival state vector
        r_1 = numpy.array(state_vec1[0:3])    # arrival object position
        v_1 = numpy.array(state_vec1[3:6])    # arrival object velocity

        tof_s = self.tof * 24 * 60 * 60  # transform to seconds
        # Solve the Lambert problem
        lam_solution = pk.lambert_problem(
            r1=r_0,
            r2=r_1,
            tof=tof_s,
            mu=cst.SUN.GM,
            cw=False,
            max_revs=1)  # Lambert's problem prograde solutions
        v_dep_1 = list(lam_solution.get_v1())
        v_arr_1 = list(lam_solution.get_v2())
        lam_solution_ret = pk.lambert_problem(
            r1=r_0,
            r2=r_1,
            tof=tof_s,
            mu=cst.SUN.GM,
            cw=True,
            max_revs=1)  # + retrograde solutions
        v_dep_2 = list(lam_solution_ret.get_v1())
        v_arr_2 = list(lam_solution_ret.get_v2())

        v_dep = v_dep_1 + v_dep_2  # departure velosity, all solutions
        v_arr = v_arr_1 + v_arr_2  # arrival velosity, all solutions

        nb_orbit_list = [0, 1, 1]
        nb_orbit = nb_orbit_list[0: (2 * lam_solution.get_Nmax() + 1)] + \
            nb_orbit_list[0: (2 * lam_solution_ret.get_Nmax() + 1)]

        # Which is the best solution
        dv_new = numpy.inf
        nb_orbits = 0
        for k in range(len(v_dep)):
            dv_temp = norm(v_dep[k] - v_0) + norm(v_arr[k] - v_1)
            if dv_temp < dv_new:
                dv_new = dv_temp
                nb_orbits = nb_orbit[k]
                v_inf0 = numpy.array(v_dep[k]) - v_0
                v_inf1 = numpy.array(v_arr[k]) - v_1
                v_dep = numpy.array(v_dep[k])
                v_arr = numpy.array(v_arr[k])

        self.sc_state_vec_dep = numpy.concatenate((r_0, v_dep))
        self.sc_state_vec_arr = numpy.concatenate((r_1, v_arr))

        self.arrival = str(arrival)
        self.v_inf0 = v_inf0
        self.v_inf1 = v_inf1
        self.nb_orbits = nb_orbits
