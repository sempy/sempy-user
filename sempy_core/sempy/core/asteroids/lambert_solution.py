#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 10:55:01 2020

@author: Irina Kovalenko
"""

import pykep as pk
import datetime
import numpy
from numpy.linalg import norm
import sempy.core.init.constants as cst


class LambertSolution:
    '''Lambert_for_parallel is used for asteroid_rdv_mission_analysys. Lambert
    solver for maximum number of revolutions < 2,
    for both clockwise and counter-clock wise solutions,
    and returns the optimal solution.

    Parameters
    ----------
    r1: ~numpy.array
        Initial position vector
    r2: ~numpy.array
        Final position vector
    departure : datetime.date
        Departure date
    tof: float
        Time of flight between both positions


    Attributes
    ----------
    departure : str
        Departure date.
    tof : float
        Mission duration limit [days]
    arrival : str
        Arrival date.
    v_inf0 : ~numpy.array
        delta-V at departure (hyperbolic excess velocity, which is the vector
        difference between Earth's velocity and the heliocentric orbit's
        velocity).
    v_inf1 : ~numpy.array
        delta-V at arrival (required for rendezvous with the asteroid,
        calculated from the vector difference between the spacecraft
        and asteroid's velocity vectors).
    dv_tot : float
        Total delta-V.

    '''

    def __init__(self, state_vec0, state_vec1, departure, tof):
        """Inits Lambert_for_parallel class."""
        self.state_vec0 = state_vec0
        self.state_vec1 = state_vec1
        self.departure = departure
        self.tof = tof

        self.arrival, self.v_inf0, self.v_inf1, self.dv_tot,\
            self.nb_orbits = self.solution()

    def solution(self):
        """The function provides Lamberts's problem solution."""

        r_0 = numpy.array(self.state_vec0[0:3])    # departure object position
        v_0 = numpy.array(self.state_vec0[3:6])    # departure object velocity

        arrival = self.departure + datetime.timedelta(days=self.tof)

        r_1 = numpy.array(self.state_vec1[0:3])    # arrival object position
        v_1 = numpy.array(self.state_vec1[3:6])    # arrival object velocity

        tof_s = self.tof * 24 * 60 * 60  # transform to seconds

        # Solve the Lambert problem
        lam_solution = pk.lambert_problem(
            r1=r_0,
            r2=r_1,
            tof=tof_s,
            mu=cst.SUN.GM,
            cw=False,
            max_revs=1)  # Lambert's problem prograde solutions
        v_dep_1 = list(lam_solution.get_v1())
        v_arr_1 = list(lam_solution.get_v2())
        l_2 = pk.lambert_problem(
            r1=r_0,
            r2=r_1,
            tof=tof_s,
            mu=cst.SUN.GM,
            cw=True,
            max_revs=1)  # + retrograde solutions
        v_dep_2 = list(l_2.get_v1())
        v_arr_2 = list(l_2.get_v2())

        v_dep = v_dep_1 + v_dep_2  # departure velosity, all solutions
        v_arr = v_arr_1 + v_arr_2  # arrival velosity, all solutions

        n_orbit_list = [0, 1, 1]
        n_orbit = n_orbit_list[0: (2 * lam_solution.get_Nmax() + 1)] + \
            n_orbit_list[0: (2 * l_2.get_Nmax() + 1)]

        # Which is the best solution
        dv_new = numpy.inf
        nb_orbits = 0
        for k in range(len(v_dep)):
            dv_temp = norm(v_dep[k] - v_0) + norm(v_arr[k] - v_1)
            if dv_temp < dv_new:
                dv_new = dv_temp
                nb_orbits = n_orbit[k]
                v_inf0 = numpy.array(v_dep[k]) - v_0
                v_inf1 = numpy.array(v_arr[k]) - v_1
                dv_tot = dv_new

        return str(arrival), v_inf0, v_inf1, dv_tot, nb_orbits
