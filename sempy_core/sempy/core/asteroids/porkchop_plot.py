#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 15:24:44 2019

@author: Irina Kovalenko
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


def plot(data, title, dv_limit=10.0, levels=20, bar_label='$\Delta V$, km/s'):
    """Generates porkchop plot:
        plot of delta-V as a function of departure date and flight time.

    Parameters
    ----------
    data : numpy.ndarray
        Array of 3 columns: launch date (str, e.g. ['2020-01-01']),
        time of flight [days] and delta-v [km/s].
    title : str
        Plot's title.
    dv_limit : float, optional
        Cut colours if dv > dv_limit.
    levels : int, optional
        Level set
    bar_label : str, optional
        Vertical colour bar title.

    """
    launch_date = data[0, :]
    tof = data[1, :]
    dv_init = data[2, :]

    # Generate launch dates span [in MJD format] for x axis
    xlist = list(dict.fromkeys(launch_date))
    xlist = mdates.datestr2num(xlist)

    # Generate TOF span [days] for y axis
    ylist = list(dict.fromkeys(tof))
    ylist = np.asarray(ylist, dtype=float)

    # Generate delta-V matrix
    d_v = []
    k = 0
    for j in range(len(xlist)):
        column = []
        for i in range(len(ylist)):
            column.append(dv_init[k])
            k += 1
        d_v.append(column)

    x, y = np.meshgrid(xlist, ylist)
    z = np.array(d_v, dtype=float).T.tolist()
    fig, ax = plt.subplots(1, 1)

    dv_levels = np.linspace(0, dv_limit, levels)
    cp = ax.contourf(x, y, z, dv_levels)
    # cp = ax.pcolormesh(x, y, z)
    fig.colorbar(cp).set_label(bar_label)  # Add a colorbar to a plot
    ax.set_title(title)
    # Format the x axis
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d"))
    # rotate and align the tick labels so they look better
    fig.autofmt_xdate()
    ax.set_xlabel('Departure date (Year-month-day)')
    ax.set_ylabel('Time of flight (days)')
    plt.show()
