# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 14:02:17 2021

@author: Irina Kovalenko
"""

import unittest
import os
import spiceypy as spice
import pickle
import sempy.core.init.constants
from sempy.core.asteroids.asteroid_rdv_mission import AsteroidRdvMission
from sempy.core.init.load_kernels import load_kernels
dirname = os.path.dirname(__file__)

class TestAsteroidRdvMission(unittest.TestCase):

    def setUp(self):

        self.mission = AsteroidRdvMission('54000953',
                                          ['2020-01-01', '2020-01-01'],
                                          [10, 11])

    def test_init(self):
        self.assertEqual(self.mission.asteroid_name, '54000953')
        self.assertEqual(self.mission.tof_range, [10, 11])
        self.assertEqual(self.mission.step_dpr, 10)
        self.assertEqual(self.mission.step_tof, 1)

    def test_data_preparation(self):
        load_kernels()
        spice.furnsh('asteroids/tests/54000953.bsp')
        self.mission.data_preparation()
        spice.kclear

        self.assertEqual(len(self.mission.data_organized), 1)

    def test_mission_analysis_solver(self):

        file_data = os.path.join(dirname, '54000953.data')

        with open(file_data, 'rb') as f:   # will close() when we leave this block
            data = pickle.load(f)

        self.mission.mission_analysis_solver(data)
        v = self.mission.v_inf0[0]
        self.assertAlmostEqual(v[0], -0.65177867, 4)
        self.assertAlmostEqual(v[1], -0.66475068, 4)
        self.assertAlmostEqual(v[2], 1.66354256, 4)

    def test_delta_v_calc(self):

        self.mission.v_inf0 = [[-0.65177867, -0.66475068,  1.66354256]]
        self.mission.v_inf1 = [[-0.75959543, -0.76512866,  1.68117949]]
        self.mission.delta_v_calc()

        self.assertAlmostEqual(self.mission.total_dV[0], 3.90351795023, 4)

    def test_optimal_dv_solution(self):

        self.mission.departures = [['2020-01-01']]
        self.mission.arrivals = [['2020-01-11']]
        self.mission.total_dV = [[3.90351795]]
        self.mission.tofs = [[10]]
        self.mission.v_inf0 = [[-0.65177867, -0.66475068,  1.66354256]]
        self.mission.v_inf1 = [[-0.75959543, -0.76512866,  1.68117949]]
        tof = self.mission.optimal_dv_solution()["TOF"]
        self.assertEqual(tof, [10])


if __name__ == '__main__':
    unittest.main()
