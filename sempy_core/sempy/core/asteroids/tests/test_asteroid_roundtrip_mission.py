# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 14:40:44 2021

@author: Irina Kovalenko
"""

import unittest
import os
import pickle
import spiceypy as spice
import sempy.core.init.constants
from sempy.core.asteroids.asteroid_roundtrip_mission import AsteroidRoundtripMission
from sempy.core.init.load_kernels import load_kernels
dirname = os.path.dirname(__file__)


class TestAsteroidRdvMission(unittest.TestCase):

    def setUp(self):

        self.mission = AsteroidRoundtripMission('54000953',
                                                ['2020-01-01', '2020-01-01'],
                                                5, 1, step_dpr=1, step_tof=2)

    def test_init(self):
        self.assertEqual(self.mission.asteroid_name, '54000953')
        self.assertEqual(self.mission.mission_duration_limit, 5)
        self.assertEqual(self.mission.step_dpr, 1)
        self.assertEqual(self.mission.step_tof, 2)

    def test_data_preparation(self):
        load_kernels()
        ast_kernels = os.path.join(dirname, '54000953.bsp')
        spice.furnsh('asteroids/tests/54000953.bsp')

        self.mission.data_preparation()

        spice.kclear

        self.assertEqual(len(self.mission.data_organized_earth2ast), 1)
        self.assertEqual(len(self.mission.data_organized_ast2earth), 1)

    def test_mission_analysis_solver(self):

        result = self.mission.mission_analysis_solver()
        self.assertEqual(result, "No data")

    def test_results_combination(self):

        data1 = os.path.join(dirname, "./result_earth2asteroid")
        data2 = os.path.join(dirname, "./result_asteroid2earth")

        self.mission.earth2asteroid = data1
        self.mission.earth2asteroid = data2

        # self.assertTrue(self.mission.earth2asteroid)
        # self.mission.results_combination()
        # v_1 = self.mission.v_inf_earth_arr[0]
        # self.assertAlmostEqual(v_1[0], 3.575218, 5)
        # self.assertAlmostEqual(v_1[1], 3.623159, 5)
        # self.assertAlmostEqual(v_1[2], -8.231905, 5)

    def test_delta_v_calc(self):
        self.mission.v_inf_earth_dep = [[-3.6039691, -3.66077403, 8.19349133]]
        self.mission.v_inf_ast_arr = [[-3.66156033, -3.73466226,  8.10431188]]
        self.mission.v_inf_ast_dep = [[3.5149293, 3.55633104, -8.29622878]]
        self.mission.v_inf_earth_arr = [[3.57521828, 3.62315935, -8.23190533]]

        self.mission.delta_v_calc()
        total_dv = self.mission.total_dv[0]

        self.assertAlmostEqual(total_dv, 29, 0)


if __name__ == '__main__':
    unittest.main()
