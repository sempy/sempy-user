#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 16:16:05 2020

@author: Irina Kovalenko
"""

import unittest

import sempy.core.init.constants as cst
import sempy.core.asteroids.ephemerides as ephemerides
from sempy.core.init.load_kernels import load_kernels
import spiceypy as spice
import os
import math
dirname = os.path.dirname(__file__)

class TestEphemerides(unittest.TestCase):

    def test_State_vector(self):
        """Test for the get_state_vector method."""

        load_kernels()
        spice.furnsh('asteroids/tests/54000953.bsp')

        day = '2020-01-01'
        state_vec_earth = ephemerides.get_state_vector('Earth', day)

        spice.kclear

        au = math.sqrt(math.pow(state_vec_earth[0], 2) +
                       math.pow(state_vec_earth[1], 2)
                       + math.pow(state_vec_earth[2], 2))

        self.assertAlmostEqual(au, cst.AU, delta=0.0335*cst.AU)


if __name__ == '__main__':
    unittest.main()
