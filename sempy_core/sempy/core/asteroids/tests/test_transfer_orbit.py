# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 20:47:27 2021

@author: Irina Kovalenko
"""
import unittest
import os
import math
import spiceypy as spice
import sempy.core.init.constants
from sempy.core.asteroids.transfer_orbit import TransferOrbit
from sempy.core.init.load_kernels import load_kernels


class TestTransferOrbit(unittest.TestCase):
    def setUp(self):
        self.orb = TransferOrbit('Earth', '54000953', '2020-01-01', 85)

    def test_init(self):
        self.assertEqual(str(self.orb.arrival), '2020-03-26')

    def test_get_solution(self):
        load_kernels()
        spice.furnsh('asteroids/tests/54000953.bsp')
        self.orb.get_solution()
        spice.kclear

        v_inf0 = math.sqrt(self.orb.v_inf0[0]**2 + self.orb.v_inf0[1]**2 +
                           self.orb.v_inf0[2]**2)

        self.assertEqual(self.orb.nb_orbits, 0)
        self.assertEqual(round(v_inf0, 2), 0.34)


if __name__ == '__main__':
    unittest.main()
