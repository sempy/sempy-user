# -*- coding: utf-8 -*-
"""
@author: Irina Kovalenko
"""

import os
import pickle
import numpy
import datetime
from numpy.linalg import norm
from multiprocessing import Pool

import sempy.core.asteroids.porkchop_plot as porkchop_plot
from sempy.core.asteroids.lambert_solution import LambertSolution
from sempy.core.asteroids.ephemerides import get_state_vector
import sempy.core.init.constants as cst


def parfunct(tof, departure, state_vec0, state_vec1):
    ''' This function if used in parallel computing and has to be declared out
    of the main class.
    '''

    earth2asteroid = LambertSolution(
        state_vec0,
        state_vec1,
        departure,
        tof)

    return earth2asteroid


class AsteroidRdvMission:
    '''Asteroid_rdv_mission computes all impulsive transfers from the
    Earth to an asteroid in a given range of launch dates.
    For a target-asteroid an SPK file can be generated here:
    https://ssd.jpl.nasa.gov/x/spk.html and has to be loaded before the method.
    The tool generates pork-chop plot (a time of flight vs launch date map)
    for total delta-V (sum of the departure and arrival V-infinities), or for
    V-infinities at departure or arrival, and enables to find optimal solution,
    minimizing  total delta-V, or V-infinities at departure or arrival, or time
    of flight.
    Optionally, if departure is performed from a parking orbit, the total
    delta-V includes the Earth departure maneuver from a LEO parking orbit.

    Parameters
    ----------
    asteroid_name : str
        Case-sensitive asteroid name (e.g. '2017 AP4'),
        or number, designation, MPC packed designation, or (SPK ID-2000000)
        number
    departure_range : list of str
        Interval of departure dates, e.g. ['2020-01-01','2050-01-01']
    tof_range : list of int
        Mission duration limit [days]
    step_dpr : int, optional
        Departure date step [days]. Default 10 days.
    step_tof : int, optional
        TOF step [days]. Default 1 day.

    **kwargs :
    leo_alt : float
        altitude of the parking Low Earth Orbit [km].


    Attributes
    ----------
    asteroid_name : str
        Case-sensitive asteroid name (e.g. '2017 AP4'),
        or number, designation, MPC packed designation, or (SPK ID-2000000)
        number
    departure_range : list of str
        Interval of departure dates, e.g. ['2020-01-01','2050-01-01']
    tof_range : list of int
        Mission duration limit [days]
    kernels_dir_path : str
        Path to directory where your 'meta_kernel.txt' file resides.
    step_dpr : int, optional
        Departure date step [days]. Default 10 days.
    step_tof : int, optional
        TOF step [days]. Default 1 day.
    dv_budget : float
        dv constraint for the plot.
    leo_alt : float
        altitude of the parking Low Earth Orbit
    v_infO : ndarray
        hyperbolic excess velocity at departure (the vector difference between
        Earth's velocity and the heliocentric orbit's velocity),
        array of v_infO results.
    v_inf1 : ndarray
        hyperbolic excess velocity at arrival (the vector difference between
        asteroid's velocity and the heliocentric orbit's velocity),
        array of v_inf1 results.
    deltaV_0 : ndarray
        delta-V required for injection from the parking Low Earth Orbit,
        array of deltaV_0 results.
    total_dV : ndarray
        Array of total mission delta-V in km/s.

    '''

    def __init__(
            self,
            asteroid_name,
            departure_range,
            tof_range,
            step_dpr=10,
            step_tof=1,
            **kwargs):
        """Inits Asteroid_rdv_mission class."""
        self.asteroid_name = asteroid_name
        self.departure_range = departure_range
        self.tof_range = tof_range
        self.step_dpr = step_dpr
        self.step_tof = step_tof

        self.parking_orbit = False

        if 'leo_alt' in kwargs:
            self.parking_orbit = True
            self.leo_alt = kwargs.get('leo_alt')

        # Attributes to prepare data for parallel computing
        self.data_organized = list()

        # Attributes to be calculated in the 'solution' funcion
        self.results = list()
        self.departures = list()
        self.arrivals = list()
        self.tofs = list()
        self.v_inf0 = list()
        self.v_inf1 = list()
        self.deltaV_0 = list()
        self.total_dV = list()

    def data_preparation(self, eph_file=False):
        '''Return data table for Parallel computing.

        Parameters
        ----------
        eph_file : boolean
            Write (True) or not (False) ephemeris data to a file.
        '''

        asteroid_name = self.asteroid_name

        departure = datetime.date.fromisoformat(self.departure_range[0])

        data_for_func = list()

        while departure <= datetime.date.fromisoformat(
                self.departure_range[len(self.departure_range) - 1]):
            for tof in range(
                    self.tof_range[0],
                    self.tof_range[1],
                    self.step_tof):

                state_vec0 = get_state_vector(
                    'Earth', str(departure))  # departure state vec

                arrival = departure + datetime.timedelta(days=tof)
                state_vec1 = get_state_vector(asteroid_name, str(
                    arrival))  # arrival state vector

                data_for_func.append({
                    "tof": tof,
                    "departure": departure,
                    "state_vec0": state_vec0,
                    "state_vec1": state_vec1
                })
            departure = departure + datetime.timedelta(days=self.step_dpr)

        self.data_organized = [
            (data["tof"],
             data["departure"],
                data["state_vec0"],
                data["state_vec1"]) for data in data_for_func]
        if eph_file:
            eph_filename = './' + asteroid_name+'.data'
            with open(eph_filename, 'wb') as filehandle:
                # store the data as binary data stream
                pickle.dump(self.data_organized, filehandle)

    def mission_analysis_solver(self, data_organized):
        '''Calculate v_inf at departure and arrival using Lamber's problem
        solver and using parallel computing for each departure dates and TOFs.

        Parameters
        ----------
        data_organized : list
            Use precomputed data orginized for parallel computing or
            download data from a file.

        '''

        dates_departure = list()
        dates_arrival = list()
        tofs = list()
        v_inf_dep_earth = list()
        v_inf_arr_ast = list()

        pool = Pool(processes=os.cpu_count())

        results = pool.starmap(parfunct, data_organized)

        pool.close()
        pool.join()

        self.results = results

        dates_departure = [str(data.departure) for data in self.results]
        dates_arrival = [data.arrival for data in self.results]
        tofs = [data.tof for data in self.results]
        v_inf_dep_earth = [data.v_inf0 for data in self.results]
        v_inf_arr_ast = [data.v_inf1 for data in self.results]

        self.departures = dates_departure
        self.arrivals = dates_arrival
        self.tofs = tofs
        self.v_inf0 = v_inf_dep_earth
        self.v_inf1 = v_inf_arr_ast

    def delta_v_calc(self):
        """
        Calculates the mission manoeuvres: insertion from the parcking LEO
        (if specified), and total delta-v.
        """
        deltaV_0 = list()
        total_dV = list()

        if self.parking_orbit:
            r0 = self.leo_alt + cst.R_EARTH_EQ
            mu0 = cst.GM_EARTH

            deltaV_0 = numpy.sqrt(
                2 * mu0 / r0
                + numpy.power(norm(self.v_inf0, axis=1), 2)) \
                - numpy.sqrt(mu0 / r0)
        else:
            deltaV_0 = norm(self.v_inf0, axis=1)

        total_dV = deltaV_0 + norm(self.v_inf1, axis=1)

        self.deltaV_0 = deltaV_0
        self.total_dV = total_dV

    def porkchop_plot_C3_dep(self, **kwargs):
        """Plot of C3 energy as a function of departure date and flight time.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """
        plot_title = 'Departure characteristic energy $C_3$'
        bar_label = '$C_3, km^2/s^2$'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 150

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        c3 = numpy.power(norm(self.v_inf0, axis=1), 2)
        data_plot = numpy.array([self.departures, self.tofs, c3])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)

    def porkchop_plot_V_inf_dep(self, **kwargs):
        """Plot of Departure V-infinity as a function of departure date
        and flight time.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """

        plot_title = r'Departure $V_\infty$'
        bar_label = r'$V_\infty$, km/s'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 13.0

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        data_plot = numpy.array(
            [self.departures, self.tofs, norm(self.v_inf0, axis=1)])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)

    def porkchop_plot_V_inf_arr(self, **kwargs):
        """Plot of Arrival V-infinity as a function of departure date
        and flight time.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """

        plot_title = r'Arrival $V_\infty$'
        bar_label = r'$V_\infty$, km/s'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 13.0

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        data_plot = numpy.array(
            [self.departures, self.tofs, norm(self.v_inf1, axis=1)])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)

    def porkchop_plot_total_dV(self, **kwargs):
        """Plot of total delta-V as a function of departure date and
        flight time.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """

        plot_title = r'Total $\Delta$ V'
        bar_label = r'Total $\Delta$ V, km/s'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 30.0

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        data_plot = numpy.array([self.departures, self.tofs, self.total_dV])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)

    def optimal_dv_solution(self):
        """Return the departure date, tof for the minimum delta-v solution.
        """
        dv = numpy.array(self.total_dV)
        dv_min = numpy.argmin(dv)
        solution = {
            "Departure": self.departures[dv_min],
            "Arrival": self.arrivals[dv_min],
            "TOF": self.tofs[dv_min],
            "Total delta-V": self.total_dV[dv_min],
            "V_inf Earth departure": self.v_inf0[dv_min],
            "V_inf Asteroid arrival": self.v_inf1[dv_min]
            }

        return solution

    def optimal_c3_departure_solution(self):
        """Return the departure and arrival dates, tof, total_dV solution for
        the minimum departure C3 energy.
        """
        c3 = numpy.power(norm(self.v_inf0, axis=1), 2)
        dv_min = numpy.argmin(c3)

        solution = {
            "Departure": self.departures[dv_min],
            "Arrival": self.arrivals[dv_min],
            "TOF": self.tofs[dv_min],
            "Total delta-V": self.total_dV[dv_min],
            "V_inf Earth departure": self.v_inf0[dv_min],
            "V_inf Asteroid arrival": self.v_inf1[dv_min]
            }

        return solution

    def optimal_v_inf_arrival_solution(self):
        """Return the departure date, tof for the minimum delta-v solution.
        """
        dv = numpy.array(norm(self.v_inf1, axis=1))
        dv_min = numpy.argmin(dv)
        solution = {
            "Departure": self.departures[dv_min],
            "Arrival": self.arrivals[dv_min],
            "TOF": self.tofs[dv_min],
            "Total delta-V": self.total_dV[dv_min],
            "V_inf Earth departure": self.v_inf0[dv_min],
            "V_inf Asteroid arrival": self.v_inf1[dv_min]
            }

        return solution

    def optimal_tof_solution(self):
        """Return the departure date, tof for the minimum TOF solution.
        """
        tof = numpy.array(self.tofs)
        tof_min = numpy.argmin(tof)
        solution = {
            "Departure": self.departures[tof_min],
            "Arrival": self.arrivals[tof_min],
            "TOF": self.tofs[tof_min],
            "Total delta-V": self.total_dV[tof_min],
            "V_inf Earth departure": self.v_inf0[tof_min],
            "V_inf Asteroid arrival": self.v_inf1[tof_min]
            }

        return solution
