#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 16:03:44 2020

@author: Irina Kovalenko
"""
import os
import pickle
import datetime
from multiprocessing import Pool
import numpy
from numpy.linalg import norm


from sempy.core.asteroids.lambert_solution import LambertSolution
from sempy.core.asteroids.ephemerides import get_state_vector
import sempy.core.asteroids.porkchop_plot as porkchop_plot

import sempy.core.init.constants as cst


def parfunct(tof, departure, state_vec0, state_vec1):
    ''' This function if used in parallel computing and has to be declared out
    of the main class.
    '''
    oneway_path = LambertSolution(state_vec0, state_vec1, departure, tof)
    return oneway_path


class AsteroidRoundtripMission:
    ''' AsteroidRoundtripMission computes all impulsive transfers for
    Earth-asteroid-Earth mission in a given range of launch dates,
    and limited mission duration.
    For a target-asteroid an SPK file can be generated here:
    https://ssd.jpl.nasa.gov/x/spk.html and has to be loaded before the method.
    The tool enables to find optimal solution, minimizing  total delta-V
    (summ of V-infinities at departures and arrivals from Earth and asteroid).
    Optionally, if departure is performed from a parking orbit,
    the total delta-V includes the Earth departure maneuver
    from a LEO parking orbit, and a manoevre for Earth 125 km altitude
    atmosphere re-entry velosity limit.

    Parameters
    ----------
    asteroid_name : str
        Asteroid SPKID number.
    departure_range : list of str
        Interval of departure dates, e.g. ['2020-01-01','2050-01-01']
    tof_range : int
        Outbound/Inbound Flight Time [days].
    stay : int
        The number of days spent at the NEA  [days].
    step_dpr : int, optional
        Departure date step [days]. Default 10 days.
    step_tof : int, optional
        TOF step [days]. Default 1 day.

    **kwargs :
    mission_duration_limit : int
        total mission duration [days].
    leo_alt : float
        altitude of the parking Low Earth Orbit [km].
    earth_reentry : float
        velocity limit at 125 km altitude Earth atmosphere re-entry.

    Attributes
    ----------
    asteroid_name : str
        Asteroid SPKID number.
    departure_range : list of str
        Interval of departure dates, e.g. ['2020-01-01','2050-01-01'].
    tof_range : list of int
        Mission duration limit [days]
    step_dpr : int, optional
        Departure date step [days]. Default 10 days.
    step_tof : int, optional
        TOF step [days]. Default 1 day.
    dv_budget : float
        dv constraint for the plot.
    leo_alt : float
        altitude of the parking Low Earth Orbit
    v_infO : ndarray
        hyperbolic excess velocity at departure (the vector difference between
        Earth's velocity and the heliocentric orbit's velocity),
        array of v_infO results.
    v_inf1 : ndarray
        hyperbolic excess velocity at arrival (the vector difference between
        asteroid's velocity and the heliocentric orbit's velocity),
        array of v_inf1 results.
    delta_v0 : ndarray
        delta-V required for injection from the parking Low Earth Orbit,
        array of delta_v0 results.
    total_dv : ndarray
        Array of total mission delta-V in km/s.

    '''

    def __init__(
            self,
            asteroid_name,
            departure_range,
            mission_duration_limit,
            stay,
            step_dpr=8,
            step_tof=1,
            **kwargs):
        """Inits Asteroid_rdv_mission class."""
        self.asteroid_name = asteroid_name
        self.departure_range = departure_range
        self.mission_duration_limit = mission_duration_limit
        self.stay = stay
        self.step_dpr = step_dpr
        self.step_tof = step_tof

        self.parking_orbit = False
        if 'leo_alt' in kwargs:
            self.parking_orbit = True
            self.leo_alt = kwargs.get('leo_alt')

        self.reentry_limit = False
        if 'earth_reentry' in kwargs:
            self.reentry_limit = True
            self.earth_reentry = kwargs.get('earth_reentry')

        # Attributes to prepare data for parallel computing
        self.data_organized_earth2ast = list()
        self.data_organized_ast2earth = list()

        # Attributes to be calculated in the 'solution' funcion
        self.earth2asteroid = list()
        self.asteroid2earth = list()
        self.departures = list()
        self.ast_arrival = list()
        self.tof1 = list()
        self.stay_total = list()
        self.ast_departure = list()
        self.earth_arrival = list()
        self.tof2 = list()
        self.v_inf_earth_dep = list()
        self.v_inf_ast_arr = list()
        self.v_inf_ast_dep = list()
        self.v_inf_earth_arr = list()
        self.mission_duration = list()
        # dv manoeuvres
        self.delta_v0 = list()
        self.dv_reentry = list()
        self.dv_ast_depart = list()
        self.dv_ast_rdv = list()
        self.total_dv = list()

    def data_preparation(self, eph_file=False):
        '''Return data table for Parallel computing.
        '''

        asteroid_name = self.asteroid_name

        departure = datetime.date.fromisoformat(self.departure_range[0])
        tof_min = self.step_tof
        tof_max = self.mission_duration_limit - self.step_tof - self.stay

        # From the Earth to asteroid
        data_for_func = list()

        while departure <= datetime.date.fromisoformat(
                self.departure_range[len(self.departure_range)-1]):
            for tof in range(tof_min, tof_max+1, self.step_tof):
                # departure state vector
                state_vec0 = get_state_vector('Earth', str(departure))
                arrival = departure + datetime.timedelta(days=tof)

                # arrival state vector
                state_vec1 = get_state_vector(asteroid_name, str(arrival))
                data_for_func.append({
                    "tof": tof,
                    "departure": departure,
                    "state_vec0": state_vec0,
                    "state_vec1": state_vec1
                })
            departure = departure + datetime.timedelta(days=self.step_dpr)

        self.data_organized_earth2ast = [
            (data["tof"],
             data["departure"],
             data["state_vec0"],
             data["state_vec1"]) for data in data_for_func]

        if eph_file:
            with open('./data_preparation_earth2asteroid', 'wb') as filehandle:
                # store the data as binary data stream
                pickle.dump(self.data_organized_earth2ast, filehandle)

        # From asteroid to the Earth
        departure = datetime.date.fromisoformat(self.departure_range[0]) \
            + datetime.timedelta(days=tof_min) \
            + datetime.timedelta(days=self.stay)
        data_for_func = list()

        date1 = datetime.date.fromisoformat(
            self.departure_range[len(self.departure_range)-1])
        # departure_from_ast_limit = date1 \
        #     + datetime.timedelta(days=self.mission_duration_limit)
        departure_from_ast_limit = (date1 +
                                    datetime.timedelta(days=tof_max) +
                                    datetime.timedelta(days=self.stay))

        while departure <= departure_from_ast_limit:
            for tof in range(tof_min, tof_max+1, self.step_tof):

                # departure state vector
                state_vec0 = get_state_vector(asteroid_name, str(departure))

                arrival = departure + datetime.timedelta(days=tof)
                # arrival state vector
                state_vec1 = get_state_vector('Earth', str(arrival))

                data_for_func.append({
                    "tof": tof,
                    "departure": departure,
                    "state_vec0": state_vec0,
                    "state_vec1": state_vec1
                })
            departure = departure + datetime.timedelta(days=self.step_dpr)

        self.data_organized_ast2earth = [
            (data["tof"],
             data["departure"],
             data["state_vec0"],
             data["state_vec1"]) for data in data_for_func]

        if eph_file:
            with open('./data_preparation_asteroid2earth', 'wb') as filehandle:
                # store the data as binary data stream
                pickle.dump(self.data_organized_ast2earth, filehandle)

    def mission_analysis_solver(self, to_file=False):
        '''Function calculates all outbound and inbound paths using parallel
        computing.
        '''

        if (len(self.data_organized_ast2earth) != 0 and
                len(self.data_organized_earth2ast) != 0):
            # Parallel computing
            # Earth to asteroid path
            results_earth2ast = list()

            # pool = Pool(processes=os.cpu_count())

            # results_earth2ast = pool.starmap(parfunct,
            #                                  self.data_organized_earth2ast)

            pool = Pool(processes=os.cpu_count())

            chunck = int(len(self.data_organized_earth2ast)/os.cpu_count()/2)
            results_earth2ast = pool.starmap(parfunct,
                                             self.data_organized_earth2ast,
                                             chunck)
            pool.close()
            pool.join()

            # Asteroid to the Earth path
            results_ast2earth = list()

            # pool = Pool(processes=os.cpu_count())
            # results_ast2earth = pool.starmap(parfunct,
            #                                    self.data_organized_ast2earth)

            pool = Pool(processes=os.cpu_count())

            chunck = int(len(self.data_organized_ast2earth)/os.cpu_count()/2)
            results_ast2earth = pool.starmap(parfunct,
                                             self.data_organized_ast2earth,
                                             chunck)

            pool.close()
            pool.join()

            self.earth2asteroid = results_earth2ast
            self.asteroid2earth = results_ast2earth

            if to_file:
                with open('./result_earth2asteroid', 'wb') as filehandle:
                    # store the data as binary data stream
                    pickle.dump(self.earth2asteroid, filehandle)

            if to_file:
                with open('./result_asteroid2earth', 'wb') as filehandle:
                    # store the data as binary data stream
                    pickle.dump(self.asteroid2earth, filehandle)

            result = "Trajectories were calculated."
        else:
            result = "No data"

        return result

    def results_combination(self):
        '''Function combines outbound and inbound paths, consistent with each
        other and with the mission duration limits.'''
        results_earth2asteroid = self.earth2asteroid
        results_asteroid2earth = self.asteroid2earth

        mission_limit = self.mission_duration_limit

        data_summary = list()
        for data1 in results_earth2asteroid:
            for data2 in results_asteroid2earth:
                # check if stay is > the given stay_limit
                ast_dep_limit = datetime.date.fromisoformat(data1.arrival) \
                    + datetime.timedelta(days=self.stay)

                if data2.departure >= ast_dep_limit:
                    stay_exact = data2.departure \
                        - datetime.date.fromisoformat(data1.arrival)
                    # total time of the misssion
                    tof_total = data1.tof + data2.tof + stay_exact.days

                    if tof_total <= mission_limit:
                        # collect all required data to calculate all delta V
                        data_summary.append({
                                "earth_departure": data1.departure,
                                "ast_arrival": data1.arrival,
                                "tof1": data1.tof,
                                "stay": stay_exact.days,
                                "ast_departure": data2.departure,
                                "earth_arrival": data2.arrival,
                                "tof2": data2.tof,
                                "mission_duration": tof_total,
                                "v_inf_earth_dep": data1.v_inf0,
                                "v_inf_ast_arr": data1.v_inf1,
                                "v_inf_ast_dep": data2.v_inf0,
                                "v_inf_earth_arr": data2.v_inf1
                                })

        self.departures = [
            str(data["earth_departure"]) for data in data_summary]
        self.ast_arrival = [(data["ast_arrival"]) for data in data_summary]
        self.tof1 = [(data["tof1"]) for data in data_summary]
        self.stay_total = [(data["stay"]) for data in data_summary]
        self.ast_departure = [
            (datetime.date.isoformat(
                data["ast_departure"])) for data in data_summary]
        self.earth_arrival = [(data["earth_arrival"]) for data in data_summary]
        self.tof2 = [(data["tof2"]) for data in data_summary]
        self.mission_duration = [
            (data["mission_duration"]) for data in data_summary]
        self.v_inf_earth_dep = [
            data["v_inf_earth_dep"] for data in data_summary]
        self.v_inf_ast_arr = [(data["v_inf_ast_arr"]) for data in data_summary]
        self.v_inf_ast_dep = [(data["v_inf_ast_dep"]) for data in data_summary]
        self.v_inf_earth_arr = [
            (data["v_inf_earth_arr"]) for data in data_summary]

    def delta_v_calc(self):
        """
        Calculates all the mission manoeuvres, including insertion from the
        parcking LEO (if specified),
        Earth atmosphere reentry, and total delta-v.
        """
        # Calculate the Total delta-V
        #  delta-v at Earth (or LEO) departure
        mu0 = cst.GM_EARTH
        if self.parking_orbit:
            r0 = self.leo_alt + cst.R_EARTH_EQ
            delta_v0 = numpy.sqrt(
                2*mu0/r0 + numpy.power(norm(self.v_inf_earth_dep, axis=1), 2))\
                - numpy.sqrt(mu0/r0)
        else:
            delta_v0 = norm(self.v_inf_earth_dep, axis=1)

        self.delta_v0 = delta_v0

        # delta-v at Earth atmosph. reentry if required
        # if there is no limit, no manoeuvres is applied
        self.dv_reentry = numpy.array([0]*len(self.v_inf_earth_arr))
        if self.reentry_limit:
            r1 = 125 + cst.R_EARTH_EQ  # at 125 km altitude
            v_reentry = numpy.sqrt(
                2*mu0/r1 + numpy.power(norm(self.v_inf_earth_arr, axis=1), 2))
            ind_vlim = v_reentry > self.earth_reentry
            self.dv_reentry[ind_vlim] = v_reentry[ind_vlim] \
                - self.earth_reentry

        # delta-v at asteroid rdv (to match asteroid's velosity)
        self.dv_ast_rdv = norm(self.v_inf_ast_arr, axis=1)

        # delta-v at asteroid departure
        self.dv_ast_depart = norm(self.v_inf_ast_dep, axis=1)

        # total mission dv
        self.total_dv = self.delta_v0 \
            + self.dv_ast_rdv + self.dv_ast_depart + self.dv_reentry

    def optimal_dv_solution(self):
        """Return the departure/arrival dates, tof for the minimum
        delta-v solution.
        """
        dv = numpy.array(self.total_dv)
        dv_min = numpy.argmin(dv)

        solution = {
            "Earth departure": self.departures[dv_min],
            "Asteroid arrival": self.ast_arrival[dv_min],
            "Mission duration (d)": self.mission_duration[dv_min],
            "Total Delta-V (km/s)": self.total_dv[dv_min],
            "Outbound Flight Time (d)": self.tof1[dv_min],
            "Stay Time (d)": self.stay_total[dv_min],
            "Inbound Flight Time (d)": self.tof2[dv_min]
            }

        return solution

    def porkchop_plot_outbound_dV(self, **kwargs):
        """Plot of outbound delta-V as a function of departure date and
        flight time. The delta-V includes a manoeuvre of departure from
        the parking LEO (if specified) and a manoeuvre to match asteroid's
        velosity.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """
        results = self.earth2asteroid
        dates_departure = [str(data.departure) for data in results]
        tofs = [data.tof for data in results]
        v_inf_dep_earth = [data.v_inf0 for data in results]
        v_inf_arr_ast = [data.v_inf1 for data in results]

        # Calculate the Total delta-V
        #  delta-v at Earth (or LEO) departure
        mu0 = cst.GM_EARTH
        if self.parking_orbit:
            r0 = self.leo_alt + cst.R_EARTH_EQ
            delta_v0 = numpy.sqrt(
                2*mu0/r0 + numpy.power(norm(v_inf_dep_earth, axis=1), 2))\
                - numpy.sqrt(mu0/r0)
        else:
            delta_v0 = norm(v_inf_dep_earth, axis=1)

        # delta-v at asteroid rdv (to match asteroid's velosity)
        dv_ast_rdv = norm(v_inf_arr_ast, axis=1)

        plot_title = r'Earth-to-Asteroid'
        bar_label = r'$\Delta$ V, km/s'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 10.0

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        dv_sum = delta_v0 + dv_ast_rdv

        data_plot = numpy.array([dates_departure, tofs,
                                 dv_sum])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)

    def porkchop_plot_inbound_dV(self, **kwargs):
        """Plot of inbound delta-V as a function of departure date and
        flight time. The delta-V includes a manoeuvre of departure from
        the asteroid and v_inf at Earth arrival.
        velosity.

        Parameters
        ----------
        **kwargs :

        dv_limit : float, optional
            Cut colours if dv > dv_limit.
        levels : int, optional
            Level set.
        """
        results = self.asteroid2earth
        dates_departure = [str(data.departure) for data in results]
        tofs = [data.tof for data in results]
        v_inf_dep_ast = [data.v_inf0 for data in results]
        v_inf_arr_earth = [data.v_inf1 for data in results]

        # delta-v at asteroid departure
        delta_v0 = norm(v_inf_dep_ast, axis=1)

        # v_infinity at Earth arrival
        vinf_earth = norm(v_inf_arr_earth, axis=1)

        plot_title = r'Asteroid-to-Earth'
        bar_label = r'$\Delta$ V, km/s'

        if 'dv_limit' in kwargs:
            dv_limit = kwargs.get('dv_limit')
        else:
            dv_limit = 10.0

        if 'levels' in kwargs:
            levels = kwargs.get('levels')
        else:
            levels = 20

        dv_sum = delta_v0 + vinf_earth

        data_plot = numpy.array([dates_departure, tofs,
                                 dv_sum])
        porkchop_plot.plot(data_plot, plot_title, dv_limit, levels, bar_label)
