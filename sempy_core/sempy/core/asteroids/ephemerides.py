#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 15:49:27 2019

@author: Irina Kovalenko
"""
import spiceypy as spice


def get_state_vector(object_name, day):
    '''Returns state vector of an object from SPICE ephemerides service.
    For asteroids an SPK file can be generated here:
    https://ssd.jpl.nasa.gov/x/spk.html and has to be loaded before the method.

    Parameters
    ----------
    object_name : str
        Target body name, e.g. 'Earth' or asteroid SPKID number.
    day : datetime.date or str
        The date-time in the format '2020-01-01' or '2020-01-01 00:55:23'

    Returns
    -------
    state : tuple or numpy.ndarray
        State vector of the target object [x, y, z, vx, vy, vz]
        in Ecliptic J2000 w.r.t Sun.

    '''
    day_et = spice.str2et(str(day))

    state = spice.spkezr(
        object_name,
        day_et,
        'ECLIPJ2000',
        'NONE',
        'SUN')[0]

    return state
