# -*- coding: utf-8 -*-
"""
This module contains the events to perform differential correction.

These events are coded in a format that the scipy numerical ode integrator, solve_ivp, accepts.

Created on Mon Jul 22 15:09:46 2019

@author: Edgar PEREZ, Alberto FOSSA', Paolo GUARDABASSO
"""

import numpy as np


def generate_plane_event(plane, direction=0, terminal=False):
    """This function generates an event to detect plane crossings.

    Parameters
    ----------
    plane : str
        coordinating plane for which the event should be created.
        Must be one of ``xy``, ``xz``, ``yz``.
    direction : int, optional
        event direction. Must be one of ``1``, ``-1``, ``0`` where:
        * | 1 -- detect events going from negative to positive w.r.t. the third coordinating axis
        * | -1 -- detect events going from positive to negative w.r.t. the third coordinating axis
        * | 0 -- detect all plane crossing events independently from their direction.
        Default to ``0``.
    terminal : bool, optional
        if ``True``, propagation is stopped at the first event occurrence. If ``False``, no action
        is taken and the propagation continues until the target time. Default to ``False``.

    Returns
    -------
    fcn : function
        an event function to be passed as keyword arguments to the various propagators.

    """

    # translation from plane name to state component equal to zero at plane crossing
    if plane == 'yz':
        comp = 0
    elif plane == 'xz':
        comp = 1
    elif plane == 'xy':
        comp = 2
    else:
        raise Exception('plane must be one of xy, xz, yz')

    if direction not in (-1, 0, 1):
        raise Exception('event direction must be one of -1, 0, 1')

    def fun(_, state):
        """Event function. """
        return state[comp]

    fun.direction = direction
    fun.terminal = terminal
    return fun


def generate_soi_event(cr3bp, direction, terminal=False):
    """ This function generates the Sphere of Influence crossing event functions to be passed to
    the solve_ivp integrator. For example, it is necessary to add soi_exit_event =
    generate_soi_event(cr3bp, "exit") in the main script, and pass soi_exit_event to solve_ivp.
    Depending on the value of direction, this function generates a SOI exit or entry event.

    For the EARTH-MOON system, the Sphere of Influence considered is the one of the Earth-Moon
    system with respect to the Sun. For all other CR3BPs where m1 is SUN, the SOI used is the one
    of m2 with respect to m1."""

    def soi_crossing(state, pos, rsoi):
        diff = np.linalg.norm(state[0:3] - pos) - rsoi
        return diff

    if cr3bp.m1.name == 'EARTH':
        prim_pos = cr3bp.m1_pos
        r_soi = cr3bp.r_soi_1
    elif cr3bp.m1.name == 'SUN':
        prim_pos = cr3bp.m2_pos
        r_soi = cr3bp.r_soi_2

    if direction == 'exit':
        direct = +1  # Direction from negative (inside) to positive (outside)
    elif direction == 'enter':  # The opposite as exit
        direct = -1  # Direction from positive (outside) to positive (inside)
    else:
        raise Exception("Direction of event must be either 'exit' or 'enter'")

    # Type of event: escaping the Sphere of Influence of M1. Non terminal.
    fun = lambda t, state: soi_crossing(state, prim_pos, r_soi)
    fun.direction = direct
    fun.type = 'soi ' + direction
    fun.terminal = terminal  # Do not stop integration if encountered
    return fun


def generate_impact_event(cr3bp, prim):
    """ This function generates the event functions to be passed to the solve_ivp integrator. It
    is necessary to add m1_impact_event = generate_impact_event(cr3bp, "m1") in the main
    script, and pass m1_impact_event to solve_ivp. Depending on the value of prim, this function
    generates an impact event with m1 or m2. """

    def primary_impact(state, prim_pos, prim_rad):
        prim_distance = np.linalg.norm(state[0:3] - prim_pos) - prim_rad
        return prim_distance

    if prim == "m1":
        # Type of event: impact with M1. Terminal.
        fun = lambda t, state: primary_impact(state, cr3bp.m1_pos, cr3bp.R1 / cr3bp.L)
        fun.type = 'm1 impact'
    elif prim == "m2":
        # Type of event: impact with M2. Terminal.
        fun = lambda t, state: primary_impact(state, cr3bp.m2_pos, cr3bp.R2 / cr3bp.L)
        fun.type = 'm2 impact'
    else:
        raise Exception("Primary has to be 'm1' or 'm2'")
    fun.terminal = True  # Stops integration if met
    fun.direction = -1  # Direction from positive to negative
    return fun


def generate_li_gate_event(cr3bp, l_i, direction, terminal=False):
    """ This function generates the event functions to be passed to the solve_ivp integrator. It
    is necessary to add L1_exit_event = generate_li_gate_event(cr3bp, cr3bp.l1, "exit") in the
    main script, and pass L1_exit_event to solve_ivp. Depending on the value of cr3bp.l1,
    this function generates an exit event from the L1 or L2 gates (L3, L4 and L5 are not
    considered). Direction can be either 'exit' or 'enter'.
    """

    def gate_crossing(state, li_x):
        return state[0] - li_x

    if direction == 'exit':
        direct = +1  # From negative (x<L1 or x<L2) to positive (x>L1 or x>L2) if enter
    elif direction == 'enter':  # The opposite as exit
        direct = -1  # From positive (x>L1 or x>L2) to negative (x<L1 or x<L2) if enter
    else:
        raise Exception("Direction of event must be either 'exit' or 'enter'")

    if l_i not in (cr3bp.l1, cr3bp.l2):
        raise Exception("Libration point has to be cr3bp.l1 or cr3bp.l2")

    # Type of event: Li gate crossing.
    fun = lambda t, state: gate_crossing(state, l_i.position[0])
    fun.direction = direct
    fun.terminal = terminal  # Stops integration if met
    fun.type = f"l{l_i.number} gate " + direction
    return fun
