"""
Unit tests for ODE event generation functions in sempy.core.diffcorr.ode_event

@author: Paolo GUARDABASSO
Created: 25/02/2021

"""

import unittest
import numpy as np

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.diffcorr.ode_event import generate_impact_event,\
                                   generate_soi_event,\
                                   generate_li_gate_event,\
                                   generate_plane_event


class TestODEEvent(unittest.TestCase):
    """ Unit tests of event generation functions"""

    def test_generate_plane_event(self):
        """Test the function that generates plane crossing events for the Earth Moon system."""
        # Initialize the CRTBP system:
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # Events initialisation
        xz_plane_event = generate_plane_event('xz', -1, True)
        xy_plane_event = generate_plane_event('xy', -1, True)
        yz_plane_event = generate_plane_event('yz', -1, True)

        # Propagate initial state:
        prop = Cr3bpSynodicPropagator(cr3bp.mu)
        _, _, _, state_events_xz =\
            prop.propagate([0, 10], np.array([0, 1, 0, 0, -1, 0]), events=(xz_plane_event,))
        _, _, _, state_events_xy =\
            prop.propagate([0, 10], np.array([0, 0, 1, 0, 0, -1]), events=(xy_plane_event,))
        _, _, _, state_events_yz =\
            prop.propagate([0, 10], np.array([1, 1, 0, -1, 0, 0]), events=(yz_plane_event,))

        # Test
        self.assertAlmostEqual(np.linalg.norm(state_events_xz[0][0][1]), 0.0, places=6)
        self.assertAlmostEqual(np.linalg.norm(state_events_xy[0][0][2]), 0.0, places=6)
        self.assertAlmostEqual(np.linalg.norm(state_events_yz[0][0][0]), 0.0, places=6)

    def test_generate_soi_event(self):
        """Test the function that generates SOI crossing events for the Earth Moon system."""
        # Initialize the CRTBP system:
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # Events initialisation
        soi_escape_event = generate_soi_event(cr3bp, "exit")
        soi_enter_event = generate_soi_event(cr3bp, "enter")

        # Propagate initial state:
        prop = Cr3bpSynodicPropagator(cr3bp.mu)
        _, _, _, state_events = prop.propagate([0, 10], np.array([2.5, 0, 0, -1, 0, 0]),
                                               events=(soi_escape_event, soi_enter_event))

        # Test
        self.assertAlmostEqual(np.linalg.norm(state_events[0][0][:3]-cr3bp.m1_pos), cr3bp.r_soi_1,
                               places=6)
        self.assertAlmostEqual(np.linalg.norm(state_events[1][0][:3]-cr3bp.m1_pos),
                               cr3bp.r_soi_1, places=6)

    def test_generate_impact_event(self):
        """Test the function that generates impact events."""

        # Initialize the CRTBP system:
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # Events initialisation
        m1_impact_event = generate_impact_event(cr3bp, "m1")
        m2_impact_event = generate_impact_event(cr3bp, "m2")

        # Propagate initial state to m1 impact:
        prop = Cr3bpSynodicPropagator(cr3bp.mu)
        state0_impact_1 = np.array([0.1, 0, 0, 0, 0, 0])
        _, _, _, state_i_1 = prop.propagate([0, 10], state0_impact_1, events=m1_impact_event)

        # Test
        r_impact_1 = np.linalg.norm(state_i_1[0][0][:3] - cr3bp.m1_pos)
        self.assertAlmostEqual(r_impact_1, cr3bp.R1/cr3bp.L, places=6)

        # Propagate initial state to m2 impact:
        state0_impact_2 = np.array([1.0, 0, 0, 0, 0, 0])
        _, _, _, state_i_2 = prop.propagate([0, 10], state0_impact_2, events=m2_impact_event)

        r_impact_2 = np.linalg.norm(state_i_2[0][0][:3] - cr3bp.m2_pos)
        self.assertAlmostEqual(r_impact_2, cr3bp.R2/cr3bp.L, places=6)

    def test_generate_li_gate_event(self):
        """Test the function that generates libration point gates crossing events."""

        # Initialize the CRTBP system:
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # Events initialisation
        l1_crossing_event = generate_li_gate_event(cr3bp, cr3bp.l1, "exit")
        l2_crossing_event = generate_li_gate_event(cr3bp, cr3bp.l2, "exit")

        # Initialize propagator
        prop = Cr3bpSynodicPropagator(cr3bp.mu)

        # Propagate initial state to L1 crossing:
        state0 = np.array([cr3bp.l1.position[0]+0.1, 0, 0, -1, 0, 0])
        _, _, _, state_li1 = prop.propagate([0, 10], state0, events=l1_crossing_event)

        # Test
        dx_l1gate = state_li1[0][0][0] - cr3bp.l1.position[0]
        self.assertAlmostEqual(dx_l1gate, 0.0, places=6)

        # # Propagate initial state to L2 crossing:
        state0 = np.array([cr3bp.l2.position[0] - 0.1, 0, 0, 1, 0, 0])
        _, _, _, state_li2 = prop.propagate([0, 10], state0, events=l2_crossing_event)

        # Test
        dx_l2gate = state_li2[0][0][0] - cr3bp.l2.position[0]
        self.assertAlmostEqual(dx_l2gate, 0.0, places=6)
