# pylint: disable = not-an-iterable, too-many-locals
"""
Dynamics in the N-body ephemeris model.

@author: Alberto FOSSA'
"""

from cffi import FFI
from numba import jit, prange
from numba.core.typing.cffi_utils import register_module
import numpy as np

try:  # import CSPICE functions from CFFI wrapper
    from sempy.core.utils.libs.cspice_wrapper import lib
    from sempy.core.utils.libs import cspice_wrapper
    register_module(cspice_wrapper)
    spkgps = lib.spkgps_c
    spkgeo = lib.spkgeo_c
except ImportError:  # CFFI wrapper not found, import CSPICE functions from loaded DLL
    cspice_wrapper = lib = None
    from sempy.core.utils.cspice_lib_loader import spkgps, spkgeo

ffi = FFI()  # FFI object to pass pointers to numpy array from within JIT functions


@jit('void(int32, float64, int8[::1], int32, float64[::1], float64[::1], float64[::1], '
     'float64[::1], float64)', nopython=True, nogil=True, fastmath=True)
def relative_position(tgt_id, e_t, ref, obs_id, r_ob, r_osc, r_bsc, lt_ob, l_ci):
    """Retrieves the relative position of a given body w.r.t. the origin and the relative position
    of the spacecraft w.r.t. the body.

    Parameters
    ----------
    tgt_id : int
        Target body NAIF ID.
    e_t : float
        Epoch time in ephemeris seconds.
    ref : ndarray
        Reference frame name encoded as a Numpy array of 8-bit integers.
    obs_id : int
        Observer body NAIF ID.
    r_ob : ndarray
        Relative position of `tgt` w.r.t. `obs` to be updated.
    r_osc : ndarray
        Relative position of the spacecraft w.r.t. `obs`.
    r_bsc : ndarray
        Relative position of the spacecraft w.r.t. `tgt` to be updated.
    lt_ob : ndarray
        Light time between `obs` and `tgt`, required by `spkgps`.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.

    """
    spkgps(tgt_id, e_t, ffi.from_buffer(ref), obs_id, ffi.from_buffer(r_ob),
           ffi.from_buffer(lt_ob))  # position of body b w.r.t. the origin [km]
    r_ob *= l_ci  # position of body b w.r.t. the origin [-]
    r_bsc[0:3] = r_osc - r_ob  # position of spacecraft w.r.t. body b [-]


@jit('void(int32, float64, int8[::1], int32, float64[::1], float64[::1], float64[::1], '
     'float64[::1], float64, float64)', nopython=True, nogil=True, fastmath=True)
def relative_state(tgt_id, e_t, ref, obs_id, state_ob, r_osc, r_bsc, lt_ob, t_c, l_ci):
    """Retrieves the relative state of a given body w.r.t. the origin and the relative position
    of the spacecraft w.r.t. the body.

    Parameters
    ----------
    tgt_id : int
        Target body NAIF ID.
    e_t : float
        Epoch time in ephemeris seconds.
    ref : ndarray
        Reference frame name encoded as a Numpy array of 8-bit integers.
    obs_id : int
        Observer body NAIF ID.
    state_ob : ndarray
        Relative state of `tgt` w.r.t. `obs` to be updated.
    r_osc : ndarray
        Relative position of the spacecraft w.r.t. `obs`.
    r_bsc : ndarray
        Relative position of the spacecraft w.r.t. `tgt` to be updated.
    lt_ob : ndarray
        Light time between `obs` and `tgt`, required by `spkgps`.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.

    """
    spkgeo(tgt_id, e_t, ffi.from_buffer(ref), obs_id, ffi.from_buffer(state_ob),
           ffi.from_buffer(lt_ob))  # state of body b w.r.t. the origin [km]
    state_ob *= l_ci
    state_ob[3:6] *= t_c  # state of body b w.r.t. the origin [-]
    r_bsc[0:3] = r_osc - state_ob[0:3]  # position of spacecraft w.r.t. body b [-]


@jit('float64(float64[::1], float64, float64[:, ::1])',
     nopython=True, nogil=True, cache=True, fastmath=True)
def gravity_gradient(r_b, gm_b, grav_grad):
    """Computes the partials of the gravitational acceleration due to a given body `b` storing
    the results in the input matrix `grav_grad`.

    Parameters
    ----------
    r_b : ndarray
        Relative position w.r.t. the body `b`.
    gm_b : float
        Standard gravitational parameter of body `b`.
    grav_grad : ndarray
        Gravity gradient components as a 3x3 matrix.

    Returns
    -------
    r_b2 :
        `r_b` squared norm.

    """

    r_b2 = r_b.dot(r_b)
    fac = 3.0 * gm_b * r_b2 ** -2.5
    grav_grad.ravel()[0:9:4] = gm_b * r_b2 ** -1.5 * (3.0 * r_b * r_b / r_b2 - 1.0)
    grav_grad[0, 1] = grav_grad[1, 0] = fac * r_b[0] * r_b[1]  # d2U/dxdy, d2U/dydx
    grav_grad[0, 2] = grav_grad[2, 0] = fac * r_b[0] * r_b[2]  # d2U/dxdz, d2U/dzdx
    grav_grad[1, 2] = grav_grad[2, 1] = fac * r_b[1] * r_b[2]  # d2U/dydz, d2U/dydz

    return r_b2


@jit('float64[::1](float64, float64[::1], float64[::1], int32[::1], int8[::1], float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def eqm_6_ephemeris(t_adim, state, gm_adim, body_ids, ref, t_c, l_ci):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations.

    The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses.

    The dynamics is expressed in an inertial reference frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions of the other ``n-1`` bodies w.r.t. the frame's origin are obtained from ephemeris
    data extrapolated using the SPICE routine `spkgps`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t_adim : float
        Current time in non-dimensional units.
    state : ndarray
        Current state in non-dimensional units.
    gm_adim : ndarray
        Standard gravitational parameters for the ``n`` considered bodies.
    body_ids : ndarray
        NAIF IDs for the ``n`` considered bodies.
    ref : ndarray
        Name of the inertial reference frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.

    Returns
    -------
    state_dot : ndarray
        First time derivative of `state`.

    """

    state_dot = np.zeros((6,))  # first time derivative of the state
    state_dot[0:3] = state[3:6]  # velocity as derivative of the position
    v_dot = state_dot[3:6]  # acceleration as derivative of the velocity
    r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

    r_ob = np.empty((3,))  # position of body b w.r.t. the origin
    r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
    lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgps)

    for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
        relative_position(body_ids[j], t_adim * t_c, ref, body_ids[0],
                          r_ob, r_osc, r_bsc, lt_ob, l_ci)  # update relative positions
        v_dot += - gm_adim[j] * (r_bsc * r_bsc.dot(r_bsc) ** -1.5 + r_ob * r_ob.dot(r_ob) ** -1.5)
    v_dot += - gm_adim[0] * r_osc * r_osc.dot(r_osc) ** -1.5  # update acceleration (origin)

    return state_dot


@jit('float64[::1](float64, float64[::1], float64[::1], int32[::1], int8[::1], float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def eqm_42_ephemeris(t_adim, state42, gm_adim, body_ids, ref, t_c, l_ci):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations.

    The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses and are propagated together with the
    corresponding 6x6 State Transition Matrix (STM).

    The dynamics is expressed in an inertial reference frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions of the other ``n-1`` bodies w.r.t. the frame's origin are obtained from ephemeris
    data extrapolated using the SPICE routine `spkgps`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t_adim : float
        Current time in non-dimensional units.
    state42 : ndarray
        Current state concatenated with STM in non-dimensional units.
    gm_adim : ndarray
        Standard gravitational parameters for the ``n`` considered bodies.
    body_ids : ndarray
        NAIF IDs for the ``n`` considered bodies.
    ref : ndarray
        Name of the inertial reference frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.

    Returns
    -------
    state42_dot : ndarray
        First time derivative of `state42`.

    """

    state42_dot = np.zeros((42,))  # first time derivative of the state
    state42_dot[0:3] = state42[3:6]  # velocity as derivative of the position
    v_dot = state42_dot[3:6]  # acceleration as derivative of the velocity
    r_osc = state42[0:3]  # position of the spacecraft w.r.t. the origin

    r_ob = np.empty((3,))  # position of body b w.r.t. the origin
    r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
    lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgps)
    grav_grad_bsc = np.empty((3, 3))  # gravity gradient of the spacecraft due to body b
    a_stm = np.zeros((6, 6))  # matrix A of the partials of the ODEs w.r.t. the state
    a_stm[0, 3] = a_stm[1, 4] = a_stm[2, 5] = 1.0  # NE 3x3 sub-matrix of A equal to identity 3x3

    for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
        relative_position(body_ids[j], t_adim * t_c, ref, body_ids[0],
                          r_ob, r_osc, r_bsc, lt_ob, l_ci)  # update relative positions
        r_bsc2 = gravity_gradient(r_bsc, gm_adim[j], grav_grad_bsc)  # gravity gradient (body b)
        v_dot += - gm_adim[j] * (r_ob * r_ob.dot(r_ob) ** -1.5 + r_bsc * r_bsc2 ** -1.5)
        a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (body b)

    r_osc2 = gravity_gradient(r_osc, gm_adim[0], grav_grad_bsc)  # gravity gradient (origin)
    v_dot += -gm_adim[0] * r_osc * r_osc2 ** -1.5  # update acceleration (origin)
    a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (origin)

    state42_dot[6:42] = a_stm.dot(state42[6:42].reshape((6, 6))).ravel()  # derivatives of STM

    return state42_dot


@jit('float64[::1](float64, float64[::1], float64[::1], int32[::1], int8[::1], float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def eqm_48_ephemeris(t_adim, state48, gm_adim, body_ids, ref, t_c, l_ci):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations.

    The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses and are propagated together with the
    corresponding 6x6 State Transition Matrix (STM) and the 6x1 partials derivatives of the state
    w.r.t. epoch time.

    The dynamics is expressed in an inertial reference frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions and velocities of the other ``n-1`` bodies w.r.t. the frame's origin are obtained
    from ephemeris data extrapolated using the SPICE routine `spkgeo`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t_adim : float
        Current time in non-dimensional units.
    state48 : ndarray
        Current state concatenated with STM and partial derivatives of the state w.r.t. epoch time
        in non-dimensional units.
    gm_adim : ndarray
        Standard gravitational parameters for the ``n`` considered bodies.
    body_ids : ndarray
        NAIF IDs for the ``n`` considered bodies.
    ref : ndarray
        Name of the inertial reference frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.

    Returns
    -------
    state48_dot : ndarray
        First time derivative of `state48`.

    """

    state48_dot = np.zeros((48,))  # first time derivative of the state
    state48_dot[0:3] = state48[3:6]  # velocity as derivative of the position
    v_dot = state48_dot[3:6]  # acceleration as derivative of the velocity
    r_osc = state48[0:3]  # position of the spacecraft w.r.t. the origin
    dx_dtau_dot = state48_dot[42:48]  # derivatives of the partials of the state w.r.t. epoch time

    state_ob = np.empty((6,))  # state of body b w.r.t. the origin
    r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
    lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgeo)
    grav_grad_bsc = np.empty((3, 3))  # gravity gradient of the spacecraft due to body b
    grav_grad_ob = np.empty((3, 3))  # gravity gradient of body b due to the origin
    a_stm = np.zeros((6, 6))  # matrix A of the partials of the ODEs w.r.t. the state
    df_dr = np.zeros((6, 3))  # partials of the ODEs w.r.t. the position of body b
    a_stm[0, 3] = a_stm[1, 4] = a_stm[2, 5] = 1.0  # NE 3x3 sub-matrix of A equal to identity 3x3

    for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
        relative_state(body_ids[j], t_adim * t_c, ref, body_ids[0], state_ob,
                       r_osc, r_bsc, lt_ob, t_c, l_ci)  # update relative state and positions

        # compute the gravity gradients (body b)
        r_bsc2 = gravity_gradient(r_bsc, gm_adim[j], grav_grad_bsc)
        r_ob2 = gravity_gradient(state_ob[0:3], gm_adim[j], grav_grad_ob)

        # update acceleration and matrix A (body b)
        v_dot += - gm_adim[j] * (state_ob[0:3] * r_ob2 ** -1.5 + r_bsc * r_bsc2 ** -1.5)
        a_stm[3:6, 0:3] += grav_grad_bsc

        # compute the partials of the ODEs w.r.t. the position of body b
        df_dr[3:6, 0:3] = grav_grad_ob - grav_grad_bsc

        # compute the derivatives of the partials of the state w.r.t. epoch time
        dx_dtau_dot += df_dr.dot(state_ob[3:6])

    r_osc2 = gravity_gradient(r_osc, gm_adim[0], grav_grad_bsc)  # gravity gradient (origin)
    v_dot += - gm_adim[0] * r_osc * r_osc2 ** -1.5  # update acceleration (origin)
    a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (origin)

    # derivatives of the partials of the state w.r.t. epoch time and derivatives of the STM
    dx_dtau_dot += a_stm.dot(state48[42:48])
    state48_dot[6:42] = a_stm.dot(state48[6:42].reshape((6, 6))).ravel()

    return state48_dot
