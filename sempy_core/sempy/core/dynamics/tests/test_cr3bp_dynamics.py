# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 17:21:07 2019

@author: Edgar PEREZ
"""

import unittest
import numpy as np
import csv
import os.path
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.orbits.thirdorder_orbit import ThirdOrderOrbit
import sempy.core.init.constants as cst

try:
    from sempy.core.dynamics.libs.cr3bp_dynamics import eqm_6_synodic, eqm_42_synodic
except ImportError:
    from sempy.core.dynamics.cr3bp_dynamics_jit import eqm_6_synodic, eqm_42_synodic

dirname = os.path.dirname(__file__)


class TestCr3bpDynamics(unittest.TestCase):

    def test_dynamics6(self):

        # The following data for the system Earth-Moon, cr3bp_derivatives_6,
        # was generated using SEMAT
        cr3bp_derivatives_6 = (0, 0.186527612353890, 0, 0.015363614500942, 0, -0.135929695436756)

        # the test begins for the system Earth-Moon
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        x, y, z = 1.116756261235440, 0, 0.0222299747777274
        vx, vy, vz = 0, 0.186527612353890, 0
        init_cond = np.array([x, y, z, vx, vy, vz])
        dot_state = eqm_6_synodic(0.0, init_cond, cr3bp.mu)

        for i in range(5):
            self.assertAlmostEqual(dot_state[i], cr3bp_derivatives_6[i], places=6)

    def test_dynamics42(self):

        # The following data for the system Earth-Moon, cr3bp_derivatives_42,
        # was generated using the equation of motion and STM in SEMAT
        filename = os.path.join(dirname, 'data', 'cr3bp_derivatives_42.csv')
        with open(filename, newline='') as file:
            cr3bp_derivatives_42 = list(csv.reader(file, delimiter=';'))
            cr3bp_derivatives_42 = np.array(cr3bp_derivatives_42, dtype=np.float)

        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)
        first_guess = ThirdOrderOrbit(orbit, t=0)
        state_stm = np.concatenate((first_guess.state0, cst.STM0), axis=None)
        dot_state_stm = eqm_42_synodic(0.0, state_stm, cr3bp.mu)

        # testing a tuple that is almost equal
        for i in range(42):
            self.assertAlmostEqual(dot_state_stm[i], cr3bp_derivatives_42[i][0], places=6)


if __name__ == '__main__':
    unittest.main()
