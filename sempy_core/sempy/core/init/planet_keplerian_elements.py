# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 11:55:59 2019

@author: Edgar PEREZ
"""

import os.path
import csv
import math
import numpy as np

import sempy.core.utils.csvdecomment as cvsdc
import sempy.core.init.constants as cst


dirname = os.path.dirname(__file__)


class PlanetKeplerianElements:
    """Will provide the planet orbital elements.

    From a .csv file this class will provide the JPL Keplerian Elements for Approximate Positions of
    the Major Planets.

    In order to do that, it only will be necessary to use the name of a planet as the class'
    argument.

    Given that the JPL keplerian elements provided for the Earth are actually the keplerian elements
    of the Earth-Moon barycenter, the name that will be provided to the class' argument will be
    'EM Bary' (this name is explicitly used by JPL in the csv file).

    Data contained in the .csv file comes from:
    https://ssd.jpl.nasa.gov/?planet_pos
    https://ssd.jpl.nasa.gov/txt/p_elem_t1.txt
    https://ssd.jpl.nasa.gov/txt/aprx_pos_planets.pdf

    The orbital period is derived from Kepler's Third Law.

    Parameters
    ----------
        planet : str
            A planet name, e.g 'Mars'

    Attributes
    ----------
        a : float
            Semi-major axis [km]
        e : float
            Eccentricity [radians]
        I : float
            Inclination [degrees]
        L : float
            Mean longitude [degrees]
        w : float
            Longitude of perihelion [degrees]
        W : float
            Longitude of the ascending node [degrees]
        T : float
            Orbital Period [s]

    """

    # Local pylint disabled commands
    # pylint: disable=invalid-name

    def __init__(self, planet='EM Bary'):
        """Inits PlanetKeplerianElements class."""
        self.a,\
        self.e,\
        self.I,\
        self.L,\
        self.w,\
        self.W = PlanetKeplerianElements.get_planet_kep_elements(planet)
        self.T = self.planet_period()

    @staticmethod
    def get_planet_kep_elements(planet='EM Bary'):
        """
        Method to get the jpl Keplerian Elements for Approximate Positions of the Major Planets.
        """
        filename = os.path.join(dirname, 'planetKepElem.csv')
        with open(filename, newline='') as file:
            planet_kep_elements = list(csv.reader(cvsdc.decomment(file), delimiter=';'))
            planet_kep_elem = np.array(planet_kep_elements)
            planets_name = planet_kep_elem[0:, 0]

        if planet != '' and planet in planets_name:
            # planet != '' is there to avoid using the second line (which name string is blank in
            # the csv file) corresponding to the planets keplerian elements rates
            i = np.where(planets_name == planet)
            # will return an array (i[0][0]) with the corresponding index (i) where the planet is
            # located within the csv file
            semi_major_axis = np.array(planet_kep_elem[i, 1], dtype=np.float) * cst.AU  # [km]
            eccentricity = np.array(planet_kep_elem[i, 2], dtype=np.float)  # [radians]
            inclination = np.array(planet_kep_elem[i, 3], dtype=np.float)  # [degrees]
            mean_longitude = np.array(planet_kep_elem[i, 4], dtype=np.float)  # [degrees]
            longitude_of_perihelion = np.array(planet_kep_elem[i, 5], dtype=np.float)  # [degrees]
            longitude_of_ascending_node = np.array(planet_kep_elem[i, 6],
                                                   dtype=np.float)  # [degrees]

        return semi_major_axis[0][0], eccentricity[0][0], inclination[0][0], mean_longitude[0][0], \
               longitude_of_perihelion[0][0], longitude_of_ascending_node[0][0]

    def planet_period(self):
        """Method that uses Kepler's Third Law to get the planet orbital period. """
        T = 2 * math.pi * ((self.a ** 3) / cst.SUN.GM) ** (1 / 2)

        return T  # [s]
