# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 10:59:29 2019

@author: Edgar PEREZ
"""

import os.path
import csv
import numpy as np

import sempy.core.init.constants as cst
import sempy.core.utils.csvdecomment as csvdc


dirname = os.path.dirname(__file__)


class SiderealPeriods:
    """"Will provide the celestial bodies sidereal periods.

    From a .csv file this class will provide the NASA celestial bodies sidereal periods.

    In order to do that, it only will be necessary to use the name of a body as the class' argument.

    Data contained in the .csv file comes from:
    https://nssdc.gsfc.nasa.gov/planetary/factsheet/
    https://ssd.jpl.nasa.gov/?planet_phys_par#B

    Args:
        body: A body name, e.g 'Mars'

    Attributes:
        Trot: Body's sidereal rotation period, is the time for one rotation of the body on its axis
            relative to the fixed stars.  A minus sign indicates retrograde rotation. [s].
        Torb: Body's sidereal orbit period, for its orbit around the Sun, relative to the fixed
            stars (for the MOON it is for its orbit about the Earth) [s].

    """
    # Local pylint disabled commands
    # pylint: disable=invalid-name, too-few-public-methods

    def __init__(self, body='Moon'):
        """Inits SiderealPeriods."""
        self.Trot, self.Torb = SiderealPeriods.get_sidereal_periods(body)

    @staticmethod
    def get_sidereal_periods(body='Moon'):
        """Method to get the sidereal periods of bodies."""
        filename = os.path.join(dirname, 'siderealPeriods.csv')
        with open(filename, newline='') as csvfile:
            bodies_sidereal_periods = list(csv.reader(csvdc.decomment(csvfile), delimiter=';'))
            bodies_sidereal_p = np.array(bodies_sidereal_periods)
            bodies_name = bodies_sidereal_p[0:, 0]

        if body != '' and body in bodies_name:
            # body != '' is there to avoid using the second line (which name string is blank in the
            # csv file) corresponding to the planets keplerian elements rates
            i = np.where(bodies_name == body)
            # will return an array (i[0][0]) with the corresponding index (i) where the body is
            # located within the csv file
            sidereal_rotation_p = np.array(bodies_sidereal_p[i, 1],
                                           dtype=np.float) * cst.DAYS2SEC  # [s]
            sidereal_orbital_p = np.array(bodies_sidereal_p[i, 2],
                                          dtype=np.float) * cst.DAYS2SEC  # [s]
        else:
            raise Exception

        return sidereal_rotation_p[0][0], sidereal_orbital_p[0][0]
