Moon's Spherical Harmonics ASCII Models derived from GRAIL mission available at
https://pds-geosciences.wustl.edu/grail/grail-l-lgrs-5-rdr-v1/grail_1001/shadr/

Model ID                ASCII file
GRGM660PRIM             gggrx_0660pm_sha.tab
GRGM900C_BOUGUER        gggrx_0900c_bouguer_sha.tab
GRGM900C                gggrx_0900c_sha.tab
GRGM1200A_BOUGUER       gggrx_1200a_bouguer_sha.tab
GRGM1200A               gggrx_1200a_sha.tab
GL0420A                 jggrx_0420a_sha.tab
GL0660B                 jggrx_0660b_sha.tab
GL0900C                 jggrx_0900c_sha.tab
GL0900D                 jggrx_0900d_sha.tab
GL1500E                 jggrx_1500e_sha.tab
