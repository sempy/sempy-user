#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 10:29:58 2019

@author: Alberto FOSSA'
"""

from scipy.constants import g
import numpy as np

from sempy.core.lowthrust.optimization.nlp import NLP
from sempy.core.lowthrust.utils.constants import STATES_NAMES, CONTROLS_NAMES


class TransferNLP(NLP):
    """TransferNLP class transcribes the continuous-time optimal control problem of finding the
    most fuel efficient transfer trajectory in the CR3BP framework.

    The transcription method, number of segments, transcription order, NLP solver and number of
    phases are set through the corresponding methods inherited from the `NLP` class.
    The constructor allows the user to specify the `Cr3bp` and `Spacecraft` objects while
    different methods allows to set up the states and controls variables options, the path
    constraints and the problem's objective function.
    Two methods are finally provided to specify the initial guess from which the optimization
    algorithms are initialized.
    An overview of those functions is provided below.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : int or tuple
        Number of segments in time in which the trajectory is split
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    cr3bp : Cr3bp
        `Cr3bp` Cr3bp
    spacecraft : Spacecraft
        `Spacecraft` object
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    pbm : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : int or tuple
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None
    transcription : GaussLobatto, Radau or list
        `GaussLobatto` or `Radau` objects
    trajectory : Trajectory
        `Trajectory` object
    phase : Phase or list
        `Phase` objects
    phase_name : str or list
        Name of the phase within OpenMDAO
    pbm_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [s]
    ac_cr3bp : float
        Characteristic acceleration [m/s^2]
    r_min : float
        Minimum distance from the second primary in non dimensional units [-]
    spacecraft : Spacecraft
        `Spacecraft` object
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    powered_kwargs : dict
        Constant parameters for the ODEs of a powered phase
    coast_kwargs : dict
        Constant parameters for the ODEs of a coasting phase
    state_nodes : ndarray or list
        States discretization nodes
    control_nodes : ndarray or list
        Controls discretization nodes
    t_state : ndarray or list
        Time vector for the states discretization nodes [-]
    t_control : ndarray or list
        Time vector for the controls discretization nodes [-]
    idx_state_control : ndarray or list
        Indices of `t_control` that corresponds to `t_state`

    See Also
    --------
    NLP : Transcribes a continuous-time optimal control problem in trajectory optimization into a
        Nonlinear Programming Problem (NLP) using the OpenMDAO and dymos packages.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                 snopt_opts=None, rec_file=None):
        """Initializes TransferNLP class. """

        NLP.__init__(self, transcription_method, nb_seg, order, solver, snopt_opts=snopt_opts,
                     rec_file=rec_file)

        # Cr3bp object and characteristics
        self.cr3bp = cr3bp
        self.tc_cr3bp = self.cr3bp.T / 2 / np.pi
        self.ac_cr3bp = 1000 * self.cr3bp.L * self.tc_cr3bp ** -2
        self.r_min = self.cr3bp.m2.Req/self.cr3bp.L

        self.spacecraft = spacecraft  # Spacecraft object

        # ODEs states and controls variables names
        self.states_names = STATES_NAMES
        self.controls_names = CONTROLS_NAMES

        # ODEs constant parameters
        self.powered_kwargs = {'mu': self.cr3bp.mu, 'Isp': self.spacecraft.isp,
                               'ac': self.ac_cr3bp, 'tc': self.tc_cr3bp}
        self.coast_kwargs = {'mu': self.cr3bp.mu}

        # initialization
        self.state_nodes = []
        self.control_nodes = []
        self.t_state = []
        self.t_control = []
        self.idx_state_control = []

    def set_time_options(self, t_bounds, t_init=0.0):
        """Sets the time options and bounds.

        The time options are imposed calling the `set_time_options` method implemented by the
        `Phase` object and they include a fixed initial time for the first phase in the
        trajectory and zero and unit reference values and lower and upper bounds on the transfer
        time of all phases.

        Parameters
        ----------
        t_bounds : ndarray
            Time of flight lower and upper bounds for each phase in the trajectory [-]
        t_init : float, optional
            Initial time [-]. Default is 0.0

        """

        if not isinstance(self.phase, list):  # single-phase trajectory
            self.phase.set_time_options(fix_initial=True, duration_ref0=np.mean(t_bounds),
                                        duration_ref=t_bounds[1],
                                        duration_bounds=t_bounds)
        else:  # multiple-phases trajectory
            duration_ref0 = np.mean(t_bounds, axis=1)
            duration_ref = t_bounds[:, 1]
            self.phase[0].set_time_options(fix_initial=True, duration_ref0=duration_ref0[0],
                                           duration_ref=duration_ref[0],
                                           duration_bounds=t_bounds[0])
            for i in range(1, np.size(duration_ref0)):
                initial_ref0 = t_init + np.sum(duration_ref0[:i])
                initial_ref = t_init + np.sum(duration_ref[:i])
                initial_bounds = t_init + np.sum(t_bounds[:i, :], axis=0)
                self.phase[i].set_time_options(initial_ref0=initial_ref0, initial_ref=initial_ref,
                                               initial_bounds=initial_bounds,
                                               duration_ref0=duration_ref0[i],
                                               duration_ref=duration_ref[i],
                                               duration_bounds=t_bounds[i])

    def set_pos_vel_options(self, fix_initial, fix_final, states_ref, phase=None):
        """Sets the position and velocity states variables options in the specified phase.

        For each state variable a unit-reference value is assigned according to the corresponding
        input parameter while the zero-reference values are computed assuming a transfer
        trajectory in the proximity of the second primary of a CR3BP system.
        The input parameters `fix_initial` and `fix_final` specify if the initial and final states
        have to be treated as fixed constants values (True) or design variables (False) during
        the optimization.

        Parameters
        ----------
        fix_initial : bool
            True if the values on the first discretization node are fixed, False otherwise
        fix_final : bool
            True if the values on the last discretization node are fixed, False otherwise
        states_ref : ndarray
            Unit-reference values for the states variables as ``[x, y, z, vx, vy, vz]``
        phase : Phase or None
            `Phase` object where the position and velocity options are set or None for
            single-phase trajectories. Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the position and velocity options are set

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory

        states_ref0 = np.hstack(((1.0 - self.cr3bp.mu), np.zeros(5)))  # zero-reference values

        for i in range(6):
            phase.set_state_options(self.states_names[i], fix_initial=fix_initial,
                                    fix_final=fix_final, ref0=states_ref0[i], ref=states_ref[i])

        return phase

    def set_mass_options(self, phase=None):
        """Sets the spacecraft mass options in the specified phase.

        Lower and upper bounds, reference and zero-reference values based on the `mass0`
        and `mass_dry` attributes of the `Spacecraft` object assigned to the Problem are set on
        the state variable `m` of the given phase.

        Parameters
        ----------
        phase : Phase or None
            `Phase` object where the mass options are set or None for single-phase trajectories.
            Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the mass options are set

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory

        mass_ref0 = (self.spacecraft.mass0 + self.spacecraft.mass_dry) * 0.5
        phase.set_state_options(self.states_names[6], fix_initial=True, ref0=mass_ref0,
                                ref=self.spacecraft.mass0, lower=self.spacecraft.mass_dry,
                                upper=self.spacecraft.mass0)

        return phase

    def set_path_constraints(self, phase=None):
        """Adds a path constraint on the thrust direction unit vector magnitude in the specified
        phase.

        Future path constraints can be added into this function using the same formalism.

        Parameters
        ----------
        phase : Phase or None
            `Phase` object where the constraint is imposed or None for single-phase trajectories.
            Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the constraint is imposed

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory

        # squared norm of the pointing vector equal to one
        phase.add_path_constraint('u2', lower=1.0, upper=1.0)

        return phase

    def set_controls(self, phase=None):
        """Sets the controls variables and design parameters options in the specified phase.

        The controls variables are the thrust magnitude `T` and the three components of the thrust
        direction `ux`, `uy` and `uz`.
        Lower and upper bounds are imposed on `T` based on the minimum and maximum thrust values
        specified by the `Spacecraft` object assigned to the problem.
        Lower and upper bounds on the thrust direction components are set equal to -1 and +1
        respectively.
        The only design parameter is the specific impulse `Isp` specified by the `Spacecraft`
        object and treated as a constant value rather than a design variable.

        Parameters
        ----------
        phase : Phase or None
            `Phase` object where the controls and design parameters are added or None for
            single-phase trajectories. Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the controls and design parameters are added

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory

        # thrust magnitude
        thrust_ref0 = (self.spacecraft.thrust_min + self.spacecraft.thrust_max) * 0.5
        phase.add_control(self.controls_names[0], continuity=False, rate_continuity=False,
                          rate2_continuity=False, ref0=thrust_ref0, ref=self.spacecraft.thrust_max,
                          lower=self.spacecraft.thrust_min, upper=self.spacecraft.thrust_max)

        # thrust direction
        for i in range(1, 4):
            phase.add_control(self.controls_names[i], continuity=True, rate_continuity=False,
                              rate2_continuity=False, lower=-1.0, upper=1.0)

        phase.add_design_parameter('Isp', val=self.spacecraft.isp, opt=False)

        return phase

    def set_objective(self, phase=None):
        """Sets the problem objective in the specified phase.

        The problem objective is chosen such that the NLP solution results in a mass-optimal
        transfer.
        Since by default the optimizer tries to minimize the objective function, the last is set
        equal to the opposite of the spacecraft mass on the last discretization node of the given
        phase.
        A different objective (ie energy-optimal or time-optimal transfers) can be imposed
        modifying the parameters passed to the `add_objective` method.

        Parameters
        ----------
        phase : Phase or None
            `Phase` object where the objective is imposed or None for single-phase trajectories.
            Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the objective is imposed

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory

        mass_ref0 = (self.spacecraft.mass_dry + self.spacecraft.mass0) * 0.5
        phase.add_objective(self.states_names[6], loc='final', ref0=mass_ref0,
                            ref=self.spacecraft.mass_dry)

        return phase

    def setup(self, phase=None):
        """Sets the path constraints, controls options, problem objective, jacobian type,
        linear solver and sets up the problem.

        Parameters
        ----------
        phase : Phase or None
            `Phase` object where the objective is imposed or None for single-phase trajectories.
            Default is None

        Returns
        -------
        phase : Phase
            `Phase` object where the objective is imposed

        """

        if phase is None:
            phase = self.phase  # unique Phase of a single-phase trajectory
        phase = self.set_path_constraints(phase=phase)
        phase = self.set_controls(phase=phase)
        phase = self.set_objective(phase=phase)
        self.setup_pbm()

        return phase

    def set_tof_guess(self, t_init, tof):
        """Sets the initial time and the time of flight initial guess for each `Phase` object in
        a given `Trajectory` to compute the time grid corresponding to the chosen transcription.

        The method takes as input the two parameters `t_init` and `tof` that specify the initial
        time and the time of flight for each phase in which the trajectory is split.
        For a single-phase trajectory those parameters have to be floats, while for
        multiple-phases trajectories they have to be one-dimensional arrays with the same size as
        the number of phases.
        During the optimization those values are then fixed or treated as design variables
        depending on the time options set with the `set_time_options` method.
        Once `t_init` and `tof` are specified the time grid corresponding to the chosen
        transcription is computed and stored in different class attributes:
        `states_nodes` and `controls_nodes` are arrays or lists of arrays with the indexes
        of the states and controls discretization nodes for each phase in the trajectory,
        while `t_state` and `t_control` are the corresponding time vectors.
        Finally, `idx_states_controls` stores the indexes of `t_control` that correspond to
        elements of `t_state`.

        Parameters
        ----------
        t_init : float or ndarray
            Initial time for each phase [-]
        tof : float or list
            Time of flight initial guess for each phase [-]

        See Also
        --------
        set_tof_guess_phase: Sets the initial time and the time of flight initial guess on a
            given `Phase`.

        """

        if isinstance(self.nb_seg, int):  # single-phase trajectory
            self.state_nodes, self.control_nodes, self.t_state, self.t_control,\
                self.idx_state_control = self.set_tof_guess_phase(t_init, tof, self.phase,
                                                                  self.phase_name)
        else:  # multiple-phases trajectory
            for i in range(len(self.nb_seg)):
                state_nodes, control_nodes, t_state, t_control, idx_state_control =\
                    self.set_tof_guess_phase(t_init[i], tof[i], self.phase[i], self.phase_name[i])
                self.state_nodes.append(state_nodes)
                self.control_nodes.append(control_nodes)
                self.t_state.append(t_state)
                self.t_control.append(t_control)
                self.idx_state_control.append(idx_state_control)

    def set_tof_guess_phase(self, t_init, tof, phase, phase_name):
        """Sets the initial time and the time of flight initial guess on a given `Phase`
        to compute the time grid corresponding to the chosen transcription.

        Parameters
        ----------
        t_init : float
            Initial time [-]
        tof : float
            Time of flight initial guess [-]
        phase : Phase
            `Phase` object
        phase_name : str
            Name of the phase within OpenMDAO

        Returns
        -------
        state_nodes : ndarray
            States discretization nodes
        control_nodes : ndarray
            Controls discretization nodes
        t_state : ndarray
            Time vector for the states discretization nodes [-]
        t_control : ndarray
            Time vector for the controls discretization nodes [-]
        idx_state_control : ndarray
            Indices of `t_control` that corresponds to `t_state`

        See Also
        --------
        set_tof_guess: Sets the initial time and the time of flight initial guess for each `Phase`
            object in the given `Trajectory`

        """

        # set the initial time and the time of flight initial guesses
        self.pbm[phase_name + '.t_initial'] = t_init
        self.pbm[phase_name + '.t_duration'] = tof

        self.pbm.run_model()  # run the model to compute the time grid
        t_all = self.pbm[phase_name + '.time']  # time vector for all nodes

        # states and controls nodes indices
        state_nodes =\
            phase.options['transcription'].grid_data.subset_node_indices['state_input']
        control_nodes =\
            phase.options['transcription'].grid_data.subset_node_indices['control_input']

        # time vectors for states and controls nodes
        t_control = np.take(t_all, control_nodes)
        t_state = np.take(t_all, state_nodes)

        # indices of the states time vector elements in the controls time vector
        idx_state_control = np.nonzero(np.isin(t_control, t_state))[0]

        return state_nodes, control_nodes, t_state, t_control, idx_state_control

    def set_initial_guess(self, states, controls, check_partials=False):
        """Sets the states and controls variables initial guesses for each `Phase` object in a
        given `Trajectory`.

        The input parameters `states` and `controls` have to provide the time series of all the
        states and controls variables on the controls discretization nodes for each phase in the
        trajectory.

        For single-phases trajectories they have to be arrays with a number of rows equal to the
        number of controls nodes and a number of columns equal to the number of states and
        controls respectively. For multiple-phases trajectories they have to be lists of arrays
        with the same number of elements as the number of phases.

        For powered phases the states are the three components of the position vector, the three
        components of the velocity vector and the spacecraft mass resulting in the
        seven-dimensional array ``[x, y, z, vx, vy, vz, m]`` while the controls are the thrust
        magnitude and the three components of the thrust direction resulting in the
        four-dimensional array ``[T, ux, uy, uz]``.
        For coasting phases the states are reduced to the six-dimensional array of positions and
        velocities components ``[x, y, z, vx, vy, vz]`` while the controls are specified as None.

        Finally, if `check_partials` is set to True the analytic derivatives implemented in the
        `compute_partials` method of the corresponding OpenMDAO `ExplicitComponent` are checked
        using complex step before proceeding with the optimization and eventual mismatches
        between analytic and numerical values are printed on the console.

        Parameters
        ----------
        states : ndarray or list
            States variables initial guess
        controls : ndarray or list
            Control variables initial guess
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False

        See Also
        --------
        set_initial_guess_phase: Sets the states and controls variables initial guesses in a
            given `Phase`.

        """

        if isinstance(self.nb_seg, int):  # single-phase trajectory
            self.set_initial_guess_phase(states, controls, self.control_nodes,
                                         self.idx_state_control, self.phase_name)
        else:  # multiple-phases trajectory
            for i in range(len(self.nb_seg)):
                self.set_initial_guess_phase(states[i], controls[i], self.control_nodes[i],
                                             self.idx_state_control[i], self.phase_name[i])

        self.pbm.run_model()

        if check_partials:
            self.pbm.check_partials(method='cs', compact_print=True, show_only_incorrect=True)

    def set_initial_guess_phase(self, states, controls, control_nodes, idx_state_control,
                                phase_name):
        """Sets the states and controls variables initial guesses in a given `Phase`.

        Parameters
        ----------
        states : ndarray
            States variables initial guess
        controls : ndarray or None
            Control variables initial guess or None for coasting arcs
        control_nodes : ndarray
            Controls discretization nodes
        idx_state_control : ndarray
            Indices of `t_control` that corresponds to `t_state`
        phase_name : str
            Name of the phase within OpenMDAO

        See Also
        --------
        set_initial_guess: Sets the states and controls variables initial guesses for each `Phase`
            object in a given `Trajectory`.

        """

        for i in range(6):
            self.pbm[phase_name + '.states:' + self.states_names[i]] =\
                np.reshape(np.take(states[:, i], idx_state_control), (len(idx_state_control), 1))

        if controls is not None:  # thrusting arcs

            self.pbm[phase_name + '.states:' + self.states_names[6]] =\
                np.reshape(np.take(states[:, 6], idx_state_control), (len(idx_state_control), 1))

            for i in range(4):
                self.pbm[phase_name + '.controls:' + self.controls_names[i]] =\
                    np.reshape(controls[:, i], (len(control_nodes), 1))

    def set_refined_guess(self, t_init, tof, states, controls, check_partials=False):
        """Sets the time, states and controls variables initial guesses for each `Phase` object in
        a given `Trajectory`.

        The input parameters `t_init` and `tof` specify the initial time and the time of flight
        for each phase in which the trajectory is split. For a single-phase trajectory those
        parameters have to be floats, while for multiple-phases trajectories they have to be
        one-dimensional arrays with the same size as the number of phases.

        The input parameters `states` and `controls` have provide the time series of all the states
        variables in the states discretization nodes and all the controls variables on the
        controls discretization nodes for each phase in the trajectory. For single-phases
        trajectories they have to be arrays with a number of rows equal to the number of controls
        nodes and a number of columns equal to the number of states and controls respectively.
        For multiple-phases trajectories they have to be lists of arrays with the same number of
        elements as the number of phases.

        For powered phases the states are the three components of the position vector, the three
        components of the velocity vector and the spacecraft mass resulting in the
        seven-dimensional array ``[x, y, z, vx, vy, vz, m]``
        while the controls are the thrust magnitude and the three components of the thrust
        direction resulting in the four-dimensional array ``[T, ux, uy, uz]``. For coasting
        phases the states are reduced to the six-dimensional array of positions and velocities
        components ``[x, y, z, vx, vy, vz]`` while the controls are specified as None.

        Finally, if `check_partials` is set to True the analytic derivatives implemented in the
        `compute_partials` method of the corresponding OpenMDAO `ExplicitComponent` are checked
        using complex step before proceeding with the optimization and eventual mismatches
        between the analytic and numerical values are printed on the console.

        Parameters
        ----------
        t_init : float or ndarray
            Initial time for each phase [-]
        tof : float or list
            Time of flight initial guess for each phase [-]
        states : ndarray or list
            States variables initial guess on the states discretization nodes
        controls : ndarray or list
            Control variables initial guess on the controls discretization nodes
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False

        See Also
        --------
        set_refined_guess_phase: Sets the time, states and controls variables initial guesses in a
            given `Phase`

        """

        if isinstance(self.nb_seg, int):
            self.set_refined_guess_phase(t_init, tof, states, controls, self.phase_name)

        else:
            for i in range(len(self.nb_seg)):
                self.set_refined_guess_phase(t_init[i], tof[i], states[i], controls[i],
                                             self.phase_name[i])

        self.pbm.run_model()

        if check_partials:
            self.pbm.check_partials(method='cs', compact_print=True, show_only_incorrect=True)

    def set_refined_guess_phase(self, t_init, tof, states, controls, phase_name):
        """Sets the time, states and controls variables initial guesses in a given `Phase`.

        Parameters
        ----------
        t_init : float
            Initial time [-]
        tof : float
            Time of flight initial guess [-]
        states : ndarray
            States variables initial guess on the states discretization nodes
        controls : ndarray or None
            Control variables initial guess on the controls discretization nodes or None for
            coasting arcs
        phase_name : str
            Name of the phase within OpenMDAO

        See Also
        --------
        set_refined_guess: Sets the time, states and controls variables initial guesses for each
            `Phase` object in a given `Trajectory`

        """

        # all phases
        self.pbm[phase_name + '.t_initial'] = t_init
        self.pbm[phase_name + '.t_duration'] = tof
        nb_states = len(states[:, 0])
        for i in range(6):
            self.pbm[phase_name + '.states:' + self.states_names[i]] =\
                np.reshape(states[:, i], (nb_states, 1))

        if controls is not None:  # thrusting arcs
            self.pbm[phase_name + '.states:' + self.states_names[6]] =\
                np.reshape(states[:, 6], (nb_states, 1))
            nb_controls = len(controls[:, 0])
            for i in range(4):
                self.pbm[phase_name + '.controls:' + self.controls_names[i]] =\
                    np.reshape(controls[:, i], (nb_controls, 1))
