#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 10:04:32 2019

@author: Alberto FOSSA'
"""

import numpy as np

from sempy.core.lowthrust.utils.constants import PHASES_NAMES
from sempy.core.lowthrust.optimization.nlp_transfer import TransferNLP
from sempy.core.lowthrust.dynamics.odes_group import ODEsSpacecraftLLO2LLO


class LLO2LLONLP(TransferNLP):
    """LLO2LLONLP transcribes the optimal control problem of finding the most fuel efficient
    transfer trajectory between two Low Lunar Orbits (LLO) in the CR3BP framework.

    The problem is modelled as a mass-optimal transfer between a fixed departure state in the
    initial orbit and an open arrival state in the target orbit.
    The design variables are the states and controls time series and the transfer time.
    The NLP is implemented as a single-phase trajectory as described in the documentation
    of `TransferNLP` and `NLP` parent classes.
    Final Boundary Conditions (BCs) are imposed on the spacecraft specific angular momentum and
    eccentricity vectors assuming a Keplerian orbit about the Moon to ensure that the transfer
    is ended on the imposed target orbit.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : int
        Number of segments in time in which the trajectory is split
    order : int
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    cr3bp : Cr3bp
        `Cr3bp` object
    spacecraft : Spacecraft
        `Spacecraft` object
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    pbm : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : int
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : int
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None
    transcription : GaussLobatto or Radau
        `GaussLobatto` or `Radau` object
    trajectory : Trajectory
        `Trajectory` object
    phase : Phase
        `Phase` object
    phase_name : str
        Name of the phase within OpenMDAO
    pbm_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [s]
    ac_cr3bp : float
        Characteristic acceleration [m/s^2]
    r_min : float
        Minimum distance from the second primary in non dimensional units [-]
    spacecraft : Spacecraft
        `Spacecraft` object
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    bcs_names : list
        Names of the variables where the final Boundary Conditions (BCs) are imposed
    powered_kwargs : dict
        Constant parameters for the ODEs of a powered phase
    coast_kwargs : dict
        Constant parameters for the ODEs of a coasting phase
    state_nodes : ndarray
        States discretization nodes
    control_nodes : ndarray
        Controls discretization nodes
    t_state : ndarray
        Time vector for the states discretization nodes [-]
    t_control : ndarray
        Time vector for the controls discretization nodes [-]
    idx_state_control : ndarray
        Indices of `t_control` that corresponds to `t_state`

    See Also
    --------
    NLP : Transcribes a continuous-time optimal control problem in trajectory optimization
        into a Nonlinear Programming Problem (NLP) using the OpenMDAO and dymos packages.
    TransferNLP : Transcribes the continuous-time optimal control problem of finding the
        most fuel efficient transfer trajectory in the CR3BP framework.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                 snopt_opts=None, rec_file=None):
        """Initializes LLO2LLONLP class. """

        TransferNLP.__init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                             snopt_opts=snopt_opts, rec_file=rec_file)

        self.bcs_names = ['hx', 'hy', 'hz', 'ex', 'ey', 'ez']
        llo_kwargs = self.powered_kwargs
        llo_kwargs['GM'] = self.cr3bp.m2.GM * (self.tc_cr3bp ** 2 / self.cr3bp.L ** 3)

        self.set_trajectory(ODEsSpacecraftLLO2LLO, llo_kwargs, PHASES_NAMES[0])

    def set_time_states(self, r_max, tof0, t_bounds):
        """Sets the time and states options and bounds.

        The time options are imposed calling the `set_time_options` method implemented by
        the `Phase` object and they include a fixed initial time and zero and unit reference
        values and lower and upper bounds on the transfer time.
        The positions and velocities options are set calling the `set_pos_vel_options` method
        implemented by `TransferNLP` and they comprise a fixed departure state and zero and unit
        reference values for the states variables among the phase.
        The mass options are set calling the `set_mass_options` implemented also by `TransferNLP`
        and they include zero and unit reference values and lower and upper bounds on the
        spacecraft mass during the transfer.

        Parameters
        ----------
        r_max : float
            Maximum distance from the second primary [km]
        tof0 : float
            Time of flight initial guess [-]
        t_bounds : array_like
            Transfer duration lower and upper bounds [-]

        """

        # time options
        self.phase.set_time_options(fix_initial=True, duration_ref0=tof0, duration_ref=t_bounds[1],
                                    duration_bounds=t_bounds)

        # position and velocity options
        r_max_adim = r_max/self.cr3bp.L
        v_circ_dim = (self.cr3bp.m2.GM / r_max) ** 0.5
        v_circ_adim = v_circ_dim * self.tc_cr3bp / self.cr3bp.L
        x_ref = (1.0 - self.cr3bp.mu + r_max_adim)
        states_ref = np.hstack((x_ref, np.ones(2)*r_max_adim, np.ones(3)*v_circ_adim))
        self.set_pos_vel_options(fix_initial=True, fix_final=False, states_ref=states_ref)

        # mass option
        self.set_mass_options()

    def set_target_orbit(self, h_vec, e_vec):
        """Impose the final Boundary Conditions as the target angular momentum vector H and
        eccentricity vector E.

        Parameters
        ----------
        h_vec : ndarray
            Target orbit specific angular momentum vector [-]
        e_vec : ndarray
            Target orbit eccentricity vector [-]

        """

        bc_val = np.hstack((h_vec, e_vec))
        for i in range(6):
            self.phase.add_boundary_constraint(self.bcs_names[i], loc='final',
                                               shape=(1,), equals=bc_val[i])


if __name__ == '__main__':

    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.lowthrust.utils.spacecraft import Spacecraft

    NLP_LLO = LLO2LLONLP('gauss-lobatto', 600, 3, 'IPOPT', Cr3bp(Primary.EARTH, Primary.MOON),
                         Spacecraft(1000, 1, 2000))

    print(NLP_LLO)
