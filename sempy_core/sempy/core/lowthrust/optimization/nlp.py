#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 09:31:27 2019

@author: Alberto FOSSA'
"""

from openmdao.api import Problem, Group, SqliteRecorder, DirectSolver, pyOptSparseDriver
from dymos import Phase, Trajectory, GaussLobatto, Radau

from sempy.core.lowthrust.utils.constants import TRAJ_NAME, CASE_REC_EXCLUDES


class NLP:
    """NLP class transcribes a continuous-time optimal control problem in trajectory optimization
    into a Nonlinear Programming Problem (NLP) using the OpenMDAO [1]_ and dymos [2]_ packages.

    The continuous-time trajectory is split into one or more subsequent phases and within
    each of them states, controls and design parameters are discretized according to the
    selected transcription method.
    Dymos provides both the High-order Gauss-Lobatto [3]_ and the Radau-Pseudospectral [4]_
    transcriptions.
    Within each phase is then possible to specify the number of segments in time in which the
    phase is split and the order of the interpolating polynomial used to approximate states,
    controls and design parameters inside each segment.

    NLP class allows to specify the transcription method for the problem of interest and the
    number of segments and the transcription order for each phase in which the trajectory is
    split.
    The number of phases is inferred from the dimensions of the `nb_seg` parameter that  has to
    be an int for single-phases trajectories or a tuple of ints for multiple-phases trajectories.
    The order of the interpolating polynomial given by the parameter `order` has also to be
    specified as int or tuple of ints.
    If `nb_seg` is a tuple while `order` is provided as int the same value for the order of the
    interpolating polynomial is replicated among all the subsequent phases whose number is
    inferred from the dimensions of `nb_seg`.

    Together with the transcription the user is also asked to specify the optimizer used to
    solve the NLP problem and optionally the file name of an SQL database that will be created
    during the problem setup to store one or more iterations and the final solution.
    The available NLP solvers are the ones supported by OpenMDAO and called through
    the `pyOptSparseDriver` object.
    For better performances is recommended to choose either ``IPOPT`` [5]_ [6]_
    or ``SNOPT`` [7]_, both provided by the pyOptSparse package [8]_.
    Finally, the optional parameter `snopt_opts` allows the user to override the ``SNOPT``
    default settings specifying a custom value for each optional parameter described in the
    ``SNOPT`` User Guide [9]_ as a key-value pair collected in a common `dict` structure.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : int or tuple
        Number of segments in time in which the trajectory is split
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    pbm : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : int or tuple
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None
    transcription : GaussLobatto, Radau or list
        `GaussLobatto` or `Radau` objects
    trajectory : Trajectory
        `Trajectory` object
    phase : Phase or list
        `Phase` objects
    phase_name : str or list
        Name of the phase within OpenMDAO
    pbm_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory

    References
    ----------
    .. [1] Gray, Justin S., et al. “OpenMDAO: An Open-Source Framework for Multidisciplinary
        Design, Analysis, and Optimization.” Structural and Multidisciplinary Optimization,
        vol. 59, no. 4, Apr. 2019, pp. 1075–104.
    .. [2] Hendricks, Eric S., et al. “Simultaneous Propulsion System and Trajectory Optimization.”
        18th AIAA/ISSMO Multidisciplinary Analysis and Optimization Conference,
        American Institute of Aeronautics and Astronautics, 2017.
    .. [3] Herman, Albert L., and Bruce A. Conway. “Direct Optimization Using Collocation
        Based on High-Order Gauss-Lobatto Quadrature Rules.”
        Journal of Guidance, Control, and Dynamics, vol. 19, no. 3, May 1996, pp. 592–99.
    .. [4] Garg, Divya, et al. “Direct Trajectory Optimization and Costate Estimation of
        General Optimal Control Problems Using a Radau Pseudospectral Method.”
        AIAA Guidance, Navigation, and Control Conference, American Institute of Aeronautics and
        Astronautics, 2009, p. 29.
    .. [5] Wachter, Andreas, and Lorenz T. Biegler. “On the Implementation of an
        Interior-Point Filter Line-Search Algorithm for Large-Scale Nonlinear Programming.”
        Mathematical Programming, vol. 106, no. 1, Mar. 2006, pp. 25–57.
    .. [6] HSL, A Collection of Fortran Codes for Large Scale Scientific Computation.
        http://www.hsl.rl.ac.uk/.
    .. [7] Gill, P., et al. “SNOPT: An SQP Algorithm for Large-Scale Constrained Optimization.”
        SIAM Review, vol. 47, no. 1, Jan. 2005, pp. 99–131.
    .. [8] Perez, Ruben E., et al. “PyOpt: A Python-Based Object-Oriented Framework for
        Nonlinear Constrained Optimization.” Structural and Multidisciplinary Optimization,
        vol. 45, no. 1, Jan. 2012, pp. 101–18.
    .. [9] Gill, Philip E., et al. User’s Guide for SNOPT Version 7.7: Software for Large-Scale
        Nonlinear Programming, Feb. 2019, p. 126.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, snopt_opts=None, rec_file=None):
        """Initializes NLP class. """

        # initialize and OpenMDAO Problem object that acts as a container for the NLP
        # formulation specified by the Trajectory object and the optimizer (Driver)
        self.pbm = Problem(model=Group())
        self.transcription_method = transcription_method

        if isinstance(order, tuple) and isinstance(nb_seg, int):
            raise TypeError('If the number of segments is an int then order must also be an int')
        if isinstance(order, tuple) and isinstance(nb_seg, tuple):
            if len(order) != len(nb_seg):
                raise ValueError('If tuples the number of segments and the order must be of '
                                 'the same length')
        if isinstance(order, int) and isinstance(nb_seg, tuple):
            order = tuple(order for _ in range(len(nb_seg)))

        self.order = order
        self.nb_seg = nb_seg
        self.solver = solver

        if self.solver == 'SNOPT':
            self.snopt_opts = snopt_opts
        else:
            self.snopt_opts = None

        if rec_file is not None:
            self.set_recorder(rec_file)
        self.rec_file = rec_file

        # initialization
        self.transcription = []
        self.trajectory = None
        self.phase = []
        self.phase_name = []
        self.pbm_exp = None

        self.set_driver()  # set the Problem driver (ie NLP solver)
        self.set_transcription()  # set the Problem transcription

    def set_recorder(self, rec_file, rec_iter=False):
        """Instantiates an `SqliteRecorder` object to be attached to the Problem.

        Parameters
        ----------
        rec_file : str
            Absolute path to the SQL database in which the trajectory will be recorded
        rec_iter : bool, optional
            True to record every iteration, False to record only the first and last steps.
            Default is False

        """

        recorder = SqliteRecorder(rec_file)

        # attach the SqliteRecorder to the Problem to record the time series at specified points
        self.pbm.add_recorder(recorder)
        opts = ['record_objectives', 'record_constraints', 'record_desvars']
        for opt in opts:
            self.pbm.recording_options[opt] = False
        self.pbm.recording_options['excludes'] = CASE_REC_EXCLUDES

        if rec_iter:  # attach the SqliteRecorder to the Driver to record each iteration
            self.pbm.driver.add_recorder(recorder)
            driver_opts = ['record_constraints', 'record_derivatives', 'record_desvars',
                           'record_inputs', 'record_model_metadata', 'record_objectives',
                           'record_responses']
            for opt in driver_opts:
                self.pbm.driver.recording_options[opt] = False
            self.pbm.driver.recording_options['includes'] = ['*.timeseries.*']

    def set_driver(self):
        """Sets the NLP solver used to perform the optimization. """

        self.pbm.driver = pyOptSparseDriver()
        self.pbm.driver.options['optimizer'] = self.solver
        self.pbm.driver.options['print_results'] = False
        self.pbm.driver.options['dynamic_derivs_sparsity'] = True

        if self.snopt_opts is not None:
            for k in self.snopt_opts.keys():
                self.pbm.driver.opt_settings[k] = self.snopt_opts[k]

        self.pbm.driver.declare_coloring(show_summary=True, show_sparsity=False)

    def set_transcription(self):
        """Sets the transcription method, number of segments and transcription order
        for each Phase.

        Initializes a `GaussLobatto` or `Radau` object for each `Phase` in the `Trajectory`
        specifying the number of segments in time in which the phase is split and the order of
        the interpolating polynomial according to the attributes `nb_seg` and `order`.

        """

        if self.transcription_method == 'gauss-lobatto':
            if isinstance(self.nb_seg, int):
                self.transcription = GaussLobatto(num_segments=self.nb_seg, order=self.order,
                                                  compressed=True)
            else:
                for i in range(len(self.nb_seg)):
                    trs = GaussLobatto(num_segments=self.nb_seg[i], order=self.order[i],
                                       compressed=True)
                    self.transcription.append(trs)
        elif self.transcription_method == 'radau-ps':
            if isinstance(self.nb_seg, int):
                self.transcription = Radau(num_segments=self.nb_seg, order=self.order,
                                           compressed=True)
            else:
                for i in range(len(self.nb_seg)):
                    trs = Radau(num_segments=self.nb_seg[i], order=self.order[i],
                                compressed=True)
                    self.transcription.append(trs)
        else:
            raise ValueError("Supported transcriptions methods are 'gauss-lobatto' and 'radau-ps'")

    def set_trajectory(self, ode_class, ode_kwargs, phase_name):
        """Sets the problem Trajectory, the ODEs and the input parameters.

        A `Trajectory` object is instantiated and set as the model subsystem of the
        OpenMDAO `Problem`.
        This object act as a container for all the phases in which the trajectory is split and
        manages the linkages between them.
        Those linkages connect together variables that appear with the same name in different
        phases and ensure continuity of their values at phase boundaries.

        For each phase in which the trajectory is split a corresponding `Phase` object is
        instantiated and added to `Trajectory`.
        Those objects are initialized specifying the ODEs that drive the dynamics of the
        phase through the `ode_class` parameter, the ODEs constant parameters given by
        `ode_kwargs` and the transcription method given by `transcription`.

        For single-phases trajectories the input parameter `ode_class` is and OpenMDAO
        `ExplicitComponent`, `ode_kwargs` a dict structure and `transcription` a `GaussLobatto`
        o `Radau` object instantiated calling the `set_transcription` method.
        For multiple-phases trajectories those inputs are list of the previously described
        objects with the same number of elements as the number of phases.

        Parameters
        ----------
        ode_class : ExplicitComponent, Group or tuple
            OpenMDAO `ExplicitComponent` or `Group` that defines the ODEs
        ode_kwargs : dict or tuple
            Constant parameters to be passed to the ODEs
        phase_name : str or tuple
            Name of the phase within OpenMDAO

        See Also
        --------
        set_transcription: Sets the transcription method, number of segments and transcription
        order for each Phase.

        """

        # initialize a Trajectory object and set it as the subsystem that handles the dynamics
        self.trajectory = self.pbm.model.add_subsystem(TRAJ_NAME, Trajectory())

        # initialize one or multiple Phase objects that defines the dynamics
        if isinstance(self.nb_seg, int):
            self.phase = \
                self.trajectory.add_phase(phase_name,
                                          Phase(ode_class=ode_class,
                                                ode_init_kwargs=ode_kwargs,
                                                transcription=self.transcription))
            self.phase_name = ''.join([TRAJ_NAME, '.', phase_name])
        else:
            for i in range(len(self.nb_seg)):
                phi = \
                    self.trajectory.add_phase(phase_name[i],
                                              Phase(ode_class=ode_class[i],
                                                    ode_init_kwargs=ode_kwargs[i],
                                                    transcription=self.transcription[i]))
                ph_name = ''.join([TRAJ_NAME, '.', phase_name[i]])
                self.phase.append(phi)
                self.phase_name.append(ph_name)

    def setup_pbm(self):
        """Assigns the jacobian type and the linear solver to the model and setup the problem. """

        self.pbm.model.options['assembled_jac_type'] = 'csc'  # sparse jacobian matrix
        self.pbm.model.linear_solver = DirectSolver(assemble_jac=True)
        self.pbm.setup(check=True, force_alloc_complex=True)

    def exp_sim(self, rec_file=None):
        """Explicitly simulates the transfer trajectory starting from the imposed initial
        conditions and the optimal controls time series.

        Parameters
        ----------
        rec_file : str or None, optional
            Absolute path for the SQL database in which the trajectory will be recorded or None.
            Default is None

        """

        if rec_file is not None:
            self.pbm_exp = self.trajectory.simulate(atol=1e-30, rtol=1e-30, record_file=rec_file)
        else:
            self.pbm_exp = self.trajectory.simulate(atol=1e-30, rtol=1e-30)

        self.cleanup()

    def cleanup(self):
        """Cleanup resources prior to exit. """

        self.trajectory.cleanup()
        self.pbm.driver.cleanup()
        self.pbm.cleanup()

    def __str__(self):
        """Prints info on the NLP solver and transcription. """

        lines = ['\n{:^40s}'.format('NLP characteristics:'),
                 '\n{:<25s}{:<15s}'.format('Solver:', self.solver),
                 '{:<25s}{:<15s}'.format('Transcription method:', self.transcription_method),
                 '{:<25s}{:<15s}'.format('Number of segments:', str(self.nb_seg)),
                 '{:<25s}{:<15s}'.format('Transcription order:', str(self.order))]

        printed = '\n'.join(lines)

        return printed
