#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 10:30:15 2019

@author: Alberto FOSSA'
"""

import numpy as np

from sempy.core.lowthrust.utils.constants import PHASES_NAMES
from sempy.core.lowthrust.optimization.nlp_transfer import TransferNLP
from sempy.core.lowthrust.dynamics.odes_group import ODEsSpacecraftJacobi, ODEsCr3bpJacobi


class NRHO2NRHOFixNLP(TransferNLP):
    """NRHO2NRHOFixNLP transcribes the optimal control problem of finding the most fuel efficient
    transfer trajectory between two Near Rectilinear Halo Orbits (NRHO) in the CR3BP framework.

    The problem is modelled as a mass-optimal transfer between fixed departure and arrival states
    in the initial and target orbits.
    Those states are selected to be the periselene of the inner NRHO and the aposelene of the
    outer one.
    The design variables are the states and controls time series and the transfer time.
    The NLP is implemented as a single-phase trajectory as described in the documentation of
    `TransferNLP` and `NLP` parent classes.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : int
        Number of segments in time in which the trajectory is split
    order : int
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    cr3bp : Cr3bp
        `Cr3bp` object
    spacecraft : Spacecraft
        `Spacecraft` object
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    pbm : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : int
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : int
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None
    transcription : GaussLobatto or Radau
        `GaussLobatto` or `Radau` object
    trajectory : Trajectory
        `Trajectory` object
    phase : Phase
        `Phase` object
    phase_name : str
        Name of the phase within OpenMDAO
    p_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [s]
    ac_cr3bp : float
        Characteristic acceleration [m/s^2]
    r_min : float
        Minimum distance from the second primary in non dimensional units [-]
    spacecraft : Spacecraft
        `Spacecraft` object
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    powered_kwargs : dict
        Constant parameters for the ODEs of a powered phase
    coast_kwargs : dict
        Constant parameters for the ODEs of a coasting phase
    state_nodes : ndarray
        States discretization nodes
    control_nodes : ndarray
        Controls discretization nodes
    t_state : ndarray
        Time vector for the states discretization nodes [-]
    t_control : ndarray
        Time vector for the controls discretization nodes [-]
    idx_state_control : ndarray
        Indices of `t_control` that corresponds to `t_state`

    See Also
    --------
    NLP : Transcribes a continuous-time optimal control problem in trajectory optimization into a
        Nonlinear Programming Problem (NLP) using the OpenMDAO and dymos packages.
    TransferNLP : Transcribes the continuous-time optimal control problem of finding the most fuel
        efficient transfer trajectory in the CR3BP framework.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                 snopt_opts=None, rec_file=None):
        """Initializes NRHO2NRHOFixNLP class. """

        TransferNLP.__init__(self, transcription_method, nb_seg, order, solver, cr3bp,
                             spacecraft, snopt_opts=snopt_opts, rec_file=rec_file)

        self.set_trajectory(ODEsSpacecraftJacobi, self.powered_kwargs, PHASES_NAMES[0])
        self.phase.add_timeseries_output('C')  # add Jacobi constant to the time series output

    def set_time_states(self, nrho, t_bounds):
        """Sets the time and states options and bounds.

        The time options are imposed calling the `set_time_options` method implemented by
        `TransferNLP` and they include a fixed initial time and zero and unit reference values
        and lower and upper bounds on the transfer time.
        The positions and velocities options are set calling the `set_pos_vel_options` method
        implemented by `TransferNLP` and they comprise fixed departure and arrival states and
        zero and unit reference values for the states variables among the phase.
        The mass options are set calling the `set_mass_options` implemented also by `TransferNLP`
        and they include zero and unit reference values and lower and upper bounds on the
        spacecraft mass during the transfer.

        Parameters
        ----------
        nrho : NRHO
            `NRHO` object from which the states reference values are computed
        t_bounds : ndarray
            Transfer duration lower and upper bounds [-]

        """

        self.set_time_options(t_bounds)  # time options

        # position and velocity options
        states_ref = np.max(np.fabs(nrho.state_vec), axis=0)  # unit-reference values for states
        self.set_pos_vel_options(fix_initial=True, fix_final=True, states_ref=states_ref)

        # mass options
        self.set_mass_options()


class NRHO2NRHOOpenArrNLP(TransferNLP):
    """NRHO2NRHOOpenArrNLP transcribes the optimal control problem of finding the most fuel
    efficient transfer trajectory between two Near Rectilinear Halo Orbits (NRHO) in the CR3BP
    framework.

    The problem is modelled as a mass-optimal transfer between fixed departure and arrival states
    in the initial and target orbits. Those states are selected to be at the periselene of the
    inner and outer NRHOs.
    The design variables are the states and controls time series and the total time of flight.
    The NLP is implemented as a two-phases trajectory as described in the documentation of
    `TransferNLP` and `NLP` parent classes.
    The first phase corresponds to a powered transfer from the periselene of the inner orbit to an
    open state in the outer orbit while the second phase describes a coasting arc on the target
    NRHO. The time at phase transition is treated as design variable to obtain a powered transfer
    trajectory from a fixed initial state to an open arrival state on the target NRHO.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : tuple
        Number of segments in time in which the trajectory is split
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    cr3bp : Cr3bp
        `Cr3bp` object
    spacecraft : Spacecraft
        `Spacecraft` object
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    pbm : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : tuple
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None
    transcription : list
        `GaussLobatto` or `Radau` objects list
    trajectory : Trajectory
        `Trajectory` object
    phase : list
        `Phase` objects list
    phase_name : list
        Name of the phases within OpenMDAO
    p_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [s]
    ac_cr3bp : float
        Characteristic acceleration [m/s^2]
    r_min : float
        Minimum distance from the second primary in non dimensional units [-]
    spacecraft : Spacecraft
        `Spacecraft` object
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    powered_kwargs : dict
        Constant parameters for the ODEs of a powered phase
    coast_kwargs : dict
        Constant parameters for the ODEs of a coasting phase
    state_nodes : list
        States discretization nodes for both phases
    control_nodes : list
        Controls discretization nodes for both phases
    t_state : list
        Time vector for the states discretization nodes [-]
    t_control : list
        Time vector for the controls discretization nodes [-]
    idx_state_control : list
        Indices of `t_control` that corresponds to `t_state`

    See Also
    --------
    NLP : Transcribes a continuous-time optimal control problem in trajectory optimization into a
        Nonlinear Programming Problem (NLP) using the OpenMDAO and dymos packages.
    TransferNLP : Transcribes the continuous-time optimal control problem of finding the most
        fuel efficient transfer trajectory in the CR3BP framework.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                 snopt_opts=None, rec_file=None):
        """Initializes NRHO2NRHOOpenArrNLP class. """

        TransferNLP.__init__(self, transcription_method, nb_seg, order, solver, cr3bp,
                             spacecraft, snopt_opts=snopt_opts, rec_file=rec_file)

        # ODEs and constant parameters for the initial powered phase and the final coasting phase
        odes = (ODEsSpacecraftJacobi, ODEsCr3bpJacobi)
        ode_kwargs = (self.powered_kwargs, self.coast_kwargs)

        link_vars = ['time']
        link_vars.extend(self.states_names[:6])  # variables to be linked between subsequent phases

        self.set_trajectory(odes, ode_kwargs, PHASES_NAMES[1])  # two-phases trajectory
        self.trajectory.link_phases(phases=list(PHASES_NAMES[1]), vars=link_vars)

        for i in range(2):  # add Jacobi constant to the time series output of both phases
            self.phase[i].add_timeseries_output('C')

    def set_time_states(self, nrho, t_bounds, t_init=0.0):
        """Sets the time and states options and bounds.

        The time options are imposed calling the `set_time_options` method implemented by
        `TransferNLP` and they include a fixed initial time for the first powered phase and zero
        and unit reference values and lower and upper bounds on the transfer time of both phases.
        The positions and velocities options are set calling the `set_pos_vel_options` method
        implemented by `TransferNLP` and they comprise fixed departure and arrival states for the
        first and second phases respectively and zero and unit reference values for the states
        variables among both phases.
        The mass options are set calling the `set_mass_options` implemented also by `TransferNLP`
        and they include zero and unit reference values and lower and upper bounds on the
        spacecraft mass for the first phase.

        Parameters
        ----------
        nrho : NRHO
            `NRHO` object from which the states reference values are computed
        t_bounds : ndarray
            Transfer duration lower and upper bounds for each phase [-]
        t_init : float, optional
            Initial time [-]. Default is 0.0

        """

        # time options on powered and coasting phases
        self.set_time_options(t_bounds, t_init)

        # positions and velocities options on powered and coasting phases
        states_ref = np.max(np.fabs(nrho.state_vec), axis=0)  # unit-reference values for states
        self.set_pos_vel_options(fix_initial=True, fix_final=False, states_ref=states_ref,
                                 phase=self.phase[0])
        self.set_pos_vel_options(fix_initial=False, fix_final=True, states_ref=states_ref,
                                 phase=self.phase[1])

        # spacecraft mass options on powered phase only
        self.set_mass_options(phase=self.phase[0])


class NRHO2NRHOOpenEndsNLP(TransferNLP):
    """NRHO2NRHOOpenEndsNLP transcribes the optimal control problem of finding the most fuel
    efficient transfer trajectory between two Near Rectilinear Halo Orbits (NRHO) in the CR3BP
    framework.

    The problem is modelled as a mass-optimal transfer between fixed departure and arrival states
    in the initial and target orbits. Those states are selected to be at the periselene of the
    inner and outer NRHOs.
    The design variables are the states and controls time series and the total time of flight.
    The NLP is implemented as a three-phases trajectory as described in the documentation of
    `TransferNLP` and `NLP` parent classes. The first phase corresponds to a complete revolution
    on the departure NRHO while the second phase describes a powered transfer from the periselene
    of the inner orbit to an open state in the outer one.
    Finally, the third phase describes a coasting arc on the target NRHO.
    The time at phase transition is treated as design variable to obtain a powered transfer
    trajectory between open states of the initial and target NRHOs.

    Parameters
    ----------
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    nb_seg : tuple
        Number of segments in time in which the trajectory is split
    order : int or tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    cr3bp : Cr3bp
        `Cr3bp` object
    spacecraft : Spacecraft
        `Spacecraft` object
    snopt_opts : dict, optional
        SNOPT optional parameters. Default is None for which the default values will be used
    rec_file : str or None, optional
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None

    Attributes
    ----------
    p : Problem
        OpenMDAO `Problem` object
    transcription_method : str
        Transcription method to discretize the continuous-time trajectory,
        allowed values are ``gauss-lobatto`` and ``radau-ps``
    order : tuple
        Order of the interpolating polynomial within each segment, must be an odd number
    nb_seg : tuple
        Number of segments in time in which the trajectory is split
    solver : str
        Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
    snopt_opts : dict or None
        SNOPT optional parameters or None to use the default values
    rec_file : str or None
        Absolute path to the SQL database in which the trajectory will be recorded or None.
        Default is None
    transcription : list
        `GaussLobatto` or `Radau` objects list
    trajectory : Trajectory
        `Trajectory` object
    phase : list
        `Phase` objects list
    phase_name : list
        Name of the phases within OpenMDAO
    p_exp : Problem
        OpenMDAO `Problem` object for the explicitly simulated trajectory
    cr3bp : Cr3bp
        `Cr3bp` object
    tc : float
        Characteristic time [s]
    ac : float
        Characteristic acceleration [m/s^2]
    r_min : float
        Minimum distance from the second primary in non dimensional units [-]
    spacecraft : Spacecraft
        `Spacecraft` object
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    powered_kwargs : dict
        Constant parameters for the ODEs of a powered phase
    coast_kwargs : dict
        Constant parameters for the ODEs of a coasting phase
    state_nodes : list
        States discretization nodes for all phases
    control_nodes : list
        Controls discretization nodes for all phases
    t_state : list
        Time vector for the states discretization nodes [-]
    t_control : list
        Time vector for the controls discretization nodes [-]
    idx_state_control : list
        Indices of `t_control` that corresponds to `t_state`

    See Also
    --------
    NLP : Transcribes a continuous-time optimal control problem in trajectory optimization into a
        Nonlinear Programming Problem (NLP) using the OpenMDAO and dymos packages.
    TransferNLP : Transcribes the continuous-time optimal control problem of finding the most fuel
        efficient transfer trajectory in the CR3BP framework.

    """

    def __init__(self, transcription_method, nb_seg, order, solver, cr3bp, spacecraft,
                 snopt_opts=None, rec_file=None):
        """Initializes NRHO2NRHOOpenEndsNLP class. """

        TransferNLP.__init__(self, transcription_method, nb_seg, order, solver, cr3bp,
                             spacecraft, snopt_opts=snopt_opts, rec_file=rec_file)

        # ODEs and constant parameters for the initial and final coasting arcs and
        # the intermediate powered phase
        odes = (ODEsCr3bpJacobi, ODEsSpacecraftJacobi, ODEsCr3bpJacobi)
        ode_kwargs = (self.coast_kwargs, self.powered_kwargs, self.coast_kwargs)

        link_vars = ['time']
        link_vars.extend(self.states_names[:6])  # variables to be linked between subsequent phases

        self.set_trajectory(odes, ode_kwargs, PHASES_NAMES[2])
        self.trajectory.link_phases(phases=list(PHASES_NAMES[2]), vars=link_vars)

        for i in range(3):  # add Jacobi constant to the time series output of all phases
            self.phase[i].add_timeseries_output('C')

    def set_time_states(self, nrho, t_bounds, t_init=0.0):
        """Sets the time and states options and bounds.

        The time options are imposed calling the `set_time_options` method implemented by
        `TransferNLP` and they include a fixed initial time for the first powered phase and zero
        and unit reference values and lower and upper bounds on the transfer time for all the
        phases.
        The positions and velocities options are set calling the `set_pos_vel_options` method
        implemented by `TransferNLP` and they comprise fixed departure and arrival states for the
        first and last phases respectively and zero and unit reference values for the states
        variables among all the phases.
        The mass options are set calling the `set_mass_options` implemented also by `TransferNLP`
        and they include zero and unit reference values and lower and upper bounds on the
        spacecraft mass for the second phase.

        Parameters
        ----------
        nrho : NRHO
            `NRHO` object from which the states reference values are computed
        t_bounds : ndarray
            Transfer duration lower and upper bounds for each phase [-]
        t_init : float, optional
            Initial time [-]. Default is 0.0

        """

        # time options on powered and coasting phases
        self.set_time_options(t_bounds, t_init)

        # positions and velocities options on powered and coasting phases
        states_ref = np.max(np.fabs(nrho.state_vec), axis=0)  # reference values for states
        self.set_pos_vel_options(fix_initial=True, fix_final=False, states_ref=states_ref,
                                 phase=self.phase[0])
        self.set_pos_vel_options(fix_initial=False, fix_final=False, states_ref=states_ref,
                                 phase=self.phase[1])
        self.set_pos_vel_options(fix_initial=False, fix_final=True, states_ref=states_ref,
                                 phase=self.phase[2])

        # spacecraft mass options on powered phase only
        self.set_mass_options(phase=self.phase[1])


if __name__ == '__main__':

    from sempy.core.init.primary import Primary
    from sempy.core.init.cr3bp import Cr3bp
    from sempy.core.lowthrust.utils.spacecraft import Spacecraft

    CR3BP_EM = Cr3bp(Primary.EARTH, Primary.MOON)
    SC = Spacecraft(1000, 1, 2000)

    NLP_FIX = NRHO2NRHOFixNLP('gauss-lobatto', 400, 3, 'IPOPT', CR3BP_EM, SC)
    NLP_OA = NRHO2NRHOOpenArrNLP('gauss-lobatto', (400, 400), 3, 'IPOPT', CR3BP_EM, SC)
    NLP_OE = NRHO2NRHOOpenEndsNLP('gauss-lobatto', (400, 400, 400), 3, 'IPOPT', CR3BP_EM, SC)

    print(NLP_FIX, NLP_OA, NLP_OE)
