# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 13:48:16 2019

@author: Edgar PEREZ, Alberto FOSSA'
"""

from enum import Enum
import numpy as np


class Cr3bpDynamics:
    """ Instances of this class will provide the dynamics for a CRTBP.

    By using this class, different dynamics, related to the Circular Restricted
    Three Body Problem, can be selected. This action can be performed by an
    instance of Eqm inner class, passed to the argument eqm_dimension.

    If eqm_dimension = Cr3bpDynamics.Eqm.dimensions6, equation of motion in the method
    eqm_6_synodic are selected.
    If eqm_dimension = Cr3bpDynamics.Eqm.dimensions42, equation of motion in the method
    eqm_42_synodic are selected.

    The equations of motion are coded in a way that can be passed to the class
    Propagate in order to be used by the integrator solve_ivp.

    Equations of motion can be found in chapter 2 of Koon et al.
    "Dynamical Systems, the Three-Body Problem and Space Mission Design" 2011
    (http://www.cds.caltech.edu/~marsden/volume/missiondesign/KoLoMaRo_DMissionBook_2011-04-25.pdf)

    Parameters
    ----------
    cr3bp : Cr3bp
        An object created with the Cr3bp class, e.g. cr3bp.
    eqm_dimension : object
        Equations of motion dimensions, e.g. Cr3bpDynamics.Eqm.dimensions42.

    Attributes
    ----------
    cr3bp : Cr3bp
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    ode : object
        Selected equations of motion, written as a set of first order ODEs.
    eqm_dimension.name : str
        Equations of motion dimensions selected.

    Examples
    --------
    Equation of motion in the method eqm_6_synodic are selected.

    >>> cr3bp_dyna = Cr3bpDynamics(cr3bp, Cr3bpDynamics.Eqm.dimensions6)

    Equation of motion in the method eqm_42_synodic are selected.

    >>> cr3bp_dyna42 = Cr3bpDynamics(cr3bp, Cr3bpDynamics.Eqm.dimensions42)

    """

    def __init__(self, cr3bp, eqm_dimension):
        """Inits Cr3bpDynamics class."""
        self.cr3bp = cr3bp

        # it is written in this way (without arguments) in order to pass the function to the
        # cr3bp_propagator, set of first order ODEs.
        if eqm_dimension == self.Eqm.dimensions6:
            self.ode = Cr3bpDynamics.eqm_6_synodic
        elif eqm_dimension == self.Eqm.dimensions42:
            self.ode = Cr3bpDynamics.eqm_42_synodic

        self.eqm_dimension = eqm_dimension  # Enum instance name

    class Eqm(Enum):
        """Eqm is an inner class that will be used to specify the equations of motion dimension.
        This class inherits from Enum.

        Options are the following instances:
            Cr3bpDynamics.Eqm.dimensions6
            Cr3bpDynamics.Eqm.dimensions42

        """

        dimensions6 = 6
        dimensions42 = 42

        def __init__(self, dimension):
            """Inits Eqm class."""
            self.dimension = dimension

    @staticmethod
    def u_bar_first_derivative(state, mu_cr3bp):
        """First partial derivative of the augmented potential U_bar.

        Parameters
        ----------
        state : iterable
            Orbit's state.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        u_bar_x : float
            Partial derivative of the augmented potential wrt x.
        u_bar_y : float
            Partial derivative of the augmented potential wrt y.
        u_bar_z : float
            Partial derivative of the augmented potential wrt z.

        """

        x_state, y_state, z_state = state[0:3]
        r_1 = (x_state + mu_cr3bp) ** 2 + y_state ** 2 + z_state ** 2
        r_2 = (x_state - 1 + mu_cr3bp) ** 2 + y_state ** 2 + z_state ** 2

        u_bar_x = mu_cr3bp * (x_state - 1 + mu_cr3bp) / r_2 ** (3 / 2) + \
            (1 - mu_cr3bp) * (x_state + mu_cr3bp) / r_1 ** (3 / 2) - x_state

        u_bar_y = mu_cr3bp * y_state / r_2 ** (3 / 2) + (1 - mu_cr3bp) * y_state / r_1 ** (3 / 2) \
            - y_state

        u_bar_z = mu_cr3bp * z_state / r_2 ** (3 / 2) + (1 - mu_cr3bp) * z_state / r_1 ** (3 / 2)

        return u_bar_x, u_bar_y, u_bar_z

    @staticmethod
    def u_bar_second_derivative(state, mu_cr3bp):
        """Second partial derivative of the augmented potential U_bar.

        Parameters
        ----------
        state : iterable
            Orbit's state.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        u_bar_xx : float
            Second partial derivative of the augmented potential wrt x, x.
        u_bar_xy : float
            Second partial derivative of the augmented potential wrt x, y.
        u_bar_xz : float
            Second partial derivative of the augmented potential wrt x, z.
        u_bar_yx : float
            Second partial derivative of the augmented potential wrt y, x.
        u_bar_yy : float
            Second partial derivative of the augmented potential wrt y, y.
        u_bar_yz : float
            Second partial derivative of the augmented potential wrt y, z.
        u_bar_zx : float
            Second partial derivative of the augmented potential wrt z, x.
        u_bar_zy : float
            Second partial derivative of the augmented potential wrt z, y.
        u_bar_zz : float
            Second partial derivative of the augmented potential wrt z, z.

        """

        x_state, y_state, z_state = state[0:3]
        r_1 = (x_state + mu_cr3bp) ** 2 + y_state ** 2 + z_state ** 2
        r_2 = (x_state - 1 + mu_cr3bp) ** 2 + y_state ** 2 + z_state ** 2

        u_bar_xx = mu_cr3bp / r_2 ** (3 / 2) + (1 - mu_cr3bp) / r_1 ** (3 / 2) - \
            3 * mu_cr3bp * (x_state - 1 + mu_cr3bp) ** 2 / r_2 ** (5 / 2) - \
            3 * (x_state + mu_cr3bp) ** 2 * (1 - mu_cr3bp) / r_1 ** (5 / 2) - 1

        u_bar_xy = 3 * y_state * (mu_cr3bp + x_state) * (mu_cr3bp - 1) / r_1 ** (5 / 2) - \
            3 * mu_cr3bp * y_state * (x_state - 1 + mu_cr3bp) / r_2 ** (5 / 2)

        u_bar_xz = 3 * z_state * (mu_cr3bp + x_state) * (mu_cr3bp - 1) / r_1 ** (5 / 2) - \
            3 * mu_cr3bp * z_state * (x_state - 1 + mu_cr3bp) / r_2 ** (5 / 2)

        u_bar_yx = u_bar_xy

        u_bar_yy = mu_cr3bp / r_2 ** (3 / 2) - (mu_cr3bp - 1) / r_1 ** (3 / 2) + \
            3 * y_state ** 2 * (mu_cr3bp - 1) / r_1 ** (5 / 2) - \
            3 * mu_cr3bp * y_state ** 2 / r_2 ** (5 / 2) - 1

        u_bar_yz = 3 * y_state * z_state * (mu_cr3bp - 1) / r_1 ** (5 / 2) - \
            3 * mu_cr3bp * y_state * z_state / r_2 ** (5 / 2)

        u_bar_zx = u_bar_xz
        u_bar_zy = u_bar_yz

        u_bar_zz = mu_cr3bp / r_2 ** (3 / 2) - (mu_cr3bp - 1) / r_1 ** (3 / 2) + \
            3 * z_state ** 2 * (mu_cr3bp - 1) / r_1 ** (5 / 2) - \
            3 * mu_cr3bp * z_state ** 2 / r_2 ** (5 / 2)

        return u_bar_xx, u_bar_xy, u_bar_xz, u_bar_yx, u_bar_yy, u_bar_yz, u_bar_zx, u_bar_zy,\
            u_bar_zz

    @staticmethod
    def eqm_6_synodic(state, mu_cr3bp):
        """Equations of motion written as a set of first order ODEs.

        Parameters
        ----------
        state : iterable
            Orbit's state.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        dot_x1 : float
            Time derivative of the first state component.
        dot_x2 : float
            Time derivative of the second state component.
        dot_x3 : float
            Time derivative of the third state component.
        dot_x4 : float
            Time derivative of the fourth state component.
        dot_x5 : float
            Time derivative of the fifth state component.
        dot_x6 : float
            Time derivative of the sixth state component.

        """

        vx_state, vy_state, vz_state = state[3:6]
        u_bar_x, u_bar_y, u_bar_z = Cr3bpDynamics.u_bar_first_derivative(state, mu_cr3bp)

        dot_x1 = vx_state
        dot_x2 = vy_state
        dot_x3 = vz_state
        dot_x4 = 2 * vy_state - u_bar_x
        dot_x5 = - 2 * vx_state - u_bar_y
        dot_x6 = - u_bar_z

        return dot_x1, dot_x2, dot_x3, dot_x4, dot_x5, dot_x6

    @staticmethod
    def eqm_42_synodic(state_stm, mu_cr3bp):
        """Equations of motion written as a set of first order ODEs.

        Parameters
        ----------
        state_stm : iterable
            Orbit's state (6-dim) concatenated with 36-dim STM.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        fode42 : ndarray
            Set of 42 first order time derivative of the 6-dim state and 36-dim STM.

        """

        # state_stm = Third-order orbit state0 together with STM (phi)
        # state = state_stm[0:6]
        vx_state, vy_state, vz_state, *phi = state_stm[3:]
        u_bar_x, u_bar_y, u_bar_z = Cr3bpDynamics.u_bar_first_derivative(state_stm[0:6], mu_cr3bp)

        # second partial derivatives of the augmented potential as 3 x 3 matrix
        d2_u_bar = Cr3bpDynamics.u_bar_second_derivative(state_stm[0:6], mu_cr3bp)
        d2_u_bar = np.array(d2_u_bar).reshape(3, 3)

        # equivalent to eqm_6_synodic
        dot_x1 = vx_state
        dot_x2 = vy_state
        dot_x3 = vz_state
        dot_x4 = 2 * vy_state - u_bar_x
        dot_x5 = - 2 * vx_state - u_bar_y
        dot_x6 = - u_bar_z

        # set of 6 first order ODEs
        fode_6_dimension = np.array([dot_x1, dot_x2, dot_x3, dot_x4, dot_x5, dot_x6])

        # first order ODEs for the STM (linear relationship between STM and its first time
        # derivative)
        omega = np.zeros((3, 3))
        omega[0, 1] = 1
        omega[1, 0] = -1

        a_stm = np.concatenate((np.concatenate((np.zeros((3, 3)), np.eye(3)), axis=1),
                                np.concatenate((-d2_u_bar, 2 * omega), axis=1)), axis=0)

        phi = np.array(phi).reshape(6, 6)  # STM as 6 x 6 matrix
        dot_phi = np.dot(a_stm, phi)  # first time derivative of the STM as 6 x 6 matrix

        # set of 42 first order ODEs.
        fode42 = np.concatenate((fode_6_dimension, dot_phi), axis=None)

        return fode42
