#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 13:49:35 2019

@author: Alberto FOSSA'
"""

import numpy as np
from dymos import declare_time, declare_state

from sempy.core.lowthrust.dynamics.comp import Component


@declare_time(targets=['t'])
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
class Syn2LCI(Component):
    """Defines the equations to convert the spacecraft state vector expressed in synodic reference
    frame into the spacecraft state vector expressed in Lunar Centred Inertial (LCI) reference
    frame using the Dymos syntax.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    t : ndarray
        Time [-]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]

    Returns
    -------
    xlci : ndarray
        Position along x axis in LCI frame [-]
    ylci : ndarray
        Position along y axis in LCI frame [-]
    zlci : ndarray
        Position along z axis in LCI frame [-]
    vxlci : ndarray
        Velocity along x axis in LCI frame [-]
    vylci : ndarray
        Velocity along y axis in LCI frame [-]
    vzlci : ndarray
        Velocity along z axis in LCI frame [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def setup(self):
        """Defines the equations inputs, outputs and jacobian components. """

        num_nodes = self.options['num_nodes']
        nodes_range = np.arange(self.options['num_nodes'])

        # equations inputs
        for k in ('t', 'x', 'y', 'z', 'vx', 'vy', 'vz'):
            self.add_input(k, val=np.zeros(num_nodes))

        # equations outputs
        for k in ('xlci', 'ylci', 'zlci', 'vxlci', 'vylci', 'vzlci'):
            self.add_output(k, val=np.zeros(num_nodes))

        # partial derivatives of the equations outputs respect to the equations inputs
        for k in ('x', 'y', 't'):
            self.declare_partials(of='xlci', wrt=k, rows=nodes_range, cols=nodes_range)
            self.declare_partials(of='ylci', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('x', 'y', 'vx', 'vy', 't'):
            self.declare_partials(of='vxlci', wrt=k, rows=nodes_range, cols=nodes_range)
            self.declare_partials(of='vylci', wrt=k, rows=nodes_range, cols=nodes_range)

        self.declare_partials(of='zlci', wrt='z', rows=nodes_range, cols=nodes_range, val=1.0)
        self.declare_partials(of='vzlci', wrt='vz', rows=nodes_range, cols=nodes_range, val=1.0)

    def compute(self, inputs, outputs):
        """Computes the equations outputs. """

        cos_t = np.cos(inputs['t'])
        sin_t = np.sin(inputs['t'])
        x_moon = inputs['x'] - 1.0 + self.options['mu']

        outputs['xlci'] = x_moon * cos_t - inputs['y'] * sin_t
        outputs['ylci'] = x_moon * sin_t + inputs['y'] * cos_t
        outputs['zlci'] = inputs['z']

        outputs['vxlci'] = -x_moon * sin_t - inputs['y'] * cos_t + inputs['vx'] * cos_t - \
            inputs['vy'] * sin_t
        outputs['vylci'] = x_moon * cos_t - inputs['y'] * sin_t + inputs['vx'] * sin_t + \
            inputs['vy'] * cos_t
        outputs['vzlci'] = inputs['vz']

    def compute_partials(self, inputs, jacobian):
        """Compute the jacobian components. """

        cos_t = np.cos(inputs['t'])
        sin_t = np.sin(inputs['t'])
        x_moon = inputs['x'] - 1.0 + self.options['mu']

        jacobian['xlci', 'x'] = cos_t
        jacobian['xlci', 'y'] = - sin_t
        jacobian['xlci', 't'] = - x_moon * sin_t - inputs['y'] * cos_t

        jacobian['ylci', 'x'] = sin_t
        jacobian['ylci', 'y'] = cos_t
        jacobian['ylci', 't'] = x_moon*cos_t - inputs['y'] * sin_t

        jacobian['vxlci', 'x'] = - sin_t
        jacobian['vxlci', 'y'] = - cos_t
        jacobian['vxlci', 'vx'] = cos_t
        jacobian['vxlci', 'vy'] = - sin_t
        jacobian['vxlci', 't'] = - x_moon * cos_t + inputs['y'] * sin_t - inputs['vx'] * sin_t - \
            inputs['vy'] * cos_t

        jacobian['vylci', 'x'] = cos_t
        jacobian['vylci', 'y'] = - sin_t
        jacobian['vylci', 'vx'] = sin_t
        jacobian['vylci', 'vy'] = cos_t
        jacobian['vylci', 't'] = - x_moon * sin_t - inputs['y'] * cos_t + inputs['vx'] * cos_t - \
            inputs['vy'] * sin_t


class LCI2HE(Component):
    """Defines the equations to compute the specific angular momentum and eccentricity vectors
    using the Dymos syntax.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    GM : float
        Central body standard gravitational parameter [-]
    xlci : ndarray
        Position along x axis in LCI frame [-]
    ylci : ndarray
        Position along y axis in LCI frame [-]
    zlci : ndarray
        Position along z axis in LCI frame [-]
    vxlci : ndarray
        Velocity along x axis in LCI frame [-]
    vylci : ndarray
        Velocity along y axis in LCI frame [-]
    vzlci : ndarray
        Velocity along z axis in LCI frame [-]

    Returns
    -------
    hx : ndarray
        Specific angular momentum along x axis [-]
    hy : ndarray
        Specific angular momentum along y axis [-]
    hz : ndarray
        Specific angular momentum along z axis [-]
    ex : ndarray
        Eccentricity vector x component [-]
    ey : ndarray
        Eccentricity vector y component [-]
    ez : ndarray
        Eccentricity vector z component [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def initialize(self):
        """Initializes LCI2HE class. """

        self.options.declare('num_nodes', types=int)
        self.options.declare('GM', types=float)

    def setup(self):
        """Defines the equations inputs, outputs and jacobian components. """

        num_nodes = self.options['num_nodes']
        nodes_range = np.arange(self.options['num_nodes'])

        # equations inputs
        for k in ('xlci', 'ylci', 'zlci', 'vxlci', 'vylci', 'vzlci'):
            self.add_input(k, val=np.zeros(num_nodes))

        # equations outputs
        for k in ('hx', 'hy', 'hz', 'ex', 'ey', 'ez'):
            self.add_output(k, val=np.zeros(num_nodes))

        # partial derivatives of the ODE outputs respect to the ODE inputs
        for k in ('ylci', 'zlci', 'vylci', 'vzlci'):
            self.declare_partials(of='hx', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('xlci', 'zlci', 'vxlci', 'vzlci'):
            self.declare_partials(of='hy', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('xlci', 'ylci', 'vxlci', 'vylci'):
            self.declare_partials(of='hz', wrt=k, rows=nodes_range, cols=nodes_range)

        # for the partial derivatives of the eccentricity vector components use complex step method
        for k in ('xlci', 'ylci', 'zlci', 'vxlci', 'vylci', 'vzlci'):
            self.declare_partials(of='ex', wrt=k, method='cs')
            self.declare_partials(of='ey', wrt=k, method='cs')
            self.declare_partials(of='ez', wrt=k, method='cs')

    def compute(self, inputs, outputs):
        """Compute the equations outputs. """

        # specific angular momentum vector components
        outputs['hx'] = inputs['ylci'] * inputs['vzlci'] - inputs['zlci'] * inputs['vylci']
        outputs['hy'] = inputs['zlci'] * inputs['vxlci'] - inputs['xlci'] * inputs['vzlci']
        outputs['hz'] = inputs['xlci'] * inputs['vylci'] - inputs['ylci'] * inputs['vxlci']

        # velocity norm squared
        v_2 = inputs['vxlci'] ** 2 + inputs['vylci'] ** 2 + inputs['vzlci'] ** 2

        # dot product between R and V
        rvr = inputs['xlci'] * inputs['vxlci'] + inputs['ylci'] * inputs['vylci'] + \
            inputs['zlci'] * inputs['vzlci']

        # position vector norm
        r_moon = (inputs['xlci'] ** 2 + inputs['ylci'] ** 2 + inputs['zlci'] ** 2) ** 0.5

        # eccentricity vector components
        trm = v_2 - self.options['GM'] / r_moon
        outputs['ex'] = (trm * inputs['xlci'] - rvr * inputs['vxlci']) / self.options['GM']
        outputs['ey'] = (trm * inputs['ylci'] - rvr * inputs['vylci']) / self.options['GM']
        outputs['ez'] = (trm * inputs['zlci'] - rvr * inputs['vzlci']) / self.options['GM']

    def compute_partials(self, inputs, jacobian):
        """Compute the jacobian components. """

        jacobian['hx', 'ylci'] = inputs['vzlci']
        jacobian['hx', 'zlci'] = - inputs['vylci']
        jacobian['hx', 'vylci'] = - inputs['zlci']
        jacobian['hx', 'vzlci'] = inputs['ylci']

        jacobian['hy', 'xlci'] = - inputs['vzlci']
        jacobian['hy', 'zlci'] = inputs['vxlci']
        jacobian['hy', 'vxlci'] = inputs['zlci']
        jacobian['hy', 'vzlci'] = - inputs['xlci']

        jacobian['hz', 'xlci'] = inputs['vylci']
        jacobian['hz', 'ylci'] = - inputs['vxlci']
        jacobian['hz', 'vxlci'] = - inputs['ylci']
        jacobian['hz', 'vylci'] = inputs['xlci']
