#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 10:18:34 2019

@author: Alberto FOSSA'
"""

from enum import Enum
from sempy.core.lowthrust.dynamics.cr3bp_dynamics_old import Cr3bpDynamics


class ArcLengthDynamics(Cr3bpDynamics):
    """Provides the dynamics for a CR3BP augmented to compute the arc length along one orbit.

    Parameters
    ----------
    cr3bp : Cr3bp
        An object created with the Cr3bp class, e.g. cr3bp.

    Attributes
    ----------
    cr3bp : Cr3bp
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    ode : object
        Equations of motion, written as a set of first order ODEs.
    eqm_dimension.name : str
        Equations of motion dimensions selected.

    """

    def __init__(self, cr3bp):
        """Inits ArcLengthDynamics class. """

        self.ode = ArcLengthDynamics.eqm_7_synodic
        Cr3bpDynamics.__init__(self, cr3bp, self.Eqm7.dimension7)

    class Eqm7(Enum):
        """Eqm7 is an inner class that will be used to specify the equations of motion dimension.
        This class inherits from Enum.

        """

        dimension7 = 7

        def __init__(self, dimension):
            """Inits Eqm7 class. """
            self.dimension = dimension

    @staticmethod
    def eqm_7_synodic(state7, mu_cr3bp):
        """Defines the Equations of Motion (EOMs) of a unit mass spacecraft subject only to the
        gravitational attraction of two primary bodies in the CR3BP framework in a synodic
        reference frame and non dimensional units computing the arc length along the orbit.

        Parameters
        ----------
        state7 : ndarray
            Orbit's state augmented with arch length.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        state_7_dot : ndarray
            First time derivatives of the augmented orbit's state.

        """

        # first 6 components from eqm_6_synodic
        dot_x, dot_y, dot_z, dot_vx, dot_vy, dot_vz =\
            Cr3bpDynamics.eqm_6_synodic(state7[0:6], mu_cr3bp)

        # first time derivative of arc length
        vx_state, vy_state, vz_state = state7[3:6]
        dot_l = (vx_state ** 2 + vy_state ** 2 + vz_state ** 2) ** 0.5

        state_7_dot = [dot_x, dot_y, dot_z, dot_vx, dot_vy, dot_vz, dot_l]

        return state_7_dot


def arc_length_event(_, state7, arc_length):
    """Event to determine when the arc length reaches the specified value.

    Parameters
    ----------
    _ : float
        Time handled implicitly by the propagator.
    state7 : ndarray
        Orbit's state augmented with arch length.
    arc_length : float
        Arc length at which the event is triggered.

    """

    return state7[-1] - arc_length


def arc_length_samples(samples):
    """List of events to sample an orbit at specified arc length values.

    Parameters
    ----------
    samples : ndarray
        Arc length values at which the events are triggered and the orbit samples stored.

    Returns
    -------
    events : list
        List of events to be triggered.

    """

    events = [lambda t, state, length=length: state[-1] - length for length in samples]

    return events
