#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 18:38:52 2019

@author: Alberto FOSSA'
"""

import numpy as np
from scipy.constants import g
from dymos import declare_time, declare_state, declare_parameter

from sempy.core.lowthrust.dynamics.cr3bp_dynamics_old import Cr3bpDynamics
from sempy.core.lowthrust.dynamics.comp import Component


@declare_time()
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
class ODEsCr3bp(Component):
    """Defines the ODEs for a unit mass particle in the CR3BP framework expressed in a synodic
    reference frame and non dimensional units using the Dymos syntax.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]

    Returns
    -------
    xdot : ndarray
        Time derivative of x [-]
    ydot : ndarray
        Time derivative of y [-]
    zdot : ndarray
        Time derivative of z [-]
    vxdot : ndarray
        Time derivative of vx [-]
    vydot : ndarray
        Time derivative of vy [-]
    vzdot : ndarray
        Time derivative of vz [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def setup(self):
        """Defines the ODEs inputs, outputs and jacobian components. """

        num_nodes = self.options['num_nodes']
        nodes_range = np.arange(self.options['num_nodes'])

        # inputs (ODE right-hand side)
        for k in ('x', 'y', 'z', 'vx', 'vy', 'vz'):
            self.add_input(k, val=np.zeros(num_nodes))

        # outputs (ODE left-hand side)
        for k in ('xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot'):
            self.add_output(k, val=np.zeros(num_nodes))

        # partial derivatives of the ODE outputs respect to the ODE inputs
        for j, k in zip(('xdot', 'ydot', 'zdot'), ('vx', 'vy', 'vz')):
            self.declare_partials(of=j, wrt=k, rows=nodes_range, cols=nodes_range, val=1.0)

        for k in ('x', 'y', 'z'):
            self.declare_partials(of='vxdot', wrt=k, rows=nodes_range, cols=nodes_range)
            self.declare_partials(of='vydot', wrt=k, rows=nodes_range, cols=nodes_range)
            self.declare_partials(of='vzdot', wrt=k, rows=nodes_range, cols=nodes_range)

        self.declare_partials(of='vxdot', wrt='vy', rows=nodes_range, cols=nodes_range, val=2.0)
        self.declare_partials(of='vydot', wrt='vx', rows=nodes_range, cols=nodes_range, val=-2.0)

    def compute(self, inputs, outputs):
        """Computes the ODEs outputs. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])
        dot_x, dot_y, dot_z, dot_vx, dot_vy, dot_vz =\
            Cr3bpDynamics.eqm_6_synodic(state, self.options['mu'])

        outputs['xdot'] = dot_x
        outputs['ydot'] = dot_y
        outputs['zdot'] = dot_z

        outputs['vxdot'] = dot_vx
        outputs['vydot'] = dot_vy
        outputs['vzdot'] = dot_vz

    def compute_partials(self, inputs, jacobian):
        """Computes the jacobian components. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])
        u_bar_xx, u_bar_xy, u_bar_xz, u_bar_yx, u_bar_yy, u_bar_yz, u_bar_zx, u_bar_zy, u_bar_zz =\
            Cr3bpDynamics.u_bar_second_derivative(state, self.options['mu'])

        jacobian['vxdot', 'x'] = - u_bar_xx
        jacobian['vxdot', 'y'] = - u_bar_xy
        jacobian['vxdot', 'z'] = - u_bar_xz

        jacobian['vydot', 'x'] = - u_bar_yx
        jacobian['vydot', 'y'] = - u_bar_yy
        jacobian['vydot', 'z'] = - u_bar_yz

        jacobian['vzdot', 'x'] = - u_bar_zx
        jacobian['vzdot', 'y'] = - u_bar_zy
        jacobian['vzdot', 'z'] = - u_bar_zz


@declare_time()
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
@declare_state('m', rate_source='mdot', targets=['m'])
@declare_parameter('ux', targets=['ux'])
@declare_parameter('uy', targets=['uy'])
@declare_parameter('uz', targets=['uz'])
@declare_parameter('T', targets=['T'])
@declare_parameter('Isp', targets=['Isp'])
class ODEsSpacecraft(Component):
    """Defines the ODEs for an electric propelled spacecraft in the CR3BP framework expressed in a
    synodic reference frame and non dimensional units using the Dymos syntax.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    Isp : float
        Specific impulse [s]
    ac : float
        Characteristic acceleration [m/s^2]
    tc : float
        Characteristic time [s]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]
    m : ndarray
        Spacecraft mass [kg]
    T : ndarray
        Thrust magnitude [N]
    ux : ndarray
        Thrust direction unit vector x component [-]
    uy : ndarray
        Thrust direction unit vector y component [-]
    uz : ndarray
        Thrust direction unit vector z component [-]

    Returns
    -------
    xdot : ndarray
        Time derivative of x [-]
    ydot : ndarray
        Time derivative of y [-]
    zdot : ndarray
        Time derivative of z [-]
    vxdot : ndarray
        Time derivative of vx [-]
    vydot : ndarray
        Time derivative of vy [-]
    vzdot : ndarray
        Time derivative of vz [-]
    mdot : ndarray
        Time derivative of m [kg/-]
    rscm : ndarray
        Distance from the second primary body [-]
    u2 : ndarray
        Thrust direction unit vector squared norm [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def initialize(self):
        """Initializes ODEsSpacecraft class. """

        Component.initialize(self)
        for k in ('Isp', 'ac', 'tc'):
            self.options.declare(k, types=float)

    def setup(self):
        """Defines the ODEs inputs, outputs and jacobian components. """

        num_nodes = self.options['num_nodes']
        nodes_range = np.arange(self.options['num_nodes'])
        isp = self.options['Isp']

        # inputs (ODE right-hand side)
        for k in ('x', 'y', 'z', 'vx', 'vy', 'vz', 'm', 'T', 'ux', 'uy', 'uz'):
            self.add_input(k, val=np.zeros(num_nodes))

        self.add_input('Isp', val=isp*np.ones(num_nodes))

        # outputs (ODE left-hand side)
        for k in ('xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot', 'mdot', 'rscm', 'u2'):
            self.add_output(k, val=np.zeros(num_nodes))

        # partial derivatives of the ODE outputs respect to the ODE inputs
        for j, k in zip(('xdot', 'ydot', 'zdot'), ('vx', 'vy', 'vz')):
            self.declare_partials(of=j, wrt=k, rows=nodes_range, cols=nodes_range, val=1.0)

        for k in ('x', 'y', 'z', 'm', 'ux', 'T'):
            self.declare_partials(of='vxdot', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('x', 'y', 'z', 'm', 'uy', 'T'):
            self.declare_partials(of='vydot', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('x', 'y', 'z', 'm', 'uz', 'T'):
            self.declare_partials(of='vzdot', wrt=k, rows=nodes_range, cols=nodes_range)

        self.declare_partials(of='vxdot', wrt='vy', rows=nodes_range, cols=nodes_range, val=2.0)
        self.declare_partials(of='vydot', wrt='vx', rows=nodes_range, cols=nodes_range, val=-2.0)

        self.declare_partials(of='mdot', wrt='T', rows=nodes_range, cols=nodes_range)
        self.declare_partials(of='mdot', wrt='Isp', rows=nodes_range, cols=nodes_range)

        for k in ('x', 'y', 'z'):
            self.declare_partials(of='rscm', wrt=k, rows=nodes_range, cols=nodes_range)

        for k in ('ux', 'uy', 'uz'):
            self.declare_partials(of='u2', wrt=k, rows=nodes_range, cols=nodes_range)

    def compute(self, inputs, outputs):
        """Computes the ODEs outputs. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])

        dot_x, dot_y, dot_z, dot_vx, dot_vy, dot_vz =\
            Cr3bpDynamics.eqm_6_synodic(state, self.options['mu'])
        r_2 = (inputs['x'] - 1 + self.options['mu']) ** 2 + inputs['y'] ** 2 + inputs['z'] ** 2

        acc = inputs['T'] / inputs['m'] / self.options['ac']  # non-dim acceleration due to thrust

        outputs['xdot'] = dot_x
        outputs['ydot'] = dot_y
        outputs['zdot'] = dot_z

        outputs['vxdot'] = dot_vx + acc * inputs['ux']
        outputs['vydot'] = dot_vy + acc * inputs['uy']
        outputs['vzdot'] = dot_vz + acc * inputs['uz']

        outputs['mdot'] = - self.options['tc'] * inputs['T'] / inputs['Isp'] / g

        outputs['rscm'] = r_2 ** 0.5  # distance from the second primary
        outputs['u2'] = inputs['ux'] ** 2 + inputs['uy'] ** 2 + inputs['uz'] ** 2

    def compute_partials(self, inputs, jacobian):
        """Computes the jacobian components. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])

        r_2 = (inputs['x'] - 1 + self.options['mu']) ** 2 + inputs['y'] ** 2 + inputs['z'] ** 2
        u_bar_xx, u_bar_xy, u_bar_xz, u_bar_yx, u_bar_yy, u_bar_yz, u_bar_zx, u_bar_zy, u_bar_zz =\
            Cr3bpDynamics.u_bar_second_derivative(state, self.options['mu'])

        mass = inputs['m']
        acc = inputs['T'] / inputs['m'] / self.options['ac']  # non-dim acceleration due to thrust

        jacobian['vxdot', 'x'] = - u_bar_xx
        jacobian['vxdot', 'y'] = - u_bar_xy
        jacobian['vxdot', 'z'] = - u_bar_xz
        jacobian['vxdot', 'T'] = inputs['ux'] / self.options['ac'] / mass
        jacobian['vxdot', 'm'] = - acc / mass * inputs['ux']
        jacobian['vxdot', 'ux'] = acc

        jacobian['vydot', 'x'] = - u_bar_yx
        jacobian['vydot', 'y'] = - u_bar_yy
        jacobian['vydot', 'z'] = - u_bar_yz
        jacobian['vydot', 'T'] = inputs['uy'] / self.options['ac'] / mass
        jacobian['vydot', 'm'] = - acc / mass * inputs['uy']
        jacobian['vydot', 'uy'] = acc

        jacobian['vzdot', 'x'] = - u_bar_zx
        jacobian['vzdot', 'y'] = - u_bar_zy
        jacobian['vzdot', 'z'] = - u_bar_zz
        jacobian['vzdot', 'T'] = inputs['uz'] / self.options['ac'] / mass
        jacobian['vzdot', 'm'] = - acc / mass * inputs['uz']
        jacobian['vzdot', 'uz'] = acc

        jacobian['mdot', 'T'] = - self.options['tc'] / self.options['Isp'] / g
        jacobian['mdot', 'Isp'] =\
            self.options['tc'] * inputs['T'] / self.options['Isp'] ** 2 / g

        jacobian['rscm', 'x'] = (inputs['x'] - 1.0 + self.options['mu']) / r_2 ** 0.5
        jacobian['rscm', 'y'] = inputs['y'] / r_2 ** 0.5
        jacobian['rscm', 'z'] = inputs['z'] / r_2 ** 0.5

        jacobian['u2', 'ux'] = 2 * inputs['ux']
        jacobian['u2', 'uy'] = 2 * inputs['uy']
        jacobian['u2', 'uz'] = 2 * inputs['uz']
