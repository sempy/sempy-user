#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 11:42:50 2019

@author: Alberto FOSSA'
"""

from __future__ import print_function, division, absolute_import
from openmdao.api import ExplicitComponent


class Component(ExplicitComponent):
    """Helper class to initialize common attributes among classes that inherits from
    OpenMDAO `ExplicitComponent`.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]

    """

    def initialize(self):
        """Initializes Component class. """

        self.options.declare('num_nodes', types=int)
        self.options.declare('mu', types=float)
