#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 11:42:50 2019

@author: Alberto FOSSA'
"""

import numpy as np
from dymos import declare_time, declare_state

from sempy.core.lowthrust.dynamics.cr3bp_dynamics_old import Cr3bpDynamics
from sempy.core.lowthrust.dynamics.comp import Component
from sempy.core.crtbp.jacobi import jacobi


@declare_time()
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
class JacobiConstant(Component):
    """Computes the Jacobi constant.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]

    Returns
    -------
    C : ndarray
        Jacobi constant [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def setup(self):
        """Defines the equations inputs, outputs and jacobian components. """

        num_nodes = self.options['num_nodes']
        nodes_range = np.arange(self.options['num_nodes'])

        for k in ('x', 'y', 'z', 'vx', 'vy', 'vz'):  # inputs
            self.add_input(k, val=np.zeros(num_nodes))

        self.add_output('C', val=np.zeros(num_nodes))  # output

        # partial derivatives of the inputs wrt the outputs
        for k in ('x', 'y', 'z', 'vx', 'vy', 'vz'):
            self.declare_partials(of='C', wrt=k, rows=nodes_range, cols=nodes_range)

    def compute(self, inputs, outputs):
        """Computes the equations outputs. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])
        outputs['C'] = jacobi(state, self.options['mu'])

    def compute_partials(self, inputs, jacobian):
        """Computes the jacobian components. """

        state = (inputs['x'], inputs['y'], inputs['z'], inputs['vx'], inputs['vy'], inputs['vz'])
        u_bar_x, u_bar_y, u_bar_z = Cr3bpDynamics.u_bar_first_derivative(state, self.options['mu'])

        jacobian['C', 'x'] = - 2 * u_bar_x
        jacobian['C', 'y'] = - 2 * u_bar_y
        jacobian['C', 'z'] = - 2 * u_bar_z

        jacobian['C', 'vx'] = - 2 * inputs['vx']
        jacobian['C', 'vy'] = - 2 * inputs['vy']
        jacobian['C', 'vz'] = - 2 * inputs['vz']
