#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 15:20:30 2019

@author: Alberto FOSSA'
"""

from openmdao.api import Group
from dymos import declare_time, declare_state, declare_parameter

from sempy.core.lowthrust.dynamics.syn2lci2he_comp import Syn2LCI, LCI2HE
from sempy.core.lowthrust.dynamics.odes_comp import ODEsCr3bp, ODEsSpacecraft
from sempy.core.lowthrust.dynamics.jacobi_comp import JacobiConstant


@declare_time()
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
@declare_state('m', rate_source='mdot', targets=['m'])
@declare_parameter('ux', targets=['ux'])
@declare_parameter('uy', targets=['uy'])
@declare_parameter('uz', targets=['uz'])
@declare_parameter('T', targets=['T'])
@declare_parameter('Isp', targets=['Isp'])
class ODEsSpacecraftJacobi(Group):
    """Defines the ODEs for an electric propelled spacecraft in the CR3BP framework expressed in a
    synodic reference frame and non dimensional units and the equations to compute the Jacobi
    constant over time.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    g0 : float
        Standard sea-level gravity [m/s^2]
    Isp : float
        Specific impulse [s]
    ac : float
        Scaling parameter for the acceleration due to thrust [s^2/m]
    tc : float
        Characteristic time [s]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]
    m : ndarray
        Spacecraft mass [kg]
    T : ndarray
        Thrust magnitude [N]
    ux : ndarray
        Thrust direction unit vector x component [-]
    uy : ndarray
        Thrust direction unit vector y component [-]
    uz : ndarray
        Thrust direction unit vector z component [-]

    Returns
    -------
    xdot : ndarray
        Time derivative of x [-]
    ydot : ndarray
        Time derivative of y [-]
    zdot : ndarray
        Time derivative of z [-]
    vxdot : ndarray
        Time derivative of vx [-]
    vydot : ndarray
        Time derivative of vy [-]
    vzdot : ndarray
        Time derivative of vz [-]
    mdot : ndarray
        Time derivative of m [kg/-]
    rscm : ndarray
        Distance from the second primary body [-]
    u2 : ndarray
        Thrust direction unit vector squared norm [-]
    C : ndarray
        Jacobi constant [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def initialize(self):
        """Initializes ODEsSpacecraftJacobi class. """

        self.options.declare('num_nodes', types=int)
        for k in ('mu', 'Isp', 'ac', 'tc'):
            self.options.declare(k, types=float)

    def setup(self):
        """Add together the different subsystems. """

        self.add_subsystem(name='odes',
                           subsys=ODEsSpacecraft(num_nodes=self.options['num_nodes'],
                                                 mu=self.options['mu'], Isp=self.options['Isp'],
                                                 ac=self.options['ac'], tc=self.options['tc']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz', 'm', 'ux', 'uy', 'uz',
                                            'T', 'Isp'],
                           promotes_outputs=['xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot',
                                             'mdot', 'rscm', 'u2'])

        self.add_subsystem(name='jacobi',
                           subsys=JacobiConstant(num_nodes=self.options['num_nodes'],
                                                 mu=self.options['mu']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz'],
                           promotes_outputs=['C'])


@declare_time()
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
class ODEsCr3bpJacobi(Group):
    """Defines the ODEs for a unit mass particle in the CR3BP framework expressed in a synodic
    reference frame and non dimensional units and the equations to compute the Jacobi constant
    over time.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]

    Returns
    -------
    xdot : ndarray
        Time derivative of x [-]
    ydot : ndarray
        Time derivative of y [-]
    zdot : ndarray
        Time derivative of z [-]
    vxdot : ndarray
        Time derivative of vx [-]
    vydot : ndarray
        Time derivative of vy [-]
    vzdot : ndarray
        Time derivative of vz [-]
    C : ndarray
        Jacobi constant [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def initialize(self):
        """Initializes ODEsSpacecraftJacobi class. """

        self.options.declare('num_nodes', types=int)
        self.options.declare('mu', types=float)

    def setup(self):
        """Add together the different subsystems. """

        self.add_subsystem(name='odes',
                           subsys=ODEsCr3bp(num_nodes=self.options['num_nodes'],
                                            mu=self.options['mu']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz'],
                           promotes_outputs=['xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot'])

        self.add_subsystem(name='jacobi',
                           subsys=JacobiConstant(num_nodes=self.options['num_nodes'],
                                                 mu=self.options['mu']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz'],
                           promotes_outputs=['C'])


@declare_time(targets=['t'])
@declare_state('x', rate_source='xdot', targets=['x'])
@declare_state('y', rate_source='ydot', targets=['y'])
@declare_state('z', rate_source='zdot', targets=['z'])
@declare_state('vx', rate_source='vxdot', targets=['vx'])
@declare_state('vy', rate_source='vydot', targets=['vy'])
@declare_state('vz', rate_source='vzdot', targets=['vz'])
@declare_state('m', rate_source='mdot', targets=['m'])
@declare_parameter('ux', targets=['ux'])
@declare_parameter('uy', targets=['uy'])
@declare_parameter('uz', targets=['uz'])
@declare_parameter('T', targets=['T'])
@declare_parameter('Isp', targets=['Isp'])
class ODEsSpacecraftLLO2LLO(Group):
    """Defines the ODEs for an electric propelled spacecraft in the CR3BP framework expressed in a
    synodic reference frame and the equations to convert the state vector in LCI and to compute
    the corresponding specific angular momentum and eccentricity vectors assuming a Keplerian
    orbit about the Moon.

    Other Parameters
    ----------------
    num_nodes : int
        Number of nodes in which the continuous-time trajectory is discretized
    mu : float
        CR3BP mass parameter [-]
    GM : float
        Central body standard gravitational parameter [km^3/s^2]
    g0 : float
        Standard sea-level gravity [m/s^2]
    Isp : float
        Specific impulse [s]
    ac : float
        Scaling parameter for the acceleration due to thrust [s^2/m]
    tc : float
        Characteristic time [s]
    t : ndarray
        Time [-]
    x : ndarray
        Position along x axis [-]
    y : ndarray
        Position along y axis [-]
    z : ndarray
        Position along z axis [-]
    vx : ndarray
        Velocity along x axis [-]
    vy : ndarray
        Velocity along y axis [-]
    vz : ndarray
        Velocity along z axis [-]
    m : ndarray
        Spacecraft mass [kg]
    T : ndarray
        Thrust magnitude [N]
    ux : ndarray
        Thrust direction unit vector x component [-]
    uy : ndarray
        Thrust direction unit vector y component [-]
    uz : ndarray
        Thrust direction unit vector z component [-]

    Returns
    -------
    xdot : ndarray
        Time derivative of x [-]
    ydot : ndarray
        Time derivative of y [-]
    zdot : ndarray
        Time derivative of z [-]
    vxdot : ndarray
        Time derivative of vx [-]
    vydot : ndarray
        Time derivative of vy [-]
    vzdot : ndarray
        Time derivative of vz [-]
    mdot : ndarray
        Time derivative of m [kg/-]
    rscm : ndarray
        Distance from the second primary body [-]
    u2 : ndarray
        Thrust direction unit vector squared norm [-]
    hx : ndarray
        Specific angular momentum along x axis [-]
    hy : ndarray
        Specific angular momentum along y axis [-]
    hz : ndarray
        Specific angular momentum along z axis [-]
    ex : ndarray
        Eccentricity vector x component [-]
    ey : ndarray
        Eccentricity vector y component [-]
    ez : ndarray
        Eccentricity vector z component [-]

    Warnings
    --------
    OpenMDAO `SqliteRecorder` uses `pickle` to store the driver iterations. After any modification
    you will not be able to load previously stored solutions for which this class is part of the
    `ode_class` of one or more `Phase` objects.

    """

    def initialize(self):
        """Initializes ODEsSpacecraftLLO2LLO_Group class. """

        self.options.declare('num_nodes', types=int)

        for k in ('mu', 'GM', 'Isp', 'ac', 'tc'):
            self.options.declare(k, types=float)

    def setup(self):
        """Add together the different subsystems. """

        self.add_subsystem(name='odes',
                           subsys=ODEsSpacecraft(num_nodes=self.options['num_nodes'],
                                                 mu=self.options['mu'], Isp=self.options['Isp'],
                                                 ac=self.options['ac'], tc=self.options['tc']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz', 'm', 'ux', 'uy', 'uz',
                                            'T', 'Isp'],
                           promotes_outputs=['xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot',
                                             'mdot', 'rscm', 'u2'])

        self.add_subsystem(name='syn2lci', subsys=Syn2LCI(num_nodes=self.options['num_nodes'],
                                                          mu=self.options['mu']),
                           promotes_inputs=['x', 'y', 'z', 'vx', 'vy', 'vz', 't'],
                           promotes_outputs=['xlci', 'ylci', 'zlci', 'vxlci', 'vylci', 'vzlci'])

        self.add_subsystem(name='lci2he', subsys=LCI2HE(num_nodes=self.options['num_nodes'],
                                                        GM=self.options['GM']),
                           promotes_inputs=['xlci', 'ylci', 'zlci', 'vxlci', 'vylci', 'vzlci'],
                           promotes_outputs=['hx', 'hy', 'hz', 'ex', 'ey', 'ez'])
