#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 14:48:03 2019

@author: Alberto FOSSA'

Defines the Equations of Motion (EOMs) for low-thrust transfers and coasting arcs in the CR3BP
framework using the dymos syntax
"""
