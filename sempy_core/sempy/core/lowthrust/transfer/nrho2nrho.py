#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 08:59:44 2019

@author: Alberto FOSSA'
"""

from copy import deepcopy

import numpy as np

from sempy.core.lowthrust.guess.refine import RefineThrustProfile
from sempy.core.lowthrust.guess.trajectory_stacking import TrajectoryStacking
from sempy.core.lowthrust.optimization.nlp_nrho2nrho import NRHO2NRHOFixNLP, NRHO2NRHOOpenArrNLP, \
    NRHO2NRHOOpenEndsNLP
from sempy.core.lowthrust.orbits.nrho import NRHO
from sempy.core.lowthrust.plots.solutions import NRHO2NRHOSolutionPlot
from sempy.core.lowthrust.transfer.transfer import Transfer
from sempy.core.lowthrust.utils.utils import InitNRHOs


class NRHO2NRHO(Transfer):
    """NRHO2NRHO class implements a Low-Thrust Transfer Trajectory between two NRHO orbits
    in the Earth-Moon CR3BP system.

    The class provides the methods to define the transfer and spacecraft characteristics,
    transcribe the optimal control problem and solve the resulting NLP problem from an adequate
    initial guess.
    The initial and target NRHO are specified either through their periselene altitude or their
    vertical extension while the initial guess is computed using the trajectory stacking
    approach.
    According to the given Boundary Conditions, a database containing the Initial Conditions for
    multiple NRHOs parametrized according to periselene altitude or vertical extension is loaded
    and used to assemble the initial guess.
    The guessed time of flight is split among all the intermediate orbits between the initial
    and target NRHOs such that the propagation time in each arc is proportional to the period of
    the corresponding orbit.
    The intermediate orbits are then propagated forward in time starting from the initial NRHO
    and up to the target NRHO and the computed states stacked together to assemble an
    adequate initial guess for the NLP solution.
    The problem is then solved starting from the given approximation and running one or multiple
    times one of the NLP solver wrapped by the pyOptSparse package.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0
    nb_rev : int, optional
        Number of complete revolutions in the transfer trajectory before insertion. Default is 0

    kwargs :
        All the keyword arguments are listed below
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : float
        Initial time in non dimensional units [-]
    nlp : TransferNLP
        `TransferNLP` object for the first solution
    nlp_refined : TransferNLP
        `TransferNLP` object for the second solution
    t_0 : ndarray or list
        Time vector initial guess in non dimensional units [-]
    tof0 : float or ndarray
        Time of flight initial guess in non dimensional units [-]
    states0 : ndarray or list
        States variables initial guess [-]
    controls0 : ndarray or list
        Controls variables initial guess [N, -]
    t_control : ndarray or list
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    nb_rev : int
        Number of complete revolutions in the transfer trajectory before insertion
    trajectory_stack : TrajectoryStacking
        `TrajectoryStacking` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    See Also
    --------
    Transfer : Implements a Low-Thrust Transfer Trajectory in the Earth-Moon CR3BP system.

    """

    def __init__(self, spacecraft, family, t_init=0.0, nb_rev=0, **kwargs):
        """Initializes NRHO2NRHO class. """

        Transfer.__init__(self, spacecraft, t_init)

        self.bcs = InitNRHOs(family, **kwargs)
        self.nb_rev = nb_rev
        self.trajectory_stack = self.sol_plot = None

    def compute_powered_guess(self, t_init, tof, t_0, throttle=None):
        """Computes the initial guess for a powered phase using the Trajectory Stacking approach.

        Parameters
        ----------
        t_init : float
            Initial time [-]
        tof : float
            Time of flight initial guess [-]
        t_0 : ndarray
            Time vector in which the states and controls variables are computed [-]
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`

        Returns
        -------
        states : ndarray
            States variables initial guess [-, kg]
        controls : ndarray
            Controls variables initial guess [N, -]

        """

        # initial guess on state vector from trajectory stacking
        if hasattr(self.bcs, 'alti'):
            self.trajectory_stack = \
                TrajectoryStacking(t_init, tof, t_0, self.bcs.family,
                                   alti=self.bcs.alti, altf=self.bcs.altf)
        elif hasattr(self.bcs, 'Azi'):
            self.trajectory_stack = \
                TrajectoryStacking(t_init, tof, t_0, self.bcs.family,
                                   Azi=self.bcs.azi, Azf=self.bcs.azf)
        else:
            raise AttributeError('Departure and arrival orbits not yet set')

        self.trajectory_stack.propagate('period')  # other methods may be implemented

        # initial guess on thrust magnitude and direction
        mass, thrust, v_dir = \
            self.compute_tangential_thrust(self.trajectory_stack.state0[:, 3:], t_0,
                                           t_init=t_init, throttle=throttle)

        states = np.hstack((self.trajectory_stack.state0, np.reshape(mass, (len(t_0), 1))))
        controls = np.hstack((np.ones((len(t_0), 1)) * thrust, v_dir))

        return states, controls

    def compute_coast_guess(self, t_0, nb_rev, orbit):
        """Computes the initial guess for a coasting phase propagating the corresponding orbit.

        Parameters
        ----------
        t_0 : ndarray
            Time vector in which the states variables are computed [-]
        nb_rev : int
            Number of revolutions in the given orbit required to span `t_0`
        orbit : str
            Orbit to be propagated, allowed values are 'dep' for departure and 'arr' for arrival

        Returns
        -------
        nrho.y : ndarray
            Positions and velocities vector initial guess [-]

        """

        if orbit == 'dep':
            if hasattr(self.bcs, 'alti'):
                nrho = NRHO(self.bcs.family, alt=self.bcs.alti, nb_rev=nb_rev)
            elif hasattr(self.bcs, 'Azi'):
                nrho = NRHO(self.bcs.family, Az=self.bcs.azi, nb_rev=nb_rev)
            else:
                raise AttributeError('Departure orbit not yet set')
        elif orbit == 'arr':
            if hasattr(self.bcs, 'altf'):
                nrho = NRHO(self.bcs.family, alt=self.bcs.altf, nb_rev=nb_rev)
            elif hasattr(self.bcs, 'Azf'):
                nrho = NRHO(self.bcs.family, Az=self.bcs.azf, nb_rev=nb_rev)
            else:
                raise AttributeError('Arrival orbit not yet set')
        else:
            raise ValueError("'orbit' must be either 'dep' or 'arr'")

        nrho.propagate(t_eval=t_0)

        return nrho.state_vec

    def solve_refined_nlp(self, method, nb_seg, order, solver, solver_ref=None, snopt_opts=None,
                          rec_file=None, throttle=None, threshold=None, run_driver=True,
                          check_partials=False, **kwargs):
        """Sets up the NLP characteristics and initial guess and runs the Driver twice to solve
        the problem using the first solution as starting point for the second run.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int or tuple
            Number of segments in time in which the trajectory is split
        order : int or tuple
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem for the first time
        solver_ref : str, optional
            Optimizer to solve the transcribed NLP problem for the second time or None.
            Default is None for which
            `solver` will be used
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`
        threshold : float, optional
            Throttle level used to refine the thrust magnitude profile forcing a bang-bang
            control scheme or None if no refinement has to be performed. Default is None
        run_driver : bool, optional
            True to perform the optimization, False to just setup the problem. Default is True
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False

        Other Parameters
        ----------------
        **kwargs :
        t_bounds : tuple
            Lower and upper time of flight bounds for each phase in the trajectory expressed
            as fractions of the corresponding transfer time.
            If not provided default values defined by the subclasses will be used

        Returns
        -------
        failed : bool
            False if the optimization was successful or `run_driver` set to False, True otherwise

        """

        if solver_ref is None:
            solver_ref = solver

        failed = self.solve_nlp(method, nb_seg, order, solver, snopt_opts=snopt_opts,
                                rec_file=rec_file, throttle=throttle, run_driver=run_driver,
                                check_partials=check_partials, **kwargs)

        if not failed:
            self.get_first_solution(threshold=threshold)
            failed = \
                self.solve_second_nlp(method, nb_seg, order, solver_ref, snopt_opts=snopt_opts,
                                      rec_file=rec_file, run_driver=run_driver,
                                      check_partials=check_partials, **kwargs)

        return failed

    def get_first_solution(self, threshold=None):
        """Retrieves the optimal states and controls profiles to initialize the second run. """

    def plot(self):
        """Plots the thrust magnitude and direction, positions, velocities and Jacobi constant
        time series and the three-dimensional transfer trajectory.

        """

        sol, sol_exp = self.plot_inputs()

        self.sol_plot = NRHO2NRHOSolutionPlot(self.cr3bp, self.bcs, sol, sol_exp=sol_exp)
        self.sol_plot.plot()

    def __str__(self):
        """Prints info on the transfer trajectory.

        Returns
        -------
        printed : str
            String to be printed

        """

        lines = [self.nlp.__str__(), self.bcs.__str__(), self.spacecraft.__str__(),
                 Transfer.__str__(self)]

        printed = '\n'.join(lines)

        return printed


class NRHO2NRHOFix(NRHO2NRHO):
    """NRHO2NRHOFix implements a Low-Thrust transfer trajectory between two fixed states in the
    synodic reference frame corresponding to the periselene and the aposelene of two NRHO orbits.

    The class provides the methods to define the transfer and spacecraft characteristics,
    transcribe the optimal control problem and solve the resulting NLP problem from an adequate
    initial guess.
    The problem is modelled as a single phase trajectory with states variables given by the
    spacecraft position, velocity and mass and controls variables given by the thrust magnitude
    and direction.
    The design variables for the resulting NLP problem are the states and controls variables in
    all the intermediate nodes, the time of flight and the final spacecraft mass.
    The initial and target NRHO are specified either through their periselene altitude or their
    vertical extension while the initial guess is computed using the trajectory stacking approach.
    According to the given Boundary Conditions, a database containing the Initial Conditions for
    multiple NRHOs parametrized according to periselene altitude or vertical extension is loaded
    and used to assemble the initial guess.
    The guessed time of flight is split among all the intermediate orbits between the initial
    and target NRHOs such that the propagation time in each arc is proportional to the period of
    the corresponding orbit.
    The intermediate orbits are then propagated forward in time starting from the initial NRHO
    and up to the target NRHO and the computed states stacked together to assemble an
    adequate initial guess for the NLP solution.
    The problem is then solved starting from the given approximation and running one or multiple
    times one of the NLP solver wrapped by the pyOptSparse package.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0
    nb_rev : int, optional
        Number of complete revolutions in the transfer trajectory before insertion. Default is 0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : float
        Initial time in non dimensional units [-]
    nlp : NRHO2NRHOFixNLP
        `NRHO2NRHOFixNLP` object for the first solution
    nlp_refined : NRHO2NRHOFixNLP
        `NRHO2NRHOFixNLP` object for the second solution
    t_0 : ndarray
        Time vector initial guess in non dimensional units [-]
    tof0 : float
        Time of flight initial guess in non dimensional units [-]
    states0 : ndarray
        States variables initial guess [-]
    controls0 : ndarray
        Controls variables initial guess [N, -]
    t_control : ndarray
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    nb_rev : int
        Number of complete revolutions in the transfer trajectory before insertion
    state_init : ndarray
        Departure state as ``[x, y, z, vx, vy, vz]``
    state_final : ndarray
        Arrival state as ``[x, y, z, vx, vy, vz]``
    trajectory_tack : TrajectoryStacking
        `TrajectoryStacking` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    See Also
    --------
    Transfer : Implements a Low-Thrust Transfer Trajectory in the Earth-Moon CR3BP system.
    NRHO2NRHO : Implements a Low-Thrust Transfer Trajectory between two NRHO orbits in
        the Earth-Moon CR3BP system.

    """

    def __init__(self, spacecraft, family, t_init=0.0, nb_rev=0, **kwargs):
        """Initializes NRHO2NRHOFix class. """

        NRHO2NRHO.__init__(self, spacecraft, family, t_init, nb_rev, **kwargs)

        self.tof0 = (0.5 + self.nb_rev) * self.bcs.nrho_arr.period  # time of flight initial guess

        # arrival NRHO propagation for half of its period to compute the state at the aposelene
        nrho = deepcopy(self.bcs.nrho_arr)
        nrho.propagate(t_final=nrho.period * 0.5)

        self.state_init = self.bcs.nrho_dep.state_vec[0]  # periselene of the departure NRHO
        self.state_final = nrho.state_vec[-1]  # aposelene of the arrival NRHO

    def compute_guess_time_series(self, t_0, throttle=None):
        """Computes the initial guess using the Trajectory Stacking approach.

        Parameters
        ----------
        t_0 : ndarray
            Time vector in which the states and controls variables are computed [-]
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`

        See Also
        --------
        compute_powered_guess: Computes the initial guess for a powered phase using the
            Trajectory Stacking approach.

        """

        self.states0, self.controls0 = \
            self.compute_powered_guess(self.t_init, self.tof0, t_0, throttle)

    def set_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                t_bounds=(0.5, 1.5)):
        """Sets up the NLP defining the states and controls variables,
        the constraints and the objective.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int
            Number of segments in time in which the trajectory is split
        order : int
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        t_bounds : tuple, optional
            Lower and upper time of flight bounds expressed as fractions of the corresponding
            transfer time. Default is (0.5, 1.5)

        Returns
        -------
        nlp : NRHO2NRHOFixNLP
            `NRHO2NRHOFixNLP` object

        """

        nlp = NRHO2NRHOFixNLP(method, nb_seg, order, solver, self.cr3bp, self.spacecraft,
                              snopt_opts=snopt_opts, rec_file=rec_file)
        nlp.set_time_states(self.bcs.nrho_arr, np.asarray(t_bounds) * self.tof0)
        nlp.setup()  # path constraints, control options, objective

        return nlp

    def get_first_solution(self, threshold=None):
        """Retrieves the optimal states and controls profiles to initialize the second run.

        Parameters
        ----------
        threshold : float, optional
            Throttle level used to refine the thrust magnitude profile forcing a bang-bang
            control scheme or None if no refinement has to be performed. Default is None

        """

        self.states0, self.controls0, self.t_0, self.t_control = self.get_states_controls()
        self.tof0 = self.t_0[-1] - self.t_0[0]

        if threshold is not None:
            rtp = RefineThrustProfile(self.t_control, self.controls0[:, 0], 'bang-bang',
                                      threshold=threshold,
                                      thrust_lim=(self.spacecraft.thrust_min,
                                                  self.spacecraft.thrust_max))
            self.controls0[:, 0] = rtp.thrust_interp.flatten()

    def get_solution_dictionary(self, pbm, jacobi_constant=True):
        """Stores in a dictionary the results obtained with an implicit solution or an
        explicit simulation of a given `Problem` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        sol = {'powered': self.get_time_series_dictionary(pbm, 'powered',
                                                          jacobi_constant=jacobi_constant)}

        return sol


class NRHO2NRHOOpenArrival(NRHO2NRHO):
    """NRHO2NRHOOpenArrival implements a Low-Thrust transfer trajectory between two NRHO orbits
    with a fixed departure state on the inner NRHO periselene and an open arrival state on the
    target orbit.

    The class provides the methods to define the transfer and spacecraft characteristics,
    transcribe the optimal control problem and solve the resulting NLP problem from an adequate
    initial guess.
    The problem is modelled as a two-phases trajectory with an initial powered phase from the
    inner NRHO periselene to an open state in the target NRHO and a second coasting phase from
    the achieved open state to the periselene of the target orbit.
    For the first phase the states variables are given by the spacecraft position, velocity
    and mass while the controls variables are given by the thrust magnitude and direction.
    The second phase has only six states variables represented by the spacecraft position and
    velocity vectors and no controls are applied along these arcs.
    The design variables for the resulting NLP problem are the states and controls variables
    in all the intermediate nodes of the first phase, the states variables in all the
    intermediate nodes of the second phase, the states at phase transition and the time of
    flight for both arcs.
    The initial and target NRHO are specified either through their periselene altitude or their
    vertical extension while the initial guess is computed using the trajectory stacking
    approach.
    According to the given Boundary Conditions, a database containing the Initial Conditions for
    multiple NRHOs parametrized according to periselene altitude or vertical extension is loaded
    and used to assemble the initial guess.
    The guessed time of flight is split among all the intermediate orbits between the initial and
    target NRHOs such that the propagation time in each arc is proportional to the period of the
    corresponding orbit.
    The intermediate orbits are then propagated forward in time starting from the initial NRHO
    and up to the target NRHO and the computed states stacked together to assemble an adequate
    initial guess for the NLP solution.
    The problem is then solved starting from the given approximation and running one or
    multiple times one of the NLP solver wrapped by the pyOptSparse package.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0
    nb_rev : int, optional
        Number of complete revolutions in the transfer trajectory before insertion. Default is 0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : ndarray
        Initial time in non dimensional units [-]
    nlp : NRHO2NRHOOpenArrNLP
        `NRHO2NRHOOpenArrNLP` object for the first solution
    nlp_refined : NRHO2NRHOOpenArrNLP
        `NRHO2NRHOOpenArrNLP` object for the second solution
    t_0 : list
        Time vector initial guess in non dimensional units [-]
    tof0 : ndarray
        Time of flight initial guess in non dimensional units [-]
    states0 : list
        States variables initial guess [-]
    controls0 : list
        Controls variables initial guess [N, -]
    t_control : list
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation
    BCs : object
        `InitNRHOs` object
    nb_rev : int
        Number of complete revolutions in the transfer trajectory before insertion
    state_init : ndarray
        Departure state as ``[x, y, z, vx, vy, vz]``
    state_tr : ndarray
        State at phase transition as ``[x, y, z, vx, vy, vz]``
    state_final : ndarray
        Arrival state as ``[x, y, z, vx, vy, vz]``
    trajectory_stack : TrajectoryStacking
        `TrajectoryStacking` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    See Also
    --------
    Transfer : Implements a Low-Thrust Transfer Trajectory in the Earth-Moon CR3BP system.
    NRHO2NRHO : Implements a Low-Thrust Transfer Trajectory between two NRHO orbits in the
        Earth-Moon CR3BP system.

    """

    def __init__(self, spacecraft, family, t_init=0.0, nb_rev=0, **kwargs):
        """Initializes NRHO2NRHOOpenArrival class. """

        NRHO2NRHO.__init__(self, spacecraft, family, t_init, nb_rev, **kwargs)

        self.tof0 = np.array([(0.5 + self.nb_rev), 0.5]) * self.bcs.nrho_arr.period
        self.t_init = np.array([0.0, self.tof0[0]]) + self.t_init

        # arrival NRHO propagation for half of its period to compute the state at the aposelene
        nrho = deepcopy(self.bcs.nrho_arr)
        nrho.propagate(t_final=nrho.period * 0.5)

        self.state_init = self.bcs.nrho_dep.state_vec[0]  # initial NRHO periselene
        self.state_tr = nrho.state_vec[-1]  # final NRHO aposelene
        self.state_final = self.bcs.nrho_arr.state_vec[0]  # final NRHO periselene

    def compute_guess_time_series(self, t_0, throttle=None):
        """Computes the initial guess using Trajectory Stacking approach for the first powered
        phase and target orbit propagation for the second coasting phase.

        Parameters
        ----------
        t_0 : list
            Time vector in which the states and controls variables are computed [-]
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`

        See Also
        --------
        compute_powered_guess: Computes the initial guess for a powered phase using the
            Trajectory Stacking approach.
        compute_coast_guess : Computes the initial guess for a coasting phase propagating
            the corresponding orbit.

        """

        states_pow, controls_pow = \
            self.compute_powered_guess(self.t_init[0], self.tof0[0], t_0[0], throttle)
        states_coast = self.compute_coast_guess(t_0[1], (self.nb_rev + 1), 'arr')

        self.states0 = [states_pow, states_coast]
        self.controls0 = [controls_pow, None]

    def set_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                t_bounds=((0.5, 1.5), (0.5, 1.5))):
        """Sets up the NLP defining the states and controls variables, the constraints and
        the objective.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int
            Number of segments in time in which the trajectory is split
        order : int
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        t_bounds : tuple, optional
            Lower and upper time of flight bounds expressed as fractions of the corresponding
            transfer time. Default is ((0.5, 1.5), (0.5, 1.5))

        Returns
        -------
        nlp : NRHO2NRHOOpenArrNLP
            `NRHO2NRHOOpenArrNLP` object

        """

        nlp = NRHO2NRHOOpenArrNLP(method, nb_seg, order, solver, self.cr3bp, self.spacecraft,
                                  snopt_opts=snopt_opts, rec_file=rec_file)
        t_bounds_abs = np.asarray(t_bounds) * np.reshape(self.tof0, (2, 1))
        nlp.set_time_states(self.bcs.nrho_arr, t_bounds_abs, t_init=self.t_init[0])
        nlp.setup(phase=nlp.phase[0])  # path constraints, controls options, objective

        return nlp

    def get_first_solution(self, threshold=None):
        """Retrieves the optimal states and controls profiles to initialize the second run.

        Parameters
        ----------
        threshold : float, optional
            Throttle level used to refine the thrust magnitude profile forcing a bang-bang
            control scheme or None if no refinement has to be performed. Default is None

        """

        states_pow, controls_pow, t0_pow, t_control_pow = \
            self.get_states_controls(self.nlp.phase[0], self.nlp.phase_name[0])

        if threshold is not None:
            rtp = RefineThrustProfile(t_control_pow, controls_pow[:, 0], 'bang-bang',
                                      threshold=threshold,
                                      thrust_lim=(self.spacecraft.Tmin, self.spacecraft.Tmax))
            controls_pow[:, 0] = rtp.thrust_interp.flatten()

        t0_coast, states_coast = self.get_pos_vel(self.nlp.phase_name[1])

        self.states0 = [states_pow, states_coast]
        self.controls0 = [controls_pow, None]
        self.t_0 = [t0_pow, t0_coast]
        self.t_control = [t_control_pow, None]
        self.t_init = np.array([t0_pow[0], t0_coast[0]])
        t_final = np.array([t0_pow[-1], t0_coast[-1]])
        self.tof0 = t_final - self.t_init

    def get_solution_dictionary(self, pbm, jacobi_constant=True):
        """Stores in a dictionary the results obtained with an implicit solution or an explicit
        simulation of a given `Problem` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        sol_pow = self.get_time_series_dictionary(pbm, 'powered',
                                                  phase_name=self.nlp.phase_name[0],
                                                  jacobi_constant=jacobi_constant)
        sol_coast = self.get_time_series_dictionary(pbm, 'coast',
                                                    phase_name=self.nlp.phase_name[1],
                                                    jacobi_constant=jacobi_constant)
        sol = {'powered': sol_pow, 'coast': sol_coast}

        return sol


class NRHO2NRHOOpenEnds(NRHO2NRHO):
    """NRHO2NRHOOpenEnds implements a Low-Thrust transfer trajectory between two NRHO orbits
    with open departure and arrival states on the initial and target orbits.

    The class provides the methods to define the transfer and spacecraft characteristics,
    transcribe the optimal control problem and solve the resulting NLP problem from an adequate
    initial guess.
    The problem is modelled as a three-phases trajectory with an initial and final coasting
    phases on the departure and arrival orbits and an intermediate powered phase from two open
    states in the inner and outer NRHOs.
    The first and third phases have only six states variables represented by the spacecraft
    position and velocity vectors and no controls are applied along these arcs.
    For the second phase the states variables are given by the spacecraft position, velocity and
    mass while the controls variables are given by the thrust magnitude and direction.
    The design variables for the resulting NLP problem are the states and controls variables in
    all the intermediate nodes of the second phase, the states variables in all the intermediate
    nodes of the first and third phases, the states at phase transitions and the time of flight
    for the three arcs.
    The initial and target NRHO are specified either through their periselene altitude or their
    vertical extension while the initial guess is computed using the trajectory stacking
    approach.
    According to the given Boundary Conditions, a database containing the Initial Conditions for
    multiple NRHOs parametrized according to periselene altitude or vertical extension is loaded
    and used to assemble the initial guess.
    The guessed time of flight is split among all the intermediate orbits between the initial and
    target NRHOs such that the propagation time in each arc is proportional to the period of the
    corresponding orbit.
    The intermediate orbits are then propagated forward in time starting from the initial NRHO
    and up to the target NRHO and the computed states stacked together to assemble an
    adequate initial guess for the NLP solution.
    The problem is then solved starting from the given approximation and running one or multiple
    times one of the NLP solver wrapped by the pyOptSparse package.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0
    nb_rev : int, optional
        Number of complete revolutions in the transfer trajectory before insertion. Default is 0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : ndarray
        Initial time in non dimensional units [-]
    nlp : NRHO2NRHOOpenEndsNLP
        `NRHO2NRHOOpenEndsNLP` object for the first solution
    nlp_refined : NRHO2NRHOOpenEndsNLP
        `NRHO2NRHOOpenEndsNLP` object for the second solution
    t_0 : list
        Time vector initial guess in non dimensional units [-]
    tof0 : ndarray
        Time of flight initial guess in non dimensional units [-]
    states0 : list
        States variables initial guess [-]
    controls0 : list
        Controls variables initial guess [N, -]
    t_control : list
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation
    BCs : object
        `InitNRHOs` object
    nb_rev : int
        Number of complete revolutions in the transfer trajectory before insertion
    states_init : ndarray
        Departure state as ``[x, y, z, vx, vy, vz]``
    states_tr : ndarray
        State at phase transition as ``[x, y, z, vx, vy, vz]``
    states_final : ndarray
        Arrival state as ``[x, y, z, vx, vy, vz]``
    trajectory_stack : TrajectoryStacking
        `TrajectoryStacking` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    See Also
    --------
    Transfer : Implements a Low-Thrust Transfer Trajectory in the Earth-Moon CR3BP system.
    NRHO2NRHO : Implements a Low-Thrust Transfer Trajectory between two NRHO orbits in the
        Earth-Moon CR3BP system.

    """

    def __init__(self, spacecraft, family, t_init=0.0, nb_rev=0, **kwargs):
        """Initializes NRHO2NRHOOpenEnds class. """

        NRHO2NRHO.__init__(self, spacecraft, family, t_init, nb_rev, **kwargs)

        tof0_coast1 = self.bcs.nrho_dep.period
        tof0_pow = (1.5 + self.nb_rev) * self.bcs.nrho_arr.period - self.bcs.nrho_dep.period
        tof0_coast2 = 0.5 * self.bcs.nrho_arr.period
        self.tof0 = np.array([tof0_coast1, tof0_pow, tof0_coast2])
        self.t_init += np.array([0.0, tof0_coast1, (tof0_coast1 + tof0_pow)])

        # arrival NRHO propagation for half of its period to compute the state at the aposelene
        nrho = deepcopy(self.bcs.nrho_arr)
        nrho.propagate(t_final=nrho.period * 0.5)

        self.state_init = self.bcs.nrho_dep.state_vec[0]  # initial NRHO periselene
        self.state_tr = nrho.state_vec[-1]  # final NRHO aposelene
        self.state_final = self.bcs.nrho_arr.state_vec[0]  # final NRHO periselene

    def compute_guess_time_series(self, t_0, throttle=None):
        """Computes the initial guess using Trajectory Stacking approach for the second powered
        phase and initial and target orbits propagation for the first and third coasting phases.

        Parameters
        ----------
        t_0 : list
            Time vector in which the states and controls variables are computed [-]
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`

        See Also
        --------
        compute_powered_guess: Computes the initial guess for a powered phase using the
            Trajectory Stacking approach.
        compute_coast_guess : Computes the initial guess for a coasting phase propagating
            the corresponding orbit.

        """

        states_coast1 = self.compute_coast_guess(t_0[0], 1, 'dep')  # coasting
        states_pow, controls_pow = \
            self.compute_powered_guess(self.t_init[1], self.tof0[1], t_0[1], throttle)  # powered
        states_coast2 = self.compute_coast_guess(t_0[2], (self.nb_rev + 2), 'arr')  # coasting

        self.states0 = [states_coast1, states_pow, states_coast2]
        self.controls0 = [None, controls_pow, None]

    def set_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                t_bounds=((0.5, 1.5), (0.5, 1.5), (0.5, 1.5))):
        """Sets up the NLP defining the states and controls variables, the constraints and
        the objective.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int
            Number of segments in time in which the trajectory is split
        order : int
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        t_bounds : tuple, optional
            Lower and upper time of flight bounds expressed as fractions of the corresponding
            transfer time. Default is ((0.5, 1.5), (0.5, 1.5), (0.5, 1.5))

        Returns
        -------
        nlp : NRHO2NRHOOpenEndsNLP
            `NRHO2NRHOOpenEndsNLP` object

        """

        nlp = NRHO2NRHOOpenEndsNLP(method, nb_seg, order, solver, self.cr3bp, self.spacecraft,
                                   snopt_opts=snopt_opts, rec_file=rec_file)
        t_bounds_abs = np.asarray(t_bounds) * np.reshape(self.tof0, (3, 1))
        nlp.set_time_states(self.bcs.nrho_arr, t_bounds_abs, t_init=self.t_init[0])
        nlp.setup(phase=nlp.phase[1])  # path constraints, controls options, objective

        return nlp

    def get_first_solution(self, threshold=None):
        """Retrieves the optimal states and controls profiles to initialize the second run.

        Parameters
        ----------
        threshold : float, optional
            Throttle level used to refine the thrust magnitude profile forcing a bang-bang
            control scheme or None if no refinement has to be performed. Default is None

        """

        t0_coast1, states_coast1 = self.get_pos_vel(self.nlp.phase_name[0])

        states_pow, controls_pow, t0_pow, t_control_pow = \
            self.get_states_controls(self.nlp.phase[1], self.nlp.phase_name[1])

        if threshold is not None:
            rtp = RefineThrustProfile(t_control_pow, controls_pow[:, 0], 'bang-bang',
                                      threshold=threshold,
                                      thrust_lim=(self.spacecraft.Tmin, self.spacecraft.Tmax))
            controls_pow[:, 0] = rtp.thrust_interp.flatten()

        t0_coast2, states_coast2 = self.get_pos_vel(self.nlp.phase_name[2])

        self.states0 = [states_coast1, states_pow, states_coast2]
        self.controls0 = [None, controls_pow, None]
        self.t_0 = [t0_coast1, t0_pow, t0_coast2]
        self.t_control = [None, t_control_pow, None]
        self.t_init = np.array([t0_coast1[0], t0_pow[0], t0_coast2[0]])
        t_final = np.array([t0_coast1[-1], t0_pow[-1], t0_coast2[-1]])
        self.tof0 = t_final - self.t_init

    def get_solution_dictionary(self, pbm, jacobi_constant=True):
        """Stores in a dictionary the results obtained with an implicit solution or an explicit
        simulation of a given `Problem` object.

        Parameters
        ----------
        pbm : object
            `Problem` object
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        sol_coast1 = self.get_time_series_dictionary(pbm, 'coast',
                                                     phase_name=self.nlp.phase_name[0],
                                                     jacobi_constant=jacobi_constant)
        sol_pow = self.get_time_series_dictionary(pbm, 'powered',
                                                  phase_name=self.nlp.phase_name[1],
                                                  jacobi_constant=jacobi_constant)
        sol_coast2 = self.get_time_series_dictionary(pbm, 'coast',
                                                     phase_name=self.nlp.phase_name[2],
                                                     jacobi_constant=jacobi_constant)
        sol = {'coast1': sol_coast1, 'powered': sol_pow, 'coast2': sol_coast2}

        return sol
