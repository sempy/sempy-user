#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 09:15:31 2019

@author: Alberto FOSSA'
"""

import numpy as np

from sempy.core.lowthrust.coc.rotmat import lci2syn, syn2lci
from sempy.core.lowthrust.coc.rotmatvec import polar2eq_vec
from sempy.core.lowthrust.guess.tangthrust import TangentialThrust
from sempy.core.lowthrust.guess.tangthrust_refined import TangThrustRefined
from sempy.core.lowthrust.optimization.nlp_llo2llo import LLO2LLONLP
from sempy.core.lowthrust.orbits.keporb import KepOrb
from sempy.core.lowthrust.plots.solutions import LLO2LLOSolutionPlot
from sempy.core.lowthrust.transfer.transfer import Transfer


class LLO2LLO(Transfer):
    """LLO2LLO class implements a low-thrust transfer between two circular and coplanar
    Low Lunar Orbits (LLO). The departure and arrival true anomalies, the initial flight path
    angle and the time of flight are treated as design variables.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    r_llo_dep : float
        Initial orbit radius [km]
    r_llo_arr : float
        Target orbit radius [km]
    ta_init : float
        Initial true anomaly [rad]
    inc : float
        Initial and target orbits inclination [rad]
    raan : float
        Initial and target orbits Right Ascension of the Ascending Node [rad]
    fpa_init : float
        Initial flight path angle [rad]
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : float
        Initial time in non dimensional units [-]
    nlp : LLO2LLONLP
        `LLO2LLONLP` object for the first solution
    nlp_refined : LLO2LLONLP
        `LLO2LLONLP` object for the second solution
    t_0 : ndarray or list
        Time vector initial guess in non dimensional units [-]
    tof0 : float or ndarray
        Time of flight initial guess in non dimensional units [-]
    states0 : ndarray or list
        States variables initial guess [-]
    controls0 : ndarray or list
        Controls variables initial guess [N, -]
    t_control : ndarray or list
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation
    r_llo_dep : float
        Initial orbit radius [km]
    r_llo_arr : float
        Target orbit radius [km]
    ta_init : float
        Initial true anomaly [rad]
    inc : float
        Initial and target orbits inclination [rad]
    raan : float
        Initial and target orbits Right Ascension of the Ascending Node [rad]
    aop : float
        Initial and target orbits Argument of Periapsis [rad]
    fpa_init : float
        Initial flight path angle [rad]
    guess : TangentialThrust
        `TangentialThrust` object
    llo_dep : object
        `KepOrb` object for the departure LLO
    llo_arr : object
        `KepOrb` object for the arrival LLO
    states0_lci : ndarray
        Positions and velocities initial guess in LCI reference frame as ``[R, V]`` [km, km/s]
    states0 : ndarray
        Positions and velocities initial guess in synodic reference frame
        as ``[x, y, z, vx, vy, vz]`` [-]
    guess_refined : TangThrustRefined
        `TangThrustRefined` object
    sol_plot : LLO2LLOSolutionPlot
        `LLO2LLOSolutionPlot` object

    """

    def __init__(self, spacecraft, r_llo_dep, r_llo_arr, ta_init, inc, raan, fpa_init, t_init=0.0):
        """Initializes LLO2LLO class. """

        Transfer.__init__(self, spacecraft, t_init)

        self.r_llo_dep = r_llo_dep
        self.r_llo_arr = r_llo_arr
        self.inc = inc
        self.raan = raan
        self.aop = 0.0
        self.ta_init = ta_init
        self.fpa_init = fpa_init

        self.guess = TangentialThrust(self.spacecraft, self.r_llo_dep, self.r_llo_arr,
                                      self.fpa_init, self.cr3bp.m2.GM,
                                      t_init=self.t_init * self.tc_cr3bp)
        self.tof0 = self.guess.tof / self.tc_cr3bp

        self.llo_dep = KepOrb(self.r_llo_dep, 0.0, self.inc, self.raan, self.aop, self.ta_init)
        self.llo_arr = KepOrb(self.r_llo_arr, 0.0, self.inc, self.raan, self.aop,
                              self.ta_init + self.guess.theta_final)

        self.states0_lci = self.guess_refined = self.sol_plot = None

    def compute_guess_time_series(self, t_0, throttle=None):
        """Computes the initial guess with Tangential Thrust approximation.

        Parameters
        ----------
        t_0 : ndarray
            Time vector in which the states and controls variables are computed [-]
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude or None [-].
            Default is None for which the thrust profile given by `TangentialThrust` will be used.

        """

        self.guess.compute_time_series(time=t_0 * self.tc_cr3bp)
        theta = self.guess.theta_vec + self.ta_init  # rotation to match the input true anomaly

        # state vector time series in LCI reference frame
        r0_lci, v0_lci = polar2eq_vec(self.guess.r_vec, theta, self.guess.u_vec, self.guess.v_vec,
                                      self.raan, self.inc, self.aop)
        v0_lci = np.vstack((self.llo_dep.V, v0_lci[1:]))
        self.states0_lci = np.hstack((r0_lci, v0_lci))

        # state vector time series in synodic reference frame and non dimensional units
        self.states0 = np.zeros((len(t_0), 6))

        for j, t_j in enumerate(t_0):
            self.states0[j] = lci2syn(self.states0_lci[j], t_j * self.tc_cr3bp, self.cr3bp.mu,
                                      self.cr3bp.L, self.tc_cr3bp)

        if throttle is None:  # thrust magnitude time series from TangentialThrust
            mass, thrust, v_dir = \
                self.compute_tangential_thrust(self.states0[:, 3:], t_0, t_init=self.t_init)
            if np.max(self.guess.thrust_vec) <= self.spacecraft.thrust_max:
                thrust = self.guess.thrust_vec
                mass = self.guess.mass_vec
            else:
                raise ValueError('Tangential Thrust magnitude not feasible! Maximum thrust: ',
                                 np.max(self.guess.thrust_vec), 'N')
        else:  # constant thrust magnitude
            mass, thrust, v_dir = \
                self.compute_tangential_thrust(self.states0[:, 3:], t_0,
                                               t_init=self.t_init, throttle=throttle)
            thrust = np.ones((len(t_0), 1)) * thrust

        # states and controls variables time series
        self.states0 = np.hstack((self.states0, np.reshape(mass, (len(mass), 1))))
        self.controls0 = np.hstack((thrust, v_dir))

    def set_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                t_bounds=(0.5, 1.5)):
        """Sets up the NLP defining the states and controls variables,
        the constraints and the objective.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int
            Number of segments in time in which the trajectory is split
        order : int
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        t_bounds : tuple, optional
            Lower and upper time of flight bounds expressed as fractions of the corresponding
            transfer time. Default is (0.5, 1.5)

        Returns
        -------
        nlp : LLO2LLONLP
            `LLO2LLONLP` object

        """

        nlp = LLO2LLONLP(method, nb_seg, order, solver, self.cr3bp, self.spacecraft,
                         snopt_opts=snopt_opts, rec_file=rec_file)
        nlp.set_time_states(self.llo_arr.a, self.tof0,
                            np.asarray(t_bounds) * self.tof0)

        # final Boundary Conditions (BCs) on specific angular momentum and eccentricity vectors
        nlp.set_target_orbit(self.llo_arr.H * self.tc_cr3bp / self.cr3bp.L ** 2, self.llo_arr.E)
        nlp.setup()  # path constraints, controls options, objective, problem setup

        return nlp

    def solve_refined_nlp(self, method, nb_seg, order, solver, solver_ref=None, snopt_opts=None,
                          rec_file=None, threshold=0.05, run_driver=True, check_partials=False,
                          kind='cubic', t_bounds=(0.5, 1.5)):
        """Sets up the NLP characteristics and initial guess and runs the Driver twice to solve
        the problem using the first solution as starting point for the second run.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int or tuple
            Number of segments in time in which the trajectory is split
        order : int or tuple
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem for the first time
        solver_ref : str, optional
            Optimizer to solve the transcribed NLP problem for the second time or None.
            Default is None for which
            `solver` will be used
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        threshold : float, optional
            Maximum negligible difference between guessed and optimal thrust magnitudes
            in fraction between 0 and 1. Default is 0.05
        run_driver : bool, optional
            True to perform the optimization, False to just setup the problem. Default is True
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False
        kind : str, optional
            Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``.
            Default is ``cubic``
        t_bounds : tuple
            Lower and upper time of flight bounds expressed as fractions of the corresponding
            transfer time. Default is (0.5, 1.5)

        Returns
        -------
        failed : bool
            False if the optimization was successful or `run_driver` set to False, True otherwise

        """

        if solver_ref is None:
            solver_ref = solver

        failed = self.solve_nlp(method, nb_seg, order, solver, snopt_opts=snopt_opts,
                                rec_file=rec_file, run_driver=run_driver,
                                check_partials=check_partials, t_bounds=t_bounds)

        if not failed:
            self.get_first_solution(kind, threshold=threshold)
            failed = \
                self.solve_second_nlp(method, nb_seg, order, solver_ref, snopt_opts=snopt_opts,
                                      rec_file=rec_file, run_driver=run_driver,
                                      check_partials=check_partials, t_bounds=t_bounds)

        return failed

    def get_first_solution(self, kind='cubic', threshold=0.05):
        """Retrieves the optimal states and controls profiles to initialize the second run.

        Parameters
        ----------
        kind : str, optional
            Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``.
            Default is ``cubic``
        threshold : float, optional
            Maximum negligible difference between guessed and optimal thrust magnitudes in
            fraction between 0 and 1. Default is 0.05

        """

        self.states0, self.controls0, self.t_0, self.t_control = self.get_states_controls()
        self.tof0 = self.t_0[-1] - self.t_0[0]

        self.guess_refined = \
            TangThrustRefined(self.t_control, self.guess.thrust_vec, self.controls0[:, 0],
                              threshold, kind, thrust_lim=(self.spacecraft.Tmin,
                                                           self.spacecraft.Tmax))

        self.controls0[:, 0] = self.guess_refined.thrust_interp.flatten()

    def get_solution_dictionary(self, pbm, jacobi_constant=False):
        """Stores in a dictionary the results obtained with an implicit solution or an explicit
        simulation of a given `Problem` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is False

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        sol0 = self.get_time_series_dictionary(pbm, 'powered', jacobi_constant=jacobi_constant)

        bcs = self.get_final_conditions(pbm, sol0)
        sol0['H'] = bcs[:3]
        sol0['E'] = bcs[3:]

        sol = {'powered': sol0}

        return sol

    def get_final_conditions(self, pbm, sol0):
        """Computes the Classical Orbital Elements at insertion and retrieves the
        corresponding specific angular momentum and eccentricity vectors from a given `Problem`
        object and solution dictionary `d0`.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        sol0 : dict
            Solution dictionary

        Returns
        -------
        bcs : ndarray
            Specific angular momentum and eccentricity vectors at insertion
            as ``[hx, hy, hz, ex, ey, ez]``

        """

        state_final = sol0['insertion']  # state vector at insertion in synodic frame
        t_final = float(sol0['time'][-1])  # time at insertion in non dimensional units
        state_final_lci = syn2lci(state_final, t_final, self.cr3bp.mu, self.cr3bp.L,
                                  self.tc_cr3bp)  # state vector at insertion in LCI frame

        # classical orbital elements at insertion
        self.llo_arr.set_state_vector(state_final_lci[:3], state_final_lci[3:])

        bcs = np.zeros(6)
        for i in range(6):
            j = pbm.get_val(self.nlp.phase_name + '.timeseries.' + self.nlp.bcs_names[i])
            bcs[i] = j[-1, -1]

        return bcs

    def plot(self):
        """Plots the thrust magnitude and direction, positions and velocities time series and
        the three-dimensional transfer trajectory.

        """

        sol, sol_exp = self.plot_inputs()
        self.sol_plot = LLO2LLOSolutionPlot(self.cr3bp, self.llo_dep, self.llo_arr, sol,
                                            sol_exp=sol_exp)
        self.sol_plot.plot()

    def __str__(self):
        """Prints info on the transfer trajectory.

        Returns
        -------
        printed : str
            String to be printed

        """

        lines = [self.nlp.__str__(), '\n{:^30s}'.format('Departure LLO:'),
                 self.llo_dep.__str__(), '\n{:^30s}'.format('Arrival LLO:'),
                 self.llo_arr.__str__(), self.spacecraft.__str__(),
                 Transfer.__str__(self)]

        printed = '\n'.join(lines)

        return printed
