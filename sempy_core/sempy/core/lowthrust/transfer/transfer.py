#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 15:45:49 2019

@author: Alberto FOSSA'
"""

from time import time
from copy import deepcopy
from scipy.constants import g
import numpy as np

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.lowthrust.optimization.nlp_transfer import TransferNLP


class Transfer:
    """Transfer class implements a Low-Thrust Transfer Trajectory in the Earth-Moon CR3BP system.

    The class provides the methods to define the transfer and spacecraft characteristics,
    transcribe the optimal control problem and solve the resulting NLP problem from an adequate
    initial guess.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    t_init : float, optional
        Initial time in dimensional units [s]. Default is 0.0

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    cr3bp : Cr3bp
        `Cr3bp` object for the Earth-Moon system
    tc_cr3bp : float
        Characteristic time [s]
    t_init : float
        Initial time in non dimensional units [-]
    nlp : TransferNLP
        `TransferNLP` object for the first solution
    nlp_refined : TransferNLP
        `TransferNLP` object for the second solution
    t_0 : ndarray or list
        Time vector initial guess in non dimensional units [-]
    tof0 : float or list
        Time of flight initial guess in non dimensional units [-]
    states0 : ndarray or list
        States variables initial guess [-]
    controls0 : ndarray or list
        Controls variables initial guess [N, -]
    t_control : ndarray or list
        Time vector initial guess on the control nodes [-]
    sol : dict
        First optimal solution
    sol_exp : dict
        First explicit simulation
    sol_ref : dict
        Second optimal solution
    sol_exp_ref : dict
        Second explicit simulation

    """

    def __init__(self, spacecraft, t_init=0.0):
        """Initializes Transfer class. """

        self.spacecraft = spacecraft
        self.cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        self.tc_cr3bp = self.cr3bp.T / 2 / np.pi
        self.t_init = t_init / self.tc_cr3bp

        self.t_0 = []
        self.tof0 = []
        self.states0 = []
        self.controls0 = []
        self.t_control = []

        self.nlp = self.nlp_refined = None
        self.sol = self.sol_exp = self.sol_ref = self.sol_exp_ref = None
        self.thrust_mag_plot = self.thrust_dir_plot = None

    def compute_tangential_thrust(self, v_vec, t_0, t_init=0.0, throttle=None):
        """Computes the time series of spacecraft mass and thrust direction assuming a constant
        thrust magnitude and a thrust direction aligned with the velocity vector.

        Parameters
        ----------
        v_vec : ndarray
            Velocity vector initial guess [-]
        t_0 : ndarray
            Time vector [-]
        t_init : float, optional
            Initial time [-]. Default is 0.0
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0`

        Returns
        -------
        mass : ndarray
            Spacecraft mass time series [kg]
        thrust : float
            Thrust magnitude [N]
        v_dir : ndarray
            Thrust direction time series [-]

        """

        v_norm = np.linalg.norm(v_vec, 2, axis=1, keepdims=True)  # velocity vector norm
        v_dir = v_vec / v_norm  # pointing vector
        thrust = self.spacecraft.thrust_min
        if throttle is not None:
            thrust += throttle * (self.spacecraft.thrust_max - self.spacecraft.thrust_min)
        mass_flow = - thrust / self.spacecraft.isp / g  # mass flow rate [kg/s]
        mass = self.spacecraft.mass0 + (mass_flow * self.tc_cr3bp) * (t_0 - t_init)  # mass [kg]

        return mass, thrust, v_dir

    def compute_guess_time_series(self, t_0, throttle=None):
        """Computes the initial guess time series for states and control variables. """

    def set_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                t_bounds=None):
        """Sets up the NLP problem defining states and controls variables,
        constraints and objective.

        Returns
        -------
        nlp : TransferNLP
            `TransferNLP` object

        """

        print(t_bounds)

        return TransferNLP(method, nb_seg, order, solver, self.cr3bp, self.spacecraft,
                           snopt_opts=snopt_opts, rec_file=rec_file)

    def solve_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                  throttle=None, run_driver=True, check_partials=False, **kwargs):
        """Sets up the NLP characteristics and initial guess and runs the Driver
        to solve the problem.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int or tuple
            Number of segments in time in which the trajectory is split
        order : int or tuple
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem, must be supported by OpenMDAO
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        throttle : float or None, optional
            Throttle level for the initial guess constant thrust magnitude [-].
            Default is None corresponding to thrust_min and equivalent to `throttle=0.0` for NRHO
            to NRHO transfers or the profile obtained with `TangentialThrust` for LLO to LLO ones
        run_driver : bool, optional
            True to perform the optimization, False to just setup the problem. Default is True
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False

        Other Parameters
        ----------------
        **kwargs :
        t_bounds : tuple
            Lower and upper time of flight bounds for each phase in the trajectory expressed
            as fractions of the corresponding transfer time. If not provided default values
            defined by the subclasses will be used

        Returns
        -------
        failed : bool
            False if the optimization was successful or `run_driver` set to False,
            True otherwise

        """

        if 't_bounds' in kwargs:
            self.nlp = self.set_nlp(method, nb_seg, order, solver, snopt_opts=snopt_opts,
                                    rec_file=rec_file, t_bounds=kwargs['t_bounds'])
        else:
            self.nlp = self.set_nlp(method, nb_seg, order, solver, snopt_opts=snopt_opts,
                                    rec_file=rec_file)

        self.nlp.set_tof_guess(self.t_init, self.tof0)
        self.compute_guess_time_series(self.nlp.t_control, throttle=throttle)
        self.nlp.set_initial_guess(self.states0, self.controls0, check_partials=check_partials)

        if run_driver:
            self.nlp, failed = self.run_driver(self.nlp)  # optimization
        else:
            failed = False

        return failed

    def solve_second_nlp(self, method, nb_seg, order, solver, snopt_opts=None, rec_file=None,
                         run_driver=True, check_partials=False, **kwargs):
        """Sets up the NLP characteristics and initial guess and runs the Driver to solve
        the problem using the first solution as starting point.

        Parameters
        ----------
        method : str
            Transcription method to discretize the continuous-time trajectory,
            allowed values are ``gauss-lobatto`` and ``radau-ps``
        nb_seg : int or tuple
            Number of segments in time in which the trajectory is split
        order : int or tuple
            Order of the interpolating polynomial within each segment, must be an odd number
        solver : str
            Optimizer to solve the transcribed NLP problem
        snopt_opts : dict, optional
            SNOPT optional parameters. Default is None for which the default values will be used
        rec_file : str or None, optional
            Absolute path to the SQL database in which the trajectory will be recorded or None.
            Default is None
        run_driver : bool, optional
            True to perform the optimization, False to just setup the problem. Default is True
        check_partials : bool, optional
            If True performs derivative checking using complex step method. Default is False

        Other Parameters
        ----------------
        **kwargs :
        t_bounds : tuple
            Lower and upper time of flight bounds for each phase in the trajectory expressed
            as fractions of the corresponding transfer time.
            If not provided default values defined by the subclasses will be used

        Returns
        -------
        failed : bool
            False if the optimization was successful or `run_driver` set to False, True otherwise

        """

        if 't_bounds' in kwargs:
            self.nlp_refined = self.set_nlp(method, nb_seg, order, solver,
                                            snopt_opts=snopt_opts, rec_file=rec_file,
                                            t_bounds=kwargs['t_bounds'])
        else:
            self.nlp_refined = self.set_nlp(method, nb_seg, order, solver,
                                            snopt_opts=snopt_opts, rec_file=rec_file)
        self.nlp_refined.set_refined_guess(self.t_init, self.tof0, self.states0,
                                           self.controls0, check_partials=check_partials)
        if run_driver:
            self.nlp_refined, failed = self.run_driver(self.nlp_refined)
        else:
            failed = False

        return failed

    @staticmethod
    def run_driver(nlp):
        """Runs the Driver attached to the specified `TransferNLP` object.

        Parameters
        ----------
        nlp : TransferNLP
            `TransferNLP` object

        Returns
        -------
        nlp : TransferNLP
            `TransferNLP` object
        failed : bool
            False if the optimization was successful, True otherwise
        """

        if nlp.rec_file is not None:
            nlp.pbm.record_iteration('initial')  # record first iteration ie initial guess

        t_start = time()
        failed = nlp.pbm.run_driver()  # OpenMDAO run_driver() method
        t_end = time()
        print('\nTime to solve the NLP problem: ' + str(t_end - t_start) + ' s\n')

        if nlp.rec_file is not None:
            nlp.pbm.record_iteration('final')  # record last iteration ie optimal solution
        nlp.cleanup()  # cleanup resources

        return nlp, failed

    def get_pos_vel(self, phase_name=None):
        """Retrieves the time on all nodes and the positions and velocities on the states
        discretization nodes from a given `Phase` object.

        Parameters
        ----------
        phase_name : str or None, optional
            Name of the `Phase` object in the `Trajectory` or None for single-phase trajectories.
            Default is None

        Returns
        -------
        t_vec : ndarray
            Time on all nodes [-]
        pos_vel_vec : ndarray
            Positions and velocities on the states discretization nodes [-]

        """

        if phase_name is None:  # single-phase trajectory
            phase_name = self.nlp.phase_name
            t_state = self.nlp.t_state
        else:  # multiple-phases trajectory
            t_state = self.nlp.t_state[self.nlp.phase_name.index(phase_name)]

        t_vec = self.nlp.pbm.get_val(phase_name + '.time')  # time vector

        # position and velocity states variables
        pos_vel_vec = np.empty((np.size(t_state), 0))
        for k in self.nlp.states_names[:6]:
            state = self.nlp.pbm.get_val(phase_name + '.states:' + k)
            pos_vel_vec = np.append(pos_vel_vec, state, axis=1)

        return t_vec, pos_vel_vec

    def get_states_controls(self, phase=None, phase_name=None):
        """Retrieves the time on all nodes and on the controls discretization nodes,
        the controls on the controls discretization nodes and the positions, velocities and
        spacecraft mass on the states discretization nodes from a given `Phase` object.

        Parameters
        ----------
        phase : Phase or None, optional
            `Phase` object or None for single-phase trajectories. Default is None
        phase_name : str or None, optional
            Name of the `Phase` object in the `Trajectory` or None for single-phase trajectories.
            Default is None

        Returns
        -------
        states : ndarray
            States variables on the states discretization nodes [-]
        controls : ndarray
            Controls variables on the controls discretization nodes [-]
        t_vec : ndarray
            Time on all nodes [-]
        t_control : ndarray
            Time on the controls discretization nodes [-]

        """

        t_vec, pos_vel_vec = self.get_pos_vel(phase_name=phase_name)

        if (phase is None) and (phase_name is None):  # single-phase trajectory
            phase = self.nlp.phase
            phase_name = self.nlp.phase_name

        states = \
            np.hstack((pos_vel_vec,
                       self.nlp.pbm.get_val(phase_name + '.states:' + self.nlp.states_names[6])))

        control_nodes = \
            phase.options['transcription'].grid_data.subset_node_indices['control_input']

        t_control = np.take(t_vec, control_nodes)
        t_control = np.reshape(t_control, (len(t_control), 1))

        controls = np.empty((np.size(t_control), 0))
        for k in self.nlp.controls_names:
            control = self.nlp.pbm.get_val(phase_name + '.controls:' + k)
            controls = np.append(controls, control, axis=1)

        return states, controls, t_vec, t_control

    def get_pos_vel_time_series(self, pbm, phase_name=None):
        """Retrieves the time of flight and the time, positions and velocities on all nodes
        from a given `Problem` and `Phase` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        phase_name : str or None, optional
            Name of the `Phase` object in the `Trajectory` or None for single-phase trajectories.
            Default is None

        Returns
        -------
        tof : float
            Time of flight [-]
        t_vec : ndarray
            Time on all nodes [-]
        pos_vel_vec : ndarray
            Positions and velocities on all nodes [-]

        """

        if phase_name is None:  # single-phase trajectory
            phase_name = self.nlp.phase_name

        tof = float(pbm.get_val(phase_name + '.t_duration'))
        t_vec = pbm.get_val(phase_name + '.timeseries.time')
        pos_vel_vec = np.empty((np.size(t_vec), 0))

        for k in self.nlp.states_names[:6]:
            state = pbm.get_val(phase_name + '.timeseries.states:' + k)
            pos_vel_vec = np.append(pos_vel_vec, state, axis=1)

        return tof, t_vec, pos_vel_vec

    def get_states_controls_time_series(self, pbm, phase_name=None):
        """Retrieves the time of flight and the time, states and controls variables on all
        nodes from a given `Problem` and `Phase` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        phase_name : str or None, optional
            Name of the `Phase` object in the `Trajectory` or None for single-phase trajectories.
            Default is None

        Returns
        -------
        tof : float
            Time of flight [-]
        t_vec : ndarray
            Time on all nodes [-]
        states : ndarray
            States variables on all nodes [-]
        controls : ndarray
            Controls variables on all nodes [-]

        """

        tof, t_vec, pos_vel_vec = self.get_pos_vel_time_series(pbm, phase_name=phase_name)

        if phase_name is None:  # single phase trajectory
            phase_name = self.nlp.phase_name

        states = \
            np.hstack((pos_vel_vec,
                       pbm.get_val(phase_name + '.timeseries.states:' + self.nlp.states_names[6])))

        controls = np.empty((np.size(t_vec), 0))
        for k in self.nlp.controls_names:
            control = pbm.get_val(phase_name + '.timeseries.controls:' + k)
            controls = np.append(controls, control, axis=1)

        return tof, t_vec, states, controls

    def get_time_series_dictionary(self, pbm, kind, phase_name=None, jacobi_constant=True):
        """Stores in a dictionary the results obtained with an implicit solution or an explicit
        simulation of a given `Problem` and `Phase` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        kind : str
            Kind of phase, allowed values are ``powered`` for powered phases and ``coast``
            for coasting arcs
        phase_name : str or None, optional
            Name of the `Phase` object in the `Trajectory` or None for single-phase trajectories.
            Default is None
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        if kind == 'powered':
            tof, t_vec, states, controls = self.get_states_controls_time_series(pbm, phase_name)
            sol = {'time': t_vec, 'tof': tof, 'time_dim': t_vec * self.tc_cr3bp,
                   'tof_dim': tof * self.tc_cr3bp, 'states': states, 'departure': states[0, :6],
                   'insertion': states[-1, :6], 'controls': controls}
        elif kind == 'coast':
            tof, t_vec, states = self.get_pos_vel_time_series(pbm, phase_name)
            sol = {'time': t_vec, 'tof': tof, 'time_dim': t_vec * self.tc_cr3bp,
                   'tof_dim': tof * self.tc_cr3bp, 'states': states}
        else:
            raise ValueError('Kind must be powered or coast')

        if jacobi_constant:
            if phase_name is None:
                sol['C'] = pbm.get_val(self.nlp.phase_name + '.timeseries.C')
            else:
                sol['C'] = pbm.get_val(phase_name + '.timeseries.C')

        return sol

    def get_solution_dictionary(self, pbm, jacobi_constant=True):
        """Stores in a dictionary the results obtained with an implicit solution or an
        explicit simulation of a given `Problem` and `Trajectory` object.

        Parameters
        ----------
        pbm : Problem
            `Problem` object
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is False

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        sol = self.get_time_series_dictionary(pbm, 'powered', phase_name=self.nlp.phase_name,
                                              jacobi_constant=jacobi_constant)

        return sol

    def get_solutions(self, jacobi_constant=True):
        """Saves as class attributes the optimal solutions and explicit simulations
        for all the instantiated `Problem` objects.

        Parameters
        ----------
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        """

        scaler = np.hstack((np.ones(3) * self.cr3bp.L,
                            np.ones(3) * self.cr3bp.L / self.tc_cr3bp))

        if self.nlp is not None:  # first run
            if self.nlp.pbm is not None:  # implicit solution
                self.sol = self.get_solution_dictionary(self.nlp.pbm,
                                                        jacobi_constant=jacobi_constant)
                self.sol['kind'] = 'implicit'
                if self.nlp.pbm_exp is not None:  # explicit simulation
                    self.sol_exp = self.get_solution_dictionary(self.nlp.pbm_exp,
                                                                jacobi_constant=jacobi_constant)
                    self.sol_exp['kind'] = 'explicit'
                    self.sol_exp['error'] = \
                        np.fabs(self.sol['powered']['insertion'] -
                                self.sol_exp['powered']['insertion']) * scaler

        if self.nlp_refined is not None:  # second run
            if self.nlp_refined.pbm is not None:  # implicit solution
                self.sol_ref = self.get_solution_dictionary(self.nlp_refined.pbm,
                                                            jacobi_constant=jacobi_constant)
                self.sol_ref['kind'] = 'implicit refined'
                if self.nlp_refined.pbm_exp is not None:  # explicit simulation
                    self.sol_exp_ref = \
                        self.get_solution_dictionary(self.nlp_refined.pbm_exp,
                                                     jacobi_constant=jacobi_constant)
                    self.sol_exp_ref['kind'] = 'explicit refined'
                    self.sol_exp_ref['error'] = \
                        np.fabs(self.sol_ref['powered']['insertion'] -
                                self.sol_exp_ref['powered']['insertion']) * scaler

    @staticmethod
    def print_solution(sol):
        """Prints info on the solution dictionary.

        Parameters
        ----------
        sol : dict
            Solution dictionary

        Returns
        -------
        printed : str
            String to be printed

        """

        sol_pow = sol['powered']
        tof_days = sol_pow['tof_dim'] / 86400
        mass_ratio = sol_pow['states'][-1, -1] / sol_pow['states'][0, -1]
        pos = '{:<10s}{:>20.12f}{:>20.12f}{:>20.12f}' \
            .format('R:', sol_pow['insertion'][0], sol_pow['insertion'][1],
                    sol_pow['insertion'][2])
        vel = '{:<10s}{:>20.12f}{:>20.12f}{:>20.12f}' \
            .format('V:', sol_pow['insertion'][3], sol_pow['insertion'][4],
                    sol_pow['insertion'][5])

        lines = ['\n{:^70s}'.format('Transfer trajectory - ' + sol['kind']),
                 '\n{:<30s}{:>30.15f}{:>5s}'.format('Time of flight:', tof_days, 'days'),
                 '{:<30s}{:>30.15f}'.format('final/initial mass ratio:', mass_ratio),
                 '\n{:^70s}'.format('Final state vectors:'), pos, vel]

        if 'error' in sol.keys():
            head_err = '\n{:^70s}'.format('Error between implicit solution and explicit '
                                          'simulation:')
            pos_err = '{:<5s}{:>20.12f}{:>20.12f}{:>20.12f}{:>3s}' \
                .format('R:', sol['error'][0], sol['error'][1], sol['error'][2], 'km')
            vel_err = '{:<5s}{:>20.12f}{:>20.12f}{:>20.12f}{:>3s}' \
                .format('V:', sol['error'][3], sol['error'][4], sol['error'][5], 'km')
            lines.extend([head_err, pos_err, vel_err])

        printed = '\n'.join(lines)

        return printed

    def plot_inputs(self):
        """Returns the available solution dictionaries to plot.

        Returns
        -------
        sol : dict
            Implicit solution to plot
        sol_exp : dict or None
            Explicit simulation to plot

        """

        if self.sol_ref:
            sol = deepcopy(self.sol_ref)
            if self.sol_exp_ref:
                sol_exp = deepcopy(self.sol_exp_ref)
            else:
                sol_exp = None
        elif self.sol:
            sol = deepcopy(self.sol)
            if self.sol_exp:
                sol_exp = deepcopy(self.sol_exp)
            else:
                sol_exp = None
        else:
            raise TypeError("Both 'sol' and 'sol_ref' are None, expected dict instead")

        return sol, sol_exp

    def __str__(self):
        """Prints info on the transfer trajectory.

        Returns
        -------
        printed : str
            String to be printed

        """

        lines = []
        for sol in (self.sol, self.sol_exp, self.sol_ref, self.sol_exp_ref):
            if sol:
                lines.append(self.print_solution(sol))

        printed = '\n'.join(lines)

        return printed
