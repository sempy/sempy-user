#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 13:33:44 2019

@author: Alberto FOSSA'

Constants module defines useful constants such as physical values, phases and trajectories names,
variables names, recording options, SNOPT options and file paths to store and read the
simulation results.

According to the file system of your machine, you may have to modify the paths set in
the `db_path` dictionary.

Attributes
----------
PHASES_NAMES : tuple
    Names of the Phases objects for single, two and three phases trajectories within OpenMDAO
TRAJ_NAME : str
    Name of the Trajectory object within OpenMDAO
STATES_NAMES : list
    Names of the states variables within OpenMDAO
CONTROLS_NAMES : list
    Names of the controls variables within OpenMDAO
STATES_RATES_NAMES : list
    Names of the states rates within OpenMDAO
CASE_REC_EXCLUDES : list
    Names of the quantities to be excluded from the recorded data
SNOPT_OPTS : dict
    SNOPT options to achieve an higher accuracy wrt the default values
DB : dict
    Absolute paths to the directories where the results are stored and read

"""


# phases and trajectory names
PHASES_NAMES = ('powered', ('powered', 'coast'), ('coast1', 'powered', 'coast2'))
TRAJ_NAME = 'traj'

# states and controls variables names
STATES_NAMES = ['x', 'y', 'z', 'vx', 'vy', 'vz', 'm']
CONTROLS_NAMES = ['T', 'ux', 'uy', 'uz']
STATES_RATES_NAMES = ['xdot', 'ydot', 'zdot', 'vxdot', 'vydot', 'vzdot', 'mdot']

# variables to be excluded while using an OpenMDAO CaseRecorder object
CASE_REC_EXCLUDES = ['*.control_rates:*', '*.control_states.*', '*.control_values:*',
                     '*.continuity_comp.*', '*.dt_dstau*', '*.design_parameters*',
                     '*.final_jump:*', '*.initial_jump:*', '*.interleave_comp.*',
                     '*.rhs_disc.*', '*.rhs_col.*', '*.state_interp.*', '*.t_initial*',
                     '*.time_phase*', '*.traj_parameters:*', '*++*', '*-+*', '*+-*', '*--*']

# default SNOPT options while looking for an highly accurate solution
SNOPT_OPTS = {'Major feasibility tolerance': 1e-12, 'Major optimality tolerance': 1e-10,
              'Minor feasibility tolerance': 1e-12, 'Major iterations limit': int(1e5),
              'Minor iterations limit': int(3e5), 'Iterations limit': int(2e6),
              'Function precision': 1e-14, 'Pivot tolerance': 3.7e-11,
              'LU singularity tolerance': 3.2e-11}

# directories where the solution have to be stored and retrieved
DB = {'ws': ('/scratch/lowthrust_data/ws/', '/scratch/lowthrust_data/pando/'),
      'laptop': ('/home/alberto/Documents/intern2019/data/laptop/',
                 'C:/Users/Valentin Prudhomme/Desktop/Dossier_Git/sempy/src/lowthrust/utils/'),
      'pando': ('', '')}
