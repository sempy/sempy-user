#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 25 06:56:10 2019

@author: Alberto FOSSA'
"""

import datetime

from sempy.core.lowthrust.utils.constants import DB
from sempy.core.lowthrust.orbits.nrho import FamilyNRHO


class InitNRHOs:
    """InitNRHOs class initializes two `NRHO` objects corresponding to the transfer's initial
    and target orbits.

    Parameters
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    azi : int
        Initial NRHO vertical extension [km]
    azf : int
        Final NRHO vertical extension [km]
    nrho_dep : object
        `NRHO` object for the initial orbit
    nrho_arr : object
        `NRHO` object for the arrival orbit

    """

    def __init__(self, family, **kwargs):
        """Initializes InitNRHOs class. """

        if ('alti' in kwargs) and ('altf' in kwargs):

            self.alti = kwargs['alti']
            self.altf = kwargs['altf']

            fam = FamilyNRHO(family, 'alt')
            fam.load_family()

            self.nrho_dep = fam.nrho_dict[str(self.alti)]
            self.nrho_arr = fam.nrho_dict[str(self.altf)]

        elif ('Azi' in kwargs) and ('Azf' in kwargs):

            self.azi = kwargs['Azi']
            self.azf = kwargs['Azf']

            fam = FamilyNRHO(family, 'Az')
            fam.load_family()

            self.nrho_dep = fam.nrho_dict[str(self.azi)]
            self.nrho_arr = fam.nrho_dict[str(self.azf)]

        else:
            raise ValueError('Either alti and altf or Azi and Azf has to be provided')

        self.family = family

    def __str__(self):
        """Prints the initial and target NRHOs attributes.

        Returns
        -------
        printed : str
            String to be printed

        """

        lines = ['\n{:^40s}'.format('Departure NRHO:'),
                 self.nrho_dep.__str__(),
                 '\n{:^40s}'.format('Arrival NRHO:'),
                 self.nrho_arr.__str__()]

        printed = '\n'.join(lines)

        return printed


class SimInfo:
    """SimInfo class provides date, time and kind for the current simulation.

    Parameters
    ----------
    kind : str
        Kind of simulation, allowed values are ``llo``, ``fix``, ``open_arr`` and ``open``

    Attributes
    ----------
    kind : str
        Kind of simulation, allowed values are ``llo``, ``fix``, ``open_arr`` and ``open``
    date : str
        Current date
    time : str
        Current time
    dt_compact : str
        Current date and time in compact format

    """

    def __init__(self, kind):
        """Initializes SimInfo class. """

        if kind in ['llo', 'fix', 'open_arr', 'open']:
            self.kind = kind
        else:
            raise ValueError("Kind must be either 'llo', 'fix', 'open_arr' or 'open'")

        # current date and time
        date_time = datetime.datetime.now()
        self.date = date_time.strftime('%d-%m-%Y')
        self.time = date_time.strftime('%H:%M:%S')
        self.dt_compact = date_time.strftime('%d%m%y_%H%M%S')

    def __str__(self):
        """Prints info on the simulation date and time.

        Returns
        -------
        printed : str
            String to be printed

        """

        lines = []

        if self.kind == 'llo':
            lines.append('Low-Thrust transfer between Low Lunar Orbits (LLO) '
                         'in the CR3BP framework')
            lines.append('the arrival state and the time of flight are treated as '
                         'design variables')
        else:
            lines.append('Low-Thrust transfer between Near Rectilinear Halo Orbits (NRHO) '
                         'in the CR3BP framework')
            if self.kind == 'fix':
                lines.append('the departure state is fixed at the inner orbit periselene and '
                             'the arrival state is fixed at the outer orbit aposelene')
                lines.append('the time of flight is treated as design variable')
            elif self.kind == 'open_arr':
                lines.append('the departure state is fixed at the inner orbit periselene')
                lines.append('the arrival state and the time of flight are treated as '
                             'design variables')
            elif self.kind == 'open':
                lines.append('the departure and arrival states and the time of flight are treated '
                             'as design variables')
            else:
                raise ValueError("Kind must be either 'llo', 'fix', 'open_arr' or 'open'")

        time_stamp = ' '.join(['\nSimulation run on', self.date, 'at', self.time])
        lines.append(time_stamp)

        printed = '\n'.join(lines)

        return printed


class SetPath:
    """SetPath class defines the location of the databases where the simulation results have
    to be stored or read.

    The absolute directory path is retrieved from the `db_path` variable defined in
    ``src.lowthrust.utils.constants`` while the default databases names are ``ddmmyy_HHMMSS.sql``
    for the implicit solution and ``ddmmyy_HHMMSS_exp.sql`` for the explicit simulation with
    ``dd-mm-yy HH:MM:SS`` date and time at which the solution is computed.

    Parameters
    ----------
    hostname : str
        Name of the host machine, allowed values are ``ws``, ``pando`` and ``laptop``
    action : str
        Action to be performed on data, allowed values are ``r`` for read or ``w`` for write
    kind : str
        Kind of simulation, allowed values are ``llo``, ``fix``, ``open_arr`` and ``open``

    Attributes
    ----------
    hostname : str
        Name of the host machine, allowed values are ``ws``, ``pando`` and ``laptop``
    db_res : dict or str
        Absolute path and names for the databases where the results have to be stored or read
    pando_res : str
        Absolute path for the database where the results obtained in Pando have to be read
    sim_info : object
        `SimInfo` object

    """

    def __init__(self, hostname, action, kind):
        """Initializes SetPath class. """

        if hostname in ['ws', 'pando', 'laptop']:
            self.hostname = hostname
            res_path = DB[hostname][0]
        else:
            raise ValueError('device not recognized')

        if action == 'w':
            self.sim_info = SimInfo(kind)
            db_imp = res_path + self.sim_info.dt_compact + '.sql'
            db_exp = res_path + self.sim_info.dt_compact + '_exp.sql'
            self.db_res = {'imp': db_imp, 'exp': db_exp}
            print(self.sim_info)

        if action == 'r':
            self.db_res = res_path
            if hostname != 'pando':
                self.pando_res = DB[hostname][1]


if __name__ == '__main__':

    IC = InitNRHOs('southern', alti=5000, altf=6000)
    print(IC)

    SP = SetPath('laptop', 'w', 'fix')
