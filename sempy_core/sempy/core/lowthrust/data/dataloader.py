#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 10:25:58 2019

@author: Alberto FOSSA'
"""

import os
from scipy.io import loadmat

DIRNAME = os.path.dirname(__file__)


def get_filename(family, param, data):
    """Returns the filename of the database with the initial conditions or the NRHO objects
    for the specified family.

    Parameters
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    param : str
        Family parametrization, allowed values are ``alt`` for periselene altitude
        and ``Az`` for vertical extension
    data : str
        Database to be loaded, allowed values are ``init`` for initial conditions
        and ``objects`` for NRHO objects

    Returns
    -------
    family : str
        L2 NRHO family
    param : str
        Family parametrization
    data : str
        Loaded database

    """

    if family not in ['northern', 'southern']:
        raise ValueError("Family must be either 'northern' or 'southern'")

    if param not in ['alt', 'Az']:
        raise ValueError("Param must be either 'alt' or 'Az'")

    if data == 'init':
        extension = '.mat'
    elif data == 'objects':
        extension = '.pkl'
    else:
        raise ValueError("Data must be either 'init' or 'objects'")

    filename = ''.join([DIRNAME, '/nrho_l2_', family[0:5], '_', param, extension])

    return family, param, filename


class InitConditions:
    """InitConditions class loads the database of initial conditions and orbital periods for the
    L2 Northern or Southern NRHO family parametrized by periselene altitude or vertical extension.

    Parameters
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    param : str
        Family parametrization, allowed values are ``alt`` for periselene altitude
        and ``Az`` for vertical extension

    Attributes
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    param : str
        Family parametrization, allowed values are ``alt`` for periselene altitude
        and ``Az`` for vertical extension
    ic_mat : dict
        NRHO family initial conditions as saved in the ``.mat`` file
    alt_dim : list
        Periselene altitudes [km]
    az_dim : list
        Vertical extensions [km]
    state0 : list
        Initial conditions in non dimensional units as ``[x, y, z, vx, vy, vz]`` [-]
    period : list
        Orbital periods in non dimensional units [-]
    nb_orb : int
        Number of orbits in the family

    Warnings
    --------
    Objects of this class are stored and read using `pickle`. After any modification rerun
    the script `nrho_propagation.py` setting ``compute = True`` to update the corresponding
    databases.

    """

    def __init__(self, family, param):
        """Initializes InitConditions class. """

        self.family, self.param, filename = get_filename(family, param, 'init')

        self.ic_mat = loadmat(filename, squeeze_me=True)

        self.state0 = self.ic_mat['y0']
        self.period = self.ic_mat['T']

        if self.param == 'alt':
            self.alt_dim = self.ic_mat['alt']
        else:
            self.az_dim = self.ic_mat['Az']

        self.nb_orb = len(self.period)
