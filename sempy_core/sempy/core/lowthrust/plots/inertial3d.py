#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 15:18:21 2019

@author: Alberto FOSSA'
"""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d


class InertialKepOrb:
    """Class InertialKepOrb displays a Keplerian orbit in a 3D inertial reference frame.

    Parameters
    ----------
    title : str
        Title on top of the figure
    r_vec : ndarray
        Position vector time series [km]

    Attributes
    ----------
    title : str
        Title on top of the figure
    r_vec : ndarray
        Position vector time series [km]

    """

    def __init__(self, title, r_vec):
        """Initializes InertialKepOrb. """

        self.title = title
        self.r_vec = r_vec

    def plot(self):
        """Plots the Keplerian orbit. """

        fig = plt.figure()
        fig.suptitle(self.title)
        axs = plt.axes(projection='3d')

        axs.plot3D(self.r_vec[:, 0], self.r_vec[:, 1], self.r_vec[:, 2])
        axs.scatter3D(self.r_vec[0, 0], self.r_vec[0, 1], self.r_vec[0, 2],
                      color='k', label='departure point')
        axs.scatter3D(self.r_vec[-1, 0], self.r_vec[-1, 1], self.r_vec[-1, 2],
                      color='r', label='arrival point')

        r_max = np.max(self.r_vec)
        axs.set_xlim(-r_max * 1.1, r_max * 1.1)
        axs.set_ylim(-r_max * 1.1, r_max * 1.1)
        axs.set_zlim(-r_max * 1.1, r_max * 1.1)

        axs.legend(bbox_to_anchor=(1, 1), loc=0)
        axs.set_xlabel('x (km)')
        axs.set_ylabel('y (km)')
        axs.set_zlabel('z (km)')


class InertialLLO2LLO:
    """Class InertialLLO2LLO displays a transfer trajectory between LLOs in LCI reference frame.

    Parameters
    ----------
    title : str
        Title on top of the figure
    llo_dep : object
        `KepOrb` object for the departure orbit
    llo_arr : object
        `KepOrb` object for the arrival orbit
    t_vec : ndarray
        Time vector [s]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [km]
    r_vec_exp : ndarray, optional
        Position vector time series for the explicitly simulated transfer trajectory [km] or None.
        Default is None for which no trajectory will be displayed

    Attributes
    ----------
    title : str
        Title on top of the figure
    r_dep : ndarray
        Position vector time series for the departure orbit [km]
    r_arr : ndarray
        Position vector time series for the arrival orbit [km]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [km]
    r_vec_exp : ndarray or None
        Position vector time series for the explicitly simulated transfer trajectory [km]

    """

    def __init__(self, title, llo_dep, llo_arr, t_vec, r_vec, r_vec_exp=None):
        """Initializes InertialLLO2LLO. """

        self.title = title

        # propagate the initial and target orbits
        r_dep, _ = llo_dep.propagate(llo_dep.ta, t_vec, 'fwd')
        r_arr, _ = llo_arr.propagate(llo_arr.ta, t_vec, 'back')

        self.r_dep = r_dep
        self.r_arr = r_arr
        self.r_vec = r_vec
        self.r_vec_exp = r_vec_exp

    def plot(self):
        """Plots the transfer trajectory. """

        fig = plt.figure()
        fig.suptitle(self.title)
        axs = plt.axes(projection='3d')

        axs.plot3D(self.r_dep[:, 0], self.r_dep[:, 1], self.r_dep[:, 2],
                   label='departure orbit')
        axs.plot3D(self.r_arr[:, 0], self.r_arr[:, 1], self.r_arr[:, 2],
                   label='target orbit')
        axs.plot3D(self.r_vec[:, 0], self.r_vec[:, 1], self.r_vec[:, 2],
                   label='transfer trajectory')

        if self.r_vec_exp is not None:
            axs.plot3D(self.r_vec_exp[:, 0], self.r_vec_exp[:, 1], self.r_vec_exp[:, 2],
                       label='explicit simulation')

        axs.scatter3D(self.r_vec[0, 0], self.r_vec[0, 1], self.r_vec[0, 2], color='k',
                      label='departure point')
        axs.scatter3D(self.r_vec[-1, 0], self.r_vec[-1, 1], self.r_vec[-1, 2], color='r',
                      label='insertion point')

        axs.legend(bbox_to_anchor=(1, 1), loc=2)
        axs.set_xlabel('x (km)')
        axs.set_ylabel('y (km)')
        axs.set_zlabel('z (km)')
