#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 16:52:31 2019

@author: Alberto FOSSA'
"""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d

from sempy.core.lowthrust.coc.rotmat import lci2syn


def set_axes_limits_labels(axs, r_vec_inner, r_vec_outer=None):
    """Set the axis limits and labels for a 3D plot in synodic reference frame centred
    on the second primary.

    Parameters
    ----------
    axs : Axes
        Matplotlib `Axes` object
    r_vec_inner : ndarray
        Position vector time series for the lowest orbit [-]
    r_vec_outer : ndarray or None, optional
        Position vector time series for the highest orbit [-] or None. Default is None

    Returns
    -------
    ax : Axes
        Matplotlib `Axes` object

    """

    if r_vec_outer is not None:
        r_vec = np.vstack((r_vec_inner, r_vec_outer))
    else:
        r_vec = r_vec_inner

    upper_bound = np.max(r_vec, axis=0)  # upper bounds as [x_max, y_max, z_max]
    lower_bound = np.min(r_vec, axis=0)  # lower bounds as [x_min, y_min, z_min]

    delta = np.max(upper_bound - lower_bound) * 0.5  # figure half-width
    origin = (upper_bound + lower_bound) * 0.5  # figure center as [x_origin, y_origin, z_origin]

    lim = np.asarray((origin - delta, origin + delta)).transpose()  # axis limits

    axs.set_xlim(lim[0])
    axs.set_ylim(lim[1])
    axs.set_zlim(lim[2])

    axs.set_xlabel('x')
    axs.set_ylabel('y')
    axs.set_zlabel('z')

    return axs


class SynodicNRHO:
    """Class SynodicNRHO displays an NRHO orbit in synodic reference frame.

    Parameters
    ----------
    mu_cr3bp : float
        CR3BP mass parameter [-]
    r_vec : ndarray
        Position vector in non dimensional units [-]

    Attributes
    ----------
    mu_cr3bp : float
        CR3BP mass parameter [-]
    r_vec : ndarray
        Position vector in non dimensional units [-]

    """

    def __init__(self, mu_cr3bp, r_vec):
        """Initializes SynodicNRHO. """

        self.mu_cr3bp = mu_cr3bp
        self.r_vec = r_vec

    def plot(self):
        """Plots the NRHO orbit. """

        fig = plt.figure()
        fig.suptitle('NRHO orbit in synodic frame')
        axs = plt.axes(projection='3d')

        axs.plot3D(self.r_vec[:, 0], self.r_vec[:, 1], self.r_vec[:, 2], label='orbit')
        axs.scatter3D(1.0 - self.mu_cr3bp, 0.0, 0.0, color='k', label='Moon')

        axs = set_axes_limits_labels(axs, self.r_vec)
        axs.legend(bbox_to_anchor=(1, 1), loc=2)


class SynodicFamilyNRHO(SynodicNRHO):
    """Class SynodicFamilyNRHO displays a family of NRHO orbits in synodic reference frame.

    Parameters
    ----------
    mu_cr3bp : float
        CR3BP mass parameter [-]
    r_vec : dict
        Position vectors in non dimensional units [-]
    nb_orb : int
        Number of orbits in the family
    samples : bool, optional
        If True displays only the 100 stored samples. Default is False

    Attributes
    ----------
    mu_cr3bp : float
        CR3BP mass parameter [-]
    r_vec : dict
        Position vectors in non dimensional units [-]
    nb_orb : int
        Number of orbits in the family
    samples : bool, optional
        If True displays only the 100 stored samples. Default is False

    """

    def __init__(self, mu_cr3bp, r_vec, nb_orb, samples):
        """Initializes SynodicFamilyNRHO class. """

        SynodicNRHO.__init__(self, mu_cr3bp, r_vec)
        self.nb_orb = nb_orb
        self.samples = samples

    def plot(self):
        """Plots the family of NRHO orbits. """

        param = []

        fig = plt.figure()
        fig.suptitle('Family of NRHO orbits in synodic frame')
        axs = plt.axes(projection='3d')

        if self.samples:
            marker = '.'
        else:
            marker = None

        for k in self.r_vec.keys():
            r_vec_k = self.r_vec[k]
            axs.plot3D(r_vec_k[:, 0], r_vec_k[:, 1], r_vec_k[:, 2], marker=marker, label=k)
            param.append(int(k))

        axs.scatter3D(1.0 - self.mu_cr3bp, 0.0, 0.0, color='k', label='Moon')

        axs = set_axes_limits_labels(axs, self.r_vec[str(min(param))],
                                     self.r_vec[str(max(param))])
        axs.legend(bbox_to_anchor=(1, 1), loc=2)


class SynodicNRHO2NRHO:
    """Class SynodicNRHO2NRHO displays a transfer trajectory between NRHOs in synodic
    reference frame.

    Parameters
    ----------
    title : str
        Title on top of the figure
    r_dep : ndarray
        Position vector time series for the departure orbit [-]
    r_arr : ndarray
        Position vector time series for the arrival orbit [-]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [-]
    r_init : ndarray
        Initial position vector [-]
    r_final : ndarray
        Final position vector [-]
    r_vec_exp : ndarray, optional
        Position vector time series for the explicitly simulated transfer trajectory [-] or None.
        Default is None for which no transfer will be displayed

    Attributes
    ----------
    title : str
        Title on top of the figure
    r_dep : ndarray
        Position vector time series for the departure orbit [-]
    r_arr : ndarray
        Position vector time series for the arrival orbit [-]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [-]
    r_final : ndarray
        Final position vector [-]
    r_init : ndarray
        Initial position vector [-]
    r_vec_exp : ndarray or None
        Position vector time series for the explicitly simulated transfer trajectory [-]

    """

    def __init__(self, title, mu_cr3bp, r_dep, r_arr, r_vec, r_init, r_final, r_vec_exp=None):
        """Initializes SynodicNRHO2NRHO. """

        self.title = title
        self.mu_cr3bp = mu_cr3bp

        self.r_dep = r_dep
        self.r_arr = r_arr
        self.r_vec = r_vec
        self.r_init = r_init
        self.r_final = r_final
        self.r_vec_exp = r_vec_exp

    def plot(self):
        """Plots the transfer trajectory. """

        fig = plt.figure()
        fig.suptitle(self.title)
        axs = plt.axes(projection='3d')

        axs.plot3D(self.r_vec[:, 0], self.r_vec[:, 1], self.r_vec[:, 2], color='#2ca02c',
                   label='transfer trajectory')

        if self.r_vec_exp is not None:
            axs.plot3D(self.r_vec_exp[:, 0], self.r_vec_exp[:, 1], self.r_vec_exp[:, 2],
                       color='#d62728', label='explicit simulation')

        axs.plot3D(self.r_dep[:, 0], self.r_dep[:, 1], self.r_dep[:, 2], color='#1f77b4',
                   label='departure orbit')
        axs.plot3D(self.r_arr[:, 0], self.r_arr[:, 1], self.r_arr[:, 2], color='#ff7f0e',
                   label='target orbit')

        axs.scatter3D(1.0 - self.mu_cr3bp, 0.0, 0.0, color='k', label='Moon')
        axs.scatter3D(self.r_init[0], self.r_init[1], self.r_init[2], color='b',
                      label='departure point')
        axs.scatter3D(self.r_final[0], self.r_final[1], self.r_final[2], color='r',
                      label='insertion point')

        axs = set_axes_limits_labels(axs, self.r_dep, self.r_arr)
        axs.legend(bbox_to_anchor=(1, 1), loc=2)


class SynodicLLO2LLO:
    """Class SynodicLLO2LLO displays a transfer trajectory between LLOs in synodic
    reference frame.

    Parameters
    ----------
    title : str
        Title on top of the figure
    llo_dep : object
        `KepOrb` object for the departure orbit
    llo_arr : object
        `KepOrb` object for the arrival orbit
    cr3bp : Cr3bp
        `Cr3bp` object
    t_vec : ndarray
        Time vector [-]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [-]
    r_vec_exp : ndarray, optional
        Position vector time series for the explicitly simulated transfer trajectory [-] or None.
        Default is None for which no trajectory will be displayed

    Attributes
    ----------
    title : str
        Title on top of the figure
    r_dep : ndarray
        Position vector time series for the departure orbit [-]
    r_arr : ndarray
        Position vector time series for the arrival orbit [-]
    r_vec : ndarray
        Position vector time series for the optimal transfer trajectory [-]
    r_vec_exp : ndarray or None
        Position vector time series for the explicitly simulated transfer trajectory [-]

    """

    def __init__(self, title, llo_dep, llo_arr, cr3bp, t_vec, r_vec, r_vec_exp=None):
        """Initializes SynodicLLO2LLO. """

        self.title = title

        # propagate the initial and target orbits
        tc_cr3bp = cr3bp.T / 2 / np.pi
        t_dim = tc_cr3bp * t_vec
        r_dep, v_dep = llo_dep.propagate(llo_dep.ta, t_dim, 'fwd')
        r_arr, v_arr = llo_arr.propagate(llo_arr.ta, t_dim, 'back')

        # convert R, V in synodic reference frame
        nb_states = len(t_vec)
        states_dep_lci = np.hstack((r_dep, v_dep))
        states_arr_lci = np.hstack((r_arr, v_arr))
        states_dep = np.zeros((nb_states, 6))
        states_arr = np.zeros((nb_states, 6))

        for j in range(nb_states):
            states_dep[j] = lci2syn(states_dep_lci[j], t_dim[j], cr3bp.mu, cr3bp.L, tc_cr3bp)
            states_arr[j] = lci2syn(states_arr_lci[j], t_dim[j], cr3bp.mu, cr3bp.L, tc_cr3bp)

        self.r_dep = states_dep[:, :3]
        self.r_arr = states_arr[:, :3]
        self.r_vec = r_vec
        self.r_vec_exp = r_vec_exp

    def plot(self):
        """Plots the transfer trajectory. """

        fig = plt.figure()
        fig.suptitle(self.title)
        axs = plt.axes(projection='3d')

        axs.plot3D(self.r_dep[:, 0], self.r_dep[:, 1], self.r_dep[:, 2],
                   label='departure orbit')
        axs.plot3D(self.r_arr[:, 0], self.r_arr[:, 1], self.r_arr[:, 2],
                   label='target orbit')
        axs.plot3D(self.r_vec[:, 0], self.r_vec[:, 1], self.r_vec[:, 2],
                   label='transfer trajectory')

        if self.r_vec_exp is not None:
            axs.plot3D(self.r_vec_exp[:, 0], self.r_vec_exp[:, 1], self.r_vec_exp[:, 2],
                       label='explicit simulation')

        axs.scatter3D(self.r_vec[0, 0], self.r_vec[0, 1], self.r_vec[0, 2], color='k',
                      label='departure point')
        axs.scatter3D(self.r_vec[-1, 0], self.r_vec[-1, 1], self.r_vec[-1, 2], color='r',
                      label='insertion point')

        axs.legend(bbox_to_anchor=(1, 1), loc=2)
        axs.set_xlabel('x')
        axs.set_ylabel('y')
        axs.set_zlabel('z')
