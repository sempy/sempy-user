#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 18:05:47 2019

@author: Alberto FOSSA'
"""

import matplotlib.pyplot as plt
import numpy as np


class ThrustMagnitudePlot:
    """Displays the thrust magnitude profile.

    Parameters
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    time_unit : str
        Units of time shown on the x axis label

    **kwargs :
    time2: ndarray
        Second time vector in (non)dimensional units [s] or [-]
    thrust2: ndarray
        Second thrust magnitude time series [N]
    labels: tuple
        Legend labels for the two set of data

    Attributes
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    time_unit : str
        Units of time shown on the x axis label
    time2: ndarray
        Second time vector in (non)dimensional units [s] or [-]
    thrust2: ndarray
        Second thrust magnitude time series [N]
    labels: tuple
        Legend labels for the two set of data

    """

    def __init__(self, time, thrust, time_unit, **kwargs):
        """Initializes ThrustMagnitudePlot. """

        self.time = time
        self.thrust = thrust
        self.time_unit = time_unit

        if ('time2' in kwargs) and ('thrust2' in kwargs) and ('labels' in kwargs):
            self.time2 = kwargs['time2']
            self.thrust2 = kwargs['thrust2']
            self.labels = kwargs['labels']

    def plot(self):
        """Plot the time series. """

        _, axs = plt.subplots(constrained_layout=True)

        if hasattr(self, 'thrust2'):
            axs.plot(self.time, self.thrust, label=self.labels[0])
            axs.plot(self.time2, self.thrust2, label=self.labels[1])
        else:
            axs.plot(self.time, self.thrust)

        axs.set_xlabel('time (' + self.time_unit + ')')
        axs.set_ylabel('thrust (N)')
        axs.set_title('Thrust magnitude profile')
        axs.grid()


class ThrustDirectionPlot:
    """Displays the thrust direction profile.

    Parameters
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    u_vec : ndarray
        Thrust direction unit vector time series [-]
    time_unit : str
        Units of time shown on the x axis label

    Attributes
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    u_vec : ndarray
        Thrust direction unit vector time series [-]
    time_unit : str
        Units of time shown on the x axis label
    u_norm : ndarray
        Thrust direction unit vector magnitude [-]

    """

    def __init__(self, time, u_vec, time_unit):
        """Initializes ThrustDirectionPlot. """

        self.time = time
        self.u_vec = u_vec
        self.time_unit = time_unit
        self.u_norm = np.linalg.norm(u_vec, 2, axis=1, keepdims=True)

    def plot(self):
        """Plot the time series. """

        _, axs = plt.subplots(constrained_layout=True)

        labels = ['ux', 'uy', 'uz']

        for i in range(3):
            axs.plot(self.time, self.u_vec[:, i], label=labels[i])

        axs.plot(self.time, self.u_norm, color='k', label='norm')
        axs.set_xlabel('time (' + self.time_unit + ')')
        axs.set_ylabel('thrust direction (-)')
        axs.set_title('Thrust direction profile')
        axs.grid()
        axs.legend(loc='best')


class ThrustComponentsPlot:
    """Displays the thrust components profile.

    Parameters
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    u_vec : ndarray
        Thrust direction unit vector time series [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    time_unit : str
        Units of time shown on the x axis label

    Attributes
    ----------
    time : ndarray
        Time vector in (non)dimensional units [s] or [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    u_mag : ndarray
        Thrust components magnitude time series [N]
    time_unit : str
        Units of time shown on the x axis label

    """

    def __init__(self, time, u_vec, thrust, time_unit):
        """Initializes ThrustComponentsPlot. """

        self.time = time
        self.time_unit = time_unit
        self.thrust = np.reshape(thrust, (len(thrust), 1))
        self.u_mag = np.fabs(u_vec) * self.thrust

    def plot(self):
        """Plot the time series. """

        _, axs = plt.subplots(constrained_layout=True)

        axs.plot(self.time, self.thrust, color='#404040', label='magnitude')

        labels = ['ux', 'uy', 'uz']

        for i in range(3):
            axs.plot(self.time, self.u_mag[:, i], label=labels[i])

        axs.set_xlabel('time (' + self.time_unit + ')')
        axs.set_ylabel('thrust components (N)')
        axs.set_title('Thrust components profile')
        axs.grid()
        axs.legend(loc='best')


class JacobiConstantPlot:
    """Displays the Jacobi constant profile.

    Parameters
    ----------
    time : ndarray
        Time vector in (non)dimensional units for the optimal transfer trajectory [s] or [-]
    c_dep : float
        Jacobi constant for the departure orbit [-]
    c_arr : float
        Jacobi constant for the arrival orbit [-]
    c_vec : ndarray
        Jacobi constant time series for the optimal transfer trajectory [-]
    time_unit : str
        Units of time shown on the x axis label
    time_exp : ndarray, optional
        Time vector in (non)dimensional units or the explicit simulation [s] or [-] or None.
        Default is None for which no time series will be displayed
    c_vec_exp: ndarray, optional
        Jacobi constant time series for the explicit simulation [-] or None.
        Default is None for which no time series will be displayed

    Attributes
    ----------
    time : ndarray
        Time vector in (non)dimensional units for the optimal transfer trajectory [s] or [-]
    c_dep : float
        Jacobi constant for the departure orbit [-]
    c_arr : float
        Jacobi constant for the arrival orbit [-]
    c_vec : ndarray
        Jacobi constant time series for the optimal transfer trajectory [-]
    time_unit : str
        Units of time shown on the x axis label
    time_exp : ndarray or None
        Time vector in (non)dimensional units or the explicit simulation [s] or [-] or None.
    c_vec_exp: ndarray or None
        Jacobi constant time series for the explicit simulation [-] or None

    """

    def __init__(self, time, c_dep, c_arr, c_vec, time_unit, time_exp=None, c_vec_exp=None):
        """Initializes JacobiConstantPlot. """

        self.time = time
        self.c_dep = c_dep
        self.c_arr = c_arr
        self.c_vec = c_vec
        self.time_unit = time_unit
        self.time_exp = time_exp
        self.c_vec_exp = c_vec_exp

    def plot(self):
        """Plot the time series. """

        _, axs = plt.subplots(constrained_layout=True)

        if (self.time_exp is None) and (self.c_vec_exp is None):
            axs.plot(self.time, self.c_vec, label='transfer')
        else:
            axs.plot(self.time, self.c_vec, label='implicit')
            axs.plot(self.time, self.c_vec, label='explicit')

        axs.scatter(self.time[0], self.c_dep, color='k', label='departure orbit')
        axs.scatter(self.time[-1], self.c_arr, color='r', label='arrival orbit')

        axs.set_xlabel('time (' + self.time_unit + ')')
        axs.set_ylabel('Jacobi constant (-)')
        axs.set_title('Jacobi constant profile')
        axs.grid()
        axs.legend(loc='best')


class StatesPlot:
    """Displays the States profile.

    Parameters
    ----------
    time : ndarray
        Time vector for the optimal transfer trajectory in any unit
    states : ndarray
        States time series for the optimal transfer trajectory
        as ``[x, y, z, vx, vy, vz]`` in any unit
    time_exp : ndarray, optional
        Time vector for the explicit simulation in any unit or None.
        Default is None for which no states will be displayed
    states_exp : ndarray, optional
        States time series for the explicit simulation
        as ``[x, y, z, vx, vy, vz]`` in any unit or None.
        Default is None for which no states will be displayed
    scalers : tuple, optional
        Scaling parameters for time, positions and velocities. Default is ``(1, 1, 1)``
    units : tuple
        Units of measure for time, positions and velocities. Default is ``('-', '-', '-')``


    Attributes
    ----------
    time : ndarray
        Time vector for the optimal transfer trajectory in any unit
    states : ndarray
        States time series for the optimal transfer trajectory
        as ``[x, y, z, vx, vy, vz]`` in any unit
    time_exp : ndarray or None
        Time vector for the explicit simulation in any unit or None
    states_exp : ndarray or None
        States time series for the explicit simulation
        as ``[x, y, z, vx, vy, vz]`` in any unit or None
    units : tuple
        Units of measure for time, positions and velocities

    """

    def __init__(self, time, states, time_exp=None, states_exp=None, scalers=(1, 1, 1),
                 units=('-', '-', '-')):

        states_scalers = np.hstack((np.ones(3) * scalers[1], np.ones(3) * scalers[2]))

        self.time = time * scalers[0]
        self.states = states * states_scalers

        if (time_exp is not None) and (states_exp is not None):
            self.time_exp = time_exp * scalers[0]
            self.states_exp = states_exp * states_scalers
        else:
            self.time_exp = None
            self.states_exp = None

        self.units = units

    def plot(self):
        """Plot the state variables profiles over time. """

        fig, axs = plt.subplots(ncols=2, constrained_layout=True)
        fig.suptitle('States variables profiles')

        labels = ['x', 'y', 'z', 'vx', 'vy', 'vz']

        for i in range(3):
            axs[0].plot(self.time, self.states[:, i], label=labels[i])
        for i in range(3, 6):
            axs[1].plot(self.time, self.states[:, i], label=labels[i])

        if self.states_exp is not None:
            for i in range(3):
                axs[0].plot(self.time_exp, self.states_exp[:, i], label=(labels[i] + ' exp'))
            for i in range(3, 6):
                axs[1].plot(self.time_exp, self.states_exp[:, i], label=(labels[i] + ' exp'))

        axs[0].set_title('Position')
        axs[0].set_ylabel('position (' + self.units[1] + ')')

        axs[1].set_title('Velocity')
        axs[1].set_ylabel('velocity (' + self.units[2] + ')')

        for i in range(2):
            axs[i].set_xlabel('time (' + self.units[0] + ')')
            axs[i].grid()
            axs[i].legend(loc='best')
