#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 14:21:43 2019

@author: Alberto FOSSA'
"""

from copy import deepcopy

import numpy as np

from sempy.core.lowthrust.plots.synodic3d import SynodicNRHO2NRHO, SynodicLLO2LLO
from sempy.core.lowthrust.plots.timeseries import ThrustMagnitudePlot, ThrustDirectionPlot, \
    ThrustComponentsPlot, StatesPlot, JacobiConstantPlot


class SolutionPlot:
    """SolutionPlot class displays the thrust magnitude and direction time series for an
    optimal transfer trajectory in the CR3BP framework.

    Parameters
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    sol : dict
        Implicit solution
    sol_exp : dict or None, optional
        Explicit simulation or None. Default is None

    Attributes
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [s]
    scalers : tuple
        Scaling parameters for time, length and speed [days/s, km, km/s]
    units : tuple
        Units to be displayed on the figures axes
    sol : dict
        Implicit solution
    sol_exp : dict or None
        Explicit simulation or None
    thrust_mag_plot : ThrustMagnitudePlot
        `ThrustMagnitudePlot` object
    thrust_dir_plot : ThrustDirectionPlot
        `ThrustDirectionPlot` object
    thrust_comp_plot : ThrustComponentsPlot
        `ThrustComponentsPlot` object

    """

    def __init__(self, cr3bp, sol, sol_exp=None):
        """Initializes SolutionPlot class. """

        self.cr3bp = cr3bp
        self.tc_cr3bp = self.cr3bp.T / 2 / np.pi
        self.scalers = (1 / 86400, self.cr3bp.L, self.cr3bp.L / self.tc_cr3bp)
        self.units = ('days', 'km', 'km/s')

        self.sol = deepcopy(sol)
        self.sol_exp = deepcopy(sol_exp)

        # controls variables time series plots
        time = self.sol['powered']['time_dim']
        controls = self.sol['powered']['controls']

        self.thrust_mag_plot = ThrustMagnitudePlot(time * self.scalers[0], controls[:, 0],
                                                   self.units[0])
        self.thrust_dir_plot = ThrustDirectionPlot(time * self.scalers[0], controls[:, 1:],
                                                   self.units[0])
        self.thrust_comp_plot = ThrustComponentsPlot(time * self.scalers[0], controls[:, 1:],
                                                     controls[:, 0], self.units[0])

    def plot(self):
        """Plots the thrust magnitude and direction time series. """

        self.thrust_mag_plot.plot()
        self.thrust_dir_plot.plot()
        self.thrust_comp_plot.plot()


class NRHO2NRHOSolutionPlot(SolutionPlot):
    """NRHO2NRHOSolutionPlot class displays the thrust magnitude and direction, positions,
    velocities and Jacobi constant time series and the three-dimensional transfer trajectory for
    an optimal transfer between NRHOs.

    Parameters
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    bcs : InitNRHOs
        `InitNRHOs` object for the initial and target orbits
    sol : dict
        Implicit solution
    sol_exp : dict or None, optional
        Explicit simulation or None. Default is None

    Attributes
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    tc : float
        Characteristic time [s]
    scalers : tuple
        Scaling parameters for time, length and speed [days/s, km, km/s]
    units : tuple
        Units to be displayed on the figures axes
    bcs : InitNRHOs
        `InitNRHOs` object for the initial and target orbits
    sol : dict
        Implicit solution
    sol_exp : dict or None
        Explicit simulation or None
    thrust_mag_plot : ThrustMagnitudePlot
        `ThrustMagnitudePlot` object
    thrust_dir_plot : ThrustDirectionPlot
        `ThrustDirectionPlot` object
    thrust_comp_plot : ThrustComponentsPlot
        `ThrustComponentsPlot` object
    trajectory_plot : SynodicNRHO2NRHO
        `SynodicNRHO2NRHO` object
    states_plot : object
        `StatesPlot` object
    jc_const_plot : JacobiConstantPlot
        `JacobiConstantPlot` object

    """

    def __init__(self, cr3bp, bcs, sol, sol_exp=None):

        SolutionPlot.__init__(self, cr3bp, sol, sol_exp)

        self.bcs = bcs  # initial and target NRHOs

        # 3D transfer trajectory and positions, velocities and Jacobi constant time series plots
        self.sol.pop('kind')
        time, states, c_jac = self.stack_solution(self.sol)
        title = 'Transfer trajectory between NRHOs in synodic frame'

        if self.sol_exp is None:  # only implicit solution

            self.trajectory_plot = \
                SynodicNRHO2NRHO(title, self.cr3bp.mu, self.bcs.nrho_dep.state_vec[:, :3],
                                 self.bcs.nrho_arr.state_vec[:, :3], states[:, :3],
                                 self.sol['powered']['departure'][:3],
                                 self.sol['powered']['insertion'][:3])
            self.states_plot = StatesPlot(time, states, scalers=self.scalers, units=self.units)
            self.jc_const_plot = \
                JacobiConstantPlot(time * self.scalers[0], self.bcs.nrho_dep.c_jac,
                                   self.bcs.nrho_arr.c_jac, c_jac, self.units[0])

        else:  # implicit solution and explicit simulation

            self.sol_exp.pop('kind')
            self.sol_exp.pop('error')
            time_exp, states_exp, c_exp = self.stack_solution(self.sol_exp)
            self.trajectory_plot = \
                SynodicNRHO2NRHO(title, self.cr3bp.mu, self.bcs.nrho_dep.state_vec[:, :3],
                                 self.bcs.nrho_arr.state_vec[:, :3], states[:, :3],
                                 self.sol['powered']['departure'][:3],
                                 self.sol['powered']['insertion'][:3], r_vec_exp=states_exp[:, :3])
            self.states_plot = StatesPlot(time, states, time_exp=time_exp, states_exp=states_exp,
                                          scalers=self.scalers, units=self.units)
            self.jc_const_plot = \
                JacobiConstantPlot(time * self.scalers[0], self.bcs.nrho_dep.c_jac,
                                   self.bcs.nrho_arr.c_jac, c_jac, self.units[0], c_vec_exp=c_exp)

    @staticmethod
    def stack_solution(sol):
        """Stacks together the time vectors and the states and Jacobi constants time series that
        belongs to different phases in the same solution dictionary.

        Parameters
        ----------
        sol : dict
            Solution dictionary

        Returns
        -------
        time : ndarray
            Time vector for all phases [-]
        states : ndarray
            States variables time series for all phases [-]
        c_jac : ndarray
            Jacobi constant time series for all phases [-]

        """

        time_list = []
        states_list = []
        c_list = []

        # stack together different phases
        for k in sol.keys():
            time_list.append(sol[k]['time_dim'])
            states_list.append(sol[k]['states'][:, :6])
            c_list.append(sol[k]['C'])

        time = np.vstack(tuple(i for i in time_list))  # time vector
        states = np.vstack(tuple(i for i in states_list))  # positions and velocities time series
        c_jac = np.vstack(tuple(i for i in c_list))  # Jacobi constant time series

        return time, states, c_jac

    def plot(self):
        """Plots the thrust magnitude and direction, positions, velocities and Jacobi
        constant time series and the three-dimensional transfer trajectory.

        """

        SolutionPlot.plot(self)

        self.trajectory_plot.plot()
        self.states_plot.plot()
        self.jc_const_plot.plot()


class LLO2LLOSolutionPlot(SolutionPlot):
    """LLO2LLOSolutionPlot class displays the thrust magnitude and direction, positions
    and velocities time series and the three-dimensional transfer trajectory for an optimal
    transfer between LLOs.

    Parameters
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    llo_dep : object
        `KepOrb` object for the departure LLO
    llo_arr : object
        `KepOrb` object for the arrival LLO
    sol : dict
        Implicit solution
    sol_exp : dict or None, optional
        Explicit simulation or None. Default is None

    Attributes
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object
    tc : float
        Characteristic time [s]
    scalers : tuple
        Scaling parameters for time, length and speed [days/s, km, km/s]
    units : tuple
        Units to be displayed on the figures axes
    llo_dep : object
        `KepOrb` object for the departure LLO
    llo_arr : object
        `KepOrb` object for the arrival LLO
    sol : dict
        Implicit solution
    sol_exp : dict or None
        Explicit simulation or None
    thrust_mag_plot : ThrustMagnitudePlot
        `ThrustMagnitudePlot` object
    thrust_dir_plot : ThrustDirectionPlot
        `ThrustDirectionPlot` object
    thrust_comp_plot : ThrustComponentsPlot
        `ThrustComponentsPlot` object
    trajectory_plot : object
        `SynodicLLO2LLO` object
    states_plot : object
        `StatesPlot` object

    """

    def __init__(self, cr3bp, llo_dep, llo_arr, sol, sol_exp=None):
        """Inits LLO2LLOSolutionPlot class. """

        SolutionPlot.__init__(self, cr3bp, sol, sol_exp)

        self.llo_dep = llo_dep  # initial LLO
        self.llo_arr = llo_arr  # target LLO

        # 3D transfer trajectory and positions and velocities time series plots
        self.sol.pop('kind')
        title = 'Transfer trajectory between LLOs in synodic frame'

        if self.sol_exp is None:  # only implicit solution

            self.trajectory_plot = SynodicLLO2LLO(title, self.llo_dep, self.llo_arr, self.cr3bp,
                                                  self.sol['powered']['time'].flatten(),
                                                  self.sol['powered']['states'][:, :3])
            self.states_plot = StatesPlot(self.sol['powered']['time_dim'],
                                          self.sol['powered']['states'][:, :6],
                                          scalers=self.scalers, units=self.units)

        else:  # implicit solution and explicit simulation

            self.sol_exp.pop('kind')
            self.sol_exp.pop('error')
            self.trajectory_plot = \
                SynodicLLO2LLO(title, self.llo_dep, self.llo_arr, self.cr3bp,
                               self.sol['powered']['time'].flatten(),
                               self.sol['powered']['states'][:, :3],
                               r_vec_exp=self.sol_exp['powered']['states'][:, :3])
            self.states_plot = \
                StatesPlot(self.sol['powered']['time_dim'], self.sol['powered']['states'][:, :6],
                           time_exp=self.sol_exp['powered']['time_dim'],
                           states_exp=self.sol_exp['powered']['states'][:, :6],
                           scalers=self.scalers, units=self.units)

    def plot(self):
        """Plots the thrust magnitude and direction, positions and velocities time series and
        the three-dimensional transfer trajectory.

        """

        SolutionPlot.plot(self)

        self.trajectory_plot.plot()
        self.states_plot.plot()
