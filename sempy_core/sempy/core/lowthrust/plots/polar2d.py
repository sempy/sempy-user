#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 08:53:41 2019

@author: Alberto FOSSA'
"""

import matplotlib.pyplot as plt
import numpy as np


class PolarTrajectory:
    """Class PolarTrajectory displays a planar trajectory in polar coordinates.

    Parameters
    ----------
    title : str
        Title on top of the figure
    r_dep : float
        Initial orbit radius [km]
    r_arr : float
        Final orbit radius [km]
    r_body : float
        Central body radius [km]
    r_vec : ndarray
        Transfer trajectory radius time series from analytic solution [km]
    ang_vec: ndarray
        Transfer trajectory angle time series from analytic solution [rad]
    r_vec_exp : ndarray or None, optional
        Transfer trajectory radius time series from numerical propagation [km] or None.
        Default is None
    ang_vec_exp: ndarray or None, optional
        Transfer trajectory angle time series from numerical propagation [rad] or None.
        Default is None

    Attributes
    ----------
    title : str
        Title on top of the figure
    r_dep : float
        Initial orbit radius [km]
    r_arr : float
        Final orbit radius [km]
    r_body : float
        Central body radius [km]
    r_vec : ndarray
        Transfer trajectory radius time series from analytic solution [km]
    ang_vec: ndarray
        Transfer trajectory angle time series from analytic solution [rad]
    r_vec_exp : ndarray or None
        Transfer trajectory radius time series from numerical propagation [km] or None
    ang_vec_exp: ndarray or None
        Transfer trajectory angle time series from numerical propagation [rad] or None

    """

    def __init__(self, title, r_dep, r_arr, r_body, r_vec, ang_vec,
                 r_vec_exp=None, ang_vec_exp=None):
        """Initializes PolarTrajectory. """

        self.title = title
        self.r_dep = r_dep
        self.r_arr = r_arr
        self.r_body = r_body
        self.r_vec = r_vec
        self.ang_vec = ang_vec
        self.r_vec_exp = r_vec_exp
        self.ang_vec_exp = ang_vec_exp

    def plot(self):
        """Plots the transfer trajectory. """

        # planet surface, initial and target orbits points
        alpha = np.linspace(0, 2 * np.pi, 1000)

        x_body = self.r_body * np.cos(alpha)
        y_body = self.r_body * np.sin(alpha)

        x_dep = self.r_dep * np.cos(alpha)
        y_dep = self.r_dep * np.sin(alpha)

        x_arr = self.r_arr * np.cos(alpha)
        y_arr = self.r_arr * np.sin(alpha)

        # transfer trajectory points
        x_vec = self.r_vec * np.cos(self.ang_vec)
        y_vec = self.r_vec * np.sin(self.ang_vec)

        # figure
        _, axs = plt.subplots(constrained_layout=True)

        axs.plot(x_body, y_body, label='Moon surface')
        axs.plot(x_dep, y_dep, label='Initial orbit')
        axs.plot(x_arr, y_arr, label='Target orbit')
        axs.plot(x_vec, y_vec, label='Transfer trajectory')

        axs.scatter(x_vec[0], y_vec[0], color='k', label='Departure point', zorder=10)
        axs.scatter(x_vec[-1], y_vec[-1], color='r', label='Insertion point', zorder=15)

        if (self.r_vec_exp is not None) and (self.ang_vec_exp is not None):
            x_vec_exp = self.r_vec_exp * np.cos(self.ang_vec_exp)
            y_vec_exp = self.r_vec_exp * np.sin(self.ang_vec_exp)
            axs.plot(x_vec_exp, y_vec_exp, '.', color='k', label='Explicit simulation')

        axs.set_aspect('equal')
        axs.grid()
        axs.legend(bbox_to_anchor=(1, 1), loc=2)
        axs.set_xlabel('x (km)')
        axs.set_ylabel('y (km)')
        axs.set_title(self.title)
        axs.tick_params(axis='x', rotation=60)


class PolarTimeSeries:
    """Class PolarTimeSeries displays the time series of states and control variables for
    a planar transfer trajectory in polar coordinates.

    Parameters
    ----------
    title : str
        Title on top of the figure
    time: ndarray
        Time vector [s]
    a_sol : object
        Time series from analytic solution as ``[r, theta, u, v]`` [km, rad, km/s, km/s]
    num_sol : object
        Time series from numerical simulation as ``[r, theta, u, v]`` [km, rad, km/s, km/s]
    mass : ndarray
        Spacecraft mass time series [kg]
    thrust : ndarray
        Thrust magnitude time series [N]


    Attributes
    ----------
    title : str
        Title on top of the figure
    time: ndarray
        Time vector [s]
    a_sol : object
        Time series from analytic solution as ``[r, theta, u, v]`` [km, rad, km/s, km/s]
    num_sol : object
        Time series from numerical simulation as ``[r, theta, u, v]`` [km, rad, km/s, km/s]
    mass : ndarray
        Spacecraft mass time series [kg]
    thrust : ndarray
        Thrust magnitude time series [N]

    """

    def __init__(self, title, time, a_sol, num_sol, mass, thrust):
        """Initializes PolarTimeSeries class. """

        self.title = title
        self.time = time
        self.a_sol = a_sol
        self.num_sol = num_sol
        self.mass = mass
        self.thrust = thrust

    def plot(self):
        """Plots the time series. """

        fig, axs = plt.subplots(2, 3, constrained_layout=True)
        fig.suptitle(self.title)

        # radius (km)
        axs[0, 0].plot(self.time, self.a_sol[:, 0], color='b', label='analytic')
        axs[0, 0].plot(self.time, self.num_sol[:, 0], '--', color='r', label='numerical')
        axs[0, 0].set_xlabel('time (s)')
        axs[0, 0].set_ylabel('r (km)')
        axs[0, 0].set_title('Radius')
        axs[0, 0].grid()
        # axs[0, 0].legend(loc=0)

        # angle (deg)
        axs[0, 1].plot(self.time, self.a_sol[:, 1] * 180 / np.pi, color='b', label='analytic')
        axs[0, 1].plot(self.time, self.num_sol[:, 1] * 180 / np.pi, '--', color='r',
                       label='numerical')
        axs[0, 1].set_xlabel('time (s)')
        axs[0, 1].set_ylabel('theta (deg)')
        axs[0, 1].set_title('Angle')
        axs[0, 1].grid()
        # axs[0, 1].legend(loc=0)

        # flight path angle (deg)
        axs[0, 2].plot(self.time, self.thrust, '--', color='r', label='numerical')
        axs[0, 2].set_xlabel('time (s)')
        axs[0, 2].set_ylabel('thrust (N)')
        axs[0, 2].set_title('Thrust magnitude')
        axs[0, 2].grid()
        # axs[0, 2].legend(loc=0)

        # radial velocity (km/s)
        axs[1, 0].plot(self.time, self.a_sol[:, 2], color='b', label='analytic')
        axs[1, 0].plot(self.time, self.num_sol[:, 2], '--', color='r', label='numerical')
        axs[1, 0].set_xlabel('time (s)')
        axs[1, 0].set_ylabel('u (km/s)')
        axs[1, 0].set_title('Radial velocity')
        axs[1, 0].grid()
        # axs[1, 0].legend(loc=0)

        # tangential velocity (km/s)
        axs[1, 1].plot(self.time, self.a_sol[:, 3], color='b', label='analytic')
        axs[1, 1].plot(self.time, self.num_sol[:, 3], '--', color='r', label='numerical')
        axs[1, 1].set_xlabel('time (s)')
        axs[1, 1].set_ylabel('v (km/s)')
        axs[1, 1].set_title('Tangential velocity')
        axs[1, 1].grid()
        # axs[1, 1].legend(loc=0)

        # mass (kg)
        axs[1, 2].plot(self.time, self.mass, '--', color='r', label='numerical')
        axs[1, 2].set_xlabel('time (s)')
        axs[1, 2].set_ylabel('mass (kg)')
        axs[1, 2].set_title('Mass')
        axs[1, 2].grid()
        # axs[1, 2].legend(loc=0)
