#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 15:51:53 2019

@author: Alberto FOSSA'
"""

import numpy as np
from scipy.optimize import root

from sempy.core.init.primary import Primary

from sempy.core.lowthrust.coc.rotmat import per2eq
from sempy.core.lowthrust.coc.coe2svvec import coe2sv_vec
from sempy.core.lowthrust.plots.inertial3d import InertialKepOrb


class KepOrb:
    """KepOrb defines a Keplerian Orbit.

    Parameters
    ----------
    a : float
        Semi-major axis [km]
    e : float
        Eccentricity [-]
    i : float
        Inclination [rad]
    W : float
        Right Ascension of the Ascending Node [rad]
    w : float
        Argument of Periapsis [rad]
    ta : float
        True anomaly [rad]
    gm : float, optional
        Central body standard gravitational parameter [km^3/s^2].
        Default is for an orbit about the Moon.
        
    Attributes
    ----------
    eps : float
        Smallest number such that ``1.0 + eps != 1.0``
    a : float
        Semi-major axis [km]
    e : float
        Eccentricity [-]
    i : float
        Inclination [rad]
    W : float
        Right Ascension of the Ascending Node [rad]
    w : float
        Argument of Periapsis [rad]
    ta : float
        True anomaly [rad]
    GM : float, optional
        Central body standard gravitational parameter [km^3/s^2]
    n : float
        Mean motion [rad/s]
    R : ndarray
        Position vector [km]
    V : ndarray
        Velocity vector [km/s]
    H : ndarray
        Specific angular momentum vector [km^2/s]
    E : ndarray
        Eccentricity vector [-]
    h : float
        Specific angular momentum magnitude [km^2/s]
    r : float
        Position vector magnitude [km]
    vr : float
        Radial velocity [km/s]
        
    """
    
    def __init__(self, a, e, i, W, w, ta, gm=Primary.MOON.GM):
        """Initializes KepOrb class. """ 
        
        self.eps = np.finfo(float).eps
        
        self.a = a
        self.e = e
        self.i = i
        self.W = W
        self.w = w
        self.ta = ta

        self.GM = gm

        # initialization
        self.n = None
        self.R = None
        self.V = None
        self.H = None
        self.E = None
        self.h = None
        self.r = None
        self.vr = None

        self.compute_state_vector()
        
    def set_state_vector(self, r, v):
        """Set the spacecraft state vector and compute the corresponding COE, H, E.

        Parameters
        ----------
        r : ndarray
            Position vector [km]
        v : ndarray
            Velocity vector [km/s]

        """
        
        self.R = r
        self.V = v
        
        self.compute_classical_orbital_elements()
        
    def set_classical_orbital_elements(self, a, e, i, W, w, ta):
        """Set the spacecraft COE and compute the corresponding state vector, H, E.

        Parameters
        ----------
        a : float
            Semi-major axis [km]
        e : float
            Eccentricity [-]
        i : float
            Inclination [rad]
        W : float
            Right Ascension of the Ascending Node [rad]
        w : float
            Argument of Periapsis [rad]
        ta : float
            True anomaly [rad]

        """
        
        self.a = a
        self.e = e
        self.i = i
        self.W = W
        self.w = w
        self.ta = ta
        
        self.compute_state_vector()
        
    def set_true_anomaly(self, ta):
        """Set the spacecraft true anomaly and update the corresponding state vector.

        Parameters
        ----------
        ta : float
            True anomaly [rad]

        """
        
        self.ta = ta
        self.compute_state_vector()
        
    def compute_mean_motion(self):
        """Computes the spacecraft mean motion. """
        
        self.n = (self.GM*self.a**-3)**0.5
        
    def compute_angular_momentum(self):
        """Computes the specific angular momentum vector. """
        
        self.H = np.cross(self.R, self.V)
        
    def compute_eccentricity(self):
        """Compute the spacecraft eccentricity vector. """
        
        self.E = np.cross(self.V, self.H)/self.GM - self.R/self.r
                    
    def compute_classical_orbital_elements(self):
        """Computes the spacecraft classical orbital elements, specific angular momentum vector
        and eccentricity vector from its state vector. """
        
        self.r = np.linalg.norm(self.R, 2)  # orbit radius
        self.vr = np.dot(self.R, self.V)/self.r  # radial velocity
        
        self.compute_angular_momentum()  # angular momentum vector
        self.h = np.linalg.norm(self.H, 2)  # angular momentum magnitude
    
        self.i = np.arccos(self.H[2]/self.h)  # inclination
        
        K = np.array([0, 0, 1])  # unit vector along inertial Z axis
        N = np.cross(K, self.H)  # node line vector
        n = np.linalg.norm(N, 2)  # node line vector magnitude
            
        # right ascension of the ascending node
        if self.i >= self.eps:  # inclined orbit
            self.W = np.arccos(N[0]/n)
            if N[1] < 0.0:
                self.W = 2*np.pi - self.W
        else:  # equatorial orbit
            self.W = 0.0
        
        self.compute_eccentricity()  # eccentricity vector
        self.e = np.linalg.norm(self.E, 2)  # eccentricity
    
        # argument of periapsis
        if self.e >= self.eps:  # elliptical orbit
            if self.i >= self.eps:  # inclined orbit
                self.w = np.arccos(np.dot(N, self.E)/(n*self.e))
                if self.E[2] < 0.0:
                    self.w = 2*np.pi - self.w
            else:  # equatorial orbit
                self.w = np.arccos(self.E[0]/self.e)
                if self.E[1] < 0.0:
                    self.w = 2*np.pi - self.w
        else:  # circular orbit
            self.w = 0.0
        
        # true anomaly
        if self.e >= self.eps:  # elliptical orbit
            self.ta = np.arccos(np.dot(self.E, self.R)/(self.e*self.r))
            if self.vr < 0.0:
                self.ta = 2*np.pi - self.ta
        else:  # circular orbit
            if self.i >= self.eps:  # inclined orbit
                self.ta = np.arccos(np.dot(N, self.R)/(n*self.r))
                if self.R[2] < 0.0:
                    self.ta = 2*np.pi - self.ta
            else:  # equatorial orbit
                self.ta = np.arccos(self.R[0]/self.r)
                if self.R[1] < 0.0:
                    self.ta = 2*np.pi - self.ta
        
        self.a = (self.h**2/self.GM)/(1 - self.e**2)  # semi-major axis
        self.compute_mean_motion()
        
    def compute_state_vector(self):
        """Computes the spacecraft state vector, specific angular momentum vector and
        eccentricity vector from its COE. """
        
        self.h = (self.GM*self.a*(1 - self.e**2))**0.5  # angular momentum magnitude
        self.r = (self.h**2/self.GM)/(1 + self.e*np.cos(self.ta))  # distance from central body

        # position and velocity vectors in perifocal reference frame
        r_per = self.r*np.array([np.cos(self.ta), np.sin(self.ta), 0.0])
        v_per = (self.GM/self.h)*np.array([-np.sin(self.ta), self.e + np.cos(self.ta), 0.0])

        # rotation matrix from perifocal to equatorial reference frame
        Q_per2eq = per2eq(self.W, self.i, self.w)
        
        self.R = Q_per2eq@r_per  # position vector in equatorial reference frame
        self.V = Q_per2eq@v_per  # velocity vector in equatorial reference frame
        
        self.vr = np.dot(self.R, self.V)/self.r  # radial velocity
        
        self.compute_angular_momentum()
        self.compute_eccentricity()
        self.compute_mean_motion()
        
    def compute_eccentric_anomaly(self, ta):
        """Compute the eccentric anomaly from a given true anomaly.

        Parameters
        ----------
        ta : float
            True anomaly [rad]

        Returns
        -------
        E : float
            Eccentric anomaly [rad]

        """

        E = 2*np.arctan(((1-self.e)/(1+self.e))**0.5*np.tan(ta/2))
        
        return E
    
    def compute_true_anomaly(self, E):
        """Compute the true anomaly from a given eccentric anomaly.

        Parameters
        ----------
        E : float
            Eccentric anomaly [rad]

        Returns
        -------
        ta : float
            True anomaly [rad]

        """

        ta = 2*np.arctan(((1+self.e)/(1-self.e))**0.5*np.tan(E/2))
        
        return ta
            
    def compute_periapsis_passage(self, ta, t):
        """Compute the time at periapsis passage given the current time and true anomaly.

        Parameters
        ----------
        t : float
            Time [s]
        ta : float
            True anomaly [rad]

        Returns
        -------
        tp : float
            Time at periapsis passage [s]

        """
        
        E = self.compute_eccentric_anomaly(ta)  # eccentric anomaly
        Me = E - self.e*np.sin(E)  # mean anomaly
        tp = t - Me/self.n  # time at periapsis passage
        
        return tp
    
    def propagate(self, ta, tvec, mode):
        """Propagate the orbit forward or backward in time solving the Kepler's time of flight
        equation.

        Parameters
        ----------
        ta : float
            Initial true anomaly [rad]
        tvec : ndarray
            Time vector [s]
        mode : str
            ``fwd`` for forward propagation or ``back`` for backward propagation

        Returns
        -------
        Rvec : ndarray
            Position vector time series [km]
        Vvec : ndarray
            Velocity vector time series [km/s]

        """
        
        nb = len(tvec)
        
        if mode == 'fwd':
            tp = self.compute_periapsis_passage(ta, tvec[0])
        elif mode == 'back':
            tp = self.compute_periapsis_passage(ta, tvec[-1])
        else:
            raise ValueError("Mode must be either 'fwd' or 'back'")
            
        E0 = np.ones(nb)  # eccentric anomaly initial guess
        
        sol = root(self.kepler_eqn, E0, args=(self.e, self.n, tvec, tp), tol=1e-12)
        
        Evec = sol.x  # eccentric anomaly time series
        tavec = self.compute_true_anomaly(Evec)  # true anomaly time series

        # state vector time series
        Rvec, Vvec = coe2sv_vec(self.a, self.e, self.i, self.W, self.w, tavec, self.GM)
        
        print('{:<50s}{:<30s}'.format("Solving Kepler's time of flight equation", sol.message))
        
        return Rvec, Vvec

    @staticmethod
    def kepler_eqn(E, e, n, t, tp):
        """Kepler's Time of Flight equation.

        Parameters
        ----------
        E : float
            Eccentric anomaly [rad]
        e : float
            Eccentricity [-]
        n : float
            Mean motion [rad/s]
        t : float
            Time [s]
        tp : float
            Time at periapsis passage [s]

        """
        
        return E - e*np.sin(E) - n*(t-tp)
    
    def __str__(self):
        """Prints the orbit Classical Orbital Elements. """
        
        d = {'a': (self.a, 'km'), 'e': (self.e, ''), 'i': (self.i*180/np.pi, 'deg'),
             'W': (self.W*180/np.pi, 'deg'), 'w': (self.w*180/np.pi, 'deg'),
             'ta': (self.ta*180/np.pi, 'deg')}

        lines = []

        for j in d.keys():
            lines.append('{:<6s}{:>20.6f} {:<4s}'.format(j, d[j][0], d[j][1]))

        s = '\n'.join(lines)

        return s
    
       
if __name__ == '__main__':

    import matplotlib.pyplot as plt

    k = KepOrb(3000, 0.0, np.pi/3, 0.0, 0.0, 0.0)
    
    T = 2*np.pi/k.GM**0.5*k.a**1.5  # one orbital period
    t = np.linspace(0, T, 1000)
    R, V = k.propagate(0, t, 'fwd')  # propagate forward for half a period

    print(k)

    # plot
    p = InertialKepOrb('Keplerian Orbit', R)
    p.plot()

    plt.show()
