#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 10:23:38 2019

@author: Alberto FOSSA'
"""

from multiprocessing import Pool
from pickle import dump, load
import numpy as np

import sempy.core.init.defaults as dft
from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.crtbp.jacobi import jacobi
from sempy.core.lowthrust.dynamics.cr3bp_dynamics_old import Cr3bpDynamics
from sempy.core.lowthrust.orbits.propagator_old import Propagate
from sempy.core.lowthrust.dynamics.arc_length_dynamics import ArcLengthDynamics, arc_length_samples
from sempy.core.lowthrust.plots.synodic3d import SynodicNRHO, SynodicFamilyNRHO
from sempy.core.lowthrust.data.dataloader import InitConditions, get_filename


class NRHO:
    """NRHO class propagates an L2 NRHO orbit from given initial conditions
    in synodic reference frame and non dimensional units.

    Parameters
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``

    **kwargs :
    alt : int
        Periselene altitude [km]
    Az : int
        Vertical extension [km]

    Attributes
    ----------
    init_cond : object
        `InitConditions` object
    alt_dim : int
        Periselene altitude [km]
    az_dim : int
        Vertical extension [km]
    idx : int
        Database index for the specified alt or Az
    state0 : ndarray
        Initial conditions as ``[x, y, x, vx, vy, vz]`` [-]
    period : float
        Orbit period [-]
    cr3bp : object
        `Cr3bp` object
    c_jac : float
        Jacobi constant [-]
    state_vec : ndarray
        States time series [-]
    t_vec : ndarray
        Time vector [-]
    arc_length : float
        Orbit arc length [-]
    state_samp : ndarray
        Sampled states [-]
    t_samp : ndarray
        Sampled times [-]
    arc_length_samp : ndarray
        Sampled arc lengths [-]
    nrho_plot : object
        `SynodicNRHO` object

    Warnings
    --------
    Objects of this class are stored and read using `pickle`. After any modification, rerun the
    script `nrho_propagation.py` setting ``compute = True`` to update the corresponding databases.

    """

    def __init__(self, family, **kwargs):
        """Initializes NRHO class. """

        if 'alt' in kwargs:  # initial conditions from altitude of periselene
            self.init_cond = InitConditions(family, 'alt')
            self.alt_dim = kwargs['alt']
            idx = np.where(self.init_cond.alt_dim == self.alt_dim)[0]
            if len(idx) > 0:
                self.idx = idx[0]
            else:
                raise ValueError('Altitude out of range')
        elif 'Az' in kwargs:  # initial conditions from vertical extension
            self.init_cond = InitConditions(family, 'Az')
            self.az_dim = kwargs['Az']
            idx = np.where(self.init_cond.az_dim == self.az_dim)[0]
            if len(idx) > 0:
                self.idx = idx[0]
            else:
                raise ValueError('Vertical extension out of range')
        else:
            raise ValueError('Must provide one between alt and Az')

        self.state0 = self.init_cond.state0[self.idx]  # initial orbit's state
        self.period = self.init_cond.period[self.idx]  # orbit's period
        self.cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # Cr3bp object
        self.c_jac = jacobi(self.state0, self.cr3bp.mu)

        self.state_vec = self.t_vec = self.arc_length = self.state_samp = self.t_samp = \
            self.arc_length_samp = self.nrho_plot = None

    def compute_arc_length(self, method=dft.method, rtol=dft.rtol, atol=dft.atol):
        """Computes the arc length after one revolution.

        Parameters
        ----------
        method : str, optional
            Integration method as defined by SciPy solve_ivp. Default is given by `init.defaults`.
        rtol : float, optional
            Relative error tolerance. Default is given by `init.defaults`.
        atol : float, optional
            Absolute error tolerance. Default is given by `init.defaults`.

        Returns
        -------
        propagation : Propagate
            Propagate object.

        """

        arc_length_dynamics = ArcLengthDynamics(self.cr3bp)
        propagation = Propagate(arc_length_dynamics, (0.0, self.period),
                                np.hstack((self.state0, 0.0)), dense_output=True, method=method,
                                rtol=rtol, atol=atol)

        self.arc_length = propagation.state_vec[-1, -1]

        return propagation

    def propagate(self, method=dft.method, rtol=dft.rtol, atol=dft.atol, **kwargs):
        """Propagates the orbit. If no keyword arguments are passed to the method, by default the
        orbit is propagated for one revolution.

        Parameters
        ----------
        method : str, optional
            Integration method as defined by SciPy solve_ivp. Default is given by `init.defaults`.
        rtol : float, optional
            Relative error tolerance. Default is given by `init.defaults`.
        atol : float, optional
            Absolute error tolerance. Default is given by `init.defaults`.
        **kwargs :
            t_eval : ndarray
                Time vector in which the states are computed.
            t_final : float
                Time for which the trajectory is propagated.
            nb_rev : int
                Number of revolutions for which the trajectory is propagated.

        Returns
        -------
        propagation : Propagate
            Propagate object.

        """

        if 't_eval' in kwargs:
            t_eval = kwargs['t_eval']
            t_span = (0.0, t_eval[-1])
        else:
            t_eval = None
            if 't_final' in kwargs:
                t_span = (0.0, kwargs['t_final'])
            elif 'nb_rev' in kwargs:
                t_span = (0.0, kwargs['nb_rev'] * self.period)
            else:
                t_span = (0.0, self.period)

        cr3bp_dyna = Cr3bpDynamics(self.cr3bp, Cr3bpDynamics.Eqm.dimensions6)
        propagation = Propagate(cr3bp_dyna, t_span, self.state0, dense_output=True,
                                method=method, rtol=rtol, atol=atol, t_eval=t_eval)

        self.state_vec = propagation.state_vec.transpose()
        self.t_vec = propagation.t_vec.transpose()

        return propagation

    def sampling(self, nb_rev=1, nb_samples=101, method=dft.method, rtol=dft.rtol, atol=dft.atol):
        """Samples the orbit at constant arc length step size.

        Parameters
        ----------
        nb_rev : int, optional
            Number of revolutions along which the samples are computed. Default is 1.
        nb_samples : int, optional
            Number of samples. Default is 101 resulting in 100 segments.
        method : str, optional
            Integration method as defined by SciPy solve_ivp. Default is given by `init.defaults`.
        rtol : float, optional
            Relative error tolerance. Default is given by `init.defaults`.
        atol : float, optional
            Absolute error tolerance. Default is given by `init.defaults`.

        Returns
        -------
        propagation : Propagate
            Propagate object.

        """

        if self.arc_length is None:  # compute the arc length after one revolution
            self.compute_arc_length(method=method, atol=atol, rtol=rtol)

        # compute the propagation time interval and defines the events to be triggered
        t_span = (0.0, 1.1 * nb_rev * self.period)
        samples = np.linspace(0.0, nb_rev * self.arc_length, nb_samples)
        events = arc_length_samples(samples)

        # define a dynamics to propagate the augmented state (state, arc length) and perform the
        # integration
        arc_length_dynamics = ArcLengthDynamics(self.cr3bp)
        propagation = Propagate(arc_length_dynamics, t_span, np.hstack((self.state0, 0.0)),
                                events=events, dense_output=True, method=method, rtol=rtol,
                                atol=atol)

        self.t_samp = np.asarray(propagation.sol.t_events)
        state7 = np.reshape(np.asarray(propagation.sol.y_events), (np.size(self.t_samp), 7))
        self.state_samp = state7[:, :-1]
        self.arc_length_samp = np.reshape(state7[:, -1], (np.size(self.t_samp), 1))

        return propagation

    def plot(self, samples=False):
        """Plots the orbit in synodic reference frame.

        Parameters
        ----------
        samples : bool, optional
            ``True`` to plot only the samples, ``False`` to plot all the states obtained with
            `propagate`. Default is ``False``.

        """

        if samples:
            self.nrho_plot = SynodicNRHO(self.cr3bp.mu, self.state_samp[:, :3])
        else:
            self.nrho_plot = SynodicNRHO(self.cr3bp.mu, self.state_vec[:, :3])
        self.nrho_plot.plot()

    def __str__(self):
        """Prints the NRHO attributes. """

        lines = ['\n{:<30s}{:>10s}'.format('L2 Family:', self.init_cond.family)]

        if hasattr(self, 'alt_dim'):
            lines.append('{:<25s}{:>12d}{:>3s}'.format('Periselene altitude:', self.alt_dim, 'km'))
        if hasattr(self, 'az_dim'):
            lines.append('{:<25s}{:>12d}{:>3s}'.format('Vertical extension:', self.az_dim, 'km'))

        printed = '\n'.join(lines)

        return printed


class FamilyNRHO:
    """FamilyNRHO propagates a family of L2 NRHO orbits from given initial conditions
    in synodic reference frame and non dimensional units.

    Parameters
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    param : str
        Family parametrization, allowed values are ``alt`` for periselene altitude
        and ``Az`` for vertical extension

    Attributes
    ----------
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    param : str
        Family parametrization, allowed values are ``alt`` for periselene altitude
        and ``Az`` for vertical extension
    init_cond : object
        `InitConditions` object
    cr3bp : object
        `Cr3bp` object
    nrho_dict : dict
        `NRHO` objects indexed by periselene altitude or vertical extension
    family_plot : object
        `SynodicFamilyNRHO` object

    """

    nb_rev = 1
    nb_samples = 101
    method = dft.method
    rtol = dft.rtol
    atol = dft.atol

    def __init__(self, family, param):
        """Initialize FamilyNRHO class. """

        self.cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        self.init_cond = InitConditions(family, param)
        self.family = family
        self.param = param

        self.nrho_dict = {}
        self.family_plot = None

    @classmethod
    def set_compute_settings(cls, nb_rev=1, nb_samples=101, method=dft.method,
                             rtol=dft.rtol, atol=dft.atol):
        """Sets the number of revolutions, number of samples and integrator settings needed to
        propagate the family of orbits.

        Parameters
        ----------
        nb_rev : int, optional
            Number of revolutions for which the orbits are propagated. Default is 1.
        nb_samples : int
            Number of sampling points. Default is 101 corresponding to 100 segments.
        method : str, optional
            Integration method as defined by SciPy solve_ivp. Default is given by `init.defaults`.
        rtol : float, optional
            Relative error tolerance. Default is given by `init.defaults`.
        atol : float, optional
            Absolute error tolerance. Default is given by `init.defaults`.

        """

        cls.nb_rev = nb_rev
        cls.nb_samples = nb_samples
        cls.method = method
        cls.rtol = rtol
        cls.atol = atol

    def compute(self, nb_rev=1, nb_samples=101, method=dft.method, rtol=dft.rtol, atol=dft.atol,
                processes=None, maxtasksperchild=None, chunksize=None):
        """Propagates and samples the family. Parallel implementation using `multiprocessing`
        module.

        Parameters
        ----------
        nb_rev : int, optional
            Number of revolutions for which the orbits are propagated. Default is 1.
        nb_samples : int
            Number of sampling points. Default is 101 corresponding to 100 segments.
        method : str, optional
            Integration method as defined by SciPy solve_ivp. Default is given by `init.defaults`.
        rtol : float, optional
            Relative error tolerance. Default is given by `init.defaults`.
        atol : float, optional
            Absolute error tolerance. Default is given by `init.defaults`.
        processes : int or None, optional
            Number of child processes. If None is set equal to `os.cpu_count()`.
            Default is None.
        maxtasksperchild : int or None, optional
            Maximum number of tasks performed by a worker before being replaced by a new one.
            Default is None.
        chunksize : int or None, optional
            Approximate size of each chunk extracted from the iterable and submitted to a
            different worker. Default is None.

        """

        self.set_compute_settings(nb_rev=nb_rev, nb_samples=nb_samples, method=method,
                                  rtol=rtol, atol=atol)

        with Pool(processes=processes, maxtasksperchild=maxtasksperchild) as pool:
            if self.param == 'alt':
                k = self.init_cond.alt_dim
                if self.family == 'southern':
                    fam = pool.map(self.init_southern_nrho_alt, k, chunksize=chunksize)
                elif self.family == 'northern':
                    fam = pool.map(self.init_northern_nrho_alt, k, chunksize=chunksize)
            elif self.param == 'Az':
                k = self.init_cond.az_dim
                if self.family == 'southern':
                    fam = pool.map(self.init_southern_nrho_az, k, chunksize=chunksize)
                elif self.family == 'northern':
                    fam = pool.map(self.init_northern_nrho_az, k, chunksize=chunksize)

        self.nrho_dict = dict(zip(list(map(str, k)), list(fam)))

    @classmethod
    def init_southern_nrho_alt(cls, alt_dim):
        """Inits an object for a southern NRHO parameterized by altitude of periselene.

        Parameters
        ----------
        alt_dim : int
            Altitude of periselene [km].

        Returns
        -------
        nrho : NRHO
            NRHO object

        """

        nrho = NRHO('southern', alt=alt_dim)
        nrho = cls.compute_nrho(nrho)

        return nrho

    @classmethod
    def init_southern_nrho_az(cls, az_dim):
        """Inits an object for a southern NRHO parameterized by vertical extension.

        Parameters
        ----------
        az_dim : int
            Vertical extension [km].

        Returns
        -------
        nrho : NRHO
            NRHO object

        """

        nrho = NRHO('southern', Az=az_dim)
        nrho = cls.compute_nrho(nrho)

        return nrho

    @classmethod
    def init_northern_nrho_alt(cls, alt_dim):
        """Inits an object for a northern NRHO parameterized by altitude of periselene.

        Parameters
        ----------
        alt_dim : int
            Altitude of periselene [km].

        Returns
        -------
        nrho : NRHO
            NRHO object

        """

        nrho = NRHO('northern', alt=alt_dim)
        nrho = cls.compute_nrho(nrho)

        return nrho

    @classmethod
    def init_northern_nrho_az(cls, az_dim):
        """Inits an object for a northern NRHO parameterized by vertical extension.

        Parameters
        ----------
        az_dim : int
            Vertical extension [km].

        Returns
        -------
        nrho : NRHO
            NRHO object

        """

        nrho = NRHO('northern', Az=az_dim)
        nrho = cls.compute_nrho(nrho)

        return nrho

    @classmethod
    def compute_nrho(cls, nrho):
        """Propagates and samples an NRHO.

        Parameters
        ----------
        nrho : NRHO
            NRHO object.

        Returns
        -------
        nrho : NRHO
            NRHO object.

        """

        nrho.propagate(nb_rev=cls.nb_rev, method=cls.method, rtol=cls.rtol, atol=cls.atol)
        nrho.sampling(nb_rev=cls.nb_rev, nb_samples=cls.nb_samples,
                      method=cls.method, rtol=cls.rtol, atol=cls.atol)

        return nrho

    def save_family(self, filename):
        """Saves a Family using pickle.

        Parameters
        ----------
        filename : str
            Full path to the Pickle file

        """

        with open(filename, 'wb') as fid:
            dump(self.nrho_dict, fid)

    def load_family(self):
        """Loads a Family using pickle. """

        self.family, self.param, filename = get_filename(self.family, self.param, 'objects')

        with open(filename, 'rb') as fid:
            self.nrho_dict = load(fid)

    def plot(self, samples=False):
        """Plots a Family.

        Parameters
        ----------
        samples : bool
            True to plot only the samples, False to plot all the states time series.
            Default is False

        """

        radii_dict = {}

        for k in self.nrho_dict.keys():
            nrho_k = self.nrho_dict[k]
            if samples:
                radii_k = nrho_k.state_samp[:, :3]
            else:
                radii_k = nrho_k.state_vec[:, :3]
            radii_dict[k] = radii_k

        self.family_plot = \
            SynodicFamilyNRHO(self.cr3bp.mu, radii_dict, len(self.nrho_dict), samples)
        self.family_plot.plot()


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    FAM = FamilyNRHO('northern', 'alt')
    FAM.load_family()

    FAM.plot()
    plt.show()
