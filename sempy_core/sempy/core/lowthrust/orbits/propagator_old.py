# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 13:49:05 2019

@author: Edgar PEREZ, Alberto FOSSA'
"""

from scipy.integrate import solve_ivp
import sempy.core.init.defaults as dft


class Propagate:
    """Propagate class performs equation of motion propagation.

    This class will propagate equations of motion according to a selected dynamics (cr3bp_dyna).

    There are 3 required arguments: cr3bp_dyna, t_span and state0.

    Arguments: events and dense_output, are passed through the Propagate class constructor.
    Use dense_output equal True and events (a function) to get the state_event at t_event,
    otherwise they will be None.

    The remaining arguments (method, rtol, atol), can be changed directly in the
    Propagate class constructor. As well, these remaining arguments can be modified in
    default.ini.

    The default integration method is RK45. More methods are available, for more details see
    solve_ivp documentation in Scipy web page. (
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html)

    Parameters
    ----------
    cr3bp_dyna : Cr3bpDynamics
        An instance of the Cr3bpDynamics class.
    t_span : ndarray
        Interval of integration, e.g. [0, 10].
    state0 : ndarray
        Orbit's initial state.
    events : function
        Events to track.
    dense_output : bool
        bool, e.g. False.
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float
        Absolute tolerance.
    t_eval : ndarray or None
        Times at which the solution is stored or None.

    Attributes
    ----------
    cr3bp_dyna : object
        Attributes of this Instance can be accessed (check attributes in Cr3bpDynamics class).
    t_span : ndarray
        Defined interval of integration.
    state0 : ndarray
        Orbit's initial state.
    events : function
        Selected events to track.
    dense_output : bool
        dense_output bool.
    method : str
        Integration method selected.
    rtol : float
        Relative tolerance selected.
    atol : float
        Absolute tolerance selected.
    sol : OdeResult
        Found solution to the ode.
    t_event : None or float
        Time at which the event is reached.
    state_event : None or ndarray
        State at t_event.
    state_vec : ndarray
        State vector (at t_vec), array of states results after the orbit's initial state
        propagation.
    t_vec : ndarray
        Time vector, array of time results after the orbit's initial state propagation.

    Examples
    --------
    >>> propagation = Propagate(cr3bp_dyna, t_span, state0)

    >>> propagation1 = Propagate(cr3bp_dyna, t_span, state0, method= 'LSODA', rtol=1e-10,
    >>> atol=1e-10)

    >>> propagation2 = Propagate(cr3bp_dyna, t_span, state0, method= 'RK45', rtol=1e-11, atol=1e-11)

    In the next example, integration stops at y=0, i.e. event.xz_plane_event. This was used in
    diff_corr_3D.py module, and it will propagate the first guess until the event is reached.

    >>> sol_fg = Propagate(cr3bp_dyna42, t_span_first_guess, stateSTM0,
    >>> events=event.xz_plane_event, dense_output=True)

    """

    def __init__(self, cr3bp_dyna, t_span, state0, events=None, dense_output=False,
                 method=dft.method, rtol=dft.rtol, atol=dft.atol, t_eval=None):
        """Inits Propagate class."""

        self.cr3bp_dyna = cr3bp_dyna
        self.t_span = t_span
        self.state0 = state0
        self.events = events
        self.method = method
        self.dense_output = dense_output
        self.rtol = rtol
        self.atol = atol

        self.sol, self.t_event, self.state_event, self.state_vec, self.t_vec =\
            self.solution(self.func, self.t_span, self.state0, self.cr3bp_dyna.cr3bp.mu,
                          self.events, self.dense_output, self.method, self.rtol, self.atol,
                          t_eval=t_eval)

    def func(self, _, state, mu_cr3bp):
        """func is coded in a way that solve_ivp accepts.

        This method will serve as function to pass different equations of motion to the solve_ivp
        integrator.

        Parameters
        ----------
        _: float
            Time handled implicitly by solve_ivp.
        state : ndarray
            Orbit's state at time `t` as 6-dim state or 6-dim state concatenated with 36-dim STM
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        dydt : ndarray
            Set of first order ODEs of the Orbit's state and optionally Orbit's STM.

        """

        if len(state) not in [6, 7, 42]:  # 7-dim state for arc length integration
            raise Exception('State dimension error')

        dydt = self.cr3bp_dyna.ode(state, mu_cr3bp)  # set of first order ODEs.

        return dydt

    @staticmethod
    def solution(func, t_span, state0, mu_cr3bp, events, dense_output, method, rtol, atol,
                 t_eval=None):
        """Here will be executed the integration of a given dynamics.

        Several options for solve_ivp can be changed directly through the
        Propagate class constructor or through defaults parameters contained
        in the defaults module. Those options are: method, rtol, atol

        The remaining solve_ivp options; events and dense_output, only can be changed
        through the Propagate class constructor.

        The default integration method is RK45. More methods are available, see solve_ivp
        documentation in Scipy web page.

        Parameters
        ----------
        func : function
            Set of first order ODEs (equations of motion) that solve_ivp will integrate.
        t_span : ndarray
            Interval of integration, e.g. [0, 10].
        state0 : ndarray
            Orbit's initial state.
        mu_cr3bp : float
            CR3BP mass parameter.
        events : function
            Events to track.
        dense_output : bool
            bool, e.g. False.
        method : str
            Integration method to use.
        rtol : float
            Relative tolerance.
        atol : float
            Absolute tolerance.
        t_eval : ndarray or None
            Times at which the solution is stored or None.

        Returns
        -------
        sol : OdeResult
            Found solution to the ode.
        t_event : None or float
            Time at which the event is reached.
        state_event : None or ndarray
            State at t_event.
        state_v : ndarray
            State vector (at t_vec), array of states results after the orbit's initial state
            propagation.
        t_v : ndarray
            Time vector, array of time results after the orbit's initial state propagation.

        """

        sol = solve_ivp(fun=lambda t, state: func(t, state, mu_cr3bp), t_span=t_span,
                        y0=state0, events=events, dense_output=dense_output, method=method,
                        rtol=rtol, atol=atol, t_eval=t_eval)

        if events is None:  # and dense_output is False
            t_event = None
            state_event = None
            state_v = sol.y
            t_v = sol.t
        else:
            t_event = sol.t_events[0][0]
            state_event = sol.y_events[0][0]
            state_v = sol.y
            t_v = sol.t

        return sol, t_event, state_event, state_v, t_v
