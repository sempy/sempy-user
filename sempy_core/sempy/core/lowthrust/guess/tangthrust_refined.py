#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 10:15:52 2019

@author: Alberto FOSSA'
"""

import numpy as np
from scipy.interpolate import interp1d


class TangThrustRefined:
    """TangThrustRefined takes the thrust magnitude profiles from the tangential thrust
    approximation and the optimal solution to compute a new initial guess closer to a bang-bang
    control profile.

    Parameters
    ----------
    time : ndarray
        Time vector for the controls discretization nodes [-]
    thrust_tangential : ndarray
        Thrust magnitude time series from tangential thrust approximation [N]
    thrust_opt : ndarray
        Thrust magnitude time series from the first optimization [N]
    threshold : float
        Maximum negligible difference between guessed and optimal thrust magnitudes in fraction
        between 0 and 1
    kind : str
        Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``
    thrust_lim : tuple, optional
        Thrust magnitude limits to which the interpolated profile is pushed when `kind` is
        equal to ``bang-bang``. Default is ``(min(thrust), max(thrust))``

    Attributes
    ----------
    time : ndarray
        Time vector for the controls discretization nodes [-]
    thrust_tangential : ndarray
        Thrust magnitude time series from tangential thrust approximation [N]
    thrust_opt : ndarray
        Thrust magnitude time series from the first optimization [N]
    threshold : float
        Maximum negligible difference between guessed and optimal thrust magnitudes in fraction
        between 0 and 1
    kind : str
        Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``
    bang_bang : bool
        True if `kind` is equal to ``bang-bang``, False otherwise
    thrust_lim : tuple
        Thrust magnitude limits to which the interpolated profile is pushed when `kind` is
        equal to ``bang-bang``
    time_samp : ndarray
        Time samples in non dimensional units [-]
    thrust_samp : ndarray
        Thrust magnitude samples [N]
    func : object
        Interpolating function, `scipy.interpolate.interpolate.interp1d` object
    thrust_interp : ndarray
        Interpolated thrust magnitude time series [N]

    """

    def __init__(self, time, thrust_tangential, thrust_opt, threshold, kind, thrust_lim=None):
        """Initializes TangThrustRefined class. """

        self.time = time.flatten()
        self.thrust_tangential = thrust_tangential.flatten()
        self.thrust_opt = thrust_opt.flatten()
        self.threshold = threshold

        if kind == 'bang-bang':
            self.kind = 'cubic'
            self.bang_bang = True
        else:
            self.kind = kind
            self.bang_bang = False

        if thrust_lim is None:
            self.thrust_lim = (np.min(self.thrust_opt), np.max(self.thrust_opt))
        else:
            self.thrust_lim = thrust_lim

        not_close = np.logical_not(np.isclose(self.thrust_opt, self.thrust_tangential,
                                              atol=0.0, rtol=self.threshold))

        self.time_samp = np.take(self.time, np.nonzero(not_close))
        self.thrust_samp = np.take(self.thrust_opt, np.nonzero(not_close))

        self.func = interp1d(self.time_samp, self.thrust_samp, kind=self.kind,
                             bounds_error=False, fill_value='extrapolate', assume_sorted=True)

        self.thrust_interp = self.func(self.time)

        if self.kind == 'cubic':
            self.thrust_interp[self.time < self.time_samp[0]] = self.thrust_samp[0]
        if self.bang_bang:
            self.thrust_interp[self.thrust_interp < self.thrust_tangential] = self.thrust_lim[0]
            self.thrust_interp[self.thrust_interp > self.thrust_tangential] = self.thrust_lim[1]
