#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 25 17:03:55 2019

@author: Alberto FOSSA'
"""

import numpy as np
from scipy.interpolate import interp1d


class RefineThrustProfile:
    """Class RefineThrustProfile refines a given thrust profile interpolating the input data
    to match one of the supported shapes between ``cubic``, ``nearest`` and ``bang-bang``.

    Parameters
    ----------
    time : ndarray
        Time vector in non dimensional units [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    kind : str
        Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``
    thrust_lim : tuple, optional
        Thrust magnitude limits to which the interpolated profile is pushed when `kind` is
        equal to ``bang-bang``. Default is ``(min(T), max(T))``
    threshold : float, optional
        Throttle level between 0 and 1 under which the thrust magnitude is considered
        to be zero when `kind` is equal to ``bang-bang``. Default is 0.5

    Attributes
    ----------
    time : ndarray
        Time vector in non dimensional units [-]
    time_samp : ndarray
        Time samples in non dimensional units [-]
    thrust : ndarray
        Thrust magnitude time series [N]
    thrust_samp : ndarray
        Thrust magnitude samples [N]
    kind : str
        Interpolation method, allowed values are ``cubic``, ``nearest`` and ``bang-bang``
    bang_bang : bool
        True if `kind` is equal to ``bang-bang``, False otherwise
    thrust_lim : tuple
        Thrust magnitude limits to which the interpolated profile is pushed when `kind`
        is equal to ``bang-bang``
    threshold : float
        Throttle level between 0 and 1 under which the thrust magnitude is considered
        to be zero when `kind` is equal to ``bang-bang``
    func : object
        Interpolating function, `scipy.interpolate.interpolate.interp1d` object
    thrust_interp : ndarray
        Interpolated thrust magnitude time series [N]

    """

    def __init__(self, time, thrust, kind, thrust_lim=None, threshold=0.5):
        """Initializes RefineThrustProfile class. """

        self.time = time
        self.thrust = thrust
        self.time_samp, idx = np.unique(time, return_index=True)
        self.thrust_samp = np.take(thrust, idx)

        if kind in ['cubic', 'nearest']:
            self.kind = kind
            self.bang_bang = False
        elif kind == 'bang-bang':
            self.kind = 'cubic'
            self.bang_bang = True
        else:
            raise ValueError('Currently supported kinds are cubic, nearest and bang-bang')

        if thrust_lim is not None:
            self.thrust_lim = thrust_lim
        else:
            self.thrust_lim = (np.min(self.thrust), np.max(self.thrust))

        delta = self.thrust_lim[1] - self.thrust_lim[0]
        self.threshold = threshold*delta + self.thrust_lim[0]

        # interpolation
        self.func = interp1d(self.time_samp, self.thrust_samp, kind=self.kind, bounds_error=False,
                             fill_value='extrapolate', assume_sorted=True)

        self.thrust_interp = self.func(self.time)

        if self.bang_bang:
            self.thrust_interp[self.thrust_interp < self.threshold] = self.thrust_lim[0]
            self.thrust_interp[self.thrust_interp > self.threshold] = self.thrust_lim[1]
