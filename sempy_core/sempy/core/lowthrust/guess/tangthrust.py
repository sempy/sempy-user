#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 09:44:00 2019

@author: Alberto FOSSA'
"""

import numpy as np
from scipy.integrate import odeint
from scipy.constants import g

import sempy.core.init.defaults as dft
from sempy.core.init.constants import MOON
from sempy.core.lowthrust.utils.spacecraft import Spacecraft
from sempy.core.lowthrust.plots.polar2d import PolarTrajectory, PolarTimeSeries


class TangentialThrust:
    """TangentialThrust computes a sub-optimal solution for a low-thrust transfer
    trajectory between two circular and coplanar orbits in the 2BP framework.
    The thrust direction is assumed to be always equal to the flight path angle.

    Parameters
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    r_init : float
        Initial orbit radius [km]
    r_final : float
        Final orbit radius [km]
    fpa_init : float
        Initial flight path angle [rad]
    gm_r2bp : float
        Central body standard gravitational parameter [km^3/s^2]
    t_init : float, optional
        Initial time [s]. Default is 0.0
    theta_init : float, optional
        Initial angle [rad]. Default is 0.0

    Attributes
    ----------
    spacecraft : Spacecraft
        `Spacecraft` object
    r_init : float
        Initial orbit radius [km]
    r_final : float
        Final orbit radius [km]
    fpa_init : float
        Initial flight path angle [rad]
    tan_fpa_init : float
        Tangent of `fpa_init` [-]
    gm_r2bp : float
        Central body standard gravitational parameter [km^3/s^2]
    t_init : float
        Initial time [s]
    theta_init : float
        Initial angle [rad]
    theta_final : float
        Final angle [rad]
    tof : float
        Time of flight [s]
    t_final : float
        Final time [s]
    t_vec : ndarray
        Time vector [s]
    theta_vec : ndarray
        Angle time series [rad]
    r_vec : ndarray
        Radius time series [km]
    u_vec : ndarray
        Radial velocity time series [km/s]
    v_vec : ndarray
        Tangential velocity time series [km/s]
    fpa_vec : ndarray
        Flight path angle and thrust direction time series [rad]
    acc_vec : ndarray
        Acceleration magnitude time series [km/s^2]
    mass_vec : ndarray
        Spacecraft mass time series [kg]
    thrust_vec : ndarray
        Thrust magnitude time series [N]
    a_sol : ndarray
        Analytical solution time series as ``[r, theta, u, v, fpa, a]``
    num_sol : ndarray
        Numerical solution time series as ``[r, theta, u, v, m]``

    """

    def __init__(self, spacecraft, r_init, r_final, fpa_init, gm_r2bp, t_init=0.0, theta_init=0.0):
        """Initializes TangentialThrust class. """

        self.spacecraft = spacecraft
        self.r_init = r_init
        self.r_final = r_final
        self.fpa_init = fpa_init
        self.tan_fpa_init = np.tan(self.fpa_init)
        self.gm_r2bp = gm_r2bp
        self.t_init = t_init
        self.theta_init = theta_init

        # final angle
        self.theta_final = \
            self.theta_init + (1.0 - self.r_init / self.r_final) / self.tan_fpa_init

        # time of flight and final time
        time, _ = odeint(self.dt_dtheta, y0=[0.0], t=[self.theta_init, self.theta_final],
                         full_output=True, rtol=dft.rtol, atol=dft.atol)

        self.tof = time[-1, -1]
        self.t_final = self.t_init + self.tof

        # initialization
        self.t_vec = self.theta_vec = self.r_vec = self.u_vec = self.v_vec = self.fpa_vec = \
            self.acc_vec = self.mass_vec = self.thrust_vec = self.a_sol = self.num_sol = None

    def compute_time_series(self, **kwargs):
        """Computes the time series of angle, radius, flight path angle, tangential velocity,
        radial velocity, spacecraft mass and thrust magnitude on a given time vector `time` or
        number of points `nb_points`.

        Other Parameters
        ----------------
        **kwargs :
        time : ndarray
            Time vector in which the time series are computed, must be monotonic increasing [s]
        nb_points : int
            Number of points equally spaced in time in which the time series are computed [-]

        """

        # time vector in which the solution is computed
        if 'time' in kwargs:
            time = kwargs['time']
            time = np.sort(np.asarray(time))
            if np.allclose(np.array([self.t_init, self.t_final]),
                           np.array([time[0], time[-1]])):
                self.t_vec = time
            else:
                raise ValueError('Elements in time must lie in [t0, tf]')

        elif 'nb_points' in kwargs:
            nb_points = kwargs['nb_points']
            if isinstance(nb_points, int) and nb_points > 1:
                self.t_vec = np.linspace(self.t_init, self.t_final, nb_points)
            else:
                raise TypeError('Number of points must be a positive integer nb_points > 1')

        else:
            raise Exception('Must provide one between time vector time or number of points '
                            'nb_points')

        # analytic solution and numerical integration
        self.analytic_solution()
        self.simulate()

        check = np.allclose(self.a_sol[:, :4], self.num_sol[:, :4], rtol=1e-3, atol=1e-3)
        print('{:<50s}{:<30s}'.format('Match between analytic and numerical solutions',
                                      str(check)))

        # mass and thrust magnitude time series
        self.mass_vec = np.reshape(self.num_sol[:, 4], (len(self.t_vec), 1))
        self.thrust_vec = self.acc_vec * self.mass_vec * 1e3

    def analytic_solution(self):
        """Analytic solution for the time series of ``[r, theta, u, v, fpa, a]``. """

        theta, sol = odeint(self.theta_dot, y0=[self.theta_init], t=self.t_vec,
                            full_output=True, rtol=dft.rtol, atol=dft.atol, tfirst=True)

        self.theta_vec = theta
        self.r_vec = self.r_theta(self.theta_vec)
        self.v_vec = self.r_vec * self.theta_dot(self.t_vec, self.theta_vec)
        self.u_vec = self.tan_fpa_theta(self.theta_vec) * self.v_vec
        self.fpa_vec = np.arctan(self.tan_fpa_theta(self.theta_vec))
        self.acc_vec = \
            self.gm_r2bp * self.tan_fpa_init ** 2 / (self.r_init ** 2 * 2 * np.sin(self.fpa_vec))
        self.a_sol = np.hstack((self.r_vec, self.theta_vec, self.u_vec, self.v_vec, self.fpa_vec,
                                self.acc_vec))

        print('{:<50s}{:<30s}'.format('Computing analytic solution', sol['message']))

    def simulate(self):
        """Explicitly simulates the transfer trajectory. """

        # initial conditions
        state0 = [self.r_init, self.theta_init, self.u_vec[0], self.v_vec[0],
                  self.spacecraft.mass0]

        states, sol = odeint(self.odes, y0=state0, t=self.t_vec, full_output=True,
                             rtol=dft.rtol, atol=dft.atol, tfirst=True)

        self.num_sol = states

        print('{:<50s}{:<30s}'.format('Simulating trajectory', sol['message']))

    def r_theta(self, theta):
        """Defines an explicit expression for the radius as a function of the angle theta.

        Parameters
        ----------
        theta : float or ndarray
            Angle [rad]

        Returns
        -------
        radius : float or ndarray
            Radius [km]

        """

        radius = self.r_init / (1.0 - (theta - self.theta_init) * self.tan_fpa_init)

        return radius

    def dr_dtheta(self, theta):
        """Defines an explicit expression for the first derivative of the radius with respect to
        the angle theta.

        Parameters
        ----------
        theta : float or ndarray
            Angle [rad]

        Returns
        -------
        dr_dtheta : float or ndarray
            Radius rate [km/s]

        """

        dr_dtheta = (self.r_theta(theta) ** 2) * (self.tan_fpa_init / self.r_init)

        return dr_dtheta

    def tan_fpa_theta(self, theta):
        """Defines an explicit expression for the tangent of the flight path angle as a function
        of the angle theta.

        Parameters
        ----------
        theta : float or ndarray
            Angle [rad]

        Returns
        -------
        tan_fpa : float or ndarray
            Tangent of the flight path angle [-]

        """

        tan_fpa = self.r_theta(theta) * (self.tan_fpa_init / self.r_init)

        return tan_fpa

    def theta_dot(self, _, theta):
        """Defines an explicit expression for the first derivative of theta with respect to time.

        Parameters
        ----------
        _ : float
            Time handled internally by integrator [s]
        theta : float
            Angle [rad]

        Returns
        -------
        theta_dot : float
            Angle rate [rad/s]

        """

        theta_dot = (self.gm_r2bp ** 0.5) * (self.r_theta(theta) ** -1.5)

        return theta_dot

    def dt_dtheta(self, time, theta):
        """Defines an explicit expression for the first derivative of the time with respect
        to the angle.

        Parameters
        ----------
        time : float or ndarray
            Time [s]
        theta : float or ndarray
            Angle [rad]

        Returns
        -------
        dt_dtheta : float or ndarray
            First derivative of `time` wrt `theta` [rad/s]

        """

        dt_dtheta = 1 / self.theta_dot(time, theta)

        return dt_dtheta

    def odes(self, _, state):
        """Defines the ODEs for a planar transfer trajectory with tangential thrust
        in polar coordinates.

        Parameters
        ----------
        _ : float
            Time handled implicitly by integrator [s]
        state : ndarray
            States variables as ``[r, theta, u, v, m]``

        Returns
        -------
        states_dot : ndarray
            States variables rates as ``[r_dot, theta_dot, u_dot, v_dot, mass_dot]``

        """

        speed = (state[2] ** 2 + state[3] ** 2) ** 0.5  # velocity magnitude [km/s]
        sin_fpa = state[2] / speed  # sinus of flight path angle
        cos_fpa = state[3] / speed  # cosines of flight path angle

        acc = self.gm_r2bp * self.tan_fpa_init ** 2 / \
            (self.r_init ** 2 * 2 * sin_fpa)  # thrust acceleration [km/s^2]
        thrust = acc * state[4] * 1e3  # thrust magnitude [N]

        r_dot = state[2]
        theta_dot = state[3] / state[0]
        u_dot = - self.gm_r2bp / state[0] ** 2 + state[3] ** 2 / state[0] + acc * sin_fpa
        v_dot = - state[2] * state[3] / state[0] + acc * cos_fpa
        mass_dot = - thrust / self.spacecraft.isp / g

        states_dot = [r_dot, theta_dot, u_dot, v_dot, mass_dot]

        return states_dot


if __name__ == '__main__':

    import matplotlib.pyplot as plt
    SC = Spacecraft(1000, 2.0, 2000)  # Spacecraft object
    TT = TangentialThrust(SC, 2000, 2500, 0.08 * np.pi / 180, MOON.GM)

    TT.compute_time_series(nb_points=2000)

    # plots
    TITLE = 'Tangential thrust approximation'
    PT = PolarTimeSeries(TITLE, TT.t_vec, TT.a_sol[:, :4], TT.num_sol[:, :4], TT.mass_vec,
                         TT.thrust_vec)
    PP = PolarTrajectory(TITLE, TT.r_init, TT.r_final, MOON.Req, TT.r_vec, TT.theta_vec)
    PT.plot()
    PP.plot()
