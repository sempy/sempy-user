#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 11:15:39 2019

@author: Alberto FOSSA'
"""

import matplotlib.pyplot as plt
import numpy as np

from sempy.core.lowthrust.orbits.nrho import NRHO, FamilyNRHO
from sempy.core.lowthrust.plots.synodic3d import set_axes_limits_labels
from sempy.core.lowthrust.utils.utils import InitNRHOs


class TrajectoryStacking:
    """TrajectoryStacking class computes an initial guess for a Low-Thrust transfer trajectory
    from the periselene of an inner NRHO to the aposelene of an outer NRHO stacking together
    multiple arcs that belong to intermediate orbits.

    Parameters
    ----------
    t_init : float
        Initial time [-]
    tof : float
        Time of flight [-]
    t_vec : ndarray
        Time vector in which the approximate solution is computed [-]
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    t_init : float
        Initial time [-]
    tof : float
        Time of flight [-]
    t_final: float
        Final time [-]
    t_vec : ndarray
        Time vector in which the approximate solution is computed [-]
    bcs : object
        `InitNRHOs` object for initial and target orbits
    state0 : ndarray
        States variables initial guess as ``[x, y, z, vx, vy, vz]`` [-]

    """

    def __init__(self, t_init, tof, t_vec, family, **kwargs):
        """Initializes TrajectoryStacking class. """

        self.t_init = t_init
        self.tof = tof
        self.t_final = self.t_init + self.tof
        self.t_vec = t_vec

        self.bcs = InitNRHOs(family, **kwargs)

        self.state0 = None

    def propagate(self, param):
        """Computes the intermediate arcs.

        Parameters
        ----------
        param : str
            Orbit parameter proportional to the time of flight spent in each intermediate arc,
            allowed values are ``period`` and ``arc_length``

        """

        if param == 'period':
            dt_arcs = self.period()
        elif param == 'arc_length':
            dt_arcs = self.arc_length()
        else:
            raise ValueError("Param must be either 'period' or 'arc_length")

        self.state0 = np.zeros((len(self.t_vec), 6))
        nb_orb = self.bcs.nrho_arr.idx - self.bcs.nrho_dep.idx + 1
        t_end = self.t_init
        idx_end = 0

        for i in range(nb_orb):

            # current integration interval
            t_start = t_end
            t_end = t_start + dt_arcs[i]

            if i == 0:
                t_vec_i = self.t_vec[self.t_vec <= t_end]
            elif i == (nb_orb - 1):
                t_vec_i = self.t_vec[self.t_vec > t_start]
            else:
                t_vec_i = self.t_vec[np.logical_and(self.t_vec > t_start, self.t_vec <= t_end)]

            # current NRHO object
            idx_nrho = i + self.bcs.nrho_dep.idx

            if hasattr(self.bcs, 'alti'):
                nrho = NRHO(self.bcs.family, alt=self.bcs.nrho_dep.init_cond.alt_dim[idx_nrho])
            elif hasattr(self.bcs, 'azi'):
                nrho = NRHO(self.bcs.family, Az=self.bcs.nrho_dep.init_cond.az_dim[idx_nrho])
            else:
                raise ValueError('Initial and target orbits not correctly initialized')

            # propagation
            nrho.propagate(t_eval=t_vec_i)

            # update guess
            idx_start = idx_end
            idx_end = idx_start + len(t_vec_i)

            self.state0[idx_start:idx_end, :] = nrho.state_vec

    def period(self):
        """Computes the integration times for each arc proportional to the corresponding
        orbit period.

        """

        periods_vec = \
            self.bcs.nrho_dep.init_cond.period[self.bcs.nrho_dep.idx:(self.bcs.nrho_arr.idx + 1)]
        periods_sum = np.sum(periods_vec)
        dt_arcs = (self.tof / periods_sum) * periods_vec

        return dt_arcs

    def arc_length(self):
        """Computes the integration times for each arc proportional to the corresponding
        orbit arc lengths.

        """

        if hasattr(self.bcs, 'alti'):
            fam = FamilyNRHO(self.bcs.family, 'alt')
            param_vec = self.bcs.nrho_dep.init_cond.alt_dim[self.bcs.nrho_dep.idx:
                                                            (self.bcs.nrho_arr.idx + 1)]
        elif hasattr(self.bcs, 'azi'):
            fam = FamilyNRHO(self.bcs.family, 'Az')
            param_vec = self.bcs.nrho_dep.init_cond.az_dim[self.bcs.nrho_dep.idx:
                                                           (self.bcs.nrho_arr.idx + 1)]
        else:
            raise ValueError('Initial and target orbits not correctly initialized')

        fam.load_family()
        arc_length_vec = []

        for param in param_vec:
            arc_length = fam.nrho_dict[str(param)].arc_length
            arc_length_vec.append(arc_length)

        arc_length_vec = np.asarray(arc_length_vec)
        arc_length_sum = np.sum(arc_length_vec)
        dt_arcs = (self.tof / arc_length_sum) * arc_length_vec

        return dt_arcs

    def plot(self):
        """Plots the approximate transfer trajectory with all the intermediate orbits. """

        # load NRHO family
        if hasattr(self.bcs, 'alti'):
            fam = FamilyNRHO(self.bcs.family, 'alt')
        elif hasattr(self.bcs, 'azi'):
            fam = FamilyNRHO(self.bcs.family, 'Az')
        else:
            raise ValueError('Initial and target orbits not correctly initialized')

        fam.load_family()

        fig = plt.figure()
        fig.suptitle('Family of NRHO orbits in synodic frame')
        axs = plt.axes(projection='3d')
        axs = set_axes_limits_labels(axs, self.bcs.nrho_arr.state_vec[:, :3])

        # intermediate NRHOs
        orbits = list(fam.nrho_dict.keys())
        intermediate_orbits = orbits[(self.bcs.nrho_dep.idx + 1):self.bcs.nrho_arr.idx]

        for k in intermediate_orbits[:-1]:
            nrho = fam.nrho_dict[k]
            r_vec_i = nrho.state_vec[:, :3]

            axs.plot3D(r_vec_i[:, 0], r_vec_i[:, 1], r_vec_i[:, 2], color='#cccccc')

        nrho = fam.nrho_dict[intermediate_orbits[-1]]
        axs.plot3D(nrho.state_vec[:, 0], nrho.state_vec[:, 1], nrho.state_vec[:, 2],
                   color='#cccccc', label='intermediate orbits')

        # departure NRHO
        dep_nrho = fam.nrho_dict[orbits[self.bcs.nrho_dep.idx]]
        axs.plot3D(dep_nrho.state_vec[:, 0], dep_nrho.state_vec[:, 1],
                   dep_nrho.state_vec[:, 2], label='departure orbit')

        # arrival NRHO
        arr_nrho = fam.nrho_dict[orbits[self.bcs.nrho_arr.idx]]
        axs.plot3D(arr_nrho.state_vec[:, 0], arr_nrho.state_vec[:, 1],
                   arr_nrho.state_vec[:, 2], label='target orbit')

        # trajectory stacking
        axs.plot3D(self.state0[:, 0], self.state0[:, 1], self.state0[:, 2],
                   label='initial guess')

        # Moon, departure, insertion
        axs.scatter3D(1.0 - self.bcs.nrho_dep.cr3bp.mu, 0.0, 0.0, color='k', label='Moon')
        axs.scatter3D(self.state0[0, 0], self.state0[0, 1], self.state0[0, 2], color='b',
                      label='departure point')
        axs.scatter3D(self.state0[-1, 0], self.state0[-1, 1], self.state0[-1, 2], color='r',
                      label='insertion point')

        axs.legend(bbox_to_anchor=(1, 1), loc=2)


if __name__ == '__main__':

    AZ_INIT = 75000
    AZ_FINAL = 77000

    IC = InitNRHOs('southern', Azi=AZ_INIT, Azf=AZ_FINAL)

    T_INIT = IC.nrho_dep.period
    TOF = IC.nrho_arr.period / 2 + (IC.nrho_arr.period - IC.nrho_dep.period)
    T_FINAL = T_INIT + TOF
    T_VEC = np.linspace(T_INIT, T_FINAL, 500)

    TS = TrajectoryStacking(T_INIT, TOF, T_VEC, 'southern', Azi=AZ_INIT, Azf=AZ_FINAL)
    TS.propagate('arc_length')

    TS.plot()
    plt.show()
