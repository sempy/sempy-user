#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 09:57:09 2019

@author: Alberto FOSSA'
"""

from sempy.core.lowthrust.casereader.transfer_reader import TransferReader
from sempy.core.lowthrust.coc.rotmat import syn2lci
from sempy.core.lowthrust.orbits.keporb import KepOrb
from sempy.core.lowthrust.plots.solutions import LLO2LLOSolutionPlot
from sempy.core.lowthrust.utils.constants import TRAJ_NAME, PHASES_NAMES


class LLO2LLOReader(TransferReader):
    """LLO2LLOReader class loads a precomputed optimal transfer trajectory between LLOs stored
    in a SQL database and retrieves the time series of states and controls variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    **kwargs :
    phase_name : str
        Name of the phase within OpenMDAO

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    llo_dep_arr : list
        `KepOrb` objects for the initial and target LLOs
    phase_name : str
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation
    sol_plot : LLO2LLOSolutionPlot
        `LLO2LLOSolutionPlot` object

    """

    def __init__(self, db_path, case_id='final', db_exp_path=None, t_init=0.0, **kwargs):
        """Initializes LLO2LLOReader class. """

        TransferReader.__init__(self, db_path, case_id=case_id, db_exp_path=db_exp_path,
                                t_init=t_init)

        phn = kwargs.get('phase_name', PHASES_NAMES[0])
        self.phase_name = '.'.join([TRAJ_NAME, phn, 'timeseries'])

        self.get_solutions()  # solution dictionaries
        self.llo_dep_arr = []  # initial and target orbits

        for i in (0, -1):
            state = self.sol['powered']['states'][i][:6]
            time = float(self.sol['powered']['time'][i])
            state_lci = syn2lci(state, time, self.cr3bp.mu, self.cr3bp.L, self.tc_cr3bp)
            llo = KepOrb(2000, 0, 0, 0, 0, 0)
            llo.set_state_vector(state_lci[:3], state_lci[3:])  # initial and final orbits
            self.llo_dep_arr.append(llo)  # initial and final orbits list

        self.sol_plot = None

    def get_solution_dictionary(self, kind='implicit', jacobi_constant=False):
        """Stores in a dictionary the solution from a given `Trajectory` and `Case` object.

        Parameters
        ----------
        kind : str, optional
            Kind of solution, allowed values are ``implicit`` or ``explicit``.
            Default is ``implicit``
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        """

        sol = TransferReader.get_solution_dictionary(self, kind=kind,
                                                     jacobi_constant=jacobi_constant)

        return sol

    def plot(self):
        """Plots the thrust magnitude and direction, positions and velocities time series and
        the three-dimensional transfer trajectory.

        """

        self.sol_plot = LLO2LLOSolutionPlot(self.cr3bp, self.llo_dep_arr[0], self.llo_dep_arr[1],
                                            self.sol, self.sol_exp)
        self.sol_plot.plot()
