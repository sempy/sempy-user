#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 09:55:55 2019

@author: Alberto FOSSA'
"""

import numpy as np
from openmdao.api import CaseReader

from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.primary import Primary
from sempy.core.lowthrust.utils.constants import STATES_NAMES, CONTROLS_NAMES


class TransferReader:
    """TransferReader class loads a precomputed optimal transfer trajectory stored in a SQL
    database and retrieves the time series of states and controls variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    phase_name : str or list
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation

    """

    def __init__(self, db_path, case_id='final', db_exp_path=None, t_init=0.0):
        """Initializes TransferReader class. """

        # implicit solution
        self.case_reader = CaseReader(db_path)

        if case_id in ['initial', 'final']:
            self.case_id = case_id
        else:
            raise ValueError("Case must be either 'initial' or 'final'")

        self.case = self.case_reader.get_case(self.case_id)

        # explicit simulation
        if db_exp_path is not None:
            self.case_reader_exp = CaseReader(db_exp_path)
            self.case_exp = self.case_reader_exp.get_case(-1)

        # initial transfer time and Cr3bp object for EM system
        self.t_init = t_init
        self.cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        self.tc_cr3bp = self.cr3bp.T / 2 / np.pi

        self.phase_name = ''
        self.states_names = STATES_NAMES
        self.controls_names = CONTROLS_NAMES
        self.sol = {}
        self.sol_exp = {}

    def switch_case(self, case_name):
        """Returns the corresponding `Case` object.

        Parameters
        ----------
        case_name : str
            Name of the `Case` object as ``implicit`` for implicit solution or ``explicit``
            for explicit simulation

        Returns
        -------
        case : Case
            `Case` object

        """

        if case_name == 'implicit':
            case = self.case
        elif case_name == 'explicit':
            case = self.case_exp
        else:
            raise ValueError("Case name must be either 'implicit' or 'explicit'")

        return case

    def get_pos_vel_time_series(self, phase_name, case_name='implicit'):
        """Retrieves the time of flight, the time vector and the positions and velocities time
        series from a given `Phase` and `Case` object.

        Parameters
        ----------
        phase_name : str
            Name of the `Phase` object in the `Trajectory`
        case_name : str
            Name of the `Case` object as ``implicit`` for implicit solution or ``explicit``
            for explicit simulation. Default is ``imp``

        Returns
        -------
        tof : float
            Time of flight [-]
        time : ndarray
            Time vector [-]
        pos_vel : ndarray
            Positions and velocities time series [-]

        """

        case = self.switch_case(case_name)

        time = case.outputs.get(phase_name + '.time')
        tof = time[-1] - time[0]

        pos_vel = np.empty((np.size(time), 0))

        for k in self.states_names[:6]:
            state = case.outputs.get(phase_name + '.states:' + k)
            pos_vel = np.append(pos_vel, state, axis=1)

        return tof, time, pos_vel

    def get_states_controls_time_series(self, phase_name, case_name='implicit'):
        """Retrieves the time of flight, the time vector and the states and controls variables
        time series from a given `Phase` and `Case` object.

        Parameters
        ----------
        phase_name : str
            Name of the `Phase` object in the `Trajectory`
        case_name : str
            Name of the `Case` object as ``implicit`` for implicit solution or ``explicit``
            for explicit simulation

        Returns
        -------
        tof : float
            Time of flight [-]
        time : ndarray
            Time vector [-]
        states : ndarray
            States variables time series [-]
        controls : ndarray
            Controls variables time series [-]

        """

        case = self.switch_case(case_name)

        tof, time, pos_vel = self.get_pos_vel_time_series(phase_name, case_name=case_name)
        mass = case.outputs.get(phase_name + '.states:' + self.states_names[6])
        states = np.hstack((pos_vel, mass))
        controls = np.empty((np.size(time), 0))

        for k in self.controls_names:
            control = case.outputs.get(phase_name + '.controls:' + k)
            controls = np.append(controls, control, axis=1)

        return tof, time, states, controls

    def get_jacobi_constant(self, phase_name, case_name='implicit'):
        """Retrieves the Jacobi constant time series from a given `Phase` and `Case` object.

        Parameters
        ----------
        phase_name : str
            Name of the `Phase` object in the `Trajectory`
        case_name : str
            Name of the `Case` object as ``implicit`` for implicit solution or ``explicit``
            for explicit simulation

        Returns
        -------
        c_jac : ndarray
            Jacobi constant time series [-]

        """

        case = self.switch_case(case_name)
        c_jac = case.outputs.get(phase_name + '.C')

        return c_jac

    def get_time_series_dictionary(self, kind, phase_name, case_name='implicit',
                                   jacobi_constant=True):
        """Stores in a dictionary the solution from a given `Phase` and `Case` object.

        Parameters
        ----------
        kind : str
            Kind of phase, allowed values are ``powered`` for powered phases and ``coast``
            for coasting arcs
        phase_name : str
            Name of the `Phase` object in the `Trajectory`
        case_name : str
            Name of the `Case` object as ``implicit`` for implicit solution or ``explicit``
            for explicit simulation
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        Returns
        -------
        sol : dict
            Solution dictionary

        """

        if kind == 'powered':
            tof, time, states, controls = \
                self.get_states_controls_time_series(phase_name=phase_name, case_name=case_name)
            sol = {'time': time, 'tof': tof, 'time_dim': time * self.tc_cr3bp,
                   'tof_dim': tof * self.tc_cr3bp, 'states': states, 'departure': states[0, :6],
                   'insertion': states[-1, :6], 'controls': controls}
        elif kind == 'coast':
            tof, time, states = \
                self.get_pos_vel_time_series(phase_name=phase_name, case_name=case_name)
            sol = {'time': time, 'tof': tof, 'time_dim': time * self.tc_cr3bp,
                   'tof_dim': tof * self.tc_cr3bp, 'states': states}
        else:
            raise ValueError('Kind must be powered or coast')

        if jacobi_constant:
            sol['C'] = self.get_jacobi_constant(phase_name, case_name)

        return sol

    def get_solution_dictionary(self, kind='implicit', jacobi_constant=True):
        """Stores in a dictionary the solution from a given `Trajectory` and `Case` object.

        Parameters
        ----------
        kind : str, optional
            Kind of solution, allowed values are ``implicit`` or ``explicit``.
            Default is ``implicit``
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        """

        sol = {'powered': self.get_time_series_dictionary('powered', self.phase_name,
                                                          case_name=kind,
                                                          jacobi_constant=jacobi_constant),
               'kind': kind}

        return sol

    def get_solutions(self):
        """Saves as class attributes the implicit solution and explicit simulation from
        a given `Trajectory` and `Case` object.

        """

        scaler = np.hstack((np.ones(3) * self.cr3bp.L, np.ones(3) * self.cr3bp.L / self.tc_cr3bp))

        self.sol = self.get_solution_dictionary()

        if hasattr(self, 'case_exp'):
            self.sol_exp = self.get_solution_dictionary(kind='explicit')
            self.sol_exp['error'] = \
                np.fabs(self.sol['powered']['insertion'] -
                        self.sol_exp['powered']['insertion']) * scaler
        else:
            self.sol_exp = None
