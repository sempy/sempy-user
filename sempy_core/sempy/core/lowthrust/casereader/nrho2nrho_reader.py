#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 09:57:09 2019

@author: Alberto FOSSA'
"""

from sempy.core.lowthrust.casereader.transfer_reader import TransferReader
from sempy.core.lowthrust.plots.solutions import NRHO2NRHOSolutionPlot
from sempy.core.lowthrust.utils.constants import PHASES_NAMES, TRAJ_NAME
from sempy.core.lowthrust.utils.utils import InitNRHOs


class NRHO2NRHOReader(TransferReader):
    """NRHO2NRHOReader class loads a precomputed optimal transfer trajectory between NRHOs stored
    in a SQL database and retrieves the time series of states and controls variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    phase_name : str or list
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    """

    def __init__(self, db_path, family, case_id='final', db_exp_path=None, t_init=0.0, **kwargs):
        """Initializes NRHO2NRHOReader class. """

        TransferReader.__init__(self, db_path, case_id=case_id, db_exp_path=db_exp_path,
                                t_init=t_init)
        self.bcs = InitNRHOs(family, **kwargs)  # initial and target NRHOs
        self.sol_plot = None

    def plot(self):
        """Plots the thrust magnitude and direction, positions, velocities and Jacobi constant
        time series and the three-dimensional transfer trajectory.

        """

        self.sol_plot = NRHO2NRHOSolutionPlot(self.cr3bp, self.bcs, self.sol, sol_exp=self.sol_exp)
        self.sol_plot.plot()


class NRHO2NRHOFixReader(NRHO2NRHOReader):
    """NRHO2NRHOReader class loads a precomputed optimal transfer trajectory from the inner NRHO
    periselene to the outer NRHO aposelene stored in a SQL database and retrieves the time series
    of states and controls variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]
    phase_name : str
        Name of the phase within OpenMDAO

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    phase_name : str
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    """

    def __init__(self, db_path, family, case_id='final', db_exp_path=None, t_init=0.0, **kwargs):
        """Initializes NRHO2NRHOFixReader class. """

        NRHO2NRHOReader.__init__(self, db_path, family, case_id=case_id, db_exp_path=db_exp_path,
                                 t_init=t_init, **kwargs)

        phn = kwargs.get('phase_name', PHASES_NAMES[0])
        self.phase_name = '.'.join([TRAJ_NAME, phn, 'timeseries'])
        self.get_solutions()


class NRHO2NRHOOpenArrReader(NRHO2NRHOReader):
    """NRHO2NRHOReader class loads a precomputed optimal transfer trajectory from the inner
    NRHO periselene to an open state on the outer NRHO stored in a SQL database and retrieves
    the time series of states and controls variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]
    phase_name : list
        Name of the phase within OpenMDAO

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    phase_name : list
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    """

    def __init__(self, db_path, family, case_id='final', db_exp_path=None, t_init=0.0, **kwargs):
        """Initializes NRHO2NRHOOpenArrReader class. """

        NRHO2NRHOReader.__init__(self, db_path, family, case_id=case_id, db_exp_path=db_exp_path,
                                 t_init=t_init, **kwargs)

        phn = kwargs.get('phase_name', PHASES_NAMES[1])
        self.phase_name = []

        for i in range(2):
            self.phase_name.append('.'.join([TRAJ_NAME, phn[i], 'timeseries']))

        self.get_solutions()

    def get_solution_dictionary(self, kind='implicit', jacobi_constant=True):
        """Stores in a dictionary the solution from a given `Trajectory` and `Case` object.

        Parameters
        ----------
        kind : str, optional
            Kind of solution, allowed values are ``implicit`` or ``explicit``.
            Default is ``implicit``
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        """

        sol_pow = \
            self.get_time_series_dictionary('powered', self.phase_name[0],
                                            case_name=kind, jacobi_constant=jacobi_constant)
        sol_coast = \
            self.get_time_series_dictionary('coast', self.phase_name[1],
                                            case_name=kind, jacobi_constant=jacobi_constant)
        sol = {'powered': sol_pow, 'coast': sol_coast, 'kind': kind}

        return sol


class NRHO2NRHOOpenEndsReader(NRHO2NRHOReader):
    """NRHO2NRHOReader class loads a precomputed optimal transfer trajectory between open states
    on two NRHOs stored in a SQL database and retrieves the time series of states and controls
    variables.

    Parameters
    ----------
    db_path : str
        Absolute path to the SQL database in which the implicit solution is stored
    family : str
        L2 NRHO family, allowed values are ``northern`` and ``southern``
    case_id : str, optional
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively. Default is ``final``
    db_exp_path : str or None, optional
        Absolute path to the SQL database in which the explicit simulation is stored or None.
        Default is None
    t_init : float, optional
        Initial time [-]. Default is 0.0

    **kwargs :
    alti : int
        Initial NRHO periselene altitude [km]
    altf : int
        Final NRHO periselene altitude [km]
    Azi : int
        Initial NRHO vertical extension [km]
    Azf : int
        Final NRHO vertical extension [km]
    phase_name : list
        Name of the phase within OpenMDAO

    Attributes
    ----------
    case_reader : CaseReader
        `CaseReader` object for the implicit solution
    case_id : str
        Case identifier, allowed values are ``initial`` and ``final`` for the first and last
        iterations respectively.
    case_reader_exp : CaseReader
        `CaseReader` object for the explicit simulation
    case : Case
        `Case` object for the implicit solution
    case_exp : Case
        `Case` object for the explicit simulation
    t_init : float, optional
        Initial time [-]
    cr3bp : Cr3bp
        `Cr3bp` object
    tc_cr3bp : float
        Characteristic time [-]
    phase_name : list
        Name of the phase within OpenMDAO
    states_names : list
        Names of the states variables within OpenMDAO
    controls_names : list
        Names of the controls variables within OpenMDAO
    sol : dict
        Implicit solution
    sol_exp : dict
        Explicit simulation
    bcs : InitNRHOs
        `InitNRHOs` object
    sol_plot : NRHO2NRHOSolutionPlot
        `NRHO2NRHOSolutionPlot` object

    """

    def __init__(self, db_path, family, case_id='final', db_exp_path=None, t_init=0.0, **kwargs):
        """Initializes NRHO2NRHOOpenEndsReader class. """

        NRHO2NRHOReader.__init__(self, db_path, family, case_id=case_id, db_exp_path=db_exp_path,
                                 t_init=t_init, **kwargs)

        phn = kwargs.get('phase_name', PHASES_NAMES[2])
        self.phase_name = []

        for i in range(3):
            self.phase_name.append('.'.join([TRAJ_NAME, phn[i], 'timeseries']))

        self.get_solutions()

    def get_solution_dictionary(self, kind='implicit', jacobi_constant=True):
        """Stores in a dictionary the solution from a given `Trajectory` and `Case` object.

        Parameters
        ----------
        kind : str, optional
            Kind of solution, allowed values are ``implicit`` or ``explicit``.
            Default is ``implicit``
        jacobi_constant : bool, optional
            True to include the Jacobi constant time series, False otherwise. Default is True

        """

        sol_coast1 = \
            self.get_time_series_dictionary('coast', self.phase_name[0],
                                            case_name=kind, jacobi_constant=jacobi_constant)
        sol_pow = \
            self.get_time_series_dictionary('powered', self.phase_name[1],
                                            case_name=kind, jacobi_constant=jacobi_constant)
        sol_coast2 = \
            self.get_time_series_dictionary('coast', self.phase_name[2],
                                            case_name=kind, jacobi_constant=jacobi_constant)
        sol = {'coast1': sol_coast1, 'powered': sol_pow, 'coast2': sol_coast2, 'kind': kind}

        return sol
